﻿using System;
using dataAccess.TransactionManagement;
using businessLogic.Models;
using crossCutting.Resources;
using dataAccess.BaseManagement;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using serviceLayer.SIF_DataContracts;


namespace businessLogic.TransactionController
{
    public class BaseTransactionController<T, K, DT, DK>: ITransactionController<T,K>
    {
        protected TransactionManager<K> _managerDb;
        protected MasterDetailManager<DK> _managerDetailDb;

        public T Add(T input)
        {
            K data = TranslateToModel(input);
            Validate(data, false);
            var result = _managerDb.Add(data);
            SaveDetails(data, result);
            return TranslateToService(data);
        }


        public bool Cancel(T input)
        {
            K data = TranslateToModel(input);
            Validate(data, false);
            var result = _managerDb.Cancel(data);
            return result;
        }

        protected virtual void SaveDetails(K data, K result)
        {
            throw new NotImplementedException();
        }

        public virtual void Validate(K data,bool isUpdate)
        {
            throw new NotImplementedException();
        }


        public List<T> GetAll(T input)
        {
            K data = TranslateToModel(input);
            var result = _managerDb.GetAll(data);
            List<T> response = new List<T>();
            foreach (K res in result)
            {
                response.Add(TranslateToService(res));
            }
            return response;
        }


        public T GetOne(T input)
        {
            var data = TranslateToModel(input);
            var result = _managerDb.GetOne(data);
            return TranslateToService(result);
        }



        public virtual T TranslateToService(K data)
        {
            throw new NotImplementedException();
        }

        public virtual K TranslateToModel(T data)
        {
            throw new NotImplementedException();
        }

    }
}
