﻿using System;
using dataAccess.TransactionManagement;
using businessLogic.Models;
using crossCutting.Resources;
using dataAccess.BaseManagement;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.TransactionController
{
    public class InputController: BaseTransactionController<InputData,Input,InputDetailData, InputDetail>
    {
        public InputController()
        {
            _managerDb = new InputManager();
            _managerDetailDb = new InputDetailManager();
        }

        public override void Validate(Input data, bool isUpdate)
        {
            if (isUpdate)
            {
                if (string.IsNullOrWhiteSpace(data.InputId))
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            if (data.WarehouseId < 0)
                throw new ArgumentException(StringResources.Instance.WarehouseValue, StringResources.Instance.Warehouse);

        }

        protected override void SaveDetails(Input data, Input result)
        {
            data.InputId = result.InputId;
            Parallel.ForEach(data.Details, (detail) => { detail.InputDetailId = _managerDetailDb.Add(detail).InputDetailId; });
        }

        public override InputData TranslateToService(Input data)
        {
            var result = new InputData()
            {
                CancelAproveUser = data.CancelAproveUser,
                CancelDate = data.CancelDate,
                CancelUser = data.CancelUser,
                Comment = data.Comment,
                Date = data.Date,
                IsCancelled = data.IsCancelled,
                IsRegistered = data.IsRegistered,
                InputId = data.InputId,
                Type = data.Type,
                WarehouseId = data.WarehouseId
            };
            result.Details = new List<InputDetailData>();

            Parallel.ForEach(data.Details,
                (details) =>
                {
                    result.Details.Add(new InputDetailData()
                    {
                        Amount = details.Amount,
                        InputDetailId = details.InputDetailId,
                        InputId = details.InputId,
                        LotId = details.LotId,
                        Price = details.Price,
                        ProductId = details.ProductId
                    });
                });
            return result;
        }

        public override Input TranslateToModel(InputData data)
        {
            var result = new Input()
            {
                CancelAproveUser = data.CancelAproveUser,
                CancelDate = data.CancelDate,
                CancelUser = data.CancelUser,
                Comment = data.Comment,
                Date = data.Date,
                IsCancelled = data.IsCancelled,
                IsRegistered = data.IsRegistered,
                InputId = data.InputId,
                Type = data.Type,
                WarehouseId = data.WarehouseId
            };
            Parallel.ForEach(data.Details,
               (detail) => {
                   result.Details.Add(new InputDetail(detail.InputDetailId,
                    detail.InputId, detail.LotId, detail.ProductId, detail.Amount,
                    detail.Price));
               });

            return result;

        }
    }
}
