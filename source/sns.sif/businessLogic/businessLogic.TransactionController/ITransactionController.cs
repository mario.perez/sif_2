﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.TransactionController
{
    interface ITransactionController<T, K>
    {
        void Validate(K data, bool isUpdate);
        T Add(T input);
        List<T> GetAll(T input);
        T GetOne(T input);
        T TranslateToService(K data);
        K TranslateToModel(T data);
        bool Cancel(T input);
    }
}
