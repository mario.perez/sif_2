﻿using System;
using dataAccess.TransactionManagement;
using businessLogic.Models;
using crossCutting.Resources;
using dataAccess.BaseManagement;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.TransactionController
{
    public class BillController : BaseTransactionController<BillData, Bill, BillDetailData, BillDetail>
    {
        public BillController()
        {
            _managerDb = new BillManager();
            _managerDetailDb = new BillDetailManager();
        }

        public override void Validate(Bill data, bool isUpdate)
        {
            if (isUpdate)
            {
                if (string.IsNullOrWhiteSpace(data.BillId))
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            if (data.WarehouseId < 0)
                throw new ArgumentException(StringResources.Instance.WarehouseValue, StringResources.Instance.Warehouse);

        }
        protected override void SaveDetails(Bill data, Bill result)
        {
            data.BillId = result.BillId;
            Parallel.ForEach(data.Details, (detail) => { detail.BillDetailId = _managerDetailDb.Add(detail).BillDetailId; });
        }

        public override BillData TranslateToService(Bill data)
        {
            var result = new BillData()
            {
                CancelAproveUser = data.CancelAproveUser,
                CancelDate = data.CancelDate,
                CancelUser = data.CancelUser,
                Comments = data.Comments,
                Date = data.Date,
                Discount = data.Discount,
                IsCancelled = data.IsCancelled,
                IsRegistered = data.IsRegistered,
                BillId = data.BillId,
                Subtotal = data.Subtotal,
                Tax = data.Tax,
                TotalAmount = data.TotalAmount,
                WarehouseId = data.WarehouseId,
                Cash = data.Cash,
                CashboxId = data.CashboxId,
                CreditCard = data.CreditCard,
                DiscountAproveUser = data.DiscountAproveUser,
                DiscountRate = data.DiscountRate,
                DiscountUser = data.DiscountUser,
                SalesmanId = data.SalesmanId,
                TypePayment = data.TypePayment
                 
            };
            result.Details = new List<BillDetailData>();

            Parallel.ForEach(data.Details,
                (details) => {
                    result.Details.Add(new BillDetailData()
                    {
                        Amount = details.Amount,
                        BillDetailId = details.BillDetailId,
                        BillId =       details.BillId,
                        LotId = details.LotId,
                        Price = details.Price,
                        ProductId = details.ProductId
                    });
                });

            return result;
        }
        public override Bill TranslateToModel(BillData data)
        {
            var result = new Bill()
            {
                CancelAproveUser = data.CancelAproveUser,
                CancelDate = data.CancelDate,
                CancelUser = data.CancelUser,
                Comments = data.Comments,
                Date = data.Date,
                Discount = data.Discount,
                IsCancelled = data.IsCancelled,
                IsRegistered = data.IsRegistered,
                BillId = data.BillId,
                Subtotal = data.Subtotal,
                Tax = data.Tax,
                TotalAmount = data.TotalAmount,
                WarehouseId = data.WarehouseId,
                Cash = data.Cash,
                CashboxId = data.CashboxId,
                CreditCard = data.CreditCard,
                DiscountAproveUser = data.DiscountAproveUser,
                DiscountRate = data.DiscountRate,
                DiscountUser = data.DiscountUser,
                SalesmanId = data.SalesmanId,
                TypePayment = data.TypePayment
            };
            Parallel.ForEach(data.Details,
               (details) => {
                   result.Details.Add(new BillDetail()
                   {
                       Amount = details.Amount,
                       BillDetailId = details.BillDetailId,
                       BillId = details.BillId,
                       LotId = details.LotId,
                       Price = details.Price,
                       ProductId = details.ProductId
                   });
               });

            return result;
        }

    }
}
