﻿using System;
using dataAccess.TransactionManagement;
using businessLogic.Models;
using crossCutting.Resources;
using dataAccess.BaseManagement;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.TransactionController
{
    public class PurchaseController: BaseTransactionController<PurchaseData, Purchase, PurchaseDetailData, PurchaseDetail>
    {
        public PurchaseController()
        {
            _managerDb = new PurchaseManager();
            _managerDetailDb = new PurchaseDetailManager();
        }

        public override void Validate(Purchase data, bool isUpdate)
        {
            if (isUpdate)
            {
                if (string.IsNullOrWhiteSpace(data.PurchaseId))
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            if (data.WarehouseId < 0)
                throw new ArgumentException(StringResources.Instance.WarehouseValue, StringResources.Instance.Warehouse);

        }

        protected override void SaveDetails(Purchase data, Purchase result)
        {
            data.PurchaseId = result.PurchaseId;
            Parallel.ForEach(data.Details, (detail) => { detail.PurchaseDetailId = _managerDetailDb.Add(detail).PurchaseDetailId; });
        }

        public override PurchaseData TranslateToService(Purchase data)
        {
            var result = new PurchaseData()
            {
                CancelAproveUser = data.CancelAproveUser,
                CancelDate = data.CancelDate,
                CancelUser = data.CancelUser,
                Comments = data.Comments,
                Date = data.Date,
                Discount = data.Discount,
                IsCancelled = data.IsCancelled,
                IsRegistered = data.IsRegistered,
                ProviderId = data.ProviderId,
                PurchaseId = data.PurchaseId,
                Subtotal = data.Subtotal,
                Tax = data.Tax,
                TotalAmount = data.TotalAmount,
                WarehouseId = data.WarehouseId
            };
            result.Details = new List<PurchaseDetailData>();

            Parallel.ForEach(data.Details,
                (details) => {
                    result.Details.Add(new PurchaseDetailData()
                    {
                        Amount = details.Amount,
                        PurchaseDetailId = details.PurchaseDetailId,
                        PurchaseId = details.PurchaseId,
                        LotId = details.LotId,
                        Price = details.Price,
                        ProductId = details.ProductId
                    });
                });

            return result;

        }

        public override Purchase TranslateToModel(PurchaseData data)
        {
            var result = new Purchase()
            {
                CancelAproveUser = data.CancelAproveUser,
                CancelDate = data.CancelDate,
                CancelUser = data.CancelUser,
                Comments = data.Comments,
                Date = data.Date,
                Discount = data.Discount,
                IsCancelled = data.IsCancelled,
                IsRegistered = data.IsRegistered,
                ProviderId = data.ProviderId,
                PurchaseId = data.PurchaseId,
                Subtotal = data.Subtotal,
                Tax = data.Tax,
                TotalAmount = data.TotalAmount,
                WarehouseId = data.WarehouseId
            };
            
            Parallel.ForEach(data.Details,
                (details) => {
                    result.Details.Add(new PurchaseDetail()
                    {
                        Amount = details.Amount,
                        PurchaseDetailId = details.PurchaseDetailId,
                        PurchaseId = details.PurchaseId,
                        LotId = details.LotId,
                        Price = details.Price,
                        ProductId = details.ProductId
                    });
                });

            return result;
        }
    }
}
