﻿using System;
using dataAccess.TransactionManagement;
using businessLogic.Models;
using crossCutting.Resources;
using dataAccess.BaseManagement;
using System.Collections.Generic;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.TransactionController
{
    public class ClosingController
    {
        private ClosingManager _managerDb;
        private ClosingDetailManager _managerDetailDb;

        public ClosingController()
        {
            _managerDb = new ClosingManager();
            _managerDetailDb = new ClosingDetailManager();
        }

        public bool Add(Closing data)
        {
            Validate(data, false);
            //var saved = _managerDb.AddUpdateCierre(data,"I");
            return true;//saved;
        }

        public bool Update(Closing data)
        {
            Validate(data, false);
            //var saved = _managerDb.AddUpdateCierre(data, "U");
            return true;//saved;
        }

        public void Modify(Closing data)
        {
            Validate(data, true);
            var updated = _managerDb.Update(data);
            if (!updated)
            {
                throw new ArgumentException(StringResources.Instance.DbFailure);
            }
            
            foreach(ClosingDetail detail in data.Details)
            {
                if (!_managerDetailDb.Update(detail))
                    throw new ArgumentException(StringResources.Instance.DbFailure);
            }
        }

        public void Remove(Closing data)
        {
            Validate(data, true);
            var deleted = _managerDb.Remove(data);
            if(!deleted)
            {
                throw new ArgumentException(StringResources.Instance.DbFailure);
            }
        }

        public bool RemoveById(int IdClosing)
        {
            //var deleted = _managerDb.RemoveClosing(IdClosing);
            return true;//deleted;
            
        }

        public void Validate(Closing data, bool isUpdate)
        {
            if(isUpdate)
            {
                if (data.Id < 0)
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);

                if (data.FinalDateTime < data.InitialDateTime)
                    throw new ArgumentException(StringResources.Instance.InitialDateVsFinalDate, StringResources.Instance.FinalDate);

            }

            if (data.CashboxId < 0)
                throw new ArgumentException(StringResources.Instance.CashboxValue, StringResources.Instance.Cashbox);

            if (string.IsNullOrWhiteSpace(data.UserName))
                throw new ArgumentException(StringResources.Instance.UserNameValue, StringResources.Instance.UserName);

           
        }

        public List<ClosingData> GetClosings()
        {
            return new List<ClosingData>();//_managerDb.GetAllClosing();
        }

        public ClosingData GetOneClosing(int idClosing)
        {
            return new ClosingData();//_managerDb.GetOneClosing(idClosing);
        }

        public Closing GetOne(Closing toSearch)
        {
            return _managerDb.GetOne(toSearch);
        }


    }
}
