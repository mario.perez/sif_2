﻿using System;
using dataAccess.TransactionManagement;
using businessLogic.Models;
using crossCutting.Resources;
using dataAccess.BaseManagement;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using serviceLayer.SIF_DataContracts;



namespace businessLogic.TransactionController
{
    public class OutputController : BaseTransactionController<OutputData, Output, OutputDetailData, OutputDetail>
    {


        public OutputController()
        {
            _managerDb = new OutputManager();
            _managerDetailDb = new OutputDetailManager();
        }


        public override void Validate(Output data, bool isUpdate)
        {
            if (isUpdate)
            {
                if (string.IsNullOrWhiteSpace(data.OutputId))
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            if (data.WarehouseId < 0)
                throw new ArgumentException(StringResources.Instance.WarehouseValue, StringResources.Instance.Warehouse);

        }
        
        protected override void SaveDetails(Output data, Output result)
        {
            data.OutputId = result.OutputId;
            Parallel.ForEach(data.Details, (detail) => { detail.OutputDetailId = _managerDetailDb.Add(detail).OutputDetailId; });
        }


        public override OutputData TranslateToService(Output data)
        {
            var result = new OutputData()
            {
                CancelAproveUser = data.CancelAproveUser,
                CancelDate = data.CancelDate,
                CancelUser = data.CancelUser,
                Comment = data.Comment,
                Date = data.Date,
                IsCancelled = data.IsCancelled,
                IsRegistered = data.IsRegistered,
                OutputId = data.OutputId,
                Type = data.Type,
                WarehouseId = data.WarehouseId
            };
            result.Details = new List<OutputDetailData>();

            Parallel.ForEach(data.Details,
                (details) => {
                    result.Details.Add(new OutputDetailData()
                    {
                        Amount = details.Amount,
                        OutputDetailId = details.OutputDetailId,
                        OutputId = details.OutputId,
                        LotId = details.LotId,
                        Price = details.Price,
                        ProductId = details.ProductId
                    });
                });

            return result;
        }

        public override Output TranslateToModel(OutputData data)
        {
            var result = new Output()
            {
                CancelAproveUser = data.CancelAproveUser,
                CancelDate = data.CancelDate,
                CancelUser = data.CancelUser,
                Comment = data.Comment,
                Date = data.Date,
                IsCancelled = data.IsCancelled,
                IsRegistered = data.IsRegistered,
                OutputId = data.OutputId,
                Type = data.Type,
                WarehouseId = data.WarehouseId
            };

            Parallel.ForEach(data.Details,
                (detail) => { result.Details.Add(new OutputDetail(detail.OutputDetailId, 
                    detail.OutputId, detail.LotId,detail.ProductId, detail.Amount, 
                    detail.Price)); });

            return result;
        }
        
    }
}
