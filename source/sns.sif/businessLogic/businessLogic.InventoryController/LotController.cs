﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dataAccess.InventoryManagement;
using crossCutting.Resources;
using businessLogic.Models;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.InventoryController
{
    public class LotController
    {
        private LotManager _managerDb;
        private LotDetailManager _managerDetailDb;

        public LotController()
        {
            _managerDb = new LotManager();
            _managerDetailDb = new LotDetailManager();
        }

        public LotData Add(LotData input)
        {
            var data = TranslateToModel(input);
            Validate(data, false);
            var saved = _managerDb.Add(data);
            if (string.IsNullOrWhiteSpace(saved.Id))
                throw new ArgumentException(StringResources.Instance.DbFailure);
            return TranslateToService(saved);
        }

        public void Modify(LotData input)
        {
            var data = TranslateToModel(input);
            Validate(data, true);
            var updated = _managerDb.Update(data);
            if(!updated)
            {
                throw new ArgumentException(StringResources.Instance.DbFailure);
            }
            foreach(LotDetail detail in data.Details)
            {
                if (!_managerDetailDb.Update(detail))
                    throw new ArgumentException(StringResources.Instance.DbFailure);
            }
        }

        public void Remove(LotData input)
        {
            var data = TranslateToModel(input);
            Validate(data, true);
            if (!_managerDb.Remove(data))
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public void Validate(Lot data, bool isUpdate)
        {
            if(isUpdate)
            {
                if(string.IsNullOrWhiteSpace(data.Id))
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            if (data.AdmissionDate > DateTime.Now)
                throw new ArgumentException(StringResources.Instance.CreateDate, StringResources.Instance.Date);

            if (!data.IsActive)
                throw new ArgumentException(StringResources.Instance.NotActiveStatus, StringResources.Instance.Status);

        }


        public List<LotData> GetLots(LotData input)
        {
            Lot toSearch = TranslateToModel(input);
            List<LotData> result = new List<LotData>();
            var resp = _managerDb.GetAll(toSearch);
            foreach(var item in resp)
            {
                result.Add(TranslateToService(item));
            }
            return result;
        }

        public LotData GetOne(LotData input)
        {
            Lot toSearch = TranslateToModel(input);
            return TranslateToService(_managerDb.GetOne(toSearch));
        }

        public Lot TranslateToModel(LotData data)
        {
            List<LotDetail> details = new List<LotDetail>();
            foreach(LotDetailData detail in data.Details)
            {
                details.Add(TranslateDetailToModel(detail));
            }
            return new Lot(data.Id, data.AdmissionDate, data.IsActive, details.ToArray());
        }

        public LotData TranslateToService(Lot data)
        {
            List<LotDetailData> details = new List<LotDetailData>();
            foreach(LotDetail detail in data.Details)
            {
                details.Add(TranslateDetailToService(detail));
            }
            return new LotData() { Id = data.Id, AdmissionDate = data.AdmissionDate, IsActive = data.IsActive, Details = details};
        }

        public LotDetail TranslateDetailToModel(LotDetailData data)
        {
            return new LotDetail(data.LotId,data.ProductId,data.Quantity,data.Cost,data.IsActive);
        }

        public LotDetailData TranslateDetailToService(LotDetail data)
        {
            return new LotDetailData() { LotId = data.LotId, ProductId = data.ProductId, Quantity = data.Quantity, Cost = data.Cost, IsActive = data.IsActive };
        }
    }
}
