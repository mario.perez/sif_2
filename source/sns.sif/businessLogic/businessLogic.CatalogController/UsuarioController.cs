﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;
using dataAccess.BaseManagement;
using System.Data;

namespace businessLogic.CatalogController
{
    public class UsuarioController
    {
        readonly UsuarioManager _managerDb;

        public UsuarioController()
        {
            _managerDb = new UsuarioManager();
        }

        public Usuario TraslateToModel(UsuarioData data)
        {
            return new Usuario() {
                Id = data.Id
                , UsuarioLogin = data.UsuarioLogin
                , NombreCompleto = data.NombreCompleto
                , Contraseña = data.Contraseña
                , Correo = data.Correo
                , Bloqueado = data.Bloqueado
                , Activo = data.Activo
                , UserCreacion = data.UserCreacion
                , UserModificacion = data.UserModificacion
                , FechaCreacion = data.FechaCreacion
                , FechaModificacion = data.FechaModificacion
                , AppCreacion = data.AppCreacion
                , AppModificacion = data.AppModificacion
                , MaqCreacion = data.MaqCreacion
                , MaqModificacion = data.MaqModificacion
            };
        }

        public UsuarioData TraslateToService(Usuario data)
        {
            return new UsuarioData()
            {
                Id = data.Id
                ,
                UsuarioLogin = data.UsuarioLogin
                ,
                NombreCompleto = data.NombreCompleto
                ,
                Contraseña = data.Contraseña
                ,
                Correo = data.Correo
                ,
                Bloqueado = data.Bloqueado
                ,
                Activo = data.Activo
                ,
                UserCreacion = data.UserCreacion
                ,
                UserModificacion = data.UserModificacion
                ,
                FechaCreacion = data.FechaCreacion
                ,
                FechaModificacion = data.FechaModificacion
                ,
                AppCreacion = data.AppCreacion
                ,
                AppModificacion = data.AppModificacion
                ,
                MaqCreacion = data.MaqCreacion
                ,
                MaqModificacion = data.MaqModificacion
            };
        }

        public void GestionarUsuario_CRUD(UsuarioData input, string operacion)
        {
            var data = TraslateToModel(input);            
            var saved = _managerDb.GestionarUsuario(data, operacion);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public List<UsuarioData> GetAllUsuario(UsuarioData usuario,string operacion) {

            var lista = _managerDb.GetTable(TraslateToModel(usuario),operacion);
            List<UsuarioData> ListaUsuario = new List<UsuarioData>();

            foreach (DataRow data in lista.Rows)
            {
                var r = new UsuarioData()
                {                    
                    Id = Int32.Parse(data[0].ToString())
                    , UsuarioLogin = data[1].ToString()
                    , Contraseña = data[2].ToString()
                    , NombreCompleto = data[3].ToString()
                    , Activo = bool.Parse(data[4].ToString())
                    , Bloqueado = bool.Parse(data[5].ToString())
                    , Correo = data[6].ToString()
                    ,UserCreacion = data[7].ToString()
        ,
                    UserModificacion = data[8].ToString()
        ,
                    FechaCreacion = Convert.ToDateTime(data[9].ToString())
        ,
                    FechaModificacion = Convert.ToDateTime(data[10].ToString())
        ,
                    AppCreacion = data[11].ToString()
        ,
                    AppModificacion = data[12].ToString()
        ,
                    MaqCreacion = data[13].ToString()
        ,
                    MaqModificacion = data[14].ToString()
                };
                ListaUsuario.Add(r);
            }

            return ListaUsuario;
        }

        public UsuarioData AutenticarUsuario(UsuarioData user, string operacion)
        {

            var dtUsuario = _managerDb.GetTable(TraslateToModel(user), operacion);
            //UsuarioData usuario = new UsuarioData();

            if (dtUsuario.Rows.Count > 0) {

                DataRow row = dtUsuario.Rows[0];

                if (bool.Parse(row["Activo"].ToString()))
                { //Validamos que este este activo
                    if (!bool.Parse(row["Bloqueado"].ToString()))
                    { // Validamos que no este bloqueado
                        if (row["Contraseña"].ToString().Equals(user.Contraseña))
                        {
                            user.Id = Int32.Parse(row["Id"].ToString());
                            user.Correo = row["Correo"].ToString();
                            user.NombreCompleto = row["NombreCompleto"].ToString();
                            return user;
                        }
                        else
                        {
                            throw new ArgumentException("Contraseña Invalida!");
                        }
                    }
                    else
                    {
                        throw new ArgumentException("Usuario esta bloqueado");
                    }
                }
                else
                {
                    throw new ArgumentException("Usuario esta inactivo");
                }                
            }
            else
            {
                throw new ArgumentException("Usuario no existe");
            }

            
        }


    }    
}
