﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogController
{
    public class ConfigurationController : ICatalogController<Configs,Configuration>
    {

        readonly ConfigurationManager _managerDb;

        public ConfigurationController()
        {
            _managerDb = new ConfigurationManager();
        }

        public void AddCatalog(Configs data)
        {
            //No se permitirá el ingreso de caonfiguraciones desde la interfaz.
            throw new NotImplementedException();
        }

        public Configs getCatalog(string parameter)
        {
            if (string.IsNullOrWhiteSpace(parameter))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            return TranslateToService(_managerDb.GetOne(parameter));
        }

        public List<Configs> ListCatalogs(string id)
        {
            var dataList = _managerDb.GetAll(id);
            var result = new List<Configs>();
            foreach(Configuration con in dataList)
            {
                result.Add(TranslateToService(con));
            }
            return result;
        }

        public void RemoveCatalog(Configs data)
        {
            //No se permitira remover configuraciones desde la interfaz.
            throw new NotImplementedException();
        }

        public Configuration TranslateToModel(Configs data)
        {
            return new Configuration(data.Id, data.Value, data.Description);
        }

        public Configs TranslateToService(Configuration data)
        {
            return new Configs() { Id = data.Id, Value = data.Value, Description = data.Description };
        }

        public void UpdateCatalog(Configs data)
        {
            var catalog = TranslateToModel(data);
            Validate(catalog, true);

            var updated = _managerDb.Update(catalog);
            if (!updated)
            {
                throw new ArgumentException(StringResources.Instance.DbFailure);
            }
        }

        public void Validate(Configuration data, bool isUpdate)
        {
            if (string.IsNullOrWhiteSpace(data.Id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            if (string.IsNullOrWhiteSpace(data.Value))
            {
                throw new ArgumentException(StringResources.Instance.EmptyValue, StringResources.Instance.Value);
            }
        }
    }
}
