﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;
using dataAccess.BaseManagement;
using System.Data;

namespace businessLogic.CatalogController
{
    public class PrivilegioController
    {
        readonly PrivilegioManager _managerDb;

        public PrivilegioController()
        {
            _managerDb = new PrivilegioManager();
        }

        public Privilegio TraslateToModel(PrivilegioData data)
        {
            return new Privilegio() {
                Id = data.Id
                , Nombre = data.Nombre
                , CodigoInterno = data.CodigoInterno
                , CodigoTag = data.CodigoTag
                , EsPadre = data.EsPadre
                , PadreId = data.PadreId
            };
        }

        public PrivilegioData TraslateToService(Privilegio data)
        {
            return new PrivilegioData()
            {
                Id = data.Id
                ,
                Nombre = data.Nombre
                ,
                CodigoInterno = data.CodigoInterno
                ,
                CodigoTag = data.CodigoTag
                ,
                EsPadre = data.EsPadre
                ,
                PadreId = data.PadreId
            };
        }

        public void GestionarPrivilegio(PrivilegioData input, string operacion)
        {
            var data = TraslateToModel(input);
            var saved = _managerDb.GestionarPrivilegio(data, operacion);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public List<PrivilegioData> GetAllPrivilegio(PrivilegioData input, string operacion) {

            var data1 = TraslateToModel(input);
            var lista = _managerDb.GetTable(data1,operacion);
            List<PrivilegioData> ListaPrivilegio = new List<PrivilegioData>();

            foreach (DataRow data in lista.Rows)
            {
                var r = new PrivilegioData()
                {
                    Id = Int32.Parse(data[0].ToString())
                    , CodigoTag = data[1].ToString()
                    , Nombre = data[2].ToString()
                    , EsPadre = bool.Parse(data[3].ToString())
                    , PadreId = Int32.Parse(data[4].ToString())
                    , CodigoInterno = data[5].ToString()
                };
                ListaPrivilegio.Add(r);
            }
            return ListaPrivilegio;
        }


    }    
}
