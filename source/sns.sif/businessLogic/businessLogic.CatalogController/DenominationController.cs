﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogController
{
    public class DenominationController : ICatalogController<DenominationData,Denomination>
    {
        readonly DenominationManager _managerDb;


        public DenominationController()
        {
            _managerDb = new DenominationManager();
        }
        
        public void AddCatalog(DenominationData input)
        {
            var data = TranslateToModel(input);
            Validate(data, false);

            var saved = _managerDb.Add(data);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public DenominationData getCatalog(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            return TranslateToService(_managerDb.GetOne(id));
        }

        public List<DenominationData> ListCatalogs(string id)
        {
            var dataList = _managerDb.GetAll(id);
            var result = new List<DenominationData>();
            foreach(Denomination den in dataList)
            {
                result.Add(TranslateToService(den));
            }
            return result;
        }


        public void RemoveCatalog(DenominationData input)
        {
            var data = TranslateToModel(input);
            if (data.Id < 0)
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            data.IsActive = false;

            var removed = _managerDb.Remove(data);
            if (!removed)
                throw new ArgumentException(StringResources.Instance.DbFailure);

        }
        public Denomination TranslateToModel(DenominationData data)
        {
            return new Denomination(data.Id, data.Description, data.CurrencyIso.Id, data.Factor, data.IsCash, data.IsActive);
        }

        public DenominationData TranslateToService(Denomination data)
        {
            return new DenominationData()
            {  Id = data.Id,
                Description = data.Description,
                CurrencyIso = new CurrencyController().getCatalog(data.CurrencyIso),
                Factor = data.Factor,
                IsCash = data.IsCash,
                IsActive = data.IsActive};
        }

        public void UpdateCatalog(DenominationData input)
        {
            var catalog = TranslateToModel(input);
            Validate(catalog, true);
            var updated = _managerDb.Update(catalog);
            if (!updated)
            {
                throw new ArgumentException(StringResources.Instance.DbFailure);
            }
        }

        public void Validate(Denomination data, bool isUpdate)
        {
            if (isUpdate)
            {
                if (data.Id<0)
                {
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
                }
            }

            if (string.IsNullOrWhiteSpace(data.Description) && !isUpdate)
            {
                throw new ArgumentException(StringResources.Instance.EmptyDescription, StringResources.Instance.Description);
            }

            if (string.IsNullOrWhiteSpace(data.CurrencyIso))
            {
                throw new ArgumentException(StringResources.Instance.CurrencyIsoValue, StringResources.Instance.CurrencyIso);
            }

            if (data.Factor<0)
            {
                throw new ArgumentException(StringResources.Instance.EmptyValue, StringResources.Instance.Factor);
            }

           
        }
        
    }
}
