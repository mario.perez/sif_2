﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogController
{
    public class CashboxController : ICatalogController<CashBoxData, Cashbox>
    {
        readonly CashboxManager _managerDb;

        public CashboxController()
        {
            _managerDb = new CashboxManager();
        }

        public void AddCatalog(CashBoxData input)
        {
            var data = TranslateToModel(input);
            Validate(data, false);
            var saved = _managerDb.Add(data);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public CashBoxData getCatalog(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            return TranslateToService(_managerDb.GetOne(id));
        }

        public List<CashBoxData> ListCatalogs(string id)
        {
            var dataList = _managerDb.GetAll(id);
            var result = new List<CashBoxData>();
            foreach (Cashbox con in dataList)
            {
                result.Add(TranslateToService(con));
            }
            return result;
        }

        public void RemoveCatalog(CashBoxData input)
        {
            var data = TranslateToModel(input);
            if (string.IsNullOrWhiteSpace(data.Id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            data.IsActive = false;

            var removed = _managerDb.Remove(data);
            if (!removed)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public Cashbox TranslateToModel(CashBoxData data)
        {
            return new Cashbox(data.Id, data.Name, data.Warehouse.Id, data.IsActive);
        }

        public CashBoxData TranslateToService(Cashbox data)
        {
            return new CashBoxData() { Id = data.Id, Name = data.Name, Warehouse = new WarehouseController().getCatalog(data.WarehouseId), IsActive = data.IsActive };
        }

        public void UpdateCatalog(CashBoxData input)
        {
            var catalog = TranslateToModel(input);
            Validate(catalog, true);

            var updated = _managerDb.Update(catalog);
            if (!updated)
            {
                throw new ArgumentException(StringResources.Instance.DbFailure);
            }
        }

        public void Validate(Cashbox data, bool isUpdate)
        {
            if(isUpdate)
            { 
                if (string.IsNullOrWhiteSpace(data.Id))
                {
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
                }
            }

            if (string.IsNullOrWhiteSpace(data.Name))
            {
                throw new ArgumentException(StringResources.Instance.EmptyName, StringResources.Instance.Name);
            }
            if (string.IsNullOrWhiteSpace(data.WarehouseId))
            {
                throw new ArgumentException(StringResources.Instance.WarehouseValue, StringResources.Instance.Warehouse);
            }
        }
    }
}
