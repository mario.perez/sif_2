﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using serviceLayer.SIF_DataContracts;
using businessLogic.Models;

namespace businessLogic.CatalogController
{
    public static class CatalogTranslator
    {
        public static Catalog TranslateToBusiness(Catalogue data)
        {
            return new Catalog(data.Id, data.Value, data.IsActive);
        }

        public static Catalogue TranslateToService(Catalog data)
        {
            return new Catalogue()
                    {
                        Id = data.Id,
                        Value = data.Name,
                        IsActive = data.isActive
                    };
        }
    }
}
