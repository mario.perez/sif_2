﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;
using dataAccess.BaseManagement;
using System.Data;

namespace businessLogic.CatalogController
{
    public class RolController
    {
        readonly RolManager _managerDb;

        public RolController()
        {
            _managerDb = new RolManager();
        }

        public Rol TraslateToModel(RolData data)
        {
            return new Rol() {
                Id = data.Id
                , Nombre = data.Nombre
                , Descripcion = data.Descripcion
                , Activo = data.Activo
                , UserCreacion = data.UserCreacion
                , UserModificacion = data.UserModificacion
                , FechaCreacion = data.FechaCreacion
                , FechaModificacion = data.FechaModificacion
                , AppCreacion = data.AppCreacion
                , AppModificacion = data.AppModificacion
                , MaqCreacion = data.MaqCreacion
                , MaqModificacion = data.MaqModificacion
            };
        }

        public RolData TraslateToService(Rol data)
        {
            return new RolData()
            {
                Id = data.Id
                ,
                Nombre = data.Nombre
                ,
                Descripcion = data.Descripcion
                ,
                Activo = data.Activo,
                UserCreacion = data.UserCreacion
                ,
                UserModificacion = data.UserModificacion
                ,
                FechaCreacion = data.FechaCreacion
                ,
                FechaModificacion = data.FechaModificacion
                ,
                AppCreacion = data.AppCreacion
                ,
                AppModificacion = data.AppModificacion
                ,
                MaqCreacion = data.MaqCreacion
                ,
                MaqModificacion = data.MaqModificacion                
            };
        }

        public void GestionarRol_CRUD(RolData input, string operacion)
        {
            var data = TraslateToModel(input);            
            var saved = _managerDb.GestionarRol(data, operacion);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public List<RolData> GetAllRolesOrRol(RolData rol,string operacion) {

            var lista = _managerDb.GetTable(TraslateToModel(rol),operacion);
            List<RolData> ListaRol = new List<RolData>();

            foreach (DataRow data in lista.Rows)
            {
                var r = new RolData()
                {                    
                    Id = Int32.Parse(data[0].ToString())
                    , Nombre = data[1].ToString()
                    , Descripcion = data[2].ToString()
                    , Activo = bool.Parse(data[3].ToString())
                    ,UserCreacion = data[4].ToString()
        ,
                    UserModificacion = data[5].ToString()
        ,
                    FechaCreacion = Convert.ToDateTime(data[6].ToString())
        ,
                    FechaModificacion = Convert.ToDateTime(data[7].ToString())
        ,
                    AppCreacion = data[8].ToString()
        ,
                    AppModificacion = data[9].ToString()
        ,
                    MaqCreacion = data[10].ToString()
        ,
                    MaqModificacion = data[1].ToString()
                };
                ListaRol.Add(r);
            }
            return ListaRol;
        }


    }    
}
