﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogController
{
    public class CurrencyController : ICatalogController<CurrencyData, Currency>
    {
        readonly CurrencyManager _managerDb;

        public CurrencyController()
        {
            _managerDb = new CurrencyManager();
        }

        public void AddCatalog(CurrencyData input)
        {
            var data = TranslateToModel(input);
            Validate(data,false);
            var saved = _managerDb.Add(data);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public CurrencyData getCatalog(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            return TranslateToService(_managerDb.GetOne(id));
        }

        public List<CurrencyData> ListCatalogs(string id)
        {
            var dataList = _managerDb.GetAll(id);
            var result = new List<CurrencyData>();
            foreach (Currency cur in dataList)
            {
                result.Add(TranslateToService(cur));
            }
            return result;
        }

        public void RemoveCatalog(CurrencyData input)
        {
            var data = TranslateToModel(input);
            var saved = _managerDb.Remove(data);
        }

        public Currency TranslateToModel(CurrencyData data)
        {
            return new Currency(data.Id, data.CurrencyNumber, data.Decimals, data.Name, data.Symbol, data.IsActive);
        }

        public CurrencyData TranslateToService(Currency data)
        {
            return new CurrencyData()
            {
                Id = data.Id,
                Name = data.Name,
                Decimals = data.Decimals,
                CurrencyNumber = data.CurrencyNumber,
                IsActive = data.IsActive,
                Symbol = data.Symbol
            };
        }

        public void UpdateCatalog(CurrencyData input)
        {
            var data = TranslateToModel(input);
            var saved = _managerDb.Update(data);
        }

        public void Validate(Currency data, bool isUpdate)
        {
            if (string.IsNullOrWhiteSpace(data.Name))
            {
                throw new ArgumentException(StringResources.Instance.EmptyName, StringResources.Instance.Name);
            }

            if (string.IsNullOrWhiteSpace(data.Id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            if (string.IsNullOrWhiteSpace(data.CurrencyNumber))
            {
                throw new ArgumentException(StringResources.Instance.CurrencyNumber, StringResources.Instance.CurrencyNumber);
            }

        }
    }
}
