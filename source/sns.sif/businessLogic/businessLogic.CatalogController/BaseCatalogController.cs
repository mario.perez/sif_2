﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;
using dataAccess.BaseManagement;

namespace businessLogic.CatalogController
{
    public class BaseCatalogController
    {
        protected ICRUD _managerDb;

        public void AddCatalog(Catalogue data)
        {
            Catalog input = CatalogTranslator.TranslateToBusiness(data);
            Validate(input, false);

            var saved = _managerDb.Add(input);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);

        }

        public Catalogue getCatalog(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            return CatalogTranslator.TranslateToService(_managerDb.GetOne(id));
        }

        public List<Catalogue> ListCatalogs()
        {
            var result = new List<Catalogue>();
            var catalogList = _managerDb.GetAll();
            foreach (Catalog cat in catalogList)
            {
                result.Add(CatalogTranslator.TranslateToService(cat));
            }
            return result;
        }

        public void RemoveCatalog(Catalogue input)
        {
            var data = CatalogTranslator.TranslateToBusiness(input);
            if (string.IsNullOrWhiteSpace(data.Id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            data.isActive = false;

            var removed = _managerDb.Remove(data);
            if (!removed)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public void UpdateCatalog(Catalogue input)
        {
            var catalog = CatalogTranslator.TranslateToBusiness(input);
            Validate(catalog, true);
            var updated = _managerDb.Update(catalog);
            if (!updated)
            {
                throw new ArgumentException(StringResources.Instance.DbFailure);
            }
        }

        public void Validate(Catalog data, bool isUpdate)
        {
            if (isUpdate)
            {
                if (string.IsNullOrWhiteSpace(data.Id))
                {
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
                }
            }

            if (string.IsNullOrWhiteSpace(data.Name))
            {
                throw new ArgumentException(StringResources.Instance.EmptyName, StringResources.Instance.Name);
            }
            
            

        }
    }
}
