using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogController
{
    public class ClientController : ICatalogController<ClientData, Client>
    {
        readonly ClientManager _managerDb;

        public ClientController()
        {
            _managerDb = new ClientManager();
        }

        public void AddCatalog(ClientData input)
        {
            var data = TranslateToModel(input);
            Validate(data,false);
            var saved = _managerDb.Add(data);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public ClientData getCatalog(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            return TranslateToService(_managerDb.GetOne(id));
        }

        public List<ClientData> ListCatalogs(string id)
        {
            var dataList = _managerDb.GetAll(id);
            var result = new List<ClientData>();
            foreach (Client cur in dataList)
            {
                result.Add(TranslateToService(cur));
            }
            return result;
        }

        public void RemoveCatalog(ClientData input)
        {
            var data = TranslateToModel(input);
            var saved = _managerDb.Remove(data);
        }

        public Client TranslateToModel(ClientData data)
        {
            return new Client()
				{
					
				ClientId = data.ClientId,
				ClientName = data.ClientName,
				Phone = data.Phone,
				Type = data.Type,
				Address = data.Address,
				Email = data.Email,
				Contact = data.Contact
				};
        }

        public ClientData TranslateToService(Client data)
        {
            return new ClientData()
            {
                
				ClientId = data.ClientId,
				ClientName = data.ClientName,
				Phone = data.Phone,
				Type = data.Type,
				Address = data.Address,
				Email = data.Email,
				Contact = data.Contact
			};
        }

        public void UpdateCatalog(ClientData input)
        {
            var data = TranslateToModel(input);
            var saved = _managerDb.Update(data);
        }

        public void Validate(Client data, bool isUpdate)
        {
            if(string.IsNullOrWhiteSpace(data.ClientId))
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            
        }
    }
}

