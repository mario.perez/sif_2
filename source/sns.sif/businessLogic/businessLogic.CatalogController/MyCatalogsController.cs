using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;
using System.Linq;

namespace businessLogic.CatalogController
{
    public class MyCatalogsController : ICatalogController<MyCatalogsData, MyCatalogs>
    {
        readonly MyCatalogsManager _managerDb;

        public MyCatalogsController()
        {
            _managerDb = new MyCatalogsManager();
        }

        public void AddCatalog(MyCatalogsData input)
        {
            var data = TranslateToModel(input);
            Validate(data,false);
            var saved = _managerDb.Add(data);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public MyCatalogsData getCatalog(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            return TranslateToService(_managerDb.GetOne(id));
        }

        public MyCatalogsData getCatalog(string Table, string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            var dataList = _managerDb.GetAll(id);
            var dataList2 = from e in dataList where e.TableName == Table && e.ItemId==id select e;
            var result = new List<MyCatalogsData>();
            foreach (MyCatalogs cur in dataList)
            {
                result.Add(TranslateToService(cur));
            }
            return result.First();            
        }

        public List<MyCatalogsData> ListCatalogs(string id)
        {
            var dataList = _managerDb.GetAll(id);
            var result = new List<MyCatalogsData>();
            foreach (MyCatalogs cur in dataList)
            {
                result.Add(TranslateToService(cur));
            }
            return result;
        }

        public List<MyCatalogsData> ListCatalogs(string Table, string id)
        {
            var dataList = _managerDb.GetAll(id);
            var dataList2 = from e in dataList where e.TableName == Table select e;
            var result = new List<MyCatalogsData>();
            foreach (MyCatalogs cur in dataList)
            {
                result.Add(TranslateToService(cur));
            }
            return result;
        }

        public void RemoveCatalog(MyCatalogsData input)
        {
            var data = TranslateToModel(input);
            var saved = _managerDb.Remove(data);
        }

        public MyCatalogs TranslateToModel(MyCatalogsData data)
        {
            return new MyCatalogs()
				{
					
				TableName = data.TableName,
				ItemId = data.ItemId,
				ItemDescription = data.ItemDescription
				};
        }

        public MyCatalogsData TranslateToService(MyCatalogs data)
        {
            return new MyCatalogsData()
            {
                
				TableName = data.TableName,
				ItemId = data.ItemId,
				ItemDescription = data.ItemDescription
			};
        }

        public void UpdateCatalog(MyCatalogsData input)
        {
            var data = TranslateToModel(input);
            var saved = _managerDb.Update(data);
        }

        public void Validate(MyCatalogs data, bool isUpdate)
        {
            if(string.IsNullOrWhiteSpace(data.TableName)|| string.IsNullOrWhiteSpace(data.ItemId))
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);

        }
    }
}

