﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using businessLogic.Models;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogController
{
    interface ICatalogController
    {
        void Validate(Catalog data, bool isUpdate);
        void AddCatalog(Catalogue data);
        void RemoveCatalog(Catalogue data);
        List<Catalogue> ListCatalogs();
        Catalogue getCatalog(String id);
        void UpdateCatalog(Catalogue catalog);

    }

    interface ICatalogController<T,K>
    {
        void Validate(K data, bool isUpdate);
        void AddCatalog(T input);
        void RemoveCatalog(T input);
        List<T> ListCatalogs(string id);
        T getCatalog(String id);
        void UpdateCatalog(T input);
        T TranslateToService(K data);
        K TranslateToModel(T data);

    }

}
