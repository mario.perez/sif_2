﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogController
{
    public class ProductController : ICatalogController<ProductData,Product>
    {
        readonly ProductManager _managerDb;
        public ProductController()
        {
            _managerDb = new ProductManager();
        }
        
        public void AddCatalog(ProductData input)
        {
            var data = TranslateToModel(input);
            Validate(data, false);
            var saved = _managerDb.Add(data);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public ProductData getCatalog(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            return TranslateToService(_managerDb.GetOne(id));
        }

        public List<ProductData> ListCatalogs(string id)
        {
            var dataList = _managerDb.GetAll(id);
            var result = new List<ProductData>();
            foreach(Product prod in dataList )
            {
                result.Add(TranslateToService(prod));
            }
            return result;
        }
        
        public void RemoveCatalog(ProductData input)
        {
            var data = TranslateToModel(input);
            if (string.IsNullOrWhiteSpace(data.ProductId))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            data.IsActive = false;

            var removed = _managerDb.Remove(data);
            if (!removed)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public Product TranslateToModel(ProductData data)
        {
            return new Product()
            {
                ProductId = data.ProductId,
                Description = data.Description,
                Category = CatalogTranslator.TranslateToBusiness(data.Category),
                Measurement = CatalogTranslator.TranslateToBusiness(data.Measurement),
                HasStock = data.HasStock,
                IsActive = data.IsActive,
                UnitsPerPackage = data.UnitsPerPackage
            };
        }

        public ProductData TranslateToService(Product data)
        {
            return new ProductData()
            {
                ProductId = data.ProductId,
                Description = data.Description,
                Category = CatalogTranslator.TranslateToService(data.Category),
                Measurement = CatalogTranslator.TranslateToService(data.Measurement),
                //Provider = new ProviderController().TranslateToService(data.Provider),
                HasStock = data.HasStock,
                IsActive = data.IsActive,
                UnitsPerPackage = data.UnitsPerPackage
            };
        }

        
        public void UpdateCatalog(ProductData input)
        {
            var catalog = TranslateToModel(input);
            Validate(catalog, true);
            var updated = _managerDb.Update(catalog);
            if (!updated)
            {
                throw new ArgumentException(StringResources.Instance.DbFailure);
            }
        }

        public void Validate(Product data, bool isUpdate)
        {
            if (string.IsNullOrWhiteSpace(data.ProductId))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            if (string.IsNullOrWhiteSpace(data.Description))
            {
                throw new ArgumentException(StringResources.Instance.EmptyDescription, StringResources.Instance.Description);
            }

            new CategoryController().Validate(data.Category, true);
            new MeasurementController().Validate(data.Measurement, true);
            //new ProviderController().Validate(data.Provider, true);

            if (data.UnitsPerPackage == 0)
            {
                throw new ArgumentException(StringResources.Instance.UnitsPerPackageValue, StringResources.Instance.UnitsPerPackage);
            }
        }
        
    }
}
