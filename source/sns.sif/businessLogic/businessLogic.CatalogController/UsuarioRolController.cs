﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;
using dataAccess.BaseManagement;
using System.Data;

namespace businessLogic.CatalogController
{
    public class UsuarioRolController
    {
        readonly UsuarioRolManager _managerDb;

        public UsuarioRolController()
        {
            _managerDb = new UsuarioRolManager();
        }

        public UsuarioRol TraslateToModel(UsuarioRolData data)
        {
            return new UsuarioRol() {
                Id = data.Id
                , RolId = data.RolId
                , UsuarioId = data.UsuarioId
            };
        }

        public UsuarioRolData TraslateToService(UsuarioRol data)
        {
            return new UsuarioRolData()
            {
                Id = data.Id
                ,                
                RolId = data.RolId
                ,
                UsuarioId = data.UsuarioId
            };
        }

        public void GestionarUsuarioRol(int usuarioId, int rolId, int id ,string operacion)
        {
            //var data = TraslateToModel(input);            
            var saved = _managerDb.GestionarUsuarioRol(usuarioId, rolId, id , operacion);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public List<UsuarioRolData> GetAllUsuarioRol(int usuarioId, int rolId, int id, string operacion, string codigoTag) {

            var lista = _managerDb.GetTable(usuarioId, rolId, id, operacion, codigoTag);
            List<UsuarioRolData> Lista = new List<UsuarioRolData>();

            foreach (DataRow data in lista.Rows)
            {
                var r = new UsuarioRolData()
                {
                    Id = Int32.Parse(data[0].ToString())
                    , RolId = Int32.Parse(data[1].ToString())
                    , NombreRol = data[2].ToString()
                    , DescripcionRol = data[3].ToString()
                    , UsuarioId = Int32.Parse(data[4].ToString())
                    , LoginUsuario = data[5].ToString()
                    , NombreUsuario = data[6].ToString()
                };
                Lista.Add(r);
            }
            return Lista;
        }

        public List<PrivilegioData> GetPrivilegioUser(int usuarioId, string operacion,string codigoTag)
        {

            var lista = _managerDb.GetTable(usuarioId, 0,0, operacion,codigoTag);
            List<PrivilegioData> ListaPrivilegio = new List<PrivilegioData>();

            foreach (DataRow data in lista.Rows)
            {
                var r = new PrivilegioData()
                {
                    Id = Int32.Parse(data[0].ToString())
                    ,
                    CodigoTag = data[1].ToString()
                    ,
                    Nombre = data[2].ToString()
                    ,
                    EsPadre = bool.Parse(data[3].ToString())
                    ,
                    PadreId = Int32.Parse(data[4].ToString())
                    ,
                    CodigoInterno = data[5].ToString()
                };
                ListaPrivilegio.Add(r);
            }
            return ListaPrivilegio;
        }
    }    
}
