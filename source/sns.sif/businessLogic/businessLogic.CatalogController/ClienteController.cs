﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;
using dataAccess.BaseManagement;
using System.Data;

namespace businessLogic.CatalogController
{
    public class ClienteController
    {
        readonly ClienteManager _managerDb;

        public ClienteController()
        {
            _managerDb = new ClienteManager();
        }

        public Cliente TraslateToModel(ClienteData data)
        {
            return new Cliente() {
                cliente_id = data.cliente_id
                , Cedula = data.Cedula
                , codigo_clte = data.codigo_clte
                , primer_apellido = data.primer_apellido
                , segundo_apellido = data.segundo_apellido
                , primer_nombre = data.primer_nombre
                , segundo_nombre = data.segundo_nombre
                , nombre_factura = data.nombre_factura
                , direccion1 = data.direccion1
                , direccion2 = data.direccion2
                , direccion3 = data.direccion3
                , ciudad = data.ciudad
                , departamento = data.departamento
                , pais = data.pais
                , telefono1 = data.telefono1
                , telefono2 = data.telefono2
                , fax = data.fax
                , Contacto = data.Contacto
                , e_mail1 = data.e_mail1
                , e_mail2 = data.e_mail2
                , activo = data.activo
                , ruc = data.ruc
                , maximo_Credito = data.maximo_Credito
                , comentario = data.comentario
                , UserCreacion = data.UserCreacion
                , UserModificacion = data.UserModificacion
                , FechaCreacion = data.FechaCreacion
                , FechaModificacion = data.FechaModificacion
                , AppCreacion = data.AppCreacion
                , AppModificacion = data.AppModificacion
                , MaqCreacion = data.MaqCreacion
                , MaqModificacion = data.MaqModificacion
                , Foto = data.Foto
            };
        }

        public ClienteData TraslateToService(Cliente data)
        {
            return new ClienteData()
            {
                cliente_id = data.cliente_id
                ,
                Cedula = data.Cedula
                ,
                codigo_clte = data.codigo_clte
                ,
                primer_apellido = data.primer_apellido
                ,
                segundo_apellido = data.segundo_apellido
                ,
                primer_nombre = data.primer_nombre
                ,
                segundo_nombre = data.segundo_nombre
                ,
                nombre_factura = data.nombre_factura
                ,
                direccion1 = data.direccion1
                ,
                direccion2 = data.direccion2
                ,
                direccion3 = data.direccion3
                ,
                ciudad = data.ciudad
                ,
                departamento = data.departamento
                ,
                pais = data.pais
                ,
                telefono1 = data.telefono1
                ,
                telefono2 = data.telefono2
                ,
                fax = data.fax
                ,
                Contacto = data.Contacto
                ,
                e_mail1 = data.e_mail1
                ,
                e_mail2 = data.e_mail2
                ,
                activo = data.activo
                ,
                ruc = data.ruc
                ,
                maximo_Credito = data.maximo_Credito
                ,
                comentario = data.comentario
                ,
                UserCreacion = data.UserCreacion
                ,
                UserModificacion = data.UserModificacion
                ,
                FechaCreacion = data.FechaCreacion
                ,
                FechaModificacion = data.FechaModificacion
                ,
                AppCreacion = data.AppCreacion
                ,
                AppModificacion = data.AppModificacion
                ,
                MaqCreacion = data.MaqCreacion
                ,
                MaqModificacion = data.MaqModificacion
                , Foto = data.Foto
            };
        }

        public void GestionarCliente_CRUD(ClienteData input, string operacion)
        {
            var data = TraslateToModel(input);            
            var saved = _managerDb.GestionarCliente(data, operacion);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public List<ClienteData> GetAllClientesOrCliente(ClienteData client) {

            var lista = _managerDb.GetTable(TraslateToModel(client));
            List<ClienteData> ListaCliente = new List<ClienteData>();

            foreach (DataRow data in lista.Rows)
            {
                var cliente = new ClienteData()
                {
                    cliente_id = Convert.ToInt32(data[0].ToString())
        ,
                    codigo_clte = data[1].ToString()
        ,
                    primer_apellido = data[2].ToString()
        ,
                    segundo_apellido = data[3].ToString()
        ,
                    primer_nombre = data[4].ToString()
        ,
                    segundo_nombre = data[5].ToString()
        ,
                    nombre_factura = data[6].ToString()
        ,
                    direccion1 = data[7].ToString()
        ,
                    direccion2 = data[8].ToString()
        ,
                    direccion3 = data[9].ToString()
        ,
                    ciudad = data[10].ToString()
        ,
                    departamento = data[11].ToString()
        ,
                    pais = data[12].ToString()
        ,
                    telefono1 = data[13].ToString()
        ,
                    telefono2 = data[14].ToString()
        ,
                    fax = data[15].ToString()
        ,
                    Contacto = data[16].ToString()
        ,
                    e_mail1 = data[17].ToString()
        ,
                    e_mail2 = data[18].ToString()
        ,
                    activo = Convert.ToBoolean(data[19].ToString())
        ,
                    ruc = data[20].ToString()
        ,
                    maximo_Credito = Convert.ToDecimal(data[21].ToString())
        ,
                    comentario = data[22].ToString()
        ,
                    UserCreacion = data[23].ToString()
        ,
                    UserModificacion = data[24].ToString()
        ,
                    FechaCreacion = Convert.ToDateTime(data[25].ToString())
        ,
                    FechaModificacion = Convert.ToDateTime(data[26].ToString())
        ,
                    AppCreacion = data[27].ToString()
        ,
                    AppModificacion = data[28].ToString()
        ,
                    MaqCreacion = data[29].ToString()
        ,
                    MaqModificacion = data[30].ToString()
                    , Cedula = data[31].ToString()
                    , Foto = DBNull.Value.Equals( data[32] ) ? null : (byte[])data[32]
                };
                ListaCliente.Add(cliente);
            }
            return ListaCliente;
        }


    }    
}
