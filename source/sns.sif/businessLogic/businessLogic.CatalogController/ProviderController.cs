﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogController
{
    public class ProviderController : ICatalogController<ProviderData,Provider>
    {
        readonly ProviderManager _managerDb;

        public ProviderController()
        {
            _managerDb = new ProviderManager();
        }
        
        public void AddCatalog(ProviderData input)
        {
            var data = TranslateToModel(input);
            Validate(data, false);

            var saved = _managerDb.Add(data);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public ProviderData getCatalog(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            return TranslateToService( _managerDb.GetOne(id));
        }
    

        public List<ProviderData> ListCatalogs(string id)
        {
            var dataList = _managerDb.GetAll(id);
            var result = new List<ProviderData>();
            foreach(Provider prov in dataList)
            {
                result.Add(TranslateToService(prov));
            }
            return result;
        }
        
        public void RemoveCatalog(ProviderData input)
        {
            var data = TranslateToModel(input);
            if (string.IsNullOrWhiteSpace(data.ProviderId))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }

            data.IsActive = false;

            var removed = _managerDb.Remove(data);
            if (!removed)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public Provider TranslateToModel(ProviderData data)
        {
            return new Provider(data.ProviderId, data.ProviderCodeId,data.ProviderName, data.Phone,data.Address,data.Email,data.Contact, data.IsActive);
        }

        public ProviderData TranslateToService(Provider data)
        {
            return new ProviderData()
             {
                ProviderId = data.ProviderId,
                ProviderCodeId = data.ProviderCodeId,
                ProviderName = data.ProviderName,
                Phone = data.Phone,
                Address = data.Address,
                Email = data.Email,
                Contact = data.Contact,
                IsActive = data.IsActive
            };

        }

        
        public void UpdateCatalog(ProviderData input)
        {
            var catalog = TranslateToModel(input);
            Validate(catalog, true);
            var updated = _managerDb.Update(catalog);
            if (!updated)
            {
                throw new ArgumentException(StringResources.Instance.DbFailure);
            }
        }

        public void Validate(Provider data, bool isUpdate)
        {
            if(isUpdate)
            {
                if (string.IsNullOrWhiteSpace(data.ProviderId))
                {
                    throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
                }
            }


            if (string.IsNullOrWhiteSpace(data.ProviderName))
            {
                throw new ArgumentException(StringResources.Instance.EmptyName, StringResources.Instance.Name);
            }                        
        }

    }
}
