﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;
using dataAccess.BaseManagement;
using System.Data;

namespace businessLogic.CatalogController
{
    public class ImpuestoController
    {
        readonly ImpuestoManager _managerDb;

        public ImpuestoController()
        {
            _managerDb = new ImpuestoManager();
        }

        public Impuesto TraslateToModel(ImpuestoData data)
        {
            return new Impuesto() {
                ImpuestoId = data.ImpuestoId
                , Codigo = data.Codigo
                , DescripcionImpuesto = data.DescripcionImpuesto
                , Activo = data.Activo
                , Nombre = data.Nombre
                , Predeterminado = data.Predeterminado
                , ValorImpuesto = data.ValorImpuesto
                ,UserCreacion = data.UserCreacion
                , UserModificacion = data.UserModificacion
                , FechaCreacion = data.FechaCreacion
                , FechaModificacion = data.FechaModificacion
                , AppCreacion = data.AppCreacion
                , AppModificacion = data.AppModificacion
                , MaqCreacion = data.MaqCreacion
                , MaqModificacion = data.MaqModificacion
            };
        }

        public ImpuestoData TraslateToService(Impuesto data)
        {
            return new ImpuestoData()
            {
                ImpuestoId = data.ImpuestoId
                ,                
                Codigo = data.Codigo
                ,
                DescripcionImpuesto = data.DescripcionImpuesto
                ,
                Activo = data.Activo
                ,
                Nombre = data.Nombre
                ,
                Predeterminado = data.Predeterminado
                ,
                ValorImpuesto = data.ValorImpuesto

                ,UserCreacion = data.UserCreacion
                ,
                UserModificacion = data.UserModificacion
                ,
                FechaCreacion = data.FechaCreacion
                ,
                FechaModificacion = data.FechaModificacion
                ,
                AppCreacion = data.AppCreacion
                ,
                AppModificacion = data.AppModificacion
                ,
                MaqCreacion = data.MaqCreacion
                ,
                MaqModificacion = data.MaqModificacion                
            };
        }

        public void GestionarImpuesto_CRUD(ImpuestoData input, string operacion)
        {
            var data = TraslateToModel(input);            
            var saved = _managerDb.GestionarImpuesto(data, operacion);
            
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);            
        }

        public List<ImpuestoData> GetAllImpuestosOrImpuesto(ImpuestoData impuesto) {

            var lista = _managerDb.GetTable(TraslateToModel(impuesto));
            List<ImpuestoData> ListaImpuesto = new List<ImpuestoData>();

            foreach (DataRow data in lista.Rows)
            {
                var cliente = new ImpuestoData()
                {
                    ImpuestoId = Int32.Parse(data[0].ToString())
                    , Codigo = data[1].ToString()
                    , DescripcionImpuesto = data[2].ToString()
                    ,
                    ValorImpuesto = decimal.Parse(data[3].ToString())                    
                    , Predeterminado = bool.Parse(data[4].ToString())                    
        ,
                    UserCreacion = data[5].ToString()
        ,
                    UserModificacion = data[6].ToString()
        ,
                    FechaCreacion = Convert.ToDateTime(data[7].ToString())
        ,
                    FechaModificacion = Convert.ToDateTime(data[8].ToString())
        ,
                    AppCreacion = data[9].ToString()
        ,
                    AppModificacion = data[10].ToString()
        ,
                    MaqCreacion = data[11].ToString()
        ,
                    MaqModificacion = data[12].ToString()
                    ,
                    Activo = bool.Parse(data[13].ToString())
                    , Nombre = data[14].ToString()
                };
                ListaImpuesto.Add(cliente);
            }
            return ListaImpuesto;
        }


    }    
}
