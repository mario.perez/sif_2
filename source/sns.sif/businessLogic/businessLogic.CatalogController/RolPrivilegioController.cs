﻿using System;
using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.CatalogManagement;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;
using dataAccess.BaseManagement;
using System.Data;

namespace businessLogic.CatalogController
{
    public class RolPrivilegioController
    {
        readonly RolPrivilegioManager _managerDb;

        public RolPrivilegioController()
        {
            _managerDb = new RolPrivilegioManager();
        }

        public RolPrivilegio TraslateToModel(RolPrivilegioData data)
        {
            return new RolPrivilegio()
            {
                Id = data.Id
                ,
                RolId = data.RolId
                ,
                PrivilegioId = data.PrivilegioId
            };
        }

        public RolPrivilegioData TraslateToService(RolPrivilegio data)
        {
            return new RolPrivilegioData()
            {
                Id = data.Id
                ,
                RolId = data.RolId
                ,
                PrivilegioId = data.PrivilegioId
            };
        }

        public void GestionarRolPrivilegio(int privilegioId, int rolId, int id, string operacion)
        {
            //var data = TraslateToModel(input);            
            var saved = _managerDb.GestionarRolPrivilegio(id,rolId, privilegioId, operacion);
            if (!saved)
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public List<PrivilegioData> GetRolPrivilegio(int privilegioId, int rolId, int id, string operacion)
        {

            var lista = _managerDb.GetTable(id, rolId, privilegioId,operacion);
            List<PrivilegioData> ListaPrivilegio = new List<PrivilegioData>();

            foreach (DataRow data in lista.Rows)
            {
                var r = new PrivilegioData()
                {
                    Id = Int32.Parse(data[0].ToString())
                    ,
                    CodigoTag = data[1].ToString()
                    ,
                    Nombre = data[2].ToString()
                    ,
                    EsPadre = bool.Parse(data[3].ToString())
                    ,
                    PadreId = Int32.Parse(data[4].ToString())
                    ,
                    CodigoInterno = data[5].ToString()
                };
                ListaPrivilegio.Add(r);
            }
            return ListaPrivilegio;
        }
    }
}
