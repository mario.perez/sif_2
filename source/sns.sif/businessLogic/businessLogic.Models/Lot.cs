﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Lot
    {
        private string _id;
        private DateTime _admissionDate;
        private bool _isActive;
        private readonly List<LotDetail> _details;

        public string Id { get => _id; set => _id = value; }
        public DateTime AdmissionDate { get => _admissionDate; set => _admissionDate = value; }
        public bool IsActive { get => _isActive; set => _isActive = value; }

        public List<LotDetail> Details => _details;

        public Lot()
        {
            _details = new List<LotDetail>();
        }
        public Lot(string Id, DateTime AdmissionDate, bool IsActive)
        {
            _id = Id;
            _admissionDate = AdmissionDate;
            _isActive = IsActive;
            _details = new List<LotDetail>();
        }

        public Lot(string Id, DateTime AdmissionDate, bool IsActive, LotDetail[] Details) : this(Id, AdmissionDate, IsActive)
        {
            _details.AddRange(Details);
        }
    }
}
