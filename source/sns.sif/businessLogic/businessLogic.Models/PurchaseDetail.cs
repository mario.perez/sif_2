﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class PurchaseDetail
    {
        private string _purchaseDetailId;
        private string _purchaseId;
        private string _productId;
        private string _lotId;
        private float _amount;
        private decimal _price;

        public string PurchaseDetailId { get => _purchaseDetailId; set => _purchaseDetailId = value; }
        public string PurchaseId { get => _purchaseId; set => _purchaseId = value; }
        public string ProductId { get => _productId; set => _productId = value; }
        public string LotId { get => _lotId; set => _lotId = value; }
        public float Amount { get => _amount; set => _amount = value; }
        public decimal Price { get => _price; set => _price = value; }

        public PurchaseDetail()
        {

        }

        public PurchaseDetail(string PurchaseDetailId, string PurchaseId, string LotId, string ProductId, float Amount, decimal Price)
        {
            _purchaseDetailId = PurchaseDetailId;
            _purchaseId = PurchaseId;
            _productId = ProductId;
            _lotId = LotId;
            _amount = Amount;
            _price = Price;
        }

    }
}
