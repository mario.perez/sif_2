﻿using System;

namespace businessLogic.Models
{
    public class Impuesto
    {
        public Impuesto() { }

        public int ImpuestoId { get; set; }
        public string Codigo { get; set; }
        public string DescripcionImpuesto { get; set; }
        public decimal ValorImpuesto { get; set; }
        public bool Predeterminado { get; set; }
        public bool Activo { get; set; }
        public string UserCreacion { get; set; }        
        public string Nombre { get; set; }
        public string UserModificacion { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public string AppCreacion { get; set; }

        public string AppModificacion { get; set; }

        public string MaqCreacion { get; set; }

        public string MaqModificacion { get; set; }
    }
}
