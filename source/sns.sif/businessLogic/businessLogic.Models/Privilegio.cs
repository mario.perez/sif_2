﻿using System;

namespace businessLogic.Models
{
    public class Privilegio
    {
        public Privilegio() { }

        public int Id { get; set; }
        public string CodigoTag { get; set; }
        public string Nombre { get; set; }
        public bool EsPadre { get; set; }
        public int PadreId { get; set; }
        public string CodigoInterno { get; set; }
    }
}
