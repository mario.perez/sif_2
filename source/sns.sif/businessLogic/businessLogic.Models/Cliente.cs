﻿using System;

namespace businessLogic.Models
{
    public class Cliente
    {

        public Cliente() { }

        public int cliente_id { get; set; }

        public string Cedula { get; set; }

        public string codigo_clte { get; set; }

        public string primer_apellido { get; set; }

        public string segundo_apellido { get; set; }

        public string primer_nombre { get; set; }

        public string segundo_nombre { get; set; }

        public string nombre_factura { get; set; }

        public string direccion1 { get; set; }

        public string direccion2 { get; set; }

        public string direccion3 { get; set; }
        
        public string ciudad { get; set; }
        
        public string departamento { get; set; }
        
        public string pais { get; set; }
        
        public string telefono1 { get; set; }
        
        public string telefono2 { get; set; }

        public string fax { get; set; }

        public string Contacto { get; set; }
        
        public string e_mail1 { get; set; }
        
        public string e_mail2 { get; set; }

        public bool activo { get; set; }

        public string ruc { get; set; }

        public decimal maximo_Credito { get; set; }

        public string comentario { get; set; }

        public string UserCreacion { get; set; }

        public string UserModificacion { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public string AppCreacion { get; set; }

        public string AppModificacion { get; set; }

        public string MaqCreacion { get; set; }

        public string MaqModificacion { get; set; }

        public byte[] Foto { get; set; }
    }
}
