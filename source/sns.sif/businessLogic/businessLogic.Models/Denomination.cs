﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Denomination
    {
        private int _id;
        private string _description;
        private string _currencyIso;
        private decimal _factor;
        private bool _isCash;
        private bool _isActive;

        public int Id { get => _id; set => _id = value; }
        public string Description { get => _description; set => _description = value; }
        public string CurrencyIso { get => _currencyIso; set => _currencyIso = value; }
        public decimal Factor { get => _factor; set => _factor = value; }
        public bool IsCash { get => _isCash; set => _isCash = value; }
        public bool IsActive { get => _isActive; set => _isActive = value; }

        public Denomination()
        {

        }

        public Denomination(int Id, string Description, string CurrencyIso,decimal Factor, bool IsCash, bool IsActive)
        {
            _id = Id;
            _description = Description;
            _currencyIso = CurrencyIso;
            _factor = Factor;
            _isCash = IsCash;
            _isActive = IsActive;
        }

    }
}
