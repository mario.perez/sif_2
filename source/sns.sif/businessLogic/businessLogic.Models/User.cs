﻿namespace businessLogic.Models
{
    public class User
    {
        private string userId;
        private string userName;
        private string password;
        //TODO: Agregar los roles.
        private bool isActive;

        public string UserId { get => userId; set => userId = value; }
        public string UserName { get => userName; set => userName = value; }
        public string Password { get => password; set => password = value; }
        public bool IsActive { get => isActive; set => isActive = value; }
    }
}
