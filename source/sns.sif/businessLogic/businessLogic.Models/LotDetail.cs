﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class LotDetail
    {
        private string _lotId;
        private string _productId;
        private double _quantity;
        private decimal _cost;
        private bool _isActive;

        public string LotId { get => _lotId; set => _lotId = value; }
        public string ProductId { get => _productId; set => _productId = value; }
        public double Quantity { get => _quantity; set => _quantity = value; }
        public decimal Cost { get => _cost; set => _cost = value; }
        public bool IsActive { get => _isActive; set => _isActive = value; }

        public LotDetail()
        {

        }

        public LotDetail(string LotId, string ProductId, double Quantity, decimal Cost, bool IsActive)
        {
            _lotId = LotId;
            _productId = ProductId;
            _quantity = Quantity;
            _cost = Cost;
            _isActive = IsActive;

        }
        
    }
}
