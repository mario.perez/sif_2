﻿using System;

namespace businessLogic.Models
{
    public class UsuarioRol
    {
        public UsuarioRol() { }

        public int Id { get; set; }
        public int RolId { get; set; }
        public int UsuarioId {get; set;}
        public string NombreRol { get; set; }
        public string DescripcionRol { get; set; }
        public string LoginUsuario { get; set; }
        public string NombreUsuario { get; set; }
    }
}
