﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class ClosingDetail
    {
        private long _closingId;
        private int _denominationId;
        private long _quantity;
        private decimal _cashAmount;

        public long ClosingId { get => _closingId; set => _closingId = value; }
        public int DenominationId { get => _denominationId; set => _denominationId = value; }
        public long Quantity { get => _quantity; set => _quantity = value; }
        public decimal CashAmount { get => _cashAmount; set => _cashAmount = value; }

        public ClosingDetail()
        {

        }

        public ClosingDetail(long ClosingId, int DenominationId, long Quantity, decimal CashAmount)
        {
            _closingId = ClosingId;
            _denominationId = DenominationId;
            _quantity = Quantity;
            _cashAmount = CashAmount;
        }
    }
}
