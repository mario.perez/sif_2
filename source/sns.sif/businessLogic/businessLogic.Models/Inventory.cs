﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Inventory
    {
        private int _warehouseId;
        private string _lotId;
        private string _productId;
        private double _quantity;
        private decimal _price;
        private bool _isActive;

        public int WarehouseId { get => _warehouseId; set => _warehouseId = value; }
        public string LotId { get => _lotId; set => _lotId = value; }
        public string ProductId { get => _productId; set => _productId = value; }
        public double Quantity { get => _quantity; set => _quantity = value; }
        public decimal Price { get => _price; set => _price = value; }
        public bool IsActive { get => _isActive; set => _isActive = value; }

        public Inventory()
        { }

        public Inventory(int WarehouseId, string LotId, string ProductId, double Quantity, decimal Price, bool IsActive)
        {
            _warehouseId = WarehouseId;
            _lotId = LotId;
            _productId = ProductId;
            _quantity = Quantity;
            _price = Price;
            _isActive = IsActive;
        }
    }
}
