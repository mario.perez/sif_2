﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Output
    {
        private string outputId;
        private DateTime date;
        private int warehouseId;
        private string comment;
        private bool isCancelled;
        private int type;
        private bool isRegistered;
        private string cancelUser;
        private string cancelAproveUser;
        private DateTime cancelDate;
        private readonly List<OutputDetail> _details;

        public string OutputId { get => outputId; set => outputId = value; }
        public DateTime Date { get => date; set => date = value; }
        public int WarehouseId { get => warehouseId; set => warehouseId = value; }
        public string Comment { get => comment; set => comment = value; }
        public bool IsCancelled { get => isCancelled; set => isCancelled = value; }
        public int Type { get => type; set => type = value; }
        public bool IsRegistered { get => isRegistered; set => isRegistered = value; }
        public string CancelUser { get => cancelUser; set => cancelUser = value; }
        public string CancelAproveUser { get => cancelAproveUser; set => cancelAproveUser = value; }
        public DateTime CancelDate { get => cancelDate; set => cancelDate = value; }

        public List<OutputDetail> Details => _details;

        public Output()
        {
            _details = new List<OutputDetail>();
        }



    }
}
