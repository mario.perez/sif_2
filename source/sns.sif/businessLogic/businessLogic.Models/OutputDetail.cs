﻿namespace businessLogic.Models
{
    public class OutputDetail
    {
        private string _outputDetailId;
        private string _outputId;
        private string _productId;
        private string _lotId;
        private float _amount;
        private decimal _price;

        public string OutputDetailId { get => _outputDetailId; set => _outputDetailId = value; }
        public string OutputId { get => _outputId; set => _outputId = value; }
        public string ProductId { get => _productId; set => _productId = value; }
        public float Amount { get => _amount; set => _amount = value; }
        public decimal Price { get => _price; set => _price = value; }
        public string LotId { get => _lotId; set => _lotId = value; }

        public OutputDetail()
        {

        }

        public OutputDetail(string OutPutDetailId, string OutputId, string LotId,string ProductId, float Ammount, decimal Price)
        {
            _outputDetailId = OutPutDetailId;
            _outputId = OutputId;
            _lotId = LotId;
            _productId = ProductId;
            _amount = Ammount;
            _price = Price;
        }
    }
}
