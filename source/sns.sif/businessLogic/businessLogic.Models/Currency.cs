﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
   public class Currency
    {
        private string _id;
        private string _currencyNumber;
        private int _decimals;
        private string _name;
        private string _symbol;
        private bool _isActive;

        public string Id { get => _id; set => _id = value; }
        public string CurrencyNumber { get => _currencyNumber; set => _currencyNumber = value; }
        public int Decimals { get => _decimals; set => _decimals = value; }
        public string Name { get => _name; set => _name = value; }
        public string Symbol { get => _symbol; set => _symbol = value; }
        public bool IsActive { get => _isActive; set => _isActive = value; }

        public Currency()
        {

        }

        public Currency(string Id, string CurrencyNumber, int Decimal, string Name, string Symbol, bool IsActive)
        {
            this._id = Id;
            this._currencyNumber = CurrencyNumber;
            this._decimals = Decimals;
            this._name = Name;
            this._symbol = Symbol;
            this._isActive = IsActive;
        }
    }
}
