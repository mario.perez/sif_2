﻿namespace businessLogic.Models
{
    public class Product
    {
        private string productId;
        private Catalog category;
        private string description;
        private Catalog measurement;
        //private Catalog proveedor;
        private bool isActive;
        private bool hasStock;
        private int unitsPerPackage;

        public string ProductId { get => productId; set => productId = value; }
        public Catalog Category { get => category; set => category = value; }
        public string Description { get => description; set => description = value; }
        public Catalog Measurement { get => measurement; set => measurement = value; }
        //public Catalog Provider { get => proveedor; set => proveedor = value; }
        public bool IsActive { get => isActive; set => isActive = value; }
        public bool HasStock { get => hasStock; set => hasStock = value; }
        public int UnitsPerPackage { get => unitsPerPackage; set => unitsPerPackage = value; }
    }
}
