using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Client
    {
        
		private string _clientid; 
		private string _clientname; 
		private string _phone; 
		private string _type; 
		private string _address; 
		private string _email; 
		private string _contact;

        public Client()
        {
        }

        public Client(
						string clientid,
						string clientname,
						string phone,
						string type,
						string address,
						string email,
						string contact)
        {
            
			_clientid = clientid; 
			_clientname = clientname; 
			_phone = phone; 
			_type = type; 
			_address = address; 
			_email = email; 
			_contact = contact;
        }


        
		public string ClientId { get=> _clientid; set => _clientid = value; } 
		public string ClientName { get=> _clientname; set => _clientname = value; } 
		public string Phone { get=> _phone; set => _phone = value; } 
		public string Type { get=> _type; set => _type = value; } 
		public string Address { get=> _address; set => _address = value; } 
		public string Email { get=> _email; set => _email = value; } 
		public string Contact { get=> _contact; set => _contact = value; }

    }
}

