﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Configuration
    {

        private readonly string _id;
        private string _value;
        private string _description;

        public Configuration()
        {
            _id = string.Empty;
            _value = string.Empty;
            _description = string.Empty;
        }

        public Configuration(string id, string value, string description)
        {
            _id = id;
            _value = value;
            _description = description;
        }

        public string Id => _id;
        public string Value { get => _value; set => _value = value; }
        public string Description { get => _description; set => _description = value; }
    }
}
