﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class BillDetail
    {
        private string _billDetailId;
        private string _billId;
        private string _productId;
        private string _lotId;
        private float _amount;
        private decimal _price;

        public string BillDetailId { get => _billDetailId; set => _billDetailId = value; }
        public string BillId { get => _billId; set => _billId = value; }
        public string ProductId { get => _productId; set => _productId = value; }
        public string LotId { get => _lotId; set => _lotId = value; }
        public float Amount { get => _amount; set => _amount = value; }
        public decimal Price { get => _price; set => _price = value; }

        public BillDetail()
        {

        }

        public BillDetail(string BillDetailId, string BillId, string ProductId, string LotId, float Amount, decimal Price)
        {
            _billDetailId = BillDetailId;
            _billId = BillId;
            _productId = ProductId;
            _lotId = LotId;
            _amount = Amount;
            _price = Price;
        }
        
    }
}
