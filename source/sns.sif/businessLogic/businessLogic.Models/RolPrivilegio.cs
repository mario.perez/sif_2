﻿using System;

namespace businessLogic.Models
{
    public class RolPrivilegio
    {
        public RolPrivilegio() { }
        public int Id { get; set; }
        public int RolId { get; set; }
        public int PrivilegioId {get;set;}
    }
}
