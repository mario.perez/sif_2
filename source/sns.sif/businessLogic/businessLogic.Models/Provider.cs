﻿namespace businessLogic.Models
{
    public class Provider
    {
        private string providerId;
        private string providerCodeId; 
        private string providerName;
        private string phone;
        private string address;
        private string email;
        private string contact;
        private bool isActive;

        public Provider()
        {

        }

        public Provider(string id, string codeId,string name, string phone, string address, string email, string contact,bool IsActive)
        {
            providerId = id;
            providerCodeId = codeId;
            providerName = name;
            this.phone = phone;
            this.address = address;
            this.email = email;
            this.contact = contact;
            isActive = IsActive;
        }

        public string ProviderName { get => providerName; set => providerName = value; }
        public string Phone { get => phone; set => phone = value; }
        public bool IsActive { get => isActive; set => isActive = value; }
        public string ProviderId { get => providerId; set => providerId = value; }
        public string ProviderCodeId { get => providerCodeId; set => providerCodeId = value; }
        public string Address { get => address; set => address = value; }
        public string Email { get => email; set => email = value; }
        public string Contact { get => contact; set => contact = value; }
    }
}
