﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Bill
    {
        private string _billId;
        private DateTime _date;
        private int _warehouseId;
        private decimal _totalAmount;
        private decimal _discount;
        private decimal _tax;
        private decimal _subtotal;
        private decimal _cash;
        private decimal _creditCard;
        private string _salesmanId;
        private string _cashboxId;
        private string _typePayment;
        private bool _isCancelled;
        private bool _isRegistered;
        private decimal _discountRate;
        private string _discountUser;
        private string _discountAproveUser;
        private string _comments;
        
        private string _cancelUser;
        private string _cancelAproveUser;
        private DateTime _cancelDate;
        private readonly List<BillDetail> _details;

        public string BillId { get => _billId; set => _billId = value; }
        public DateTime Date { get => _date; set => _date = value; }
        public int WarehouseId { get => _warehouseId; set => _warehouseId = value; }
        public decimal TotalAmount { get => _totalAmount; set => _totalAmount = value; }
        public decimal Discount { get => _discount; set => _discount = value; }
        public decimal Tax { get => _tax; set => _tax = value; }
        public decimal Subtotal { get => _subtotal; set => _subtotal = value; }
        public decimal Cash { get => _cash; set => _cash = value; }
        public decimal CreditCard { get => _creditCard; set => _creditCard = value; }
        public string SalesmanId { get => _salesmanId; set => _salesmanId = value; }
        public string CashboxId { get => _cashboxId; set => _cashboxId = value; }
        public string TypePayment { get => _typePayment; set => _typePayment = value; }
        public bool IsCancelled { get => _isCancelled; set => _isCancelled = value; }
        public bool IsRegistered { get => _isRegistered; set => _isRegistered = value; }
        public decimal DiscountRate { get => _discountRate; set => _discountRate = value; }
        public string DiscountUser { get => _discountUser; set => _discountUser = value; }
        public string DiscountAproveUser { get => _discountAproveUser; set => _discountAproveUser = value; }
        public string Comments { get => _comments; set => _comments = value; }
        public string CancelUser { get => _cancelUser; set => _cancelUser = value; }
        public string CancelAproveUser { get => _cancelAproveUser; set => _cancelAproveUser = value; }
        public DateTime CancelDate { get => _cancelDate; set => _cancelDate = value; }

        public List<BillDetail> Details => _details;

        public Bill()
        {
            _details = new List<BillDetail>();
        }


    }
}
