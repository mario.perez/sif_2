﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Closing
    {
        private long _id;
        private int _cashboxId;
        private DateTime _initialDateTime;
        private DateTime _finalDateTime;
        private string _userName;
        private readonly List<ClosingDetail> _details;

        public long Id { get => _id; set => _id = value; }
        public int CashboxId { get => _cashboxId; set => _cashboxId = value; }
        public DateTime InitialDateTime { get => _initialDateTime; set => _initialDateTime = value; }
        public DateTime FinalDateTime { get => _finalDateTime; set => _finalDateTime = value; }
        public string UserName { get => _userName; set => _userName = value; }

        public List<ClosingDetail> Details => _details;

        public Closing()
        {
            _details = new List<ClosingDetail>();
        }

        public Closing(long Id, int CashboxId, DateTime InitialDateTime, DateTime FinalDateTime, string UserName)
        {
            _id = Id;
            _cashboxId = CashboxId;
            _initialDateTime = InitialDateTime;
            _finalDateTime = FinalDateTime;
            _userName = UserName;
            _details = new List<ClosingDetail>();
        }
    }
}
