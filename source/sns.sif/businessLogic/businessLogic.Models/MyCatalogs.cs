using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class MyCatalogs
    {
        
		private string _tablename; 
		private string _itemid; 
		private string _itemdescription;

        public MyCatalogs()
        {
        }

        public MyCatalogs(
						string tablename,
						string itemid,
						string itemdescription)
        {
            
			_tablename = tablename; 
			_itemid = itemid; 
			_itemdescription = itemdescription;
        }


        
		public string TableName { get=> _tablename; set => _tablename = value; } 
		public string ItemId { get=> _itemid; set => _itemid = value; } 
		public string ItemDescription { get=> _itemdescription; set => _itemdescription = value; }

    }
}

