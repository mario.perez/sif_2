﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class InputDetail
    {
        private string _inputDetailId;
        private string _inputId;
        private string _productId;
        private string _lotId;
        private float _amount;
        private decimal _price;

        public string InputDetailId { get => _inputDetailId; set => _inputDetailId = value; }
        public string InputId { get => _inputId; set => _inputId = value; }
        public string ProductId { get => _productId; set => _productId = value; }
        public string LotId { get => _lotId; set => _lotId = value; }
        public float Amount { get => _amount; set => _amount = value; }
        public decimal Price { get => _price; set => _price = value; }

        public InputDetail()
        {

        }

        public InputDetail(string InputDetailId, string InputId, string LotId, string ProductId, float Ammount, decimal Price)
        {
            _inputDetailId = InputDetailId;
            _inputId = InputId;
            _lotId = LotId;
            _productId = ProductId;
            _amount = Ammount;
            _price = Price;
        }
    }
}
