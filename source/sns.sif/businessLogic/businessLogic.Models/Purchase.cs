﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Purchase
    {
        private string _purchaseId;
        private string _providerId;
        private DateTime _date;
        private int _warehouseId;
        private decimal _totalAmount;
        private decimal _discount;
        private decimal _tax;
        private decimal _subtotal;
        private string _comments;
        private bool isCancelled;
        private bool isRegistered;
        private string cancelUser;
        private string cancelAproveUser;
        private DateTime cancelDate;
        private readonly List<PurchaseDetail> _details;

        public string PurchaseId { get => _purchaseId; set => _purchaseId = value; }
        public string ProviderId { get => _providerId; set => _providerId = value; }
        public DateTime Date { get => _date; set => _date = value; }
        public int WarehouseId { get => _warehouseId; set => _warehouseId = value; }
        public decimal TotalAmount { get => _totalAmount; set => _totalAmount = value; }
        public decimal Discount { get => _discount; set => _discount = value; }
        public decimal Tax { get => _tax; set => _tax = value; }
        public decimal Subtotal { get => _subtotal; set => _subtotal = value; }
        public string Comments { get => _comments; set => _comments = value; }
        public bool IsCancelled { get => isCancelled; set => isCancelled = value; }
        public bool IsRegistered { get => isRegistered; set => isRegistered = value; }
        public string CancelUser { get => cancelUser; set => cancelUser = value; }
        public string CancelAproveUser { get => cancelAproveUser; set => cancelAproveUser = value; }
        public DateTime CancelDate { get => cancelDate; set => cancelDate = value; }

        public List<PurchaseDetail> Details => _details;

        public Purchase()
        {
            _details = new List<PurchaseDetail>();
        }
    }
}
