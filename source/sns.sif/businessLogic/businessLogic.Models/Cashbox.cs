﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Cashbox
    {
        private string _id;
        private string _name;
        private string _warehouseId;
        private bool _isActive;

        public Cashbox()
        {
        }

        public Cashbox(string id, string name, string warehouseId, bool isActive)
        {
            _id = id;
            _name = name;
            _warehouseId = warehouseId;
            _isActive = isActive;
        }


        public string Id { get => _id; set => _id = value; }
        public string Name { get => _name; set => _name = value; }
        public string WarehouseId { get => _warehouseId; set => _warehouseId = value; }
        public bool IsActive { get => _isActive; set => _isActive = value; }

    }
}
