﻿namespace businessLogic.Models
{
    public class Catalog
    {
        private string _id;
        private string _name;
        private bool _isActive;

        public Catalog()
        {
        }

        public Catalog(string Id, string name, bool isActive)
        {
            _id = Id;
            _name = name;
            _isActive = isActive;

        }
        public string Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }

        }
        public string Name {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        
        public bool isActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        public override string ToString()
        {
            return string.Concat("(", _id, ")", _name);
        }
    }
}
