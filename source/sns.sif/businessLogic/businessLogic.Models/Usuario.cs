﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Models
{
    public class Usuario
    {
        public Usuario() { }

        public int Id { get; set; }
        public string UsuarioLogin { get; set; }
        public string Contraseña { get; set; }
        public string NombreCompleto { get; set; }
        public string Correo { get; set; }
        public bool Activo { get; set; }
        public bool Bloqueado { get; set; }
        public string UserCreacion { get; set; }
        public string UserModificacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string AppCreacion { get; set; }
        public string AppModificacion { get; set; }
        public string MaqCreacion { get; set; }
        public string MaqModificacion { get; set; }
    }
}
