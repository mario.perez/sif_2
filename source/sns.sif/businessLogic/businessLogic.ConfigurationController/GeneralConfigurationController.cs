﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dataAccess.CatalogManagement;
using businessLogic.Models;
using crossCutting.Resources;

namespace businessLogic.ConfigurationController
{
    public class GeneralConfigurationController
    {
        readonly dataAccess.CatalogManagement.ConfigurationManager _managerDb;

        public GeneralConfigurationController()
        {
            _managerDb = new ConfigurationManager();
        }

        public void AddConfiguration(Configuration conf)
        {
            ValidConfiguration(conf);
            if(!_managerDb.Add(conf))
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public void UpdateConfiguration(Configuration conf)
        {
            ValidConfiguration(conf);
            if(!_managerDb.Update(conf))
                throw new ArgumentException(StringResources.Instance.DbFailure);
        }

        public List<Configuration> ListConfigs()
        {
            return _managerDb.GetAll();
        }

        private void ValidConfiguration(Configuration conf)
        {
            if (string.IsNullOrWhiteSpace(conf.Id))
            {
                throw new ArgumentException(StringResources.Instance.EmptyId, StringResources.Instance.Id);
            }
            if (string.IsNullOrWhiteSpace(conf.Value))
            {
                throw new ArgumentException(StringResources.Instance.EmptyValue, StringResources.Instance.Value);
            }
            if (string.IsNullOrWhiteSpace(conf.Description))
            {
                throw new ArgumentException(StringResources.Instance.EmptyDescription, StringResources.Instance.Description);
            }
        }

    }
}
