﻿using System;
using System.Collections.Generic;
using serviceLayer.SIF_DataContracts;
using businessLogic.CatalogController;


namespace serviceLayer.SIF_Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SIFService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SIFService.svc or SIFService.svc.cs at the Solution Explorer and start debugging.
    public class SIFService : ICatalogs
    {
        public GeneralMessage<bool> AddCashbox(CashBoxData Cashbox)
        {
            try
            {
                CashboxController caller = new CashboxController();
                caller.AddCatalog(Cashbox);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddCategory(Catalogue Category)
        {
            try
            {
                CategoryController caller = new CategoryController();
                caller.AddCatalog(Category);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddDenomination(DenominationData Denomination)
        {
            try
            {
                DenominationController caller = new DenominationController();
                caller.AddCatalog(Denomination);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddMeasurement(Catalogue Measurement)
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                caller.AddCatalog(Measurement);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddProduct(ProductData product)
        {
            try
            {
                ProductController caller = new ProductController();
                caller.AddCatalog(product);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddProvider(SIF_DataContracts.ProviderData provider)
        {
            try
            {
                ProviderController caller = new ProviderController();
                caller.AddCatalog(provider);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddSalesman(Catalogue Salesman)
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                caller.AddCatalog(Salesman);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddWarehouse(Catalogue Warehouse)
        {
            try { 
                WarehouseController caller = new WarehouseController();
                caller.AddCatalog(Warehouse);
                return new GeneralMessage<bool>(0,"",false,true);
            }
            catch(ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch(Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddCurrency(CurrencyData currency) {
            try
            {
                CurrencyController caller = new CurrencyController();
                caller.AddCatalog(currency);
                return new GeneralMessage<bool>(0,"",false,true);
            }
            catch (ArgumentException ex) {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<CashBoxData[]> GetAllCashboxes()
        {
            try
            {
                CashboxController caller = new CashboxController();
                var resp = caller.ListCatalogs();
                
                return new GeneralMessage<CashBoxData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<CashBoxData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<CashBoxData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue[]> GetAllCategories()
        {
            try
            {
                CategoryController caller = new CategoryController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<Catalogue[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Configs[]> GetAllConfigurations()
        {
            try
            {
                ConfigurationController caller = new ConfigurationController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<Configs[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Configs[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Configs[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<CurrencyData[]> GetAllCurrencies()
        {
            try
            {
                CurrencyController caller = new CurrencyController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<CurrencyData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<CurrencyData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<CurrencyData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<DenominationData[]> GetAllDenominations()
        {
            try
            {
                DenominationController caller = new DenominationController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<DenominationData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<DenominationData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<DenominationData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue[]> GetAllMeasurements()
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<Catalogue[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ProductData[]> GetAllProducts()
        {
            try
            {
                ProductController caller = new ProductController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<ProductData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ProductData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ProductData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ProviderData[]> GetAllProviders()
        {
            try
            {
                ProviderController caller = new ProviderController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<ProviderData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ProviderData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ProviderData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue[]> GetAllSalesmen()
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<Catalogue[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue[]> GetAllWarehouses()
        {
            try
            {
                WarehouseController caller = new WarehouseController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<Catalogue[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<CashBoxData> GetCashbox(string Id)
        {
            try
            {
                CashboxController caller = new CashboxController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<CashBoxData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<CashBoxData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<CashBoxData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue> GetCategory(string Id)
        {
            try
            {
                CategoryController caller = new CategoryController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Catalogue>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Configs> GetConfiguration(string Id)
        {
            try
            {
                ConfigurationController caller = new ConfigurationController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Configs>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Configs>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Configs>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<CurrencyData> GetCurrency(string Id)
        {
            try
            {
                CurrencyController caller = new CurrencyController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<CurrencyData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<CurrencyData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<CurrencyData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<DenominationData> GetDenomination(string Id)
        {
            try
            {
                DenominationController caller = new DenominationController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<DenominationData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<DenominationData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<DenominationData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue> GetMeasurement(string Id)
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Catalogue>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ProductData> GetProduct(string Id)
        {
            try
            {
                ProductController caller = new ProductController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<ProductData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ProductData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ProductData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ProviderData> GetProvider(string Id)
        {
            try
            {
                ProviderController caller = new ProviderController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<ProviderData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ProviderData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ProviderData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue> GetSalesman(string Id)
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Catalogue>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue> GetWarehouse(string Id)
        {
            try
            {
                WarehouseController caller = new WarehouseController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Catalogue>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<bool> RemoveCashbox(CashBoxData Cashbox)
        {
            try
            {
                CashboxController caller = new CashboxController();
                caller.RemoveCatalog(Cashbox);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveCategory(Catalogue Category)
        {
            try
            {
                CategoryController caller = new CategoryController();
                caller.RemoveCatalog(Category);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveDenomination(DenominationData Denomination)
        {
            try
            {
                DenominationController caller = new DenominationController();
                caller.RemoveCatalog(Denomination);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveMeasurement(Catalogue Measurement)
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                caller.RemoveCatalog(Measurement);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveProduct(ProductData product)
        {
            try
            {
                ProductController caller = new ProductController();
                caller.RemoveCatalog(product);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveProvider(ProviderData provider)
        {
            try
            {
                ProviderController caller = new ProviderController();
                caller.RemoveCatalog(provider);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveSalesman(Catalogue Salesman)
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                caller.RemoveCatalog(Salesman);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveWarehouse(Catalogue Warehouse)
        {
            try
            {
                WarehouseController caller = new WarehouseController();
                caller.RemoveCatalog(Warehouse);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveCurrency(CurrencyData currency) {
            try
            {
                CurrencyController caller = new CurrencyController();
                caller.RemoveCatalog(currency);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex) {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex) {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> UpdateCashbox(CashBoxData Cashbox)
        {
            try
            {
                CashboxController caller = new CashboxController();
                caller.UpdateCatalog(Cashbox);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateCategory(Catalogue Category)
        {
            try
            {
                CategoryController caller = new CategoryController();
                caller.UpdateCatalog(Category);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateConfiguration(Configs Configuration)
        {
            try
            {
                ConfigurationController caller = new ConfigurationController();
                caller.UpdateCatalog(Configuration);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateDenomination(DenominationData Denomination)
        {
            try
            {
                DenominationController caller = new DenominationController();
                caller.UpdateCatalog(Denomination);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateMeasurement(Catalogue Measurement)
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                caller.UpdateCatalog(Measurement);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateProduct(ProductData product)
        {
            try
            {
                ProductController caller = new ProductController();
                caller.UpdateCatalog(product);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateProvider(ProviderData provider)
        {
            try
            {
                ProviderController caller = new ProviderController();
                caller.UpdateCatalog(provider);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateSalesman(Catalogue Salesman)
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                caller.UpdateCatalog(Salesman);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateWarehouse(Catalogue Warehouse)
        {
            try
            {
                WarehouseController caller = new WarehouseController();
                caller.UpdateCatalog(Warehouse);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateCurrency(CurrencyData currency) {
            try {
                CurrencyController caller = new CurrencyController();
                caller.UpdateCatalog(currency);
                return new GeneralMessage<bool>(0,"", false, true);
            }
            catch (ArgumentException ex) {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex) {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }


        public GeneralMessage<bool> AddMyCatalogs(MyCatalogsData MyCatalogs)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                caller.AddCatalog(MyCatalogs);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }


        public GeneralMessage<MyCatalogsData[]> GetAllMyCatalogs(string Table)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                var resp = caller.ListCatalogs();

                return new GeneralMessage<MyCatalogsData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<MyCatalogsData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<MyCatalogsData[]>(2, ex.ToString(), true, null);
            }
        }



        public GeneralMessage<MyCatalogsData> GetMyCatalogs(string Table, string Id)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<MyCatalogsData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<MyCatalogsData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<MyCatalogsData>(2, ex.ToString(), true, null);
            }
        }


        public GeneralMessage<bool> RemoveMyCatalogs(MyCatalogsData MyCatalogs)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                caller.RemoveCatalog(MyCatalogs);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }


        public GeneralMessage<bool> UpdateMyCatalogs(MyCatalogsData MyCatalogs)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                caller.UpdateCatalog(MyCatalogs);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddClient(ClientData Client)
        {
            try
            {
                ClientController caller = new ClientController();
                caller.AddCatalog(Client);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveClient(ClientData Client)
        {
            try
            {
                ClientController caller = new ClientController();
                caller.RemoveCatalog(Client);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateClient(ClientData Client)
        {
            try
            {
                ClientController caller = new ClientController();
                caller.UpdateCatalog(Client);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<ClientData[]> GetAllClient()
        {
            try
            {
                ClientController caller = new ClientController();
                var resp = caller.ListCatalogs();

                return new GeneralMessage<ClientData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ClientData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ClientData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ClientData> GetClient(string Id)
        {
            try
            {
                ClientController caller = new ClientController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<ClientData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ClientData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ClientData>(2, ex.ToString(), true, null);
            }
        }
    }
}
