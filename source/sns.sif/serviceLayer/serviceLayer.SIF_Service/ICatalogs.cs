﻿using System.Collections.Generic;
using System.ServiceModel;
using serviceLayer.SIF_DataContracts;

namespace serviceLayer.SIF_Service
{
    [ServiceContract]
    public interface ICatalogs
    {
        [OperationContract]
        GeneralMessage<bool> AddCashbox(CashBoxData Cashbox);


        [OperationContract]
        GeneralMessage<bool> RemoveCashbox(CashBoxData Cashbox);


        [OperationContract]
        GeneralMessage<bool> UpdateCashbox(CashBoxData Cashbox);

        [OperationContract]
        GeneralMessage<CashBoxData[]> GetAllCashboxes(string id);

        [OperationContract]
        GeneralMessage<CashBoxData> GetCashbox(string Id);


        [OperationContract]
        GeneralMessage<bool> AddCategory(Catalogue Category);


        [OperationContract]
        GeneralMessage<bool> RemoveCategory(Catalogue Category);


        [OperationContract]
        GeneralMessage<bool> UpdateCategory(Catalogue Category);

        [OperationContract]
        GeneralMessage<Catalogue[]> GetAllCategories();

        [OperationContract]
        GeneralMessage<CurrencyData[]> GetAllCurrencies(string id);

        [OperationContract]
        GeneralMessage<CurrencyData> GetCurrency(string Id);

        [OperationContract]
        GeneralMessage<Catalogue> GetCategory(string Id);

        [OperationContract]
        GeneralMessage<bool> UpdateConfiguration(Configs Configuration);

        [OperationContract]
        GeneralMessage<Configs[]> GetAllConfigurations(string id);

        [OperationContract]
        GeneralMessage<Configs> GetConfiguration(string Id);

        [OperationContract]
        GeneralMessage<bool> AddDenomination(DenominationData Denomination);

        [OperationContract]
        GeneralMessage<bool> RemoveDenomination(DenominationData Denomination);


        [OperationContract]
        GeneralMessage<bool> UpdateDenomination(DenominationData Denomination);

        [OperationContract]
        GeneralMessage<DenominationData[]> GetAllDenominations(string id);

        [OperationContract]
        GeneralMessage<DenominationData> GetDenomination(string Id);

        [OperationContract]
        GeneralMessage<bool> AddMeasurement(Catalogue Measurement);


        [OperationContract]
        GeneralMessage<bool> RemoveMeasurement(Catalogue Measurement);


        [OperationContract]
        GeneralMessage<bool> UpdateMeasurement(Catalogue Measurement);

        [OperationContract]
        GeneralMessage<Catalogue[]> GetAllMeasurements();

        [OperationContract]
        GeneralMessage<Catalogue> GetMeasurement(string Id);

        [OperationContract]
        GeneralMessage<bool> AddProduct(ProductData product);


        [OperationContract]
        GeneralMessage<bool> RemoveProduct(ProductData product);


        [OperationContract]
        GeneralMessage<bool> UpdateProduct(ProductData product);

        [OperationContract]
        GeneralMessage<ProductData[]> GetAllProducts(string id);

        [OperationContract]
        GeneralMessage<ProductData> GetProduct(string Id);

        [OperationContract]
        GeneralMessage<bool> AddProvider(ProviderData provider);


        [OperationContract]
        GeneralMessage<bool> RemoveProvider(ProviderData provider);


        [OperationContract]
        GeneralMessage<bool> UpdateProvider(ProviderData provider);

        [OperationContract]
        GeneralMessage<ProviderData[]> GetAllProviders(string id);

        [OperationContract]
        GeneralMessage<ProviderData> GetProvider(string Id);

        [OperationContract]
        GeneralMessage<bool> AddSalesman(Catalogue Salesman);


        [OperationContract]
        GeneralMessage<bool> RemoveSalesman(Catalogue Salesman);


        [OperationContract]
        GeneralMessage<bool> UpdateSalesman(Catalogue Salesman);

        [OperationContract]
        GeneralMessage<Catalogue[]> GetAllSalesmen();

        [OperationContract]
        GeneralMessage<Catalogue> GetSalesman(string Id);

        [OperationContract]
        GeneralMessage<bool> AddWarehouse(Catalogue Warehouse);

        // MONEDA

        [OperationContract]
        GeneralMessage<bool> AddCurrency(CurrencyData currency);

        [OperationContract]
        GeneralMessage<bool> RemoveCurrency(CurrencyData currency);

        [OperationContract]
        GeneralMessage<bool> UpdateCurrency(CurrencyData currency);


        [OperationContract]
        GeneralMessage<bool> RemoveWarehouse(Catalogue Warehouse);


        [OperationContract]
        GeneralMessage<bool> UpdateWarehouse(Catalogue Warehouse);

        [OperationContract]
        GeneralMessage<Catalogue[]> GetAllWarehouses();

        [OperationContract]
        GeneralMessage<Catalogue> GetWarehouse(string Id);

        // Clientes - CREATED BY CHARLIE

        [OperationContract]
        GeneralMessage<bool> AddMyCatalogs(MyCatalogsData MyCatalogs);

        [OperationContract]
        GeneralMessage<bool> AddCliente(ClienteData cliente);

        [OperationContract]
        GeneralMessage<bool> UpdateCliente(ClienteData cliente);

        [OperationContract]
        GeneralMessage<bool> RemoveCliente(ClienteData cliente);

        //[OperationContract]
        //GeneralMessage<ClienteData> GetCliente(int Id);

        [OperationContract]
        GeneralMessage<bool> RemoveMyCatalogs(MyCatalogsData MyCatalogs);


        [OperationContract]
        GeneralMessage<bool> UpdateMyCatalogs(MyCatalogsData MyCatalogs);

        [OperationContract]
        GeneralMessage<MyCatalogsData[]> GetAllMyCatalogs(string Table, string id);

        [OperationContract]
        GeneralMessage<MyCatalogsData> GetMyCatalogs(string Table, string Id);

        [OperationContract]
        GeneralMessage<bool> AddClient(ClientData Client);


        [OperationContract]
        GeneralMessage<bool> RemoveClient(ClientData Client);

        [OperationContract]
        GeneralMessage<List<ClienteData>> GetAllCliente(int id);

        // Impuestos - CREATED BY CHARLIE

        [OperationContract]
        GeneralMessage<bool> AddImpuesto(ImpuestoData impuesto);

  		[OperationContract]
        GeneralMessage<bool> UpdateClient(ClientData Client);

        [OperationContract]
        GeneralMessage<ClientData[]> GetAllClient(string id);

        [OperationContract]
        GeneralMessage<ClientData> GetClient(string Id);


        [OperationContract]
        GeneralMessage<bool> UpdateImpuesto(ImpuestoData impuesto);

        [OperationContract]
        GeneralMessage<bool> RemoveImpuesto(ImpuestoData impuesto);

        [OperationContract]
        GeneralMessage<List<ImpuestoData>> GetAllImpuesto(ImpuestoData impuesto);


    }
}
