﻿using System;
using System.Collections.Generic;
using serviceLayer.SIF_DataContracts;
using businessLogic.CatalogController;


namespace serviceLayer.SIF_Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SIFService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SIFService.svc or SIFService.svc.cs at the Solution Explorer and start debugging.
    public class SIFService : ICatalogs
    {
        public GeneralMessage<bool> AddCashbox(CashBoxData Cashbox)
        {
            try
            {
                CashboxController caller = new CashboxController();
                caller.AddCatalog(Cashbox);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddCategory(Catalogue Category)
        {
            try
            {
                CategoryController caller = new CategoryController();
                caller.AddCatalog(Category);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddDenomination(DenominationData Denomination)
        {
            try
            {
                DenominationController caller = new DenominationController();
                caller.AddCatalog(Denomination);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddMeasurement(Catalogue Measurement)
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                caller.AddCatalog(Measurement);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddProduct(ProductData product)
        {
            try
            {
                ProductController caller = new ProductController();
                caller.AddCatalog(product);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddProvider(SIF_DataContracts.ProviderData provider)
        {
            try
            {
                ProviderController caller = new ProviderController();
                caller.AddCatalog(provider);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddSalesman(Catalogue Salesman)
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                caller.AddCatalog(Salesman);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddWarehouse(Catalogue Warehouse)
        {
            try { 
                WarehouseController caller = new WarehouseController();
                caller.AddCatalog(Warehouse);
                return new GeneralMessage<bool>(0,"",false,true);
            }
            catch(ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch(Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddCurrency(CurrencyData currency) {
            try
            {
                CurrencyController caller = new CurrencyController();
                caller.AddCatalog(currency);
                return new GeneralMessage<bool>(0,"",false,true);
            }
            catch (ArgumentException ex) {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<CashBoxData[]> GetAllCashboxes(string id)
        {
            try
            {
                CashboxController caller = new CashboxController();
                var resp = caller.ListCatalogs(id);
                
                return new GeneralMessage<CashBoxData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<CashBoxData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<CashBoxData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue[]> GetAllCategories()
        {
            try
            {
                CategoryController caller = new CategoryController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<Catalogue[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Configs[]> GetAllConfigurations(string id)
        {
            try
            {
                ConfigurationController caller = new ConfigurationController();
                var resp = caller.ListCatalogs(id);
                return new GeneralMessage<Configs[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Configs[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Configs[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<CurrencyData[]> GetAllCurrencies(string id)
        {
            try
            {
                CurrencyController caller = new CurrencyController();
                var resp = caller.ListCatalogs(id);
                return new GeneralMessage<CurrencyData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<CurrencyData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<CurrencyData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<DenominationData[]> GetAllDenominations(string id)
        {
            try
            {
                DenominationController caller = new DenominationController();
                var resp = caller.ListCatalogs(id);
                return new GeneralMessage<DenominationData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<DenominationData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<DenominationData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue[]> GetAllMeasurements()
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<Catalogue[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ProductData[]> GetAllProducts(string id)
        {
            try
            {
                ProductController caller = new ProductController();
                var resp = caller.ListCatalogs(id);
                return new GeneralMessage<ProductData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ProductData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ProductData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ProviderData[]> GetAllProviders(string id)
        {
            try
            {
                ProviderController caller = new ProviderController();
                var resp = caller.ListCatalogs(id);
                return new GeneralMessage<ProviderData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ProviderData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ProviderData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue[]> GetAllSalesmen()
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<Catalogue[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue[]> GetAllWarehouses()
        {
            try
            {
                WarehouseController caller = new WarehouseController();
                var resp = caller.ListCatalogs();
                return new GeneralMessage<Catalogue[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<CashBoxData> GetCashbox(string Id)
        {
            try
            {
                CashboxController caller = new CashboxController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<CashBoxData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<CashBoxData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<CashBoxData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue> GetCategory(string Id)
        {
            try
            {
                CategoryController caller = new CategoryController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Catalogue>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Configs> GetConfiguration(string Id)
        {
            try
            {
                ConfigurationController caller = new ConfigurationController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Configs>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Configs>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Configs>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<CurrencyData> GetCurrency(string Id)
        {
            try
            {
                CurrencyController caller = new CurrencyController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<CurrencyData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<CurrencyData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<CurrencyData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<DenominationData> GetDenomination(string Id)
        {
            try
            {
                DenominationController caller = new DenominationController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<DenominationData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<DenominationData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<DenominationData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue> GetMeasurement(string Id)
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Catalogue>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ProductData> GetProduct(string Id)
        {
            try
            {
                ProductController caller = new ProductController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<ProductData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ProductData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ProductData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ProviderData> GetProvider(string Id)
        {
            try
            {
                ProviderController caller = new ProviderController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<ProviderData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ProviderData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ProviderData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue> GetSalesman(string Id)
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Catalogue>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<Catalogue> GetWarehouse(string Id)
        {
            try
            {
                WarehouseController caller = new WarehouseController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<Catalogue>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<Catalogue>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<Catalogue>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<bool> RemoveCashbox(CashBoxData Cashbox)
        {
            try
            {
                CashboxController caller = new CashboxController();
                caller.RemoveCatalog(Cashbox);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveCategory(Catalogue Category)
        {
            try
            {
                CategoryController caller = new CategoryController();
                caller.RemoveCatalog(Category);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveDenomination(DenominationData Denomination)
        {
            try
            {
                DenominationController caller = new DenominationController();
                caller.RemoveCatalog(Denomination);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveMeasurement(Catalogue Measurement)
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                caller.RemoveCatalog(Measurement);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveProduct(ProductData product)
        {
            try
            {
                ProductController caller = new ProductController();
                caller.RemoveCatalog(product);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveProvider(ProviderData provider)
        {
            try
            {
                ProviderController caller = new ProviderController();
                caller.RemoveCatalog(provider);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveSalesman(Catalogue Salesman)
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                caller.RemoveCatalog(Salesman);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveWarehouse(Catalogue Warehouse)
        {
            try
            {
                WarehouseController caller = new WarehouseController();
                caller.RemoveCatalog(Warehouse);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveCurrency(CurrencyData currency) {
            try
            {
                CurrencyController caller = new CurrencyController();
                caller.RemoveCatalog(currency);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex) {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex) {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> UpdateCashbox(CashBoxData Cashbox)
        {
            try
            {
                CashboxController caller = new CashboxController();
                caller.UpdateCatalog(Cashbox);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateCategory(Catalogue Category)
        {
            try
            {
                CategoryController caller = new CategoryController();
                caller.UpdateCatalog(Category);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateConfiguration(Configs Configuration)
        {
            try
            {
                ConfigurationController caller = new ConfigurationController();
                caller.UpdateCatalog(Configuration);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateDenomination(DenominationData Denomination)
        {
            try
            {
                DenominationController caller = new DenominationController();
                caller.UpdateCatalog(Denomination);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateMeasurement(Catalogue Measurement)
        {
            try
            {
                MeasurementController caller = new MeasurementController();
                caller.UpdateCatalog(Measurement);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateProduct(ProductData product)
        {
            try
            {
                ProductController caller = new ProductController();
                caller.UpdateCatalog(product);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateProvider(ProviderData provider)
        {
            try
            {
                ProviderController caller = new ProviderController();
                caller.UpdateCatalog(provider);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateSalesman(Catalogue Salesman)
        {
            try
            {
                SalesmanController caller = new SalesmanController();
                caller.UpdateCatalog(Salesman);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateWarehouse(Catalogue Warehouse)
        {
            try
            {
                WarehouseController caller = new WarehouseController();
                caller.UpdateCatalog(Warehouse);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateCurrency(CurrencyData currency) {
            try {
                CurrencyController caller = new CurrencyController();
                caller.UpdateCatalog(currency);
                return new GeneralMessage<bool>(0,"", false, true);
            }
            catch (ArgumentException ex) {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex) {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }


        public GeneralMessage<bool> AddMyCatalogs(MyCatalogsData MyCatalogs)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                caller.AddCatalog(MyCatalogs);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }


        public GeneralMessage<MyCatalogsData[]> GetAllMyCatalogs(string Table,string id)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                var resp = caller.ListCatalogs(id);

                return new GeneralMessage<MyCatalogsData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<MyCatalogsData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<MyCatalogsData[]>(2, ex.ToString(), true, null);
            }
        }



        public GeneralMessage<MyCatalogsData> GetMyCatalogs(string Table, string Id)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<MyCatalogsData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<MyCatalogsData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<MyCatalogsData>(2, ex.ToString(), true, null);
            }
        }


        public GeneralMessage<bool> RemoveMyCatalogs(MyCatalogsData MyCatalogs)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                caller.RemoveCatalog(MyCatalogs);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }


        public GeneralMessage<bool> UpdateMyCatalogs(MyCatalogsData MyCatalogs)
        {
            try
            {
                MyCatalogsController caller = new MyCatalogsController();
                caller.UpdateCatalog(MyCatalogs);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> AddClient(ClientData Client)
        {
            try
            {
                ClientController caller = new ClientController();
                caller.AddCatalog(Client);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveClient(ClientData Client)
        {
            try
            {
                ClientController caller = new ClientController();
                caller.RemoveCatalog(Client);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateClient(ClientData Client)
        {
            try
            {
                ClientController caller = new ClientController();
                caller.UpdateCatalog(Client);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<ClientData[]> GetAllClient(string id)
        {
            try
            {
                ClientController caller = new ClientController();
                var resp = caller.ListCatalogs(id);

                return new GeneralMessage<ClientData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ClientData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ClientData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ClientData> GetClient(string Id)
        {
            try
            {
                ClientController caller = new ClientController();
                var resp = caller.getCatalog(Id);
                return new GeneralMessage<ClientData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ClientData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ClientData>(2, ex.ToString(), true, null);
            }
        }


        public GeneralMessage<bool> AddCliente(ClienteData cliente)
        {
            try
            {
                ClienteController caller = new ClienteController();
                cliente.FechaModificacion = cliente.FechaCreacion;
                caller.GestionarCliente_CRUD(cliente, "I");
                return new GeneralMessage<bool>(0, "Cliente Agregado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> UpdateCliente(ClienteData cliente)
        {
            try
            {
                ClienteController caller = new ClienteController();
                caller.GestionarCliente_CRUD(cliente, "U");
                return new GeneralMessage<bool>(0, "Cliente Actualizado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }
        public GeneralMessage<bool> RemoveCliente(ClienteData cliente)
        {
            try
            {
                ClienteController caller = new ClienteController();
                caller.GestionarCliente_CRUD(cliente, "D");
                return new GeneralMessage<bool>(0, "Cliente Eliminado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<List<ClienteData>> GetAllCliente(int id)
        {
            try
            {
                ClienteController caller = new ClienteController();
                var lista = caller.GetAllClientesOrCliente(new ClienteData() { cliente_id = id});
                return new GeneralMessage<List<ClienteData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<ClienteData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<ClienteData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<bool> AddImpuesto(ImpuestoData impuesto)
        {
            try
            {
                ImpuestoController caller = new ImpuestoController();
                impuesto.FechaModificacion = impuesto.FechaCreacion;
                caller.GestionarImpuesto_CRUD(impuesto, "I");
                return new GeneralMessage<bool>(0, "Impuesto Agregado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> UpdateImpuesto(ImpuestoData impuesto)
        {
            try
            {
                ImpuestoController caller = new ImpuestoController();
                caller.GestionarImpuesto_CRUD(impuesto, "U");
                return new GeneralMessage<bool>(0, "Impuesto Actualizado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }
        public GeneralMessage<bool> RemoveImpuesto(ImpuestoData impuesto)
        {
            try
            {
                ImpuestoController caller = new ImpuestoController();
                caller.GestionarImpuesto_CRUD(impuesto, "D");
                return new GeneralMessage<bool>(0, "Impuesto Eliminado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<List<ImpuestoData>> GetAllImpuesto(ImpuestoData impuesto)
        {
            try
            {
                ImpuestoController caller = new ImpuestoController();
                var lista = caller.GetAllImpuestosOrImpuesto(impuesto);
                return new GeneralMessage<List<ImpuestoData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<ImpuestoData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<ImpuestoData>>(2, ex.Message, true, null);
            }
        }

        //public GeneralMessage<bool> AddRol(RolData rol)
        //{
        //    try
        //    {
        //        RolController caller = new RolController();
        //        rol.FechaModificacion = rol.FechaCreacion;
        //        caller.GestionarRol_CRUD(rol, "I");
        //        return new GeneralMessage<bool>(0, "Rol Agregado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<bool> UpdateRol(RolData rol)
        //{
        //    try
        //    {
        //        RolController caller = new RolController();
        //        rol.FechaModificacion = rol.FechaCreacion;
        //        caller.GestionarRol_CRUD(rol, "U");
        //        return new GeneralMessage<bool>(0, "Rol Modificado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<bool> RemoveRol(RolData rol)
        //{
        //    try
        //    {
        //        RolController caller = new RolController();
        //        rol.FechaModificacion = rol.FechaCreacion;
        //        caller.GestionarRol_CRUD(rol, "D");
        //        return new GeneralMessage<bool>(0, "Rol Eliminado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<List<RolData>> GetAllRol(RolData rol)
        //{
        //    try
        //    {
        //        RolController caller = new RolController();
        //        var lista = caller.GetAllRolesOrRol(rol,"S");
        //        return new GeneralMessage<List<RolData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<RolData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<RolData>>(2, ex.Message, true, null);
        //    }
        //}

        ////public GeneralMessage<List<PrivilegioData>> GetPrivilegioRol(int rolId)
        ////{
        ////    try
        ////    {
        ////        PrivilegioController caller = new PrivilegioController();
        ////        var lista = caller.GetAllPrivilegio(0, rolId, "P");
        ////        return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
        ////    }
        ////    catch (ArgumentException ex)
        ////    {
        ////        return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
        ////    }
        ////}

        //public GeneralMessage<bool> AddPrivilegioRol(int privilegioId, int rolId)
        //{
        //    try
        //    {
        //        RolPrivilegioController caller = new RolPrivilegioController();                
        //        caller.GestionarRolPrivilegio(privilegioId, rolId, 0,"I");
        //        return new GeneralMessage<bool>(0, "Privilegio Asignado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<bool> RemovePrivilegioRol(int privilegioId, int rolId)
        //{
        //    try
        //    {
        //        RolPrivilegioController caller = new RolPrivilegioController();
        //        caller.GestionarRolPrivilegio(privilegioId, rolId,0, "D");
        //        return new GeneralMessage<bool>(0, "Privilegio restringido", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<List<PrivilegioData>> GetPrivilegioAsignado(int privilegioId, int rolId)
        //{
        //    try
        //    {
        //        RolPrivilegioController caller = new RolPrivilegioController();
        //        var lista = caller.GetRolPrivilegio(privilegioId, rolId,0,"C" );
        //        return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<List<PrivilegioData>> GetPrivilegioNoAsignado(int privilegioId)
        //{
        //    try
        //    {
        //        RolPrivilegioController caller = new RolPrivilegioController();
        //        var lista = caller.GetRolPrivilegio(privilegioId,0,0, "N");
        //        return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<PrivilegioData> GetRolPrivilegioValidacion(int privilegioId, int rolId)
        //{
        //    try
        //    {
        //        RolPrivilegioController caller = new RolPrivilegioController();
        //        var lista = caller.GetRolPrivilegio(privilegioId, rolId,0, "V");
        //        return new GeneralMessage<PrivilegioData>(0, "", false, lista[0]);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<PrivilegioData>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<PrivilegioData>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<List<PrivilegioData>> GetPrivilegioHijos(int padreId)
        //{
        //    try
        //    {
        //        PrivilegioData privilegio = new PrivilegioData();
        //        privilegio.PadreId = padreId;

        //        PrivilegioController caller = new PrivilegioController();
        //        var lista = caller.GetAllPrivilegio(privilegio, "S");
        //        return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<bool> AddUsuario(UsuarioData usuario)
        //{
        //    try
        //    {
        //        UsuarioController caller = new UsuarioController();
        //        usuario.FechaModificacion = usuario.FechaCreacion;
        //        caller.GestionarUsuario_CRUD(usuario, "I");
        //        return new GeneralMessage<bool>(0, "Usuario Agregado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<bool> UpdateUsuario(UsuarioData usuario)
        //{
        //    try
        //    {
        //        UsuarioController caller = new UsuarioController();
        //        usuario.FechaModificacion = usuario.FechaCreacion;
        //        caller.GestionarUsuario_CRUD(usuario, "U");
        //        return new GeneralMessage<bool>(0, "Usuario Modificado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<bool> RemoveUsuario(UsuarioData usuario)
        //{
        //    try
        //    {
        //        UsuarioController caller = new UsuarioController();
        //        usuario.FechaModificacion = usuario.FechaCreacion;
        //        caller.GestionarUsuario_CRUD(usuario, "D");
                
        //        return new GeneralMessage<bool>(0, "Usuario Eliminado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<List<UsuarioData>> GetAllUsuario(UsuarioData usuario)
        //{
        //    try
        //    {
        //        UsuarioController caller = new UsuarioController();
        //        var lista = caller.GetAllUsuario(usuario, "S");
        //        return new GeneralMessage<List<UsuarioData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<UsuarioData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<UsuarioData>>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<bool> AddUsuarioRol(int usuarioId, int rolId, int id, string operacion)
        //{
        //    try
        //    {
        //        UsuarioRolController caller = new UsuarioRolController();
        //        caller.GestionarUsuarioRol(usuarioId, rolId, id, "I");
        //        return new GeneralMessage<bool>(0, "Añadido", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<bool> RemoveUsuarioRol(int usuarioId, int rolId, int id, string operacion)
        //{
        //    try
        //    {
        //        UsuarioRolController caller = new UsuarioRolController();
        //        caller.GestionarUsuarioRol(usuarioId, rolId, id, "D");
        //        return new GeneralMessage<bool>(0, "Eliminado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<List<UsuarioRolData>> GetAllUsuarioRol(int usuarioId, int rolId, int id, string operacion)
        //{
        //    try
        //    {
        //        UsuarioRolController caller = new UsuarioRolController();
        //        var lista = caller.GetAllUsuarioRol(usuarioId, rolId, id, "S",string.Empty);
        //        return new GeneralMessage<List<UsuarioRolData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<UsuarioRolData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<UsuarioRolData>>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<List<UsuarioRolData>> GetAllUsuarioRol_NoRol(int usuarioId)
        //{
        //    try
        //    {
        //        UsuarioRolController caller = new UsuarioRolController();
        //        var lista = caller.GetAllUsuarioRol(usuarioId, 0,0, "N",string.Empty);
        //        return new GeneralMessage<List<UsuarioRolData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<UsuarioRolData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<UsuarioRolData>>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<List<UsuarioRolData>> GetAllUsuarioRol_NoUsuario(int rolId)
        //{
        //    try
        //    {
        //        UsuarioRolController caller = new UsuarioRolController();
        //        var lista = caller.GetAllUsuarioRol(0, rolId, 0, "M",string.Empty);
        //        return new GeneralMessage<List<UsuarioRolData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<UsuarioRolData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<UsuarioRolData>>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<UsuarioData> AutenticarUsuario(string usuarioLogin,string contraseña)
        //{
        //    try
        //    {
        //        UsuarioController caller = new UsuarioController();
        //        UsuarioData usuario = new UsuarioData();
        //        usuario.FechaCreacion = DateTime.Now;
        //        usuario.FechaModificacion = DateTime.Now;
        //        usuario.UsuarioLogin = usuarioLogin;
        //        usuario.Contraseña = contraseña;

        //        var user = caller.AutenticarUsuario(usuario, "V");
        //        return new GeneralMessage<UsuarioData>(0, "", false, user);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<UsuarioData>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<UsuarioData>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<List<PrivilegioData>> GetPrivilegioUsuario(int idUsuario)
        //{
        //    try
        //    {
        //        UsuarioRolController caller = new UsuarioRolController();
        //        var lista = caller.GetPrivilegioUser(idUsuario,"P",string.Empty);
        //        return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<List<PrivilegioData>> GetPrivilegioAccion(int idUsuario, string codigoTag)
        //{
        //    try
        //    {
        //        UsuarioRolController caller = new UsuarioRolController();
        //        var lista = caller.GetPrivilegioUser(idUsuario, "A",codigoTag);
        //        return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
        //    }
        //}

        //public GeneralMessage<bool> AddPrivilegio(PrivilegioData privilegio)
        //{
        //    try
        //    {
        //        PrivilegioController caller = new PrivilegioController();
        //        caller.GestionarPrivilegio(privilegio, "I");
        //        return new GeneralMessage<bool>(0, "Añadido", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<bool> RemovePrivilegio(PrivilegioData privilegio)
        //{
        //    try
        //    {
        //        PrivilegioController caller = new PrivilegioController();
        //        caller.GestionarPrivilegio(privilegio, "D");
        //        return new GeneralMessage<bool>(0, "Eliminado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}

        //public GeneralMessage<bool> UpdatePrivilegio(PrivilegioData privilegio)
        //{
        //    try
        //    {
        //        PrivilegioController caller = new PrivilegioController();
        //        caller.GestionarPrivilegio(privilegio, "U");
        //        return new GeneralMessage<bool>(0, "Modificado", false, true);
        //    }
        //    catch (ArgumentException ex)
        //    {
        //        return new GeneralMessage<bool>(1, ex.Message, true, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new GeneralMessage<bool>(2, ex.Message, true, false);
        //    }
        //}


    }
}
