﻿using System;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class RolData
    {
        public RolData() { }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public bool Activo { get; set; }

        [DataMember]
        public string UserCreacion { get; set; }

        [DataMember]
        public string UserModificacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public DateTime? FechaModificacion { get; set; }

        [DataMember]
        public string AppCreacion { get; set; }

        [DataMember]
        public string AppModificacion { get; set; }

        [DataMember]
        public string MaqCreacion { get; set; }

        [DataMember]
        public string MaqModificacion { get; set; }
    }
}
