﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class BillDetailData
    {
        [DataMember]
        public string BillDetailId { get; set; }
        [DataMember]
        public string BillId { get; set; }
        [DataMember]
        public string ProductId { get; set; }
        [DataMember]
        public string LotId { get; set; }
        [DataMember]
        public float Amount { get; set; }
        [DataMember]
        public decimal Price { get; set; }

    }
}
