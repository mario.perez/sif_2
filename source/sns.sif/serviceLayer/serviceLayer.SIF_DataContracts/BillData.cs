﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class BillData
    {
        [DataMember]
        public string BillId { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public int WarehouseId { get; set; }
        [DataMember]
        public decimal TotalAmount { get; set; }
        [DataMember]
        public decimal Discount { get; set; }
        [DataMember]
        public decimal Tax { get; set; }
        [DataMember]
        public decimal Subtotal { get; set; }
        [DataMember]
        public decimal Cash { get; set; }
        [DataMember]
        public decimal CreditCard { get; set; }
        [DataMember]
        public string SalesmanId { get; set; }
        [DataMember]
        public string CashboxId { get; set; }
        [DataMember]
        public string TypePayment { get; set; }
        [DataMember]
        public bool IsCancelled { get; set; }
        [DataMember]
        public bool IsRegistered { get; set; }
        [DataMember]
        public decimal DiscountRate { get; set; }
        [DataMember]
        public string DiscountUser { get; set; }
        [DataMember]
        public string DiscountAproveUser { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string CancelUser { get; set; }
        [DataMember]
        public string CancelAproveUser { get; set; }
        [DataMember]
        public DateTime CancelDate { get; set; }
        [DataMember]
        public List<BillDetailData> Details { get; set; }

    }
}
