﻿using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class CashBoxData
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public Catalogue Warehouse { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
    }
}
