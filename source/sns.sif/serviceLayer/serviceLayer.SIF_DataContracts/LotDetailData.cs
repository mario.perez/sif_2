﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;



namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class LotDetailData
    {
        [DataMember]
        public string LotId { get; set; }
        [DataMember]
        public string ProductId { get; set; }
        [DataMember]
        public double Quantity { get; set; }
        [DataMember]
        public decimal Cost { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool IsUpdated { get; set; }
    }
}
