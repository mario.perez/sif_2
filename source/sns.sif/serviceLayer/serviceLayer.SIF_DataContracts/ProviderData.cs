﻿using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class ProviderData
    {
        [DataMember]
        public string ProviderId { get; set; }
        [DataMember]
        public string ProviderName { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string ProviderCodeId { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Contact { get; set; }

    }
}
