﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class OutputData
    {
        [DataMember]
        public string OutputId { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public int WarehouseId { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public bool IsCancelled { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public bool IsRegistered { get; set; }
        [DataMember]
        public string CancelUser { get; set; }
        [DataMember]
        public string CancelAproveUser { get; set; }
        [DataMember]
        public DateTime CancelDate { get; set; }

        [DataMember]
        public List<OutputDetailData> Details { get; set; }
    }
}
