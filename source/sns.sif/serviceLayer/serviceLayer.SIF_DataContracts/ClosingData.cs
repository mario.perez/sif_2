﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;


namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class ClosingData
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public int CashBoxId { get; set; }

        [DataMember]
        public DateTime InitialDateTime { get; set; }

        [DataMember]
        public DateTime FinalDatetime { get; set; }

        [DataMember]
        public string UserName { get; set; }

        //[DataMember]
        //public List<ClosingDetail> Detail { get; set; }
    }
}
