﻿namespace serviceLayer.SIF_DataContracts
{
    public class CurrencyData
    {
        public string Id { get; set; }
        public string CurrencyNumber { get; set; }
        public int Decimals { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        public bool IsActive { get; set; }
    }
}
