﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class OutputDetailData
    {
        [DataMember]
        public string OutputDetailId { get; set; }
        [DataMember]
        public string OutputId { get; set; }
        [DataMember]
        public string ProductId { get; set; }
        [DataMember]
        public string LotId { get; set; }
        [DataMember]
        public float Amount { get; set; }
        [DataMember]
        public decimal Price { get; set; }

    }
}
