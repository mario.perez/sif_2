﻿using System;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class PrivilegioData
    {
        public PrivilegioData() { }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string CodigoTag { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public bool EsPadre { get; set; }

        [DataMember]
        public int PadreId { get; set; }

        [DataMember]
        public string CodigoInterno { get; set; }
    }
}
