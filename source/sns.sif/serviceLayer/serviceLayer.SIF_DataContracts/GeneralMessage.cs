﻿using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class GeneralMessage<T>
    {
        [DataMember]
        public long CodeResponse { get; set; }
        [DataMember]
        public string MessageResponse { get; set; }
        [DataMember]
        public bool IsError { get; set; }
        [DataMember]
        public T Data { get; set; }

        public GeneralMessage(long Code, string MessageResponse, bool IsError, T Data)
        {
            CodeResponse = Code;
            this.MessageResponse = MessageResponse;
            this.IsError = IsError;
            this.Data = Data;

        }
        public GeneralMessage()
        {

        }
    
    }
}
