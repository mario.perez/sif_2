﻿using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class Catalogue 
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Value { get; set;}
        [DataMember]
        public bool IsActive { get; set; }
    }
}
