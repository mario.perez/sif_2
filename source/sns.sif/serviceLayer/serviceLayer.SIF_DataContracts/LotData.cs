﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class LotData
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public DateTime AdmissionDate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsUpdated { get; set; }


        [DataMember]
        public List<LotDetailData> Details {get;set;}


    }
}
