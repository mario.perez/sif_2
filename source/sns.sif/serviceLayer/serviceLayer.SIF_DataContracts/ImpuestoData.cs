﻿using System;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class ImpuestoData
    {
        [DataMember]
        public int ImpuestoId { get; set; }
        [DataMember]
        public string Codigo { get; set; }
        [DataMember]
        public string DescripcionImpuesto { get; set; }
        [DataMember]
        public decimal ValorImpuesto { get; set; }
        [DataMember]
        public bool Predeterminado { get; set; }
        [DataMember]
        public bool Activo { get; set; }

        [DataMember]
        public string UserCreacion { get; set; }

        [DataMember]
        public string UserModificacion { get; set; }

        [DataMember]
        public DateTime? FechaCreacion { get; set; }

        [DataMember]
        public DateTime? FechaModificacion { get; set; }

        [DataMember]
        public string AppCreacion { get; set; }

        [DataMember]
        public string AppModificacion { get; set; }

        [DataMember]
        public string MaqCreacion { get; set; }        

        [DataMember]
        public string MaqModificacion { get; set; }

        [DataMember]
        public string Nombre { get; set; }
    }
}
