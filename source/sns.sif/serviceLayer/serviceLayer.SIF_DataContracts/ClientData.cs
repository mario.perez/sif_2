using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
	[DataContract]
    public class ClientData
    {
        [DataMember]
		public string ClientId { get; set; }
        [DataMember]
        public string ClientName { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Contact { get; set; }
    }
}

