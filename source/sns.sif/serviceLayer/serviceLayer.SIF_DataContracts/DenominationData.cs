﻿using System.Runtime.Serialization;


namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class DenominationData
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public CurrencyData CurrencyIso { get; set; }
        [DataMember]
        public decimal Factor { get; set; }
        [DataMember]
        public bool IsCash { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
    }
}
