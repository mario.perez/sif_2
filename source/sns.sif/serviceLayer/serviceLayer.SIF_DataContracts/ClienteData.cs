﻿using System;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class ClienteData
    {
        [DataMember]
        public int cliente_id { get; set; }

        [DataMember]
        public byte[] Foto { get; set; }

        [DataMember]
        public string Cedula { get; set; }

        [DataMember]
        public string codigo_clte { get; set; }

        [DataMember]
        public string primer_apellido { get; set; }

        [DataMember]
        public string segundo_apellido { get; set; }

        [DataMember]
        public string primer_nombre { get; set; }

        [DataMember]
        public string segundo_nombre { get; set; }

        [DataMember]
        public string nombre_factura { get; set; }

        [DataMember]
        public string direccion1 { get; set; }

        [DataMember]
        public string direccion2 { get; set; }

        [DataMember]
        public string direccion3 { get; set; }

        [DataMember]
        public string ciudad { get; set; }

        [DataMember]
        public string departamento { get; set; }

        [DataMember]
        public string pais { get; set; }

        [DataMember]
        public string telefono1 { get; set; }

        [DataMember]
        public string telefono2 { get; set; }

        [DataMember]
        public string fax { get; set; }

        [DataMember]
        public string Contacto { get; set; }

        [DataMember]
        public string e_mail1 { get; set; }

        [DataMember]
        public string e_mail2 { get; set; }

        [DataMember]
        public bool activo { get; set; }

        [DataMember]
        public string ruc { get; set; }

        [DataMember]
        public decimal maximo_Credito { get; set; }

        [DataMember]
        public string comentario { get; set; }

        [DataMember]
        public string UserCreacion { get; set; }

        [DataMember]
        public string UserModificacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public DateTime? FechaModificacion { get; set; }

        [DataMember]
        public string AppCreacion { get; set; }

        [DataMember]
        public string AppModificacion { get; set; }

        [DataMember]
        public string MaqCreacion { get; set; }

        [DataMember]
        public string MaqModificacion { get; set; }
    }
}