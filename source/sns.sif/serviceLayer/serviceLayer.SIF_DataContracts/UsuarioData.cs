﻿using System;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class UsuarioData
    {
        public UsuarioData() { }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UsuarioLogin { get; set; }
        [DataMember]
        public string Contraseña { get; set; }
        [DataMember]
        public string NombreCompleto { get; set; }
        [DataMember]
        public string Correo { get; set; }
        [DataMember]
        public bool Activo { get; set; }
        [DataMember]
        public bool Bloqueado { get; set; }

        [DataMember]
        public string UserCreacion { get; set; }

        [DataMember]
        public string UserModificacion { get; set; }

        [DataMember]
        public DateTime FechaCreacion { get; set; }

        [DataMember]
        public DateTime? FechaModificacion { get; set; }

        [DataMember]
        public string AppCreacion { get; set; }

        [DataMember]
        public string AppModificacion { get; set; }

        [DataMember]
        public string MaqCreacion { get; set; }

        [DataMember]
        public string MaqModificacion { get; set; }
    }
}
