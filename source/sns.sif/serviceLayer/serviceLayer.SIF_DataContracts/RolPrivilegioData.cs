﻿using System;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class RolPrivilegioData
    {

        public RolPrivilegioData() { }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int RolId { get; set; }
        [DataMember]
        public int PrivilegioId { get; set; }
    }
}
