using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class MyCatalogsData
    {
        [DataMember]
		public string TableName { get; set; }
        [DataMember]
        public string ItemId { get; set; }
        [DataMember]
        public string ItemDescription { get; set; }
    }
}

