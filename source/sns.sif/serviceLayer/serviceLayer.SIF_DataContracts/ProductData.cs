﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;



namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class ProductData
    {
        [DataMember]
        public string ProductId { get; set; }
        [DataMember]
        public Catalogue Category { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Catalogue Measurement { get; set; }
        //[DataMember]
        //public Catalogue Provider { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool HasStock { get; set; }
        [DataMember]
        public int UnitsPerPackage { get; set; }
    }
}
