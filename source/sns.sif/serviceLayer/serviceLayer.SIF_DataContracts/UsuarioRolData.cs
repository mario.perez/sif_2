﻿using System;
using System.Runtime.Serialization;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class UsuarioRolData
    {
        public UsuarioRolData() { }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int RolId { get; set; }
        [DataMember]
        public int UsuarioId { get; set; }

        [DataMember]
        public string NombreRol { get; set; }
        [DataMember]
        public string DescripcionRol { get; set; }
        [DataMember]
        public string LoginUsuario { get; set; }
        [DataMember]
        public string NombreUsuario { get; set; }
    }
}
