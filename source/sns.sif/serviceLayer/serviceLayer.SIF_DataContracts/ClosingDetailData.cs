﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace serviceLayer.SIF_DataContracts
{
    [DataContract]
    public class ClosingDetailData
    {
        [DataMember]
        public long CodigoCierre { get; set; }

        [DataMember]
        public int CodigoDenominacion { get; set;}

        [DataMember]
        public long Cantidad { get; set; }

        [DataMember]
        public decimal MontoEquivalente { get; set; }
    }
}
