﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using serviceLayer.SIF_DataContracts;
using businessLogic.InventoryController;

namespace serviceLayer.SIF_Inventory_Service
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : ILot
    {
        public GeneralMessage<LotData> AddLot(LotData data)
        {
            try
            {
                LotController caller = new LotController();
                var result = caller.Add(data);
                return new GeneralMessage<LotData>(0, "", false, result);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<LotData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<LotData>(2, ex.ToString(), true, null);
            }
        }
        
        public GeneralMessage<List<LotData>> GetLots(LotData data)
        {
            try
            {
                LotController caller = new LotController();
                var resp = caller.GetLots(data);
                return new GeneralMessage<List<LotData>>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<LotData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<LotData>>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<LotData> GetOneLots(LotData data)
        {
            try
            {
                LotController caller = new LotController();
                var resp = caller.GetOne(data);
                return new GeneralMessage<LotData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<LotData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<LotData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<bool> ModifyLot(LotData data)
        {
            try
            {
                LotController caller = new LotController();
                caller.Modify(data);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveLot(LotData data)
        {
            try
            {
                LotController caller = new LotController();
                caller.Remove(data);
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }
    }
}
