﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using serviceLayer.SIF_DataContracts;

namespace serviceLayer.SIF_Inventory_Service
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ILot
    {

        [OperationContract]
        GeneralMessage<LotData> AddLot(LotData data);


        [OperationContract]
        GeneralMessage<bool> ModifyLot(LotData data);

        [OperationContract]
        GeneralMessage<bool> RemoveLot(LotData data);

        [OperationContract]
        GeneralMessage<List<LotData>> GetLots(LotData data);

        [OperationContract]
        GeneralMessage<LotData> GetOneLots(LotData data);


        // TODO: agregue aquí sus operaciones de servicio
    }


}
