﻿using System;
using System.Collections.Generic;
using serviceLayer.SIF_DataContracts;
using businessLogic.SecurityController;

namespace serviceLayer.SIF_Security
{
    public class SIFSecurity : ISecurity
    {        

        public GeneralMessage<bool> AddRol(RolData rol)
        {
            try
            {
                RolController caller = new RolController();
                rol.FechaModificacion = rol.FechaCreacion;
                caller.GestionarRol_CRUD(rol, "I");
                return new GeneralMessage<bool>(0, "Rol Agregado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> UpdateRol(RolData rol)
        {
            try
            {
                RolController caller = new RolController();
                rol.FechaModificacion = rol.FechaCreacion;
                caller.GestionarRol_CRUD(rol, "U");
                return new GeneralMessage<bool>(0, "Rol Modificado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> RemoveRol(RolData rol)
        {
            try
            {
                RolController caller = new RolController();
                rol.FechaModificacion = rol.FechaCreacion;
                caller.GestionarRol_CRUD(rol, "D");
                return new GeneralMessage<bool>(0, "Rol Eliminado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<List<RolData>> GetAllRol(RolData rol)
        {
            try
            {
                RolController caller = new RolController();
                var lista = caller.GetAllRolesOrRol(rol,"S");
                return new GeneralMessage<List<RolData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<RolData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<RolData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<bool> AddPrivilegioRol(int privilegioId, int rolId)
        {
            try
            {
                RolPrivilegioController caller = new RolPrivilegioController();                
                caller.GestionarRolPrivilegio(privilegioId, rolId, 0,0,"I");
                return new GeneralMessage<bool>(0, "Privilegio Asignado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> RemovePrivilegioRol(int privilegioId, int rolId)
        {
            try
            {
                RolPrivilegioController caller = new RolPrivilegioController();
                caller.GestionarRolPrivilegio(privilegioId, rolId,0,0, "D");
                return new GeneralMessage<bool>(0, "Privilegio restringido", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<List<PrivilegioData>> GetPrivilegioAsignado(int privilegioId, int rolId)
        {
            try
            {
                RolPrivilegioController caller = new RolPrivilegioController();
                var lista = caller.GetRolPrivilegio(privilegioId, rolId,0,"C" );
                return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<List<PrivilegioData>> GetPrivilegioNoAsignado(int privilegioId)
        {
            try
            {
                RolPrivilegioController caller = new RolPrivilegioController();
                var lista = caller.GetRolPrivilegio(privilegioId,0,0, "N");
                return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<PrivilegioData> GetRolPrivilegioValidacion(int privilegioId, int rolId)
        {
            try
            {
                RolPrivilegioController caller = new RolPrivilegioController();
                var lista = caller.GetRolPrivilegio(privilegioId, rolId,0, "V");
                return new GeneralMessage<PrivilegioData>(0, "", false, lista[0]);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<PrivilegioData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<PrivilegioData>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<List<PrivilegioData>> GetPrivilegioHijos(int padreId)
        {
            try
            {
                PrivilegioData privilegio = new PrivilegioData();
                privilegio.PadreId = padreId;

                PrivilegioController caller = new PrivilegioController();
                var lista = caller.GetAllPrivilegio(privilegio, "S");
                return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<bool> CopyPrivilegios_FromRol(int RolIdCopy, int RolIdPaste)
        {
            try
            {
                RolPrivilegioController caller = new RolPrivilegioController();
                caller.GestionarRolPrivilegio(0, RolIdCopy, 0, RolIdPaste, "A");
                return new GeneralMessage<bool>(0, "Privilegios Copiados", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }


        public GeneralMessage<bool> AddUsuario(UsuarioData usuario)
        {
            try
            {
                UsuarioController caller = new UsuarioController();
                usuario.FechaModificacion = usuario.FechaCreacion;
                caller.GestionarUsuario_CRUD(usuario, "I");
                return new GeneralMessage<bool>(0, "Usuario Agregado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> UpdateUsuario(UsuarioData usuario)
        {
            try
            {
                UsuarioController caller = new UsuarioController();
                usuario.FechaModificacion = usuario.FechaCreacion;
                caller.GestionarUsuario_CRUD(usuario, "U");
                return new GeneralMessage<bool>(0, "Usuario Modificado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> RemoveUsuario(UsuarioData usuario)
        {
            try
            {
                UsuarioController caller = new UsuarioController();
                usuario.FechaModificacion = usuario.FechaCreacion;
                caller.GestionarUsuario_CRUD(usuario, "D");
                
                return new GeneralMessage<bool>(0, "Usuario Eliminado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<List<UsuarioData>> GetAllUsuario(UsuarioData usuario)
        {
            try
            {
                UsuarioController caller = new UsuarioController();
                var lista = caller.GetAllUsuario(usuario, "S");
                return new GeneralMessage<List<UsuarioData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<UsuarioData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<UsuarioData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<bool> AddUsuarioRol(int usuarioId, int rolId, int id, string operacion)
        {
            try
            {
                UsuarioRolController caller = new UsuarioRolController();
                caller.GestionarUsuarioRol(usuarioId, rolId, id, "I");
                return new GeneralMessage<bool>(0, "Añadido", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> RemoveUsuarioRol(int usuarioId, int rolId, int id, string operacion)
        {
            try
            {
                UsuarioRolController caller = new UsuarioRolController();
                caller.GestionarUsuarioRol(usuarioId, rolId, id, "D");
                return new GeneralMessage<bool>(0, "Eliminado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<List<UsuarioRolData>> GetAllUsuarioRol(int usuarioId, int rolId, int id, string operacion)
        {
            try
            {
                UsuarioRolController caller = new UsuarioRolController();
                var lista = caller.GetAllUsuarioRol(usuarioId, rolId, id, "S",string.Empty);
                return new GeneralMessage<List<UsuarioRolData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<UsuarioRolData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<UsuarioRolData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<List<UsuarioRolData>> GetAllUsuarioRol_NoRol(int usuarioId)
        {
            try
            {
                UsuarioRolController caller = new UsuarioRolController();
                var lista = caller.GetAllUsuarioRol(usuarioId, 0,0, "N",string.Empty);
                return new GeneralMessage<List<UsuarioRolData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<UsuarioRolData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<UsuarioRolData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<List<UsuarioRolData>> GetAllUsuarioRol_NoUsuario(int rolId)
        {
            try
            {
                UsuarioRolController caller = new UsuarioRolController();
                var lista = caller.GetAllUsuarioRol(0, rolId, 0, "M",string.Empty);
                return new GeneralMessage<List<UsuarioRolData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<UsuarioRolData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<UsuarioRolData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<UsuarioData> AutenticarUsuario(string usuarioLogin,string contraseña)
        {
            try
            {
                UsuarioController caller = new UsuarioController();
                UsuarioData usuario = new UsuarioData();
                usuario.FechaCreacion = DateTime.Now;
                usuario.FechaModificacion = DateTime.Now;
                usuario.UsuarioLogin = usuarioLogin;
                usuario.Contraseña = contraseña;

                var user = caller.AutenticarUsuario(usuario, "V");
                return new GeneralMessage<UsuarioData>(0, "", false, user);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<UsuarioData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<UsuarioData>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<List<PrivilegioData>> GetPrivilegioUsuario(int idUsuario)
        {
            try
            {
                UsuarioRolController caller = new UsuarioRolController();
                var lista = caller.GetPrivilegioUser(idUsuario,"P",string.Empty);
                return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<List<PrivilegioData>> GetPrivilegioAccion(int idUsuario, string codigoTag)
        {
            try
            {
                UsuarioRolController caller = new UsuarioRolController();
                var lista = caller.GetPrivilegioUser(idUsuario, "A",codigoTag);
                return new GeneralMessage<List<PrivilegioData>>(0, "", false, lista);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<List<PrivilegioData>>(2, ex.Message, true, null);
            }
        }

        public GeneralMessage<bool> AddPrivilegio(PrivilegioData privilegio)
        {
            try
            {
                PrivilegioController caller = new PrivilegioController();
                caller.GestionarPrivilegio(privilegio, "I");
                return new GeneralMessage<bool>(0, "Añadido", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> RemovePrivilegio(PrivilegioData privilegio)
        {
            try
            {
                PrivilegioController caller = new PrivilegioController();
                caller.GestionarPrivilegio(privilegio, "D");
                return new GeneralMessage<bool>(0, "Eliminado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }

        public GeneralMessage<bool> UpdatePrivilegio(PrivilegioData privilegio)
        {
            try
            {
                PrivilegioController caller = new PrivilegioController();
                caller.GestionarPrivilegio(privilegio, "U");
                return new GeneralMessage<bool>(0, "Modificado", false, true);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.Message, true, false);
            }
        }
    }
}
