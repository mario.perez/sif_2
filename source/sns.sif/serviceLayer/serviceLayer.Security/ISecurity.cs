﻿using System.Collections.Generic;
using System.ServiceModel;
using serviceLayer.SIF_DataContracts;

namespace serviceLayer.SIF_Security
{
    [ServiceContract]
    public interface ISecurity
    {
        // Rol - CREATED BY CARLOS BEJARANO
        [OperationContract]
        GeneralMessage<bool> AddRol(RolData rol);

        [OperationContract]
        GeneralMessage<bool> UpdateRol(RolData rol);

        [OperationContract]
        GeneralMessage<bool> RemoveRol(RolData rol);

        [OperationContract]
        GeneralMessage<List<RolData>> GetAllRol(RolData rol);

        [OperationContract]
        GeneralMessage<bool> AddPrivilegioRol(int privilegioId, int rolId);

        [OperationContract]
        GeneralMessage<bool> RemovePrivilegioRol(int privilegioId, int rolId);

        [OperationContract]
        GeneralMessage<List<PrivilegioData>> GetPrivilegioAsignado(int privilegioId, int rolId);

        [OperationContract]
        GeneralMessage<List<PrivilegioData>> GetPrivilegioNoAsignado(int privilegioId);

        [OperationContract]
        GeneralMessage<PrivilegioData> GetRolPrivilegioValidacion(int privilegioId, int rolId);

        [OperationContract]
        GeneralMessage<bool> AddUsuario(UsuarioData usuario);

        [OperationContract]
        GeneralMessage<bool> UpdateUsuario(UsuarioData usuario);

        [OperationContract]
        GeneralMessage<bool> RemoveUsuario(UsuarioData usuario);

        [OperationContract]
        GeneralMessage<List<UsuarioData>> GetAllUsuario(UsuarioData usuario);

        [OperationContract]
        GeneralMessage<bool> AddUsuarioRol(int usuarioId, int rolId, int id, string operacion);

        [OperationContract]
        GeneralMessage<bool> RemoveUsuarioRol(int usuarioId, int rolId, int id, string operacion);

        [OperationContract]
        GeneralMessage<List<UsuarioRolData>> GetAllUsuarioRol(int usuarioId, int rolId, int id, string operacion);

        [OperationContract]
        GeneralMessage<List<UsuarioRolData>> GetAllUsuarioRol_NoRol(int usuarioId);

        [OperationContract]
        GeneralMessage<List<UsuarioRolData>> GetAllUsuarioRol_NoUsuario(int rolId);

        [OperationContract]
        GeneralMessage<UsuarioData> AutenticarUsuario(string usuarioLogin, string contraseña);

        [OperationContract]
        GeneralMessage<List<PrivilegioData>> GetPrivilegioUsuario(int rolId);

        [OperationContract]
        GeneralMessage<List<PrivilegioData>> GetPrivilegioAccion(int idUsuario, string codigoTag);

        [OperationContract]
        GeneralMessage<List<PrivilegioData>> GetPrivilegioHijos(int padreId);

        [OperationContract]
        GeneralMessage<bool> AddPrivilegio(PrivilegioData privilegio);

        [OperationContract]
        GeneralMessage<bool> RemovePrivilegio(PrivilegioData privilegio);

        [OperationContract]
        GeneralMessage<bool> UpdatePrivilegio(PrivilegioData privilegio);

        [OperationContract]
        GeneralMessage<bool> CopyPrivilegios_FromRol(int RolIdCopy, int RolIdPaste);

    }
}
