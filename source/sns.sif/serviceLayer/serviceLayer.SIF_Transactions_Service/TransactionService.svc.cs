﻿using businessLogic.Models;
using businessLogic.TransactionController;
using serviceLayer.SIF_DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace serviceLayer.SIF_Transactions_Service
{
    public class TransactionService : ITransactions
    {
        public GeneralMessage<bool> AddClosing(ClosingData closing_)
        {
            try
            {
                ClosingController caller = new ClosingController();
                caller.Add(new Closing() {
                    CashboxId = closing_.CashBoxId
                    , InitialDateTime = closing_.InitialDateTime
                    , FinalDateTime = closing_.FinalDatetime
                    , UserName = closing_.UserName
                    //, Details = closing_.Detail
                });
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> UpdateClosing(ClosingData closing_)
        {
            try
            {
                ClosingController caller = new ClosingController();
                caller.Update(new Closing()
                {
                    Id = closing_.ID
                    ,
                    CashboxId = closing_.CashBoxId
                    ,
                    InitialDateTime = closing_.InitialDateTime
                    ,
                    FinalDateTime = closing_.FinalDatetime
                    ,
                    UserName = closing_.UserName
                    //, Details = closing_.Detail
                });
                return new GeneralMessage<bool>(0, "", false, true);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<bool> RemoveClosing(int IdClosing) {
            try
            {
                ClosingController caller = new ClosingController();
                caller.RemoveById(IdClosing);
                return new GeneralMessage<bool>(0, "", false, true);

            }
            catch (Exception ex)
            {

                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        
        public GeneralMessage<ClosingData[]> GetAllClosing()
        {
            try
            {
                ClosingController caller = new ClosingController();
                var resp = caller.GetClosings();
                return new GeneralMessage<ClosingData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ClosingData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ClosingData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<ClosingData> GetClosing(int idClosing)
        {
            try
            {
                ClosingController caller = new ClosingController();
                var resp = caller.GetOneClosing(idClosing);
                return new GeneralMessage<ClosingData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<ClosingData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<ClosingData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<OutputData> AddOutput(OutputData output)
        {
            try
            {
                OutputController caller = new OutputController();
                var result = caller.Add(output);
                return new GeneralMessage<OutputData>(0, "", false, result);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<OutputData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<OutputData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<bool> CancelOutput(OutputData output)
        {
            try
            {
                OutputController caller = new OutputController();
                return new GeneralMessage<bool>(0, "", false, caller.Cancel(output));
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<OutputData[]> GetAllOutputs(OutputData output)
        {
            try
            {
                OutputController caller = new OutputController();
                var resp = caller.GetAll(output);
                return new GeneralMessage<OutputData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<OutputData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<OutputData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<OutputData> GetOutput(OutputData output)
        {
            try
            {
                OutputController caller = new OutputController();
                var resp = caller.GetOne(output);
                return new GeneralMessage<OutputData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<OutputData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<OutputData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<PurchaseData> AddPurchase(PurchaseData output)
        {
            try
            {
                PurchaseController caller = new PurchaseController();
                var result = caller.Add(output);
                return new GeneralMessage<PurchaseData>(0, "", false, result);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<PurchaseData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<PurchaseData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<bool> CancelPurchase(PurchaseData purchase)
        {
            try
            {
                PurchaseController caller = new PurchaseController();
                return new GeneralMessage<bool>(0, "", false, caller.Cancel(purchase));
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<PurchaseData[]> GetAllPurchases(PurchaseData output)
        {
            try
            {
                PurchaseController caller = new PurchaseController();
                var resp = caller.GetAll(output);
                return new GeneralMessage<PurchaseData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<PurchaseData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<PurchaseData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<PurchaseData> GetPurchase(PurchaseData output)
        {
            try
            {
                PurchaseController caller = new PurchaseController();
                var resp = caller.GetOne(output);
                return new GeneralMessage<PurchaseData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<PurchaseData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<PurchaseData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<BillData> AddBill(BillData output)
        {
            try
            {
                BillController caller = new BillController();
                var result = caller.Add(output);
                return new GeneralMessage<BillData>(0, "", false, result);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<BillData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<BillData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<bool> CancelBill(BillData output)
        {
            try
            {
                BillController caller = new BillController();
                return new GeneralMessage<bool>(0, "", false, caller.Cancel(output));
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<BillData[]> GetAllBills(BillData output)
        {
            try
            {
                BillController caller = new BillController();
                var resp = caller.GetAll(output);
                return new GeneralMessage<BillData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<BillData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<BillData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<BillData> GetBill(BillData output)
        {
            try
            {
                BillController caller = new BillController();
                var resp = caller.GetOne(output);
                return new GeneralMessage<BillData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<BillData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<BillData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<InputData> AddInput(InputData Input)
        {
            try
            {
                InputController caller = new InputController();
                var result = caller.Add(Input);
                return new GeneralMessage<InputData>(0, "", false, result);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<InputData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<InputData>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<bool> CancelInput(InputData Input)
        {
            try
            {
                InputController caller = new InputController();
                return new GeneralMessage<bool>(0, "", false, caller.Cancel(Input));
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<bool>(1, ex.Message, true, false);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<bool>(2, ex.ToString(), true, false);
            }
        }

        public GeneralMessage<InputData[]> GetAllInputs(InputData Input)
        {
            try
            {
                InputController caller = new InputController();
                var resp = caller.GetAll(Input);
                return new GeneralMessage<InputData[]>(0, "", false, resp.ToArray());
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<InputData[]>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<InputData[]>(2, ex.ToString(), true, null);
            }
        }

        public GeneralMessage<InputData> GetInput(InputData Input)
        {
            try
            {
                InputController caller = new InputController();
                var resp = caller.GetOne(Input);
                return new GeneralMessage<InputData>(0, "", false, resp);
            }
            catch (ArgumentException ex)
            {
                return new GeneralMessage<InputData>(1, ex.Message, true, null);
            }
            catch (Exception ex)
            {
                return new GeneralMessage<InputData>(2, ex.ToString(), true, null);
            }
        }
    }
}
