﻿using serviceLayer.SIF_DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace serviceLayer.SIF_Transactions_Service
{
    [ServiceContract]
    public interface ITransactions
    {
        //========================================================
        //                      Output
        //========================================================
        [OperationContract]
        GeneralMessage<OutputData> AddOutput(OutputData output);

        [OperationContract]
        GeneralMessage<bool> CancelOutput(OutputData output);

        [OperationContract]
        GeneralMessage<OutputData[]> GetAllOutputs(OutputData output);

        [OperationContract]
        GeneralMessage<OutputData> GetOutput(OutputData output);
        //========================================================
        //                      Purchase
        //========================================================
        [OperationContract]
        GeneralMessage<PurchaseData> AddPurchase(PurchaseData output);

        [OperationContract]
        GeneralMessage<bool> CancelPurchase(PurchaseData output);

        [OperationContract]
        GeneralMessage<PurchaseData[]> GetAllPurchases(PurchaseData output);

        [OperationContract]
        GeneralMessage<PurchaseData> GetPurchase(PurchaseData output);
        //========================================================
        //                      Bill
        //========================================================
        [OperationContract]
        GeneralMessage<BillData> AddBill(BillData output);

        [OperationContract]
        GeneralMessage<bool> CancelBill(BillData output);

        [OperationContract]
        GeneralMessage<BillData[]> GetAllBills(BillData output);

        [OperationContract]
        GeneralMessage<BillData> GetBill(BillData output);

        //========================================================
        //                      Closing
        //========================================================
        [OperationContract]
        GeneralMessage<bool> AddClosing(ClosingData closing);

        [OperationContract]
        GeneralMessage<bool> UpdateClosing(ClosingData closing);

        [OperationContract]
        GeneralMessage<bool> RemoveClosing(int Idclosing);

        [OperationContract]
        GeneralMessage<ClosingData[]> GetAllClosing();

        [OperationContract]
        GeneralMessage<ClosingData> GetClosing(int idClosing);

        //========================================================
        //                      Input
        //========================================================
        [OperationContract]
        GeneralMessage<InputData> AddInput(InputData Input);

        [OperationContract]
        GeneralMessage<bool> CancelInput(InputData Input);

        [OperationContract]
        GeneralMessage<InputData[]> GetAllInputs(InputData Input);

        [OperationContract]
        GeneralMessage<InputData> GetInput(InputData Input);

    }

}
