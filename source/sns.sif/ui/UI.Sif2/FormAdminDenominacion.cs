﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;

namespace UI.Sif2
{
    public partial class FormAdminDenominacion : FormAdminGenerico
    {
        DenominationData Denominacion;
        TextBox txtCodigo ;
        TextBox txtDescripcion ;
        ComboBox cmbMoneda;
        TextBox txtFactor ;
        CheckBox chkActivo ;
        CheckBox chkEfectivo ;

        public FormAdminDenominacion(string op, DataGridViewColumnCollection col, string titulo,DenominationData deno)
        {
            InitializeComponent();
            operacion = op;
            columnas = col;
            Text = titulo;
            Denominacion = deno;            
        }

        public override void VincularControles()
        {
            txtCodigo = (TextBox)this.Controls.Find("Codigo_Filtro_NotNull", true)[0];
            txtDescripcion = (TextBox)this.Controls.Find("Descripcion_NotNull", true)[0];
            cmbMoneda = (ComboBox)this.Controls.Find("Moneda_NotNull", true)[0];
            txtFactor = (TextBox)this.Controls.Find("Factor_NotNull", true)[0];
            chkActivo = (CheckBox)this.Controls.Find("EsActivo_NotNull", true)[0];
            chkEfectivo = (CheckBox)this.Controls.Find("EsEfectivo_NotNull", true)[0];
        }        

        public override void FillData()
        {
            try
            {
                if (Denominacion != null)
                {               
                    txtCodigo.Text = Denominacion.Id.ToString();
                    txtDescripcion.Text = Denominacion.Description;
                    cmbMoneda.SelectedValue = Denominacion.CurrencyIso.Id ;
                    txtFactor.Text = Denominacion.Factor.ToString();
                    chkActivo.Checked = Denominacion.IsActive;
                    chkEfectivo.Checked = Denominacion.IsCash;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
            
        }

        public override void Guardar()
        {
            try
            {
                ActualizarDenominacion();
                GeneralMessageOfboolean respuesta = new GeneralMessageOfboolean();
                switch (operacion)
                {
                    case "I":
                        respuesta = new CatalogsClient().AddDenomination(Denominacion);
                        break;
                    case "U":
                        respuesta = new CatalogsClient().UpdateDenomination(Denominacion);
                        break;
                }
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void ActualizarDenominacion() {
            try
            {
                Denominacion.Id = Int32.Parse(txtCodigo.Text);
                Denominacion.Description = txtDescripcion.Text ;
                Denominacion.CurrencyIso = new CatalogsClient().GetCurrency(cmbMoneda.SelectedValue.ToString()).Data;
                Denominacion.Factor = decimal.Parse(txtFactor.Text);
                Denominacion.IsActive = chkActivo.Checked;
                Denominacion.IsCash = chkEfectivo.Checked;
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void LlenarComboBoxMoneda()
        {
            try
            {
                var lista = new CatalogsClient().GetAllCurrencies().Data.Select(x => new { Codigo = x.Id, Nombre = x.Name }).ToList();

                var combo = (ComboBox)this.Controls.Find("Moneda_NotNull", true)[0];

                combo.DataSource = lista;
                combo.DisplayMember = "Nombre";
                combo.ValueMember = "Codigo";

            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void VerificarTipoOperacion()
        {
            base.VerificarTipoOperacion();

            if (operacion.Equals("U"))
            {
                TextBox txtCodigo = (TextBox)this.Controls.Find("Codigo_Filtro_NotNull", true)[0];
                txtCodigo.ReadOnly = true;
            }
            if (operacion.Equals("I")) {
                Denominacion = new DenominationData();
            }
        }



        private void FormAdminDenominacion_Load(object sender, EventArgs e)
        {
            panelControles.Controls.Add(MetodosGenericos.CreacionLayoutEntidad(columnas, "Admin"));            
            LlenarComboBoxMoneda();
            VincularControles();
            FillData();
            VerificarTipoOperacion();
        }

    }
}
