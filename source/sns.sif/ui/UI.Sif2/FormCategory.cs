﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;
using serviceLayer.serviceClient;

namespace UI.Sif2
{
    public partial class FormCategory : FormGenericoCatalogo
    {
        public FormCategory()
        {
            InitializeComponent();
            MetodosGenericos.MostrarOcultarSplitFiltro(splitContainer1);
            HabilitarPermisos();
            LoadCategory();
        }

        private void LoadCategory()
        {
            try
            {

                List<Catalogue> lista = new List<Catalogue>();
                if (string.IsNullOrEmpty(txtCodigo.Text))
                {
                    lista = new CatalogsClient().GetAllCategories().Data.ToList();

                }
                else
                {
                    var category = new CatalogsClient().GetCategory(txtCodigo.Text).Data;
                    if (category != null) {
                        lista.Add(category);
                    }
                        
                }
                LoadCatalog(lista.ToArray());
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void Agregar()
        {
            new FormAdminCategory(null, "I","Agregar Categoria").ShowDialog();
        }

        public override void Consultar()
        {
            new FormAdminCategory(ObtenerCatalogue(), "C", "Consultar Categoria").ShowDialog();
        }

        public override void Modificar()
        {
            new FormAdminCategory(ObtenerCatalogue(), "U", "Modificar Categoria").ShowDialog();
        }

        public override void Eliminar()
        {
            if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar?") == DialogResult.Yes)
            {
                var eliminar = ObtenerCatalogue();
                var respuesta = new CatalogsClient().RemoveCategory(eliminar);
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }            
        }

        public override void RefrescarDatos()
        {
            LoadCategory();
        }
    }
}
