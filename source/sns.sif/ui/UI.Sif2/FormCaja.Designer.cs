﻿namespace UI.Sif2
{
    partial class FormCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.Codigo_Filtro_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bodega_NotNull = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Activo_NotNull = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Size = new System.Drawing.Size(865, 218);
            this.splitContainer1.SplitterDistance = 285;
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(572, 22);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv);
            this.panel2.Size = new System.Drawing.Size(572, 194);
            // 
            // panelFiltro
            // 
            this.panelFiltro.Size = new System.Drawing.Size(283, 194);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv.ColumnHeadersHeight = 30;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo_Filtro_NotNull,
            this.Nombre_NotNull,
            this.Bodega_NotNull,
            this.Activo_NotNull});
            this.dgv.ContextMenuStrip = this.MenuOperacion;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(572, 194);
            this.dgv.TabIndex = 25;
            // 
            // Codigo_Filtro_NotNull
            // 
            this.Codigo_Filtro_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Codigo_Filtro_NotNull.HeaderText = "Codigo";
            this.Codigo_Filtro_NotNull.Name = "Codigo_Filtro_NotNull";
            this.Codigo_Filtro_NotNull.ReadOnly = true;
            this.Codigo_Filtro_NotNull.Width = 83;
            // 
            // Nombre_NotNull
            // 
            this.Nombre_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nombre_NotNull.HeaderText = "Nombre";
            this.Nombre_NotNull.MaxInputLength = 32;
            this.Nombre_NotNull.Name = "Nombre_NotNull";
            this.Nombre_NotNull.ReadOnly = true;
            // 
            // Bodega_NotNull
            // 
            this.Bodega_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Bodega_NotNull.HeaderText = "Bodega";
            this.Bodega_NotNull.Name = "Bodega_NotNull";
            this.Bodega_NotNull.ReadOnly = true;
            this.Bodega_NotNull.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Bodega_NotNull.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Bodega_NotNull.Width = 88;
            // 
            // Activo_NotNull
            // 
            this.Activo_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Activo_NotNull.HeaderText = "Activo";
            this.Activo_NotNull.Name = "Activo_NotNull";
            this.Activo_NotNull.ReadOnly = true;
            this.Activo_NotNull.Width = 57;
            // 
            // FormCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 243);
            this.Name = "FormCaja";
            this.Tag = "CAJAS_BAS";
            this.TagBtnConsultar = "GETCAJA";
            this.TagBtnEliminar = "REMOVECAJA";
            this.TagBtnModificar = "UPDATECAJA";
            this.TagBtnNuevo = "ADDCAJA";
            this.Text = "Mantenimiento de Caja";
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo_Filtro_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre_NotNull;
        private System.Windows.Forms.DataGridViewComboBoxColumn Bodega_NotNull;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Activo_NotNull;
    }
}