﻿using serviceLayer.serviceClient.CatalogServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Sif2
{
    public partial class FormProveedor : FormGenerico
    {
        private List<ProviderData> lista;

        public FormProveedor()
        {
            InitializeComponent();
            HabilitarPermisos();
        }

        private ProviderData ObtenerSeleccionado()
        {
            try
            {
                if (dgv.CurrentRow != null)
                {
                    var codigo = dgv.CurrentRow.Cells["Codigo_Filtro_NotNull"].Value.ToString();
                    return new CatalogsClient().GetProvider(codigo).Data;
                }
                else
                {
                    return null;
                }                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return null;
            }
        }

        public override void Agregar()
        {
            new FormAdminProveedor("I",dgv.Columns,"Agregar Proveedor",null).ShowDialog();
        }

        public override void Modificar()
        {
            new FormAdminProveedor("U", dgv.Columns, "Modificar Proveedor", ObtenerSeleccionado()).ShowDialog();
        }

        public override void RefrescarDatos()
        {
            CargarGrid();
        }

        public override void Consultar()
        {
            new FormAdminProveedor("C", dgv.Columns, "Consultar Proveedor", ObtenerSeleccionado()).ShowDialog();
        }

        public override void Eliminar()
        {
            try
            {                
                if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar?") == DialogResult.Yes) {
                    var eliminar = ObtenerSeleccionado();
                    var respuesta = new CatalogsClient().RemoveProvider(eliminar);
                    MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }


        public override void CargarGrid()
        {
            try
            {
                dgv.Rows.Clear();

                var txtCodigo = (TextBox)this.Controls.Find(Codigo_Filtro_NotNull.Name.ToString(), true)[0];

                if (string.IsNullOrEmpty(txtCodigo.Text))
                {
                    lista = new CatalogsClient().GetAllProviders().Data.ToList();
                }
                else
                {
                    lista = new List<ProviderData>();
                    var provider = new CatalogsClient().GetProvider(txtCodigo.Text).Data;
                    if (provider != null)
                    {
                        lista.Add(provider);
                    }

                }

                foreach (var item in lista)
                {
                    dgv.Rows.Add(
                        item.ProviderId
                        , item.ProviderCodeId
                        , item.ProviderName
                        , item.IsActive
                        , item.Phone
                        , item.Contact
                        , item.Email
                        ,item.Address
                        );
                }

                lblTotalRegistro.Text = "Total de Registro: " + lista.Count().ToString();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void FormProveedor_Load(object sender, EventArgs e)
        {

        }
    }
}
