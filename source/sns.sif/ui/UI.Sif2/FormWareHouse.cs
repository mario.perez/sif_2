﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;

namespace UI.Sif2
{
    public partial class FormWareHouse : FormGenericoCatalogo
    {
        public FormWareHouse()
        {
            InitializeComponent();
            MetodosGenericos.MostrarOcultarSplitFiltro(splitContainer1);
            HabilitarPermisos();
            LoadWareHouse();
        }

        private void LoadWareHouse()
        {
            try
            {

                List<Catalogue> lista = new List<Catalogue>();
                if (string.IsNullOrEmpty(txtCodigo.Text))
                {
                    lista = new CatalogsClient().GetAllWarehouses().Data.ToList();

                }
                else
                {
                    var warehouse = new CatalogsClient().GetWarehouse(txtCodigo.Text).Data;
                    if (warehouse != null)
                    {
                        lista.Add(warehouse);
                    }
                }
                LoadCatalog(lista.ToArray());
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void Agregar()
        {
            new FormAdminWareHouse(null,"I","Agregar Bodega").ShowDialog();
        }

        public override void Consultar()
        {
            new FormAdminWareHouse(ObtenerCatalogue(),"C","Consultar Bodega").ShowDialog();
        }

        public override void Modificar()
        {
            new FormAdminWareHouse(ObtenerCatalogue(), "U","Modificar Bodega").ShowDialog();
        }

        public override void Eliminar()
        {
            if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar?") == DialogResult.Yes)
            {
                var eliminar = ObtenerCatalogue();
                var respuesta = new CatalogsClient().RemoveWarehouse(eliminar);
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
             
        }

        public override void RefrescarDatos()
        {
            LoadWareHouse();
        }

    }
}
