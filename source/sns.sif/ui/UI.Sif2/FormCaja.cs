﻿using serviceLayer.serviceClient.CatalogServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Sif2
{
    public partial class FormCaja : FormGenerico
    {

        List<CashBoxData> lista = new List<CashBoxData>();

        public FormCaja()
        {
            InitializeComponent();
            HabilitarPermisos();
        }

        public override void RefrescarDatos()
        {
            CargarGrid();
        }

        public override void CargarGrid()
        {
            try
            {

                var txtCodigo = (TextBox)this.Controls.Find(Codigo_Filtro_NotNull.Name.ToString(), true)[0];

                if (string.IsNullOrEmpty(txtCodigo.Text))
                {
                    lista = new CatalogsClient().GetAllCashboxes().Data.ToList();
                }
                else
                {
                    lista = new List<CashBoxData>();
                    var cashbox = new CatalogsClient().GetCashbox(txtCodigo.Text).Data;
                    if (cashbox != null)
                    {
                        lista.Add(cashbox);
                    }

                }

                if (Bodega_NotNull.DataSource == null)
                {
                    Bodega_NotNull.DataSource = new CatalogsClient().GetAllWarehouses().Data.Select(x => new { Codigo = x.Id, Nombre = x.Value }).ToList();
                    Bodega_NotNull.DisplayMember = "Nombre";
                    Bodega_NotNull.ValueMember = "Codigo";
                }

                dgv.Rows.Clear();
                foreach (var item in lista)
                {
                    dgv.Rows.Add(item.Id
                        , item.Name
                        , item.Warehouse.Id
                        , item.IsActive);
                }

                lblTotalRegistro.Text = "Total de Registro: " + lista.Count().ToString();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void Agregar()
        {
            new FormAdminCaja("I",dgv.Columns,"Agregar Caja", null).ShowDialog();
        }

        public override void Modificar()
        {
            var caja = ObtenerSeleccionado();
            if (caja != null) {
                new FormAdminCaja("U",dgv.Columns,"Modificar Caja", caja).ShowDialog();
            }
        }

        public override void Consultar()
        {
            var caja = ObtenerSeleccionado();
            if (caja != null)
            {
                new FormAdminCaja("C", dgv.Columns, "Consultar Caja", caja).ShowDialog();
            }
        }

        public override void Eliminar()
        {
            try
            {
                var caja = ObtenerSeleccionado();
                if (caja != null) {
                    var respuesta = new CatalogsClient().RemoveCashbox(ObtenerSeleccionado());
                    MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private CashBoxData ObtenerSeleccionado() {
            try
            {
                if (dgv.CurrentRow != null)
                {
                    var codigo = dgv.CurrentRow.Cells["Codigo_Filtro_NotNull"].Value.ToString();
                    return new CatalogsClient().GetCashbox(codigo).Data;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return null;
            }
        }
    }
}
