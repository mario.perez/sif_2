﻿using serviceLayer.serviceClient.CatalogServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Sif2
{
    public partial class FormAdminProveedor : FormAdminGenerico
    {
        ProviderData provider;
        TextBox txtCodigo, txtIdentificacion, txtNombre;
        TextBox txtTelefono, txtDireccion, txtCorreo, txtContacto;
        CheckBox chkActivo;

        public FormAdminProveedor(string op, DataGridViewColumnCollection col, string titulo, ProviderData prov)
        {
            InitializeComponent();
            provider = prov;
            operacion = op;
            columnas = col;
            Text = titulo;
        }

        public override void VincularControles()
        {
            try
            {
                txtCodigo = (TextBox)this.Controls.Find("Codigo_Filtro_NotNull", true)[0];
                txtNombre = (TextBox)this.Controls.Find("NombreProveedor_NotNull", true)[0];
                txtIdentificacion = (TextBox)this.Controls.Find("Identificacion_NotNull", true)[0];
                txtContacto = (TextBox)this.Controls.Find("Contacto", true)[0];
                txtCorreo = (TextBox)this.Controls.Find("Correo", true)[0];
                txtDireccion = (TextBox)this.Controls.Find("Direccion", true)[0];
                txtTelefono = (TextBox)this.Controls.Find("Telefono", true)[0];
                chkActivo = (CheckBox)this.Controls.Find("EsActivo_NotNull", true)[0];
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void FillData()
        {
            try
            {
                txtCodigo.Text = provider.ProviderId;
                txtIdentificacion.Text = provider.ProviderCodeId;
                txtNombre.Text = provider.ProviderName;
                txtContacto.Text = provider.Contact;
                txtDireccion.Text = provider.Address;
                txtTelefono.Text = provider.Phone;
                txtCorreo.Text = provider.Email;
                chkActivo.Checked = provider.IsActive;
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void FormAdminProveedor_Load(object sender, EventArgs e)
        {
            panelControles.Controls.Add(MetodosGenericos.CreacionLayoutEntidad(columnas, "Admin"));
            VincularControles();
            VerificarTipoOperacion();            
            FillData();            
        }

        public override void VerificarTipoOperacion()
        {
            base.VerificarTipoOperacion();
            if (operacion.Equals("U")) {
                txtCodigo.ReadOnly = true;
            }
            else
            {
                if (operacion.Equals("I")) {
                    provider = new ProviderData();
                }
            }
        }

        private void ActualizarInformacion() {
            try
            {
                provider.ProviderId = txtCodigo.Text;
                provider.ProviderCodeId = txtIdentificacion.Text;
                provider.ProviderName = txtNombre.Text;
                provider.Address = txtDireccion.Text;
                provider.Email = txtCorreo.Text;
                provider.Phone = txtTelefono.Text;
                provider.Contact = txtContacto.Text;
                provider.IsActive = chkActivo.Checked;
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void Guardar()
        {
            try
            {
                GeneralMessageOfboolean respuesta = new GeneralMessageOfboolean();
                ActualizarInformacion();
                switch (operacion)
                {
                    case "I":
                        respuesta = new CatalogsClient().AddProvider(provider);
                        break;
                    case "U":
                        respuesta = new CatalogsClient().UpdateProvider(provider);
                        break;
                }

                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
    }
}
