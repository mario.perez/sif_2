﻿namespace UI.Sif2
{
    partial class FormMoneda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.Codigo_Filtro_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumeroMoneda_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Decimal_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Simbolo_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EsActivo_NotNull = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Size = new System.Drawing.Size(800, 272);
            this.splitContainer1.SplitterDistance = 214;
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(578, 22);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv);
            this.panel2.Size = new System.Drawing.Size(578, 248);
            // 
            // panelFiltro
            // 
            this.panelFiltro.Size = new System.Drawing.Size(212, 248);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.ColumnHeadersHeight = 30;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo_Filtro_NotNull,
            this.Nombre_NotNull,
            this.NumeroMoneda_NotNull,
            this.Decimal_NotNull,
            this.Simbolo_NotNull,
            this.EsActivo_NotNull});
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(578, 248);
            this.dgv.TabIndex = 23;
            // 
            // Codigo_Filtro_NotNull
            // 
            this.Codigo_Filtro_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Codigo_Filtro_NotNull.HeaderText = "Codigo";
            this.Codigo_Filtro_NotNull.Name = "Codigo_Filtro_NotNull";
            this.Codigo_Filtro_NotNull.ReadOnly = true;
            this.Codigo_Filtro_NotNull.Width = 83;
            // 
            // Nombre_NotNull
            // 
            this.Nombre_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nombre_NotNull.HeaderText = "Nombre";
            this.Nombre_NotNull.Name = "Nombre_NotNull";
            this.Nombre_NotNull.ReadOnly = true;
            // 
            // NumeroMoneda_NotNull
            // 
            this.NumeroMoneda_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NumeroMoneda_NotNull.HeaderText = "Número Moneda";
            this.NumeroMoneda_NotNull.Name = "NumeroMoneda_NotNull";
            this.NumeroMoneda_NotNull.ReadOnly = true;
            this.NumeroMoneda_NotNull.Width = 147;
            // 
            // Decimal_NotNull
            // 
            this.Decimal_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Decimal_NotNull.HeaderText = "Decimal";
            this.Decimal_NotNull.Name = "Decimal_NotNull";
            this.Decimal_NotNull.ReadOnly = true;
            this.Decimal_NotNull.Width = 90;
            // 
            // Simbolo_NotNull
            // 
            this.Simbolo_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Simbolo_NotNull.HeaderText = "Símbolo";
            this.Simbolo_NotNull.Name = "Simbolo_NotNull";
            this.Simbolo_NotNull.ReadOnly = true;
            this.Simbolo_NotNull.Width = 90;
            // 
            // EsActivo_NotNull
            // 
            this.EsActivo_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.EsActivo_NotNull.HeaderText = "Es Activo";
            this.EsActivo_NotNull.Name = "EsActivo_NotNull";
            this.EsActivo_NotNull.ReadOnly = true;
            this.EsActivo_NotNull.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EsActivo_NotNull.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EsActivo_NotNull.Width = 98;
            // 
            // FormMoneda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 297);
            this.Name = "FormMoneda";
            this.Tag = "MONEDA_CONTR";
            this.TagBtnConsultar = "GETMONEDA";
            this.TagBtnEliminar = "REMOVEMONEDA";
            this.TagBtnModificar = "UPDATEMONEDA";
            this.TagBtnNuevo = "ADDMONEDA";
            this.Text = "Mantenimiento Moneda";
            this.Load += new System.EventHandler(this.FormMoneda_Load);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo_Filtro_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroMoneda_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn Decimal_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn Simbolo_NotNull;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EsActivo_NotNull;
    }
}