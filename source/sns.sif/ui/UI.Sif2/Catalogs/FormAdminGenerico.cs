﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace UI.Sif2
{
    public partial class FormAdminGenerico : Form
    {                
        public string operacion = string.Empty;
        public DataGridViewColumnCollection columnas ;

        public FormAdminGenerico()
        {
            InitializeComponent();                
        }

        // metodo usado para llenar los controles del FormAdmin con la informacion del 
        // registro seleccionado que permite al usuario Crear, Modificar o Consultar
        public virtual void FillData(){}
        
        // Usado para vincular los controles creado automaticamente por el metodo CreacionLayoutEntidad
        public virtual void VincularControles() { }

        // metodo  usado para Guardar los datos en la base de datos usando  los webservices
        public virtual void Guardar() { }

        //Metodo usado para validar que controles activar y cuales no de acuerdo si es consulta, modificacion o Agregar
        public virtual void VerificarTipoOperacion() 
        {
            if (operacion.Equals("C"))
            {
                LecturaControles(true);                
            }
        }

        public virtual void LecturaControles(bool bandera)
        {
            this.ObtenerControles<TextBox>().ForEach(x => x.ReadOnly = bandera);
            //this.ObtenerControles<Button>().ForEach(x => x.Enabled = bandera);
            this.ObtenerControles<CheckBox>().ForEach(x => x.Enabled = !bandera);
            this.ObtenerControles<ComboBox>().ForEach(x => x.Enabled = !bandera);
            btnGuardar.Enabled = false;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();            
        }        

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormAdminGenericoCatalogo_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.G == e.KeyData && Keys.Control == e.KeyData)
            {
                Guardar();
            }

            if (e.KeyData == Keys.Escape)
            {
                Close();
            }
        }

        private void FormAdminGenerico_Load(object sender, EventArgs e)
        {
            
        }
    }
}
