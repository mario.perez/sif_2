﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Sif2
{
    public partial class FormAdminMoneda : FormAdminGenerico
    {
        public FormAdminMoneda(string op, DataGridViewColumnCollection col, string titulo)
        {
            InitializeComponent();
            columnas = col;
            operacion = op;
            Text = titulo;
        }

        public override void FillData()
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void FormAdminMoneda_Load(object sender, EventArgs e)
        {
            
        }
    }
}
