﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.ServiceModel;
using businessLogic.CatalogController;
using UI.ViewModels;
using serviceLayer.SIF_DataContracts;

namespace UI.Sif2
{
    public partial class frmCategory : Form
    {
        private CategoryController _controller;
        private WarehouseController _warehouseController;
        private bool _isNew;

        public frmCategory()
        {
            _controller = new CategoryController();
            _warehouseController = new WarehouseController();
            _isNew = true;
            InitializeComponent();
        }


        #region Private Methods...

        private void Limpiar()
        {
            txtId.Text = string.Empty;
            txtName.Text = string.Empty;
            
        }

        private void InitializeValues()
        {
            pbProgress.Visible = true;
            EnableAll(false);
            Limpiar();
            bgwGetAll.RunWorkerAsync();

        }

        private void EnableAll(bool v)
        {
            txtName.Enabled = v;
            
            btnNew.Enabled = v;
            btnRemove.Enabled = v;
            btnSave.Enabled = v;
            btnExit.Enabled = v;
            lstData.Enabled = v;
        }

        private List<Catalog> loadList()
        {
            var catalogs = new List<Catalog>();
            Catalogue[] list = new List<Catalogue>().ToArray();
            try
            {
                var result = _warehouseController.ListCatalogs();
                
                list = result.ToArray();

            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            for (int i = 0; i < list.Length; i++)
            {
                catalogs.Add(new Catalog(list[i]));
            }
            return catalogs;
        }

        private void loadList(List<Catalog> list)
        {
            lstData.Items.Clear();
            lstData.Items.AddRange(list.ToArray());
        }


        private void RemoveOne(Catalogue catalogue)
        {
            try
            {
                _warehouseController.RemoveCatalog(catalogue);
                
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void UpdateOldOne(Catalogue catalogue)
        {
            try
            {
                _warehouseController.UpdateCatalog(catalogue);
                
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void AddNew(Catalogue catalogue)
        {
            try
            {
                _warehouseController.AddCatalog(catalogue);                
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }




        }




        #endregion

        #region Events...
        private void frmCategory_Load(object sender, EventArgs e)
        {
            InitializeValues();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            
            txtName.Text = string.Empty;
            txtId.Text = string.Empty;
            txtName.Focus();
            _isNew = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwRemove.RunWorkerAsync(new Catalogue() { Id = txtId.Text, Value = txtName.Text, IsActive = false });
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwSave.RunWorkerAsync(new Catalogue() { Id = txtId.Text, Value = txtName.Text, IsActive = false });
        }

        private void lstData_DoubleClick(object sender, EventArgs e)
        {
            if (lstData.SelectedItem != null)
            {
                Catalog selected = (Catalog)lstData.SelectedItem;
                _isNew = false;
                txtId.Text = selected.Id;
                txtName.Text = selected.Value;
                
            }
        }

        private void frmCategory_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !btnExit.Enabled;
        }
        #endregion

        #region Threads Workers...
        private void bgwGetAll_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.ListCatalogs();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Fallo al obtener las categorias",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bgwGetAll_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Catalog>();
            Catalogue[] list = new List<Catalogue>().ToArray();
            try
            {
                var result = (List<Catalogue>)e.Result;
                
                list = result.ToArray();
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Catalog(list[i]));
                }
                loadList(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwSave_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_isNew)
                {
                    _controller.AddCatalog((Catalogue)e.Argument);
                }
                else
                {
                    _controller.UpdateCatalog((Catalogue)e.Argument);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fallo al guardar una categoria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bgwSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {                
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwRemove_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                _controller.RemoveCatalog((Catalogue)e.Argument);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fallo al eliminar una categoria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bgwRemove_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }
        #endregion
    }
}
