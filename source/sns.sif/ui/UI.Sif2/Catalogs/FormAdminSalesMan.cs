﻿using businessLogic.CatalogController;
using serviceLayer.SIF_DataContracts;
using System;

namespace UI.Sif2
{
    public partial class FormAdminSalesMan : FormAdminGenericoCatalogo
    {
        public FormAdminSalesMan(Catalogue cat, string op, string titulo)
        {
            InitializeComponent();
            catalogue = cat;
            operacion = op;
            Text = titulo;
            FillData();
            VerificarTipoOperacion();
        }

        public override void Guardar()
        {

            try
            {                
                var vendedor = ActualizarEntidad();
                switch (operacion)
                {
                    case "I":
                        new SalesmanController().AddCatalog(vendedor);
                        break;
                    case "U":
                        new SalesmanController().UpdateCatalog(vendedor);
                        break;
                }
                //MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
    }
}
