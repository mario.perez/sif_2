﻿using businessLogic.CatalogController;
using serviceLayer.SIF_DataContracts;
using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace UI.Sif2
{
    public partial class FormSalesMan : FormGenericoCatalogo
    {
        public FormSalesMan()
        {
            InitializeComponent();

            MetodosGenericos.MostrarOcultarSplitFiltro(splitContainer1);
            HabilitarPermisos();
            LoadSalesMan();
        }

        private void LoadSalesMan()
        {
            try
            {
                List<Catalogue> lista = new List<Catalogue>();
                if (string.IsNullOrEmpty(txtCodigo.Text)) {
                    lista = new SalesmanController().ListCatalogs();                    
                }
                else
                {
                    var ven = new SalesmanController().getCatalog(txtCodigo.Text);
                    lista.Add(ven);
                }
                LoadCatalog(lista.ToArray());
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void Eliminar()
        {
            if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar?") == DialogResult.Yes)
            {
                var eliminar = ObtenerCatalogue();
                new SalesmanController().RemoveCatalog(eliminar);
                //MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
        }

        public override void Agregar()
        {
            new FormAdminSalesMan(ObtenerCatalogue(),"I","Agregar Vendedor").ShowDialog();
        }

        public override void Modificar()
        {
            new FormAdminSalesMan(ObtenerCatalogue(), "U", "Modificar Vendedor").ShowDialog();
        }

        public override void Consultar()
        {
            new FormAdminSalesMan(ObtenerCatalogue(), "C", "Consultar Vendedor").ShowDialog();
        }
        public override void RefrescarDatos()
        {
            LoadSalesMan();
        }

        private void FormSalesMan_Load(object sender, EventArgs e)
        {

        }
    }
}
