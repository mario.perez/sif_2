﻿using businessLogic.CatalogController;
using serviceLayer.SIF_DataContracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace UI.Sif2
{
    public partial class frmAdminImpuesto : Form
    {
        private string operacion;
        private ImpuestoData impuesto;

        public frmAdminImpuesto(string op, ImpuestoData imp)
        {
            InitializeComponent();
            operacion = op;
            impuesto = imp;
            NombrarFormulario();
            HabilitarControles_por_TipoOperacion();
            LlenarFormulario_InfoImpuesto();
        }

        private void LlenarFormulario_InfoImpuesto() {
            try
            {
                if (!operacion.Equals("I")) {
                    txtCodigo.Text = impuesto.Codigo;
                    txtNombre.Text = impuesto.Nombre;
                    txtDescripcion.Text = impuesto.DescripcionImpuesto;
                    txtValorImpuesto.Text = impuesto.ValorImpuesto.ToString();
                    chkActivo.Checked = impuesto.Activo;
                    chkPredeterminado.Checked = impuesto.Predeterminado;
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void HabilitarControles_por_TipoOperacion() {
            // Si la operacion es consultar, coloca los controles en modo lectura
            if (operacion.Equals("S")) {
                txtCodigo.ReadOnly = true;
                txtNombre.ReadOnly = true;
                txtDescripcion.ReadOnly = true;
                txtValorImpuesto.ReadOnly = true;
                chkActivo.Enabled = false;
                chkPredeterminado.Enabled = false;
                btnGuardar.Enabled = false;
            }
        }

        private void NombrarFormulario()
        {
            switch (operacion)
            {
                case "I":
                    Text = "Agregar Impuesto";
                    break;
                case "U":
                    Text = "Modificar Impuesto";
                    break;
                case "S":
                    Text = "Consultar Impuesto";
                    break;
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void Guardar() {
            try
            {
                if (ValidarValorImpuesto() && ValidarNombre()) {
                    if (operacion.Equals("I"))
                    {
                        impuesto = new ImpuestoData();
                        impuesto.FechaCreacion = DateTime.Now;
                        impuesto.MaqCreacion = Environment.MachineName;
                    }
                    ActualizarInformacion();

                    

                    if (operacion.Equals("U"))
                    {
                        impuesto.FechaModificacion = DateTime.Now;
                        impuesto.MaqModificacion = Environment.MachineName;
                        new ImpuestoController().GestionarImpuesto_CRUD(impuesto,"U");
                    }
                    else
                    {
                        if (operacion.Equals("I"))
                        {
                            new ImpuestoController().GestionarImpuesto_CRUD(impuesto, "I");                            
                            LimpiarControles();
                            
                        }
                    }

                    //MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
                    
                }

            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void ActualizarInformacion() {
            impuesto.Codigo = txtCodigo.Text;
            impuesto.Nombre = txtNombre.Text;
            impuesto.DescripcionImpuesto = txtDescripcion.Text;
            impuesto.ValorImpuesto = decimal.Parse(txtValorImpuesto.Text);
            impuesto.Activo = chkActivo.Checked;
            impuesto.Predeterminado = chkPredeterminado.Checked;
        }

        private void LimpiarControles()
        {
            txtCodigo.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtDescripcion.Text = string.Empty;
            txtValorImpuesto.Text = 0.ToString();
            impuesto.Activo = false;
            impuesto.Predeterminado = false; ;
        }

        private void txtNombre_Validating(object sender, CancelEventArgs e)
        {
            ValidarNombre();
        }

        private bool ValidarNombre() {
            try
            {
                errorProvider1.Clear();
                if (string.IsNullOrEmpty(txtNombre.Text)) {
                    errorProvider1.SetIconAlignment(txtNombre,ErrorIconAlignment.MiddleLeft);
                    errorProvider1.SetError(txtNombre, "Nombre no puede ser vacío");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return false;
            }
        }

        private void txtValorImpuesto_Validating(object sender, CancelEventArgs e)
        {
            ValidarValorImpuesto();
        }

        private bool ValidarValorImpuesto() {
            try
            {
                errorProvider1.Clear();
                var valor = decimal.Parse(txtValorImpuesto.Text);
                return true;

            }
            catch 
            {
                errorProvider1.SetIconAlignment(txtValorImpuesto, ErrorIconAlignment.MiddleLeft);
                errorProvider1.SetError(txtValorImpuesto, "Valor debe ser decimal");
                return false;
            }
        }

        private void frmAdminImpuesto_KeyDown(object sender, KeyEventArgs e)
        {
            CombinacionTeclas(e);
        }

        private void CombinacionTeclas(KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                throw;
            }
        }
    }
}
