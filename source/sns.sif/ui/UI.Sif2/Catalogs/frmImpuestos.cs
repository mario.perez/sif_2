﻿using businessLogic.CatalogController;
using serviceLayer.SIF_DataContracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace UI.Sif2
{
    public partial class frmImpuestos : FormGenerico
    {
        private ImpuestoController _controller;        
        private ImpuestoData impuestoFiltro = new ImpuestoData() { FechaCreacion = null, FechaModificacion = null };

        public frmImpuestos()
        {
            InitializeComponent();
            _controller = new ImpuestoController();
            HabilitarPermisos();            
        }


        private ImpuestoData ObtenerImpuestoSeleccionado() {

            try
            {
                if (dgv.CurrentRow != null)
                {
                    var impuesto = new ImpuestoData();

                    var id = Int32.Parse(dgv.CurrentRow.Cells["Id"].Value.ToString());
                    impuesto = _controller.GetAllImpuestosOrImpuesto(new ImpuestoData() { ImpuestoId = id })[0];
                    return impuesto;
                }
                else
                {
                    return null;
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return null;
            }
        }

        private void ListarImpuesto_al_Grid(RunWorkerCompletedEventArgs e)
        {
            ImpuestoData[] list = new List<ImpuestoData>().ToArray();
            try
            {
                var result = (List<ImpuestoData>)e.Result;                

                list = result.ToArray();
                dgv.Rows.Clear();
                lblTotalRegistro.Text = "Total Registro: " + list.Length.ToString();
                foreach (var item in list)
                {
                    dgv.Rows.Add(
                        item.ImpuestoId
                        , item.Codigo
                        , item.Nombre
                        , item.DescripcionImpuesto
                        , item.ValorImpuesto                        
                        , item.Predeterminado
                        , item.Activo
                        );
                }
            }
            catch (ArgumentException ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void bgw_GetAll_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllImpuestosOrImpuesto(impuestoFiltro);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al obtener Impuestos", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void bgw_GetAll_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ListarImpuesto_al_Grid(e);   
        }

        private void frmImpuestos_Load(object sender, EventArgs e)
        {
            bgw_GetAll.RunWorkerAsync();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar();
        }

        public override void Consultar() {
            try
            {
                var imp = ObtenerImpuestoSeleccionado();
                if (imp != null) {
                    new frmAdminImpuesto("S",imp).ShowDialog();
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Agregar();
        }

        public override void Agregar() {
            new frmAdminImpuesto("I", null).Show();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        public override void Modificar() {
            try
            {
                var imp = ObtenerImpuestoSeleccionado();
                if (imp != null)
                {
                    new frmAdminImpuesto("U", imp).ShowDialog();
                }

            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Agregar();
        }

        private void modificartoolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Eliminar();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Consultar();
        }

        public override void Eliminar() {
            try
            {
                if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar el Impuesto seleccionado?") == DialogResult.Yes) {
                    var imp = ObtenerImpuestoSeleccionado();
                    _controller.GestionarImpuesto_CRUD(imp,"D");
                    //MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
                }
                

            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
        }

        public override void RefrescarDatos()
        {

            try
            {
                TextBox txtCodigo = (TextBox)this.Controls.Find("CodigoFiltro", true)[0];
                TextBox txtNombre = (TextBox)this.Controls.Find("NombreFiltro", true)[0];

                impuestoFiltro.Codigo = txtCodigo.Text;
                impuestoFiltro.Nombre = txtNombre.Text;

                bgw_GetAll.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }                        
            
        }

    }
}
