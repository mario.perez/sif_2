﻿namespace UI.Sif2
{
    partial class FormAdminCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // panelControles
            // 
            this.panelControles.Size = new System.Drawing.Size(381, 159);
            // 
            // FormAdminCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 184);
            this.Name = "FormAdminCaja";
            this.Text = "Mantenimiento de Caja";
            this.Load += new System.EventHandler(this.FormAdminCaja_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}