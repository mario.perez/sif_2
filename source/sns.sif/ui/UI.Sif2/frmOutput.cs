﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;
using serviceLayer.serviceClient;
using System.ServiceModel;
using serviceLayer.transactionServiceClient.TransactionServiceReference;

namespace UI.Sif2
{
    public partial class frmOutput : Form
    {
        private CatalogsClient _controller;
        private bool _isNew;
        private int _catalogsLoaded;
        private List<ComboBox> catalogsComboBoxes = new List<ComboBox>();
        public frmOutput()
        {
            _controller = new CatalogsClient();
            _isNew = true;
            InitializeComponent();
            catalogsComboBoxes.Add(cmbWarehouse);
        }

        private void frmOutput_Load(object sender, EventArgs e)
        {
            InitializeValues();            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void InitializeValues()
        {
            pbProgress.Visible = true;
            _catalogsLoaded = 0;
            EnableAll(false);
            EnableComboboxes(false);
            Limpiar();
            bgwGetAll.RunWorkerAsync();
            bgwGetWarehouse.RunWorkerAsync();
        }
        private void EnableAll(bool v)
        {
            btnNew.Enabled = v;
            btnRemove.Enabled = v;
            btnSave.Enabled = v;
            btnExit.Enabled = v;
            txtId.Enabled = v;
            txtProductId.Enabled = v;
            txtDescription.Enabled = v;
            txtMeasurement.Enabled = v;
            txtPrice.Enabled = v;
            txtAmount.Enabled = v;
            txtPackageDescripcion.Enabled = v;
            txtQuantity.Enabled = v;
            txtType.Enabled = v;
            chkCancelled.Enabled = v;
            chbRegistered.Enabled = v;
            lvDetails.Enabled = v;
        }
        private void EnableComboboxes(bool v)
        {
            foreach (ComboBox cmb in catalogsComboBoxes)
            {
                cmb.Enabled = v;
            }
        }
        private void frmOutput_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !btnExit.Enabled;
        }
        private List<Catalog> loadList()
        {
            var catalogs = new List<Catalog>();
            Catalogue[] list = new List<Catalogue>().ToArray();
            try
            {
                var result = _controller.GetAllWarehouses();
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;

            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            for (int i = 0; i < list.Length; i++)
            {
                catalogs.Add(new Catalog(list[i]));
            }
            return catalogs;
        }
        private void loadWareHouses(List<Catalog> catalogs)
        {
            cmbWarehouse.Items.Clear();
            cmbWarehouse.Items.AddRange(catalogs.ToArray());
        }
        private void Limpiar()
        {
            LimpiarControlesProducto();
            LimpiarControlesSalida();
        }
        private void bgwGetAll_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllCashboxes();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfCashBoxDatanqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }
        private void bgwGetAll_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Cashbox>();
            CashBoxData[] list = new List<CashBoxData>().ToArray();
            try
            {
                GeneralMessageOfArrayOfCashBoxDatanqvBR1WM result = (GeneralMessageOfArrayOfCashBoxDatanqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Cashbox(list[i]));
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwGetWarehouse_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllWarehouses();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfCataloguenqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }
        private void bgwGetWarehouse_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Catalog>();
            Catalogue[] list = new List<Catalogue>().ToArray();
            try
            {
                GeneralMessageOfArrayOfCataloguenqvBR1WM result = (GeneralMessageOfArrayOfCataloguenqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Catalog(list[i]));
                }
                loadWareHouses(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            _catalogsLoaded++;
            EnableComboboxes(catalogsComboBoxes.Count == _catalogsLoaded);

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Limpiar();
            EnableAll(true);
            _isNew = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);            
            bgwSave.RunWorkerAsync(new OutputData()
            {
                OutputId = txtId.Text,
                Comment = txtDescription.Text,
                IsRegistered = chbRegistered.Checked,
                IsCancelled = chkCancelled.Checked,
                Type = 0,//Int32.Parse(txtType.Text),
                WarehouseId = cmbWarehouse.SelectedIndex,
                Details = detalleSalida()
            });
        }

        private OutputDetailData[] detalleSalida()
        {
            OutputDetailData[] listads = new OutputDetailData[lvDetails.Items.Count];
            foreach (var item in lvDetails.Items)
            {
                OutputDetailData detail = new OutputDetailData();                
            }
            return listads;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            agregar_productoDetalle_Salida();
        }

        private void agregar_productoDetalle_Salida() {

            ListViewItem item = new ListViewItem(txtProductId.Text);
            item.SubItems.Add(txtDescription.Text);
            item.SubItems.Add(txtPrice.Text);
            item.SubItems.Add(txtAmount.Text);
            item.SubItems.Add("Lote Default Test");            

            lvDetails.Items.Add(item);
        }

        private void LimpiarControlesProducto() {
            txtProductId.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtPrice.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtMeasurement.Text = string.Empty;
            cmbPackageId.Text = string.Empty;
            txtPackageDescripcion.Text = string.Empty;
            txtQuantity.Text = string.Empty;
        }

        private void LimpiarControlesSalida() {
            txtId.Text = string.Empty;
            cmbWarehouse.Text = string.Empty;
            dtpDate.Text = string.Empty;
            txtType.Text = string.Empty;
            chkCancelled.Checked = false;
            chbRegistered.Checked = false;
        }
    }
}
