﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;

namespace UI.Sif2
{
    public partial class FormMeasurement : FormGenericoCatalogo
    {
        public FormMeasurement()
        {
            InitializeComponent();
            MetodosGenericos.MostrarOcultarSplitFiltro(splitContainer1);
            HabilitarPermisos();
            LoadMeasurement();
        }

        private void LoadMeasurement()
        {
            try
            {
                var lista = new CatalogsClient().GetAllMeasurements().Data;
                LoadCatalog(lista);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void Agregar()
        {
            new FormAdminMeasurement(null, "I", "Agregar Unidad de Medida").ShowDialog();
        }

        public override void Consultar()
        {
            new FormAdminCategory(ObtenerCatalogue(), "C", "Consultar Unidad de Medida").ShowDialog();
        }

        public override void Modificar()
        {
            new FormAdminCategory(ObtenerCatalogue(), "U", "Modificar Unidad de Medida").ShowDialog();
        }

        public override void Eliminar()
        {
            if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar?") == DialogResult.Yes)
            {
                var eliminar = ObtenerCatalogue();
                var respuesta = new CatalogsClient().RemoveCategory(eliminar);
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
            
        }

        public override void RefrescarDatos()
        {
            LoadMeasurement();
        }
    }
}
