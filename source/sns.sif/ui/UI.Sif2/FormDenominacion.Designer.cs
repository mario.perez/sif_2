﻿namespace UI.Sif2
{
    partial class FormDenominacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.Codigo_Filtro_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moneda_NotNull = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Factor_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EsActivo_NotNull = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EsEfectivo_NotNull = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Size = new System.Drawing.Size(844, 288);
            this.splitContainer1.SplitterDistance = 226;
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(610, 22);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv);
            this.panel2.Size = new System.Drawing.Size(610, 264);
            // 
            // panelFiltro
            // 
            this.panelFiltro.Size = new System.Drawing.Size(224, 264);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.ColumnHeadersHeight = 30;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo_Filtro_NotNull,
            this.Descripcion_NotNull,
            this.Moneda_NotNull,
            this.Factor_NotNull,
            this.EsActivo_NotNull,
            this.EsEfectivo_NotNull});
            this.dgv.ContextMenuStrip = this.MenuOperacion;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(610, 264);
            this.dgv.TabIndex = 24;
            // 
            // Codigo_Filtro_NotNull
            // 
            this.Codigo_Filtro_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Codigo_Filtro_NotNull.HeaderText = "Codigo";
            this.Codigo_Filtro_NotNull.Name = "Codigo_Filtro_NotNull";
            this.Codigo_Filtro_NotNull.ReadOnly = true;
            this.Codigo_Filtro_NotNull.Width = 83;
            // 
            // Descripcion_NotNull
            // 
            this.Descripcion_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Descripcion_NotNull.HeaderText = "Descripción";
            this.Descripcion_NotNull.Name = "Descripcion_NotNull";
            this.Descripcion_NotNull.ReadOnly = true;
            // 
            // Moneda_NotNull
            // 
            this.Moneda_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Moneda_NotNull.DefaultCellStyle = dataGridViewCellStyle3;
            this.Moneda_NotNull.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Moneda_NotNull.HeaderText = "Moneda";
            this.Moneda_NotNull.Name = "Moneda_NotNull";
            this.Moneda_NotNull.ReadOnly = true;
            this.Moneda_NotNull.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Moneda_NotNull.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Moneda_NotNull.Width = 89;
            // 
            // Factor_NotNull
            // 
            this.Factor_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Factor_NotNull.HeaderText = "Factor";
            this.Factor_NotNull.Name = "Factor_NotNull";
            this.Factor_NotNull.ReadOnly = true;
            this.Factor_NotNull.Width = 77;
            // 
            // EsActivo_NotNull
            // 
            this.EsActivo_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.EsActivo_NotNull.HeaderText = "Es Activo";
            this.EsActivo_NotNull.Name = "EsActivo_NotNull";
            this.EsActivo_NotNull.ReadOnly = true;
            this.EsActivo_NotNull.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EsActivo_NotNull.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EsActivo_NotNull.Width = 98;
            // 
            // EsEfectivo_NotNull
            // 
            this.EsEfectivo_NotNull.HeaderText = "Es Efectivo";
            this.EsEfectivo_NotNull.Name = "EsEfectivo_NotNull";
            this.EsEfectivo_NotNull.ReadOnly = true;
            // 
            // FormDenominacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 313);
            this.Name = "FormDenominacion";
            this.Tag = "DENOMI_CONTR";
            this.TagBtnConsultar = "GETDENOMIN";
            this.TagBtnEliminar = "REMOVEDENOMIN";
            this.TagBtnModificar = "UPDATEDENOMIN";
            this.TagBtnNuevo = "ADDDENOMIN";
            this.Text = "Mantenimiento de Denominación";
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo_Filtro_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion_NotNull;
        private System.Windows.Forms.DataGridViewComboBoxColumn Moneda_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factor_NotNull;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EsActivo_NotNull;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EsEfectivo_NotNull;
    }
}