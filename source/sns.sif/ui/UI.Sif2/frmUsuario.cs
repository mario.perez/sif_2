﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.Security.SecurityServiceReference;
using serviceLayer.Security;

namespace UI.Sif2
{
    public partial class frmUsuario : Form
    {
        private UsuarioData usuarioFiltro = new UsuarioData() {
            FechaCreacion = DateTime.Now
            , FechaModificacion = DateTime.Now
        };
        private SecurityClient _controller;
        int anchoPanelFiltro = 257;
        private UsuarioData[] ListaUsuario = new List<UsuarioData>().ToArray();

        public frmUsuario()
        {
            InitializeComponent();            
            _controller = new SecurityClient();
            panel1.Width = anchoPanelFiltro;
            GetAllUsuario();
            Habilitar_Acciones();
        }

        private void Habilitar_Acciones()
        {
            try
            {
                var acciones = _controller.GetPrivilegioAccion(MetodosGenericos.UsuarioConectado.Id, this.Tag.ToString()).Data.ToList();
                MetodosGenericos.Habilitar_Operacion_PrivilegioAccion(barraOperacion, acciones);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            AgregarUsuario();
        }

        private void AgregarUsuario() {
            new frmAdminUsuario("I", new UsuarioData()).ShowDialog();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bgw_Usuario_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllUsuario(usuarioFiltro);
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfUsuarioDatanqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgw_Usuario_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ObtenerUsuario(e);
        }


        private void ObtenerUsuario(RunWorkerCompletedEventArgs e)
        {
            UsuarioData[] list = new List<UsuarioData>().ToArray();
            try
            {
                GeneralMessageOfArrayOfUsuarioDatanqvBR1WM result = (GeneralMessageOfArrayOfUsuarioDatanqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);

                list = result.Data;
                ListaUsuario = result.Data;

                LlenarDataGrid(string.Empty, string.Empty, string.Empty, true, false);

            }
            catch (ArgumentException ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void LlenarDataGrid(string usuarioLogin, string nombreCompleto, string correo
            , bool activo, bool bloqueado
            )
        {

            //dgvUsuario.CellValueChanged -= dgvUsuario_CellValueChanged;
            //MetodosGenericos.FormatearDataGridView(dgvUsuario);
            //dgvUsuario.CellValueChanged += dgvUsuario_CellValueChanged;
            dgvUsuario.Rows.Clear();
            foreach (var item in ListaUsuario)
            {

                if (
                       (string.IsNullOrEmpty(usuarioLogin) || string.IsNullOrWhiteSpace(usuarioLogin) || item.UsuarioLogin.Contains(usuarioLogin))
                       && (string.IsNullOrEmpty(nombreCompleto) || string.IsNullOrWhiteSpace(nombreCompleto) || item.NombreCompleto.Contains(nombreCompleto))
                       && (item.Activo == activo || activo == false)
                       && (item.Bloqueado == bloqueado || bloqueado == false)
                    )
                {
                    dgvUsuario.Rows.Add(
                        item.Id
                        , item.UsuarioLogin
                        , item.Correo
                        , item.Activo
                        , item.Bloqueado
                        , item.NombreCompleto
                    );

                    // Indicamos que los registros mayores al primero son de lectura
                    //dgvUsuario.Rows[dgvUsuario.Rows.Count - 1].ReadOnly = true;

                }

            }
            // Al count le restamos -1 para que no tome el cuenta el registro usado para filtrar el grid
            lbl_totalRegistro.Text = "Total Registros: " + (dgvUsuario.Rows.Count - 1).ToString();
        }

        private void GetAllUsuario()
        {
            try
            {
                usuarioFiltro.UsuarioLogin = txtUsuarioLogin.Text;
                usuarioFiltro.NombreCompleto = txtNombreCompleto.Text;
                usuarioFiltro.Correo = txtCorreo.Text;

                bgw_Usuario.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void dgvUsuario_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == 0)
            {
                if (dgvUsuario.CurrentRow != null)
                {

                    string Nombre = dgvUsuario.CurrentRow.Cells["LoginUsuario"].Value != null ? dgvUsuario.CurrentRow.Cells["LoginUsuario"].Value.ToString() : string.Empty;
                    string nombrecompleto = dgvUsuario.CurrentRow.Cells["NombreCompleto"].Value != null ? dgvUsuario.CurrentRow.Cells["NombreCompleto"].Value.ToString() : string.Empty;
                    string correo = dgvUsuario.CurrentRow.Cells["Correo"].Value != null ? dgvUsuario.CurrentRow.Cells["Correo"].Value.ToString() : string.Empty;
                    bool activo = dgvUsuario.CurrentRow.Cells["Activo"].Value != null ? bool.Parse(dgvUsuario.CurrentRow.Cells["Activo"].Value.ToString()) : false;
                    bool bloqueado = dgvUsuario.CurrentRow.Cells["Bloqueado"].Value != null ? bool.Parse(dgvUsuario.CurrentRow.Cells["Bloqueado"].Value.ToString()) : false;

                    LlenarDataGrid(Nombre, nombrecompleto, correo, activo, bloqueado);
                }
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            GetAllUsuario();
        }

        private void LimpiarFiltros() {
            txtUsuarioLogin.Text = txtNombreCompleto.Text = txtCorreo.Text = string.Empty;
        }

        private void btnFiltros_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarPanelFiltro(anchoPanelFiltro, panel1);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarFiltros();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarUsuario();
        }

        private void ModificarUsuario() {
            var usu = ObtenerUsuarioSeleccionado();
            if (usu != null) {
                new frmAdminUsuario("U", usu).ShowDialog();
            }
            
        }

        private UsuarioData ObtenerUsuarioSeleccionado() {
            UsuarioData UsuarioSeleccionado = null;

            try
            {
                if (dgvUsuario.CurrentRow != null && dgvUsuario.CurrentRow.Index > -1)
                {
                    UsuarioSeleccionado = new UsuarioData();

                    UsuarioSeleccionado.Id = Convert.ToInt32(dgvUsuario.CurrentRow.Cells["Id"].Value);
                    UsuarioSeleccionado.FechaCreacion = DateTime.Now;
                    UsuarioSeleccionado.FechaModificacion = DateTime.Now;                    
                    UsuarioSeleccionado = _controller.GetAllUsuario(UsuarioSeleccionado).Data[0];
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
            return UsuarioSeleccionado;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarUsuario();
        }

        private void EliminarUsuario()
        {
            try
            {
                var usu = ObtenerUsuarioSeleccionado();
                if (usu != null) {
                    if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar el usuario?") == DialogResult.Yes)
                    {
                        var resultado = _controller.RemoveUsuario(usu);
                        MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
                    }
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ConsultarUsuario();
        }

        private void ConsultarUsuario() {
            var usu = ObtenerUsuarioSeleccionado();
            if (usu != null) {
                new frmAdminUsuario("S", usu).ShowDialog();
            }            
        }

        private void limpiarFiltroGridToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.LimpiarFiltrosGrid(dgvUsuario);
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarUsuario();
        }

        private void modificartoolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificarUsuario();
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EliminarUsuario();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarUsuario();
        }
    }
}
