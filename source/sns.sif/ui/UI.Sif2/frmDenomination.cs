﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.ServiceModel;

namespace UI.Sif2
{
    public partial class frmDenomination : Form
    {
        private CatalogsClient _controller;
        private bool _isNew;
        private int _catalogsLoaded;
        private List<ComboBox> catalogsComboBoxes = new List<ComboBox>();


        public frmDenomination()
        {
            _controller = new CatalogsClient();
            _isNew = true;
            InitializeComponent();
            catalogsComboBoxes.Add(cmbCurrency);
        }




        #region Private Methods...


        private void EnableComboboxes(bool v)
        {
            foreach (ComboBox cmb in catalogsComboBoxes)
            {
                cmb.Enabled = v;
            }
        }

        private void loadCurrencies(List<Currency> catalogs)
        {
            cmbCurrency.Items.Clear();
            cmbCurrency.Items.AddRange(catalogs.ToArray());
        }

        private void Limpiar()
        {
            txtId.Text = string.Empty;
            txtDescription.Text = string.Empty;
            
        }

        private DenominationData getActualData()
        {
            var result = new DenominationData();
            result.Id = int.Parse(txtId.Text == ""?"0": txtId.Text);
            result.Description = txtDescription.Text;
            result.IsActive = false;
            result.IsCash = chkIsCash.Checked;
            result.Factor = decimal.Parse(txtFactor.Text);
            result.CurrencyIso = ((Currency)cmbCurrency.SelectedItem).ToCurrencyData();
            return result;
        }

        private void InitializeValues()
        {
            pbProgress.Visible = true;
            _catalogsLoaded = 0;
            EnableAll(false);
            EnableComboboxes(false);
            Limpiar();
            bgwGetAll.RunWorkerAsync();
            bgwGetCurrencies.RunWorkerAsync();

        }

        private void EnableAll(bool v)
        {
            txtDescription.Enabled = v;
            
            chkIsCash.Enabled = v;
            txtFactor.Enabled = v;
            btnNew.Enabled = v;
            btnRemove.Enabled = v;
            btnSave.Enabled = v;
            btnExit.Enabled = v;
            lstData.Enabled = v;
        }

        private List<Denomination> loadList()
        {
            var catalogs = new List<Denomination>();
            DenominationData[] list = new List<DenominationData>().ToArray();
            try
            {
                var result = _controller.GetAllDenominations();
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;

            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            for (int i = 0; i < list.Length; i++)
            {
                catalogs.Add(new Denomination(list[i]));
            }
            return catalogs;
        }

        private void loadList(List<Denomination> list)
        {
            lstData.Items.Clear();
            lstData.Items.AddRange(list.ToArray());
        }


        private void RemoveOne(DenominationData catalogue)
        {
            try
            {
                var result = _controller.RemoveDenomination(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void UpdateOldOne(DenominationData catalogue)
        {
            try
            {
                var result = _controller.UpdateDenomination(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void AddNew(DenominationData catalogue)
        {
            try
            {
                var result = _controller.AddDenomination(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }




        }




        #endregion

        #region Events...
        private void frmDenomination_Load(object sender, EventArgs e)
        {
            InitializeValues();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            
            txtDescription.Text = string.Empty;
            txtId.Text = string.Empty;
            txtFactor.Text = "0.00";
            chkIsCash.Checked = false;
            cmbCurrency.Text = string.Empty;
            cmbCurrency.SelectedItem = null;
            txtDescription.Focus();
            _isNew = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwRemove.RunWorkerAsync(getActualData());

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwSave.RunWorkerAsync(getActualData());
        }

        private void lstData_DoubleClick(object sender, EventArgs e)
        {
            if (lstData.SelectedItem != null)
            {
                Denomination selected = (Denomination)lstData.SelectedItem;
                _isNew = false;
                txtId.Text = selected.Id.ToString();
                txtDescription.Text = selected.Description;
                txtFactor.Text = selected.Factor.ToString();
                chkIsCash.Checked = selected.IsCash;
                cmbCurrency.SelectedItem = new Currency(selected.CurrencyIso);
                cmbCurrency.Text = new Currency(selected.CurrencyIso).ToString();
                
            }
        }

        private void frmDenomination_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !btnExit.Enabled;
        }
        #endregion

        #region Threads Workers...
        private void bgwGetAll_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllDenominations();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfDenominationDatanqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetAll_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Denomination>();
            DenominationData[] list = new List<DenominationData>().ToArray();
            try
            {
                GeneralMessageOfArrayOfDenominationDatanqvBR1WM result = (GeneralMessageOfArrayOfDenominationDatanqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Denomination(list[i]));
                }
                loadList(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwSave_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_isNew)
                {
                    e.Result = _controller.AddDenomination((DenominationData)e.Argument);
                }
                else
                {
                    e.Result = _controller.UpdateDenomination((DenominationData)e.Argument);
                }
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfboolean() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var result = (GeneralMessageOfboolean)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwRemove_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.RemoveDenomination((DenominationData)e.Argument);
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfboolean() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwRemove_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var result = (GeneralMessageOfboolean)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                MessageBox.Show("El dato se eliminó correctamente.", "Exitoso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }
        #endregion

        private void bgwGetCurrencies_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllCurrencies();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfCurrencyDatanqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetCurrencies_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Currency>();
            CurrencyData[] list = new List<CurrencyData>().ToArray();
            try
            {
                GeneralMessageOfArrayOfCurrencyDatanqvBR1WM result = (GeneralMessageOfArrayOfCurrencyDatanqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Currency(list[i]));
                }
                loadCurrencies(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            _catalogsLoaded++;
            EnableComboboxes(catalogsComboBoxes.Count == _catalogsLoaded);
        }

    }
}
