﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;

namespace UI.Sif2
{
    public partial class FormAdminMeasurement : FormAdminGenericoCatalogo
    {        

        public FormAdminMeasurement(Catalogue mease,string op, string titulo)
        {
            InitializeComponent();
            catalogue = mease;
            operacion = op;
            Text = titulo;
            FillData();
            VerificarTipoOperacion();
        }

        public override void Guardar()
        {

            try
            {
                GeneralMessageOfboolean respuesta = new GeneralMessageOfboolean();
                var unidad = ActualizarEntidad();
                switch (operacion)
                {
                    case "I":
                        respuesta = new CatalogsClient().AddMeasurement(unidad);
                        break;
                    case "U":
                        respuesta = new CatalogsClient().UpdateMeasurement(unidad);
                        break;
                }
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void FormAdminMeasurement_Load(object sender, EventArgs e)
        {            
        }
    }
}
