﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;
using serviceLayer.serviceClient;
using System.ServiceModel;


namespace UI.Sif2
{
    public partial class frmProduct : Form
    {
        private CatalogsClient _controller;
        private bool _isNew;
        private int _catalogsLoaded;
        private List<ComboBox> catalogsComboBoxes = new List<ComboBox>();

        public frmProduct()
        {
            _controller = new CatalogsClient();
            _isNew = true;
            InitializeComponent();
            catalogsComboBoxes.Add(cmbCategory);
            catalogsComboBoxes.Add(cmbMeasurements);
        }



        #region Private Methods...


        private void EnableComboboxes(bool v)
        {
            foreach (ComboBox cmb in catalogsComboBoxes)
            {
                cmb.Enabled = v;
            }
        }

        private void loadCategories(List<Catalog> catalogs)
        {
            cmbCategory.Items.Clear();
            cmbCategory.Items.AddRange(catalogs.ToArray());
        }

        private void loadMeasurements(List<Catalog> catalogs)
        {
            cmbMeasurements.Items.Clear();
            cmbMeasurements.Items.AddRange(catalogs.ToArray());
        }

        private void Limpiar()
        {
            txtId.Text = string.Empty;
            txtDescription.Text = string.Empty;
            
        }

        private ProductData getActualData()
        {
            var result = new ProductData();
            result.ProductId = txtId.Text;
            result.Description = txtDescription.Text;
            result.HasStock = chkHasExistence.Checked;
            result.Measurement = ((Catalog)cmbMeasurements.SelectedItem).ToCatalogue();
            result.Category = ((Catalog)cmbCategory.SelectedItem).ToCatalogue();
            result.UnitsPerPackage = int.Parse(string.IsNullOrWhiteSpace(txtUnitsPerPackage.Text) ? "0" : txtUnitsPerPackage.Text);
            result.IsActive = true;
            return result;
        }

        private void InitializeValues()
        {
            pbProgress.Visible = true;
            _catalogsLoaded = 0;
            EnableAll(false);
            EnableComboboxes(false);
            Limpiar();
            bgwGetAll.RunWorkerAsync();
            bgwGetCategories.RunWorkerAsync();
            bgwGetMeasurements.RunWorkerAsync();

        }

        private void EnableAll(bool v)
        {
            txtDescription.Enabled = v;
            
            chkHasExistence.Enabled = v;
            txtUnitsPerPackage.Enabled = v;
            txtId.Enabled = v;
            btnNew.Enabled = v;
            btnRemove.Enabled = v;
            btnSave.Enabled = v;
            btnExit.Enabled = v;
            lstData.Enabled = v;
        }

        private List<Product> loadList()
        {
            var catalogs = new List<Product>();
            ProductData[] list = new List<ProductData>().ToArray();
            try
            {
                var result = _controller.GetAllProducts();
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;

            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            for (int i = 0; i < list.Length; i++)
            {
                catalogs.Add(new Product(list[i]));
            }
            return catalogs;
        }

        private void loadList(List<Product> list)
        {
            lstData.Items.Clear();
            lstData.Items.AddRange(list.ToArray());
        }


        private void RemoveOne(ProductData catalogue)
        {
            try
            {
                var result = _controller.RemoveProduct(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void UpdateOldOne(ProductData catalogue)
        {
            try
            {
                var result = _controller.UpdateProduct(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void AddNew(ProductData catalogue)
        {
            try
            {
                var result = _controller.AddProduct(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }



        #endregion

        #region Events...
        private void frmProduct_Load(object sender, EventArgs e)
        {
            InitializeValues();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            
            txtDescription.Text = string.Empty;
            txtId.Text = string.Empty;
            txtUnitsPerPackage.Text = "0";
            chkHasExistence.Checked = false;
            cmbCategory.Text = string.Empty;
            cmbCategory.SelectedItem = null;
            cmbMeasurements.Text = string.Empty;
            cmbMeasurements.SelectedItem = null;
            txtId.Focus();
            _isNew = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwRemove.RunWorkerAsync(getActualData());
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwSave.RunWorkerAsync(getActualData());
        }

        private void lstData_DoubleClick(object sender, EventArgs e)
        {
            if (lstData.SelectedItem != null)
            {
                Product selected = (Product)lstData.SelectedItem;
                _isNew = false;
                txtId.Text = selected.ProductId.ToString();
                txtDescription.Text = selected.Description;
                txtUnitsPerPackage.Text = selected.UnitsPerPackage.ToString();
                chkHasExistence.Checked = selected.HasStock;
                cmbCategory.SelectedItem = new Catalog(selected.Category);
                cmbCategory.Text = new Catalog(selected.Category).ToString();
                cmbMeasurements.SelectedItem = new Catalog(selected.Measurement);
                cmbMeasurements.Text = new Catalog(selected.Measurement).ToString();
                
            }
        }

        private void frmProduct_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !btnExit.Enabled;
        }
        #endregion

        #region Threads Workers...

        #endregion

        private void bgwGetAll_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllProducts();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfProductDatanqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetAll_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Product>();
            ProductData[] list = new List<ProductData>().ToArray();
            try
            {
                GeneralMessageOfArrayOfProductDatanqvBR1WM result = (GeneralMessageOfArrayOfProductDatanqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Product(list[i]));
                }
                loadList(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwSave_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_isNew)
                {
                    e.Result = _controller.AddProduct((ProductData)e.Argument);
                }
                else
                {
                    e.Result = _controller.UpdateProduct((ProductData)e.Argument);
                }
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfboolean() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var result = (GeneralMessageOfboolean)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwRemove_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.RemoveProduct((ProductData)e.Argument);
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfboolean() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwRemove_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var result = (GeneralMessageOfboolean)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                MessageBox.Show("El dato se eliminó correctamente.", "Exitoso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwGetCategories_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllCategories();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfCataloguenqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetCategories_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Catalog>();
            Catalogue[] list = new List<Catalogue>().ToArray();
            try
            {
                GeneralMessageOfArrayOfCataloguenqvBR1WM result = (GeneralMessageOfArrayOfCataloguenqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Catalog(list[i]));
                }
                loadCategories(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            _catalogsLoaded++;
            EnableComboboxes(catalogsComboBoxes.Count == _catalogsLoaded);
        }

        private void bgwGetMeasurements_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllMeasurements();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfCataloguenqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetMeasurements_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Catalog>();
            Catalogue[] list = new List<Catalogue>().ToArray();
            try
            {
                GeneralMessageOfArrayOfCataloguenqvBR1WM result = (GeneralMessageOfArrayOfCataloguenqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Catalog(list[i]));
                }
                loadMeasurements(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            _catalogsLoaded++;
            EnableComboboxes(catalogsComboBoxes.Count == _catalogsLoaded);
        }
    }
}
