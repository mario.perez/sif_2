﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;

namespace UI.Sif2
{
    public partial class FormDenominacion : FormGenerico
    {

        List<DenominationData> lista = new List<DenominationData>();

        public FormDenominacion()
        {
            InitializeComponent();
            HabilitarPermisos();
        }

        public override void CargarGrid()
        {
            try
            {               

                var txtCodigo = (TextBox)this.Controls.Find(Codigo_Filtro_NotNull.Name.ToString(),true)[0];

                if (string.IsNullOrEmpty(txtCodigo.Text)) {
                    lista = new CatalogsClient().GetAllDenominations().Data.ToList();
                }
                else
                {
                    lista = new List<DenominationData>();
                    var deno = new CatalogsClient().GetDenomination(txtCodigo.Text).Data;
                    if (deno != null) {
                        lista.Add(deno);
                    }
                    
                }

                if (Moneda_NotNull.DataSource == null) {
                    Moneda_NotNull.DataSource = new CatalogsClient().GetAllCurrencies().Data.Select(x => new { Codigo = x.Id, Nombre = x.Name }).ToList();
                    Moneda_NotNull.DisplayMember = "Nombre";
                    Moneda_NotNull.ValueMember = "Codigo";
                }

                dgv.Rows.Clear();
                foreach (var item in lista)
                {                    
                    dgv.Rows.Add(item.Id
                        , item.Description
                        , item.CurrencyIso.Id
                        , item.Factor
                        , item.IsActive
                        , item.IsCash);
                }                

                lblTotalRegistro.Text = "Total de Registro: " + lista.Count().ToString();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void Agregar()
        {
            new FormAdminDenominacion("I", dgv.Columns, "Agregar Denominación", null).ShowDialog();

        }

        public override void Consultar()
        {
            var denominacion = ObtenerSeleccionado();
            if (denominacion != null)
            {
                new FormAdminDenominacion("C", dgv.Columns, "Consultar Denominación", denominacion).ShowDialog();
            }
            
        }

        public override void Modificar()
        {
            var denominacion = ObtenerSeleccionado();
            if (denominacion != null)
            {
                new FormAdminDenominacion("U", dgv.Columns, "Modificar Denominación",denominacion).ShowDialog();
            }
             
        }

        private DenominationData ObtenerSeleccionado()
        {
            try
            {
                if (dgv.CurrentRow != null)
                {
                    var codigo = dgv.CurrentRow.Cells["Codigo_Filtro_NotNull"].Value.ToString();
                    return new CatalogsClient().GetDenomination(codigo).Data;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return null;
            }
        }

        public override void RefrescarDatos()
        {
            CargarGrid();
        }

        public override void Eliminar()
        {
            try
            {
                var respuesta = new CatalogsClient().RemoveDenomination(ObtenerSeleccionado());
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
    }
}
