﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Sif2
{
    public class MyControl : UserControl
    {
        public MyControl() { }
        public enum DisplayOption
        {
            ToList,
            ToCloud
        }        
        
        [Browsable(true),
        Category("Appearance"),
        DefaultValue(DisplayOption.ToList),
        Description("Indica el tipo de desplegado usado para la visualización.")]
        public DisplayOption Display { get; set; }

        [
            Browsable(true),
            Category("Diseño"),
            Description("Seleccionar el formulario a mostrar")
        ]
        public Form formulario { get; set; }
    }
}
