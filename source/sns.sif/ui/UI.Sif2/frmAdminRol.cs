﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.Security;
using serviceLayer.Security.SecurityServiceReference;

namespace UI.Sif2
{
    public partial class frmAdminRol : Form, Interface1
    {
        private string operacion = string.Empty;
        private RolData rol;
        private SecurityClient _controller;
        private UsuarioRolData[] ListaUsuarioRoles = new List<UsuarioRolData>().ToArray();
       
        public frmAdminRol(string op, RolData r)
        {
            InitializeComponent();
            _controller = new SecurityClient();
            operacion = op;
            rol = r;            
            NombrarFormulario();            
            LlenarFormulario_Rol();
            HabilitarControles_por_TipoOperacion();
            //MetodosGenericos.FormatearDataGridView(dgvUsuario);            
            RefrescarCargaPrivilegiosAsignados();            
        }

        private void CargarUsuarios() {
            try
            {
                var lista = _controller.GetAllUsuarioRol(0,rol.Id,0,"S").Data.ToArray();
                ListaUsuarioRoles = lista;
                LlenarDataGrid(string.Empty,string.Empty);
                                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void LlenarDataGrid(string loginUsuario, string nombreUsuario)
        {

            //dgvUsuario.CellValueChanged -= dgvUsuario_CellValueChanged;
            //MetodosGenericos.FormatearDataGridView(dgvUsuario);
            //dgvUsuario.CellValueChanged += dgvUsuario_CellValueChanged;
            dgvUsuario.Rows.Clear();

            foreach (var item in ListaUsuarioRoles)
            {

                if (
                       (string.IsNullOrEmpty(loginUsuario) || string.IsNullOrWhiteSpace(loginUsuario) || item.LoginUsuario.Contains(loginUsuario))
                       && (string.IsNullOrEmpty(nombreUsuario) || string.IsNullOrWhiteSpace(nombreUsuario) || item.NombreUsuario.Contains(nombreUsuario))
                    )
                {
                    dgvUsuario.Rows.Add(
                        item.Id
                        , item.LoginUsuario
                        , item.NombreUsuario
                        , item.UsuarioId
                    );

                    // Indicamos que los registros mayores al primero son de lectura
                    dgvUsuario.Rows[dgvUsuario.Rows.Count - 1].ReadOnly = true;

                }

            }
            // Al count le restamos -1 para que no tome el cuenta el registro usado para filtrar el grid
            lbl_totalRegistro.Text = "Total Registros: " + (dgvUsuario.Rows.Count ).ToString("n2");
        }

        private void RefrescarCargaPrivilegiosAsignados() {
            trvPrivilegios.Nodes.Clear();
            CargarPrivilegiosAsignados(trvPrivilegios, null, 0);            
        }

        private void NombrarFormulario()
        {
            switch (operacion)
            {
                case "I":
                    Text = "Agregar Rol";
                    break;
                case "U":
                    Text = "Modificar Rol";
                    break;
                case "S":
                    Text = "Consultar Rol";
                    break;
            }
        }

        private void LlenarFormulario_Rol() {
            try
            {
                if (!operacion.Equals("I")) {
                    txtNombre.Text = rol.Nombre;
                    txtDescripcion.Text = rol.Descripcion;
                    chkActivo.Checked = rol.Activo;
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void HabilitarControles_por_TipoOperacion()
        {
            if (operacion.Equals("S"))
            {
                txtNombre.ReadOnly = true;
                txtDescripcion.ReadOnly = true;
                groupBox1.Enabled = false;
                btnGuardarRol.Enabled = false;
            }
            if (!operacion.Equals("U")) {
                barraOperacionPrivilegio.Enabled = false;
                btnNuevoUsuario.Enabled = false;
                btnEliminarUsuario.Enabled = false;
            }
            
        }

        private void frmAdminRol_KeyDown(object sender, KeyEventArgs e)
        {
            Teclado_AccesoDirecto(e);
        }

        private void Teclado_AccesoDirecto(KeyEventArgs e) {
            try
            {
                if (e.KeyCode == Keys.Escape) {
                    Close();
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnGuardarPrivilegio_Click(object sender, EventArgs e)
        {
            GuardarOperacion();
        }

        private void GuardarOperacion() {
            try
            {
                ActualizarInformacionRol();
                GeneralMessageOfboolean resultado = new GeneralMessageOfboolean();

                switch (operacion)
                {
                    case "I":
                        rol.MaqCreacion = Environment.MachineName;
                        rol.FechaCreacion = DateTime.Now;
                        rol.AppCreacion = "Desktop";
                        resultado = _controller.AddRol(rol);                        
                        break;
                    case "U":
                        rol.MaqModificacion = Environment.MachineName;
                        rol.FechaModificacion = DateTime.Now;
                        rol.AppModificacion = "Desktop";
                        resultado = _controller.UpdateRol(rol);
                        break;
                }
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void ActualizarInformacionRol() {
            try
            {
                rol.Nombre = txtNombre.Text;
                rol.Descripcion = txtDescripcion.Text;
                rol.Activo = chkActivo.Checked;
            }
            catch (Exception e)
            {
                MetodosGenericos.MensajeError_Exception(e);
            }
        }

        private void txtNombre_Validating(object sender, CancelEventArgs e)
        {
            ValidarNombre();
        }

        private void ValidarNombre() {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(txtNombre.Text) || string.IsNullOrWhiteSpace(txtNombre.Text)) {
                errorProvider1.SetIconAlignment(txtNombre, ErrorIconAlignment.MiddleLeft);
                errorProvider1.SetError(txtNombre,"Nombre no puede ser vacio");
            }
        }

        private void btnNuevoPrivilegio_Click(object sender, EventArgs e)
        {
            new frmPermiso(rol).ShowDialog(this);
        }

        public void ObtenerObjeto<T>(T objetos)
        {
            try
            {
                object obj = objetos;
                bool respuesta = (bool)obj;
                if (respuesta)
                {
                    MetodosGenericos.MensajeInformativo("Privilegios Asignados");
                    RefrescarCargaPrivilegiosAsignados();
                }

            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public void CargarPrivilegiosAsignados(TreeView view, TreeNode nodos, int idPadre)
        {

            PrivilegioData[] list = new List<PrivilegioData>().ToArray();
            var resultado = _controller.GetPrivilegioAsignado(idPadre, rol.Id);

            int id;
            string nombreNodo;

            foreach (var item in resultado.Data)
            {
                id = item.Id;

                nombreNodo = item.Nombre;
                TreeNode node = new TreeNode(nombreNodo);
                node.Name = item.Id.ToString();
                node.Tag = item.CodigoInterno;

                MetodosGenericos.AgregarImagenNodoPrivilegio(node,item.CodigoInterno);

                if (nodos == null)
                {
                    view.Nodes.Add(node);
                }
                else
                {
                    nodos.Nodes.Add(node);
                    if (item.EsPadre)
                    //if (!item.CodigoInterno.Equals("ACCION"))
                    {
                        nodos.Expand();//Que me expanda aquellos nodos que tienen hijos
                    }
                }
                CargarPrivilegiosAsignados(view, node, id);                
            }
        }

        private void btnEliminarPrivilegio_Click(object sender, EventArgs e)
        {
            try
            {
                EliminarPrivilegioRol(trvPrivilegios.SelectedNode);
                MetodosGenericos.MensajeInformativo("Privilegio eliminado");
                RefrescarCargaPrivilegiosAsignados();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }            
        }

        private void EliminarPrivilegioRol(TreeNode nodo) {
           

                // Si nodo tiene hijos, entrara al foreach
                foreach (TreeNode item in nodo.Nodes)
                {
                    //Si el item tiene hijos, llamara al metodo EliminarPermiso

                    if (item.Nodes.Count > 0)
                    {
                        EliminarPrivilegioRol(item);
                    }
                    else
                    {
                        _controller.RemovePrivilegioRol(Int32.Parse(item.Name.ToString()),rol.Id);
                    }
                }
            EliminarNodos_sin_Hijos(nodo);
        }

        private void EliminarNodos_sin_Hijos(TreeNode nodo) {

        // Validar que el nodo a eliminar, tenga padre
        // Si tiene padre, validar si tiene mas de 1 hijo, En caso que tenga solo un hijo...Eliminar el hijo y el padre
        // En caso que tenga mas de dos hijos, eliminar solo el nodo pasado como parametro
          
            if (nodo.Parent != null)
            {
                if (nodo.Parent.Nodes.Count >= 2)
                {
                    _controller.RemovePrivilegioRol(Int32.Parse(nodo.Name.ToString()),rol.Id);
                }                    
                else
                {
                    TreeNode nodoPadre = nodo.Parent;

                    _controller.RemovePrivilegioRol(Int32.Parse(nodo.Name.ToString()), rol.Id);

                    EliminarNodos_sin_Hijos(nodoPadre);
                }
            }
            else
            {
                _controller.RemovePrivilegioRol(Int32.Parse(nodo.Name.ToString()), rol.Id);

            }            
        }

        private void btnGuardarRol_Click(object sender, EventArgs e)
        {
            GuardarOperacion();
        }

        private void dgvUsuario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvUsuario_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == 0)
                {
                    if (dgvUsuario.CurrentRow != null)
                    {

                        string login = dgvUsuario.CurrentRow.Cells["LoginUsuario"].Value != null ? dgvUsuario.CurrentRow.Cells["LoginUsuario"].Value.ToString() : string.Empty;
                        string nombreCompleto = dgvUsuario.CurrentRow.Cells["NombreCompleto"].Value != null ? dgvUsuario.CurrentRow.Cells["NombreCompleto"].Value.ToString() : string.Empty;

                        LlenarDataGrid(login, nombreCompleto);
                    }
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Evento Cargar Usuarios se va a cargar cuando el usuario cambie de tabs
            if (tabControl1.SelectedIndex == 1 && ListaUsuarioRoles.Count() == 0 ) {
                CargarUsuarios();
            }
        }

        private void btnRefrescarUsuario_Click(object sender, EventArgs e)
        {
            CargarUsuarios();
        }

        private void btnConsultarUsuario_Click(object sender, EventArgs e)
        {
            ConsultarUsuario();
        }       

        private void EliminarUsuario() {
            try
            {
                if (dgvUsuario.CurrentRow != null) {
                    if (dgvUsuario.CurrentRow.Index > 0)
                    {
                        int id = Int32.Parse(dgvUsuario.CurrentRow.Cells["Id"].Value.ToString());
                        var remove = _controller.RemoveUsuarioRol(0,0,id,"D");
                        MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(remove);
                        if (remove.CodeResponse == 0)
                        {
                            CargarUsuarios();
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnEliminarUsuario_Click(object sender, EventArgs e)
        {
            EliminarUsuario();
        }

        private void ConsultarUsuario()
        {
            var usu = ObtenerUsuarioSeleccionado();
            if (usu != null)
            {
                new frmAdminUsuario("S", usu).ShowDialog();
            }
        }

        private UsuarioData ObtenerUsuarioSeleccionado()
        {
            UsuarioData UsuarioSeleccionado = null;

            try
            {
                if (dgvUsuario.CurrentRow != null && dgvUsuario.CurrentRow.Index > 0)
                {
                    UsuarioSeleccionado = new UsuarioData();

                    UsuarioSeleccionado.Id = Convert.ToInt32(dgvUsuario.CurrentRow.Cells["UsuarioId"].Value);
                    UsuarioSeleccionado.FechaCreacion = DateTime.Now;
                    UsuarioSeleccionado.FechaModificacion = DateTime.Now;
                    UsuarioSeleccionado = _controller.GetAllUsuario(UsuarioSeleccionado).Data[0];
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
            return UsuarioSeleccionado;
        }

        private void btnNuevoUsuario_Click(object sender, EventArgs e)
        {
            new frmUsuarioLista(rol.Id).ShowDialog();
        }

        private void btnClonarRol_Click(object sender, EventArgs e)
        {
            new frmRoleLista(MetodosGenericos.UsuarioConectado.Id,rol.Id,"frmAdminRol").ShowDialog();
        }
    }
}