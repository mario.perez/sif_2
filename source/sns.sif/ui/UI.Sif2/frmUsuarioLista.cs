﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.Security;
using serviceLayer.Security.SecurityServiceReference;

namespace UI.Sif2
{
    public partial class frmUsuarioLista : Form
    {
        private SecurityClient _controller;
        private RolData rolFiltro = new RolData();      
        private UsuarioRolData[] ListaUsuario = new List<UsuarioRolData>().ToArray();
        int RolId;

        public frmUsuarioLista(int rolId)
        {
            InitializeComponent();
            _controller = new SecurityClient();
            RolId = rolId;
        }

        private UsuarioData ObtenerClienteSeleccionado()
        {
            UsuarioData UsuarioSeleccionado = null;

            try
            {
                if (dgvUsuario.CurrentRow != null && dgvUsuario.CurrentRow.Index > 0)
                {
                    UsuarioSeleccionado = new UsuarioData();
                    var id = Convert.ToInt32(dgvUsuario.CurrentRow.Cells["Id"].Value);
                    UsuarioSeleccionado.Id = id;
                    UsuarioSeleccionado.FechaCreacion = DateTime.Now;
                    UsuarioSeleccionado.FechaModificacion = DateTime.Now;

                    UsuarioSeleccionado = _controller.GetAllUsuario(UsuarioSeleccionado).Data[0];
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
            return UsuarioSeleccionado;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //CrearRol();
        }
        private void ConsultarUsuario()
        {
            try
            {
                var usu = ObtenerClienteSeleccionado();
                if (usu != null) {
                    var form = new frmAdminUsuario("S", usu);
                    form.Text = "Consultar Usuario";
                    form.ShowDialog();
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        
        private void btnModificar_Click(object sender, EventArgs e)
        {
            //ModificarRol();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ConsultarUsuario();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GetAllRol() {
            try
            {                
                bgw_GetUsuario.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void bgw_GetRol_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllUsuarioRol_NoUsuario(RolId);
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfUsuarioDatanqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgw_GetRol_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ObtenerRol(e);
        }

        private void ObtenerRol(RunWorkerCompletedEventArgs e) {
            UsuarioRolData[] list = new List<UsuarioRolData>().ToArray();
            try
            {
                GeneralMessageOfArrayOfUsuarioRolDatanqvBR1WM result = (GeneralMessageOfArrayOfUsuarioRolDatanqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);

                list = result.Data;
                ListaUsuario = result.Data;                

                LlenarDataGrid(string.Empty, string.Empty);

            }
            catch (ArgumentException ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void LlenarDataGrid(string login, string nombreCompleto)
        {

            dgvUsuario.CellValueChanged -= dgvRoles_CellValueChanged;
            MetodosGenericos.FormatearDataGridView(dgvUsuario);
            dgvUsuario.CellValueChanged += dgvRoles_CellValueChanged;            

            foreach (var item in ListaUsuario)
            {

                if (
                       (string.IsNullOrEmpty(login) || string.IsNullOrWhiteSpace(login) || item.LoginUsuario.Contains(login))
                       && (string.IsNullOrEmpty(nombreCompleto) || string.IsNullOrWhiteSpace(nombreCompleto) || item.NombreUsuario.Contains(nombreCompleto))                       
                    )
                {
                    dgvUsuario.Rows.Add(
                        item.UsuarioId
                        , item.LoginUsuario
                        , item.NombreUsuario
                    );

                    // Indicamos que los registros mayores al primero son de lectura
                    dgvUsuario.Rows[dgvUsuario.Rows.Count - 1].ReadOnly = true;                    
                }

            }
            // Al count le restamos -1 para que no tome el cuenta el registro usado para filtrar el grid
            lbl_totalRegistro.Text = "Total Registros: " + (dgvUsuario.Rows.Count - 1).ToString();
        }

        private void dgvRoles_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == 0)
            {
                if (dgvUsuario.CurrentRow != null)
                {

                    string Nombre = dgvUsuario.CurrentRow.Cells["LoginUsuario"].Value != null ? dgvUsuario.CurrentRow.Cells["LoginUsuario"].Value.ToString() : string.Empty;
                    string Descripcion = dgvUsuario.CurrentRow.Cells["NombreCompleto"].Value != null ? dgvUsuario.CurrentRow.Cells["NombreCompleto"].Value.ToString() : string.Empty;                    
                    //bool activo = dgvRoles.CurrentRow.Cells["Activo"].Value != null ? bool.Parse(dgvRoles.CurrentRow.Cells["Activo"].Value.ToString()) : false;

                    LlenarDataGrid(Nombre, Descripcion);
                }
            }
        }

        private void frmRole_Load(object sender, EventArgs e)
        {
            GetAllRol();
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            GetAllRol();
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CrearRol();
        }

        private void modificartoolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ModificarRol();
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {            
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarUsuario();
        }

        private void btnFiltros_Click(object sender, EventArgs e)
        {                        
            //MetodosGenericos.MostrarOcultarPanelFiltro(anchoPanelFiltro,panel1);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            //LimpiarFiltros();   
        }
       

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            GuardarRolSeleccionado();
        }

        private void GuardarRolSeleccionado() {
            try
            {
                if (dgvUsuario.CurrentRow != null) {
                    if (dgvUsuario.CurrentRow.Index > 0) {
                        int usuId = Int32.Parse(dgvUsuario.CurrentRow.Cells["Id"].Value.ToString());
                        var resultado = _controller.AddUsuarioRol(usuId, RolId,0,"I");
                        MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
                    }
                    else
                    {
                        MetodosGenericos.MensajeInformativo("Debe seleccionar un Usuario.");
                    }
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }


    }
}
