﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace UI.Sif2
{
    public static class MetodosGenericos
    {
        public static int RolConectado = 7;
        //public static UsuarioData UsuarioConectado;

        public static void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void MensajeError_Exception(Exception ex)
        {
            MetodosGenericos.MensajeError(ex.Message + ": " + Environment.NewLine + Environment.NewLine + (ex.InnerException != null ? ex.InnerException.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace : ex.StackTrace));
        }

        public static void MensajeInformativo(string mensaje)
        {
            MessageBox.Show(mensaje, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void MensajeAdvertencia(string mensaje)
        {
            MessageBox.Show(mensaje, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static DialogResult MensajeSINO(string mensaje)
        {
            DialogResult result = MessageBox.Show(mensaje, "Confirmación", MessageBoxButtons.YesNo);
            return result;
        }

        public static DialogResult MensajeOK(string mensaje)
        {
            DialogResult result = MessageBox.Show(mensaje, "Confirmación", MessageBoxButtons.OK);
            return result;
        }

        public static Image ByteToImage(byte[] imageBytes)
        {
            // Convert byte[] to Image
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = new Bitmap(ms);
            return image;
        }

        public static byte[] ImageToByte(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();
                return imageBytes;
            }
        }

        public static void MostrarOcultarColumna(DataGridViewColumn columna, ToolStripMenuItem menu) {
            if (columna.Visible) {
                columna.Visible = false;
                menu.ForeColor = Color.DarkGray;
            }
            else
            {
                columna.Visible = true;
                menu.ForeColor = Color.Black;
            }
        }

        public static void RestaurarColumnas(ContextMenuStrip menu, DataGridView view) {
            foreach (ToolStripMenuItem item in menu.Items)
            {
                item.ForeColor = Color.Black;
            }
            foreach (DataGridViewColumn item in view.Columns)
            {
                if (item.Visible == false && !item.HeaderText.Equals("Id")) {
                    item.Visible = true;
                }
            }
        }
        /*
        public static void MostrarMensaje_RespuestaServidor_GestionCrud(GeneralMessageOfboolean resultado) {
            try
            {
                if (resultado.CodeResponse == 0)
                {
                    MetodosGenericos.MensajeInformativo(resultado.MessageResponse);

                }
                else
                {
                    if (resultado.CodeResponse == 1)
                    {
                        MetodosGenericos.MensajeAdvertencia(resultado.MessageResponse);
                    }
                    else
                    {
                        if (resultado.CodeResponse == 2)
                        {
                            MetodosGenericos.MensajeError(resultado.MessageResponse);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
        */
        /*
        public static void MostrarMensaje_RespuestaServidor_GestionCrud(GeneralMessageOfboolean1 resultado)
        {
            try
            {
                if (resultado.CodeResponse == 0)
                {
                    MetodosGenericos.MensajeInformativo(resultado.MessageResponse);

                }
                else
                {
                    if (resultado.CodeResponse == 1)
                    {
                        MetodosGenericos.MensajeAdvertencia(resultado.MessageResponse);
                    }
                    else
                    {
                        if (resultado.CodeResponse == 2)
                        {
                            MetodosGenericos.MensajeError(resultado.MessageResponse);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
        */
        // Metodo se encarga de Limpiar el datagridview que recibe y le deja la primera fila
        // para ser usado como filtro para el usuario
        public static void FormatearDataGridView(DataGridView dgv) {
            try
            {
                if (dgv.Rows.Count > 0)
                {
                    // Guardamos en memoria la primera fija que es lo que quiere filtrar el usuario
                    DataGridViewRow row = dgv.Rows[0];
                    dgv.Rows.Clear();// Limpiamos el datagridview

                    dgv.Rows.Add(row); // ingresamos como primera fila los campos que quiere filtrar
                }
                else
                {
                    dgv.Rows.Add();
                }

                // Le aplicamos un color al primer Row del datagridview
                dgv.Rows[0].DefaultCellStyle.BackColor = Color.LightBlue;
                // Le indicamos que la primera fila no es de solo lectura
                dgv.Rows[0].ReadOnly = false;
                // Indicamos que la primera fila se mantenga congelado aunque el usuario se desplace hacia abajo
                dgv.Rows[0].Frozen = true;

                // Recorremos las columnas para buscar una que sea tipo booleano y lo activamos
                for (int i = 0; i < dgv.Columns.Count; i++)
                {
                    //if (dgv.Rows[0].Cells[i].ValueType.Name.Equals("Boolean") && dgv.Rows.Count == 1)
                    //{
                    //    dgv.Rows[0].Cells[i].Value = true;
                    //}
                    dgv.Rows[0].Cells[i].ReadOnly = false;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }

        }

        public static void LimpiarFiltrosGrid(DataGridView dgv)
        {
            try
            {
                for (int i = 0; i < dgv.Columns.Count; i++)
                {
                    if (dgv.Rows[0].Cells[i].ValueType.Name.Equals("Boolean") && dgv.Rows.Count == 1)
                    {
                        dgv.Rows[0].Cells[i].Value = true;
                    }
                    else
                    {
                        dgv.Rows[0].Cells[i].Value = null;
                    }
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public static void MostrarOcultarPanelFiltro(int ancho, Panel panel) {
            try
            {
                if (panel.Width == ancho) {
                    panel.Width = 0;
                }
                else
                {
                    panel.Width = ancho;
                }
            }
            catch (Exception ex)
            {
                MensajeError_Exception(ex);
            }
        }

        public static void MostrarOcultarSplitFiltro(SplitContainer container)
        {
            if (container.Panel1Collapsed)
            {
                container.Panel1Collapsed = false;
            }
            else
            {
                container.Panel1Collapsed = true;
            }
        }

        public static void AgregarImagenNodoPrivilegio(TreeNode node, string CodigoInterno) {
            try
            {
                node.ImageIndex = node.SelectedImageIndex = CodigoInterno.Equals("MOD") ? 1 :
                    CodigoInterno.Equals("TAB") ? 2 :
                    CodigoInterno.Equals("PANEL") ? 3 :
                    CodigoInterno.Equals("FORM") ? 4 :
                    CodigoInterno.Equals("ACCION") ? 5 : 0;
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
        /*
        public static void Habilitar_Operacion_PrivilegioAccion(ToolStrip barra, List<PrivilegioData> operaciones)
        {
            try
            {
                foreach (ToolStripButton item in barra.Items)
                {
                    if (item.Tag != null)
                    {
                        if (operaciones.Select(x => x.CodigoTag).Contains(item.Tag))
                        {
                            item.Visible = true;
                            item.Enabled = true;
                        }
                        else
                        {
                            item.Visible = false;
                            item.Enabled = false;
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
        */
        public static TableLayoutPanel CreacionLayoutEntidad(DataGridViewColumnCollection columnas, string solicitante)
        {
            try
            {
                TableLayoutPanel layout = new TableLayoutPanel();

                layout = solicitante.Equals("Filtro") ? CrearFiltroTableLayoutPanel(columnas)
                         : CrearGestionTableLayoutPanel(columnas);

                return layout;
            }
            catch (Exception ex)
            {
                MensajeError_Exception(ex);
                return null;
            }
        }
        private static TableLayoutPanel CrearFiltroTableLayoutPanel(DataGridViewColumnCollection columnas)
        {

            int cantfiltros = columnas.Count;

            TableLayoutPanel tableLayoutPanel2 = new TableLayoutPanel();
            tableLayoutPanel2.ColumnCount = 1;            
            tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            tableLayoutPanel2.Name = "layoutFiltro";
            tableLayoutPanel2.RowCount = cantfiltros + 1;         
            tableLayoutPanel2.Size = new System.Drawing.Size(234, 410);
            tableLayoutPanel2.TabIndex = 1;
            tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            try
            {
                int contador = 0;
                foreach (DataGridViewColumn item in columnas)
                {                    
                    if (item.Name.Contains("Filtro"))
                    {
                        tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));

                        TableLayoutPanel tableLayoutPanel3 = new TableLayoutPanel();
                        tableLayoutPanel3.ColumnCount = 2;
                        tableLayoutPanel3.RowCount = 1;
                        tableLayoutPanel3.Dock = DockStyle.Fill;
                        tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
                        tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
                        tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));


                        Label label = new Label();
                        label.Text = item.HeaderText;
                        label.Dock = DockStyle.Fill;
                        label.TextAlign = ContentAlignment.MiddleLeft;


                        if (item.CellType.Name.Equals("DataGridViewTextBoxCell"))
                        {

                            TextBox texbox = new TextBox();
                            texbox.Name = item.Name;
                            texbox.Dock = DockStyle.Fill;
                            texbox.MaxLength = ((DataGridViewTextBoxColumn)item).MaxInputLength;

                            tableLayoutPanel3.Controls.Add(label, 0, 0);
                            tableLayoutPanel3.Controls.Add(texbox, 1, 0);
                        }
                        else
                        {
                            if (item.CellType.Name.Equals("DataGridViewCheckBoxCell"))
                            {
                                CheckBox check = new CheckBox();
                                check.Name = item.Name;
                                check.Dock = DockStyle.Fill;
                                check.Text = item.HeaderText;
                                tableLayoutPanel3.Controls.Add(check, 1, 0);
                            }
                            else
                            {
                                if (item.CellType.Name.Equals("DataGridViewComboBoxCell"))
                                {

                                    ComboBox combo = new ComboBox();
                                    combo.Name = item.Name;
                                    combo.Dock = DockStyle.Fill;

                                    tableLayoutPanel3.Controls.Add(label, 0, 0);
                                    tableLayoutPanel3.Controls.Add(combo, 1, 0);
                                }
                            }
                            
                        }

                        tableLayoutPanel2.Controls.Add(tableLayoutPanel3,0,contador);

                        contador += 1;
                    }
                    else
                    {

                    }
                }
                tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            }
            catch (Exception ex)
            {
                MensajeError_Exception(ex);
            }
            return tableLayoutPanel2;
        }

        private static TableLayoutPanel CrearGestionTableLayoutPanel(DataGridViewColumnCollection columnas)
        {

            int cantfiltros = columnas.Count;

            TableLayoutPanel tableLayoutPanel2 = new TableLayoutPanel();
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            tableLayoutPanel2.Name = "layoutAdmin";
            tableLayoutPanel2.RowCount = cantfiltros+1;
            tableLayoutPanel2.Size = new System.Drawing.Size(234, 410);
            tableLayoutPanel2.TabIndex = 1;
            tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            try
            {
                int contador = 0;
                foreach (DataGridViewColumn item in columnas)
                {
                    
                    tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));

                    TableLayoutPanel tableLayoutPanel3 = new TableLayoutPanel();
                    tableLayoutPanel3.Dock = DockStyle.Fill;

                    tableLayoutPanel3.RowCount = 1;                    
                    tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));

                    tableLayoutPanel3.ColumnCount = 2;
                    tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
                    tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));


                    Label label = new Label();
                    label.Text = item.HeaderText;
                    label.Dock = DockStyle.Fill;
                    label.TextAlign = ContentAlignment.MiddleLeft;

                    if (item.CellType.Name.Equals("DataGridViewTextBoxCell"))
                    {


                        if (item.Name.Contains("NotNull"))
                        {
                            label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                            label.ForeColor = System.Drawing.SystemColors.HotTrack;
                        }                            

                        TextBox texbox = new TextBox();
                        texbox.Name = item.Name;
                        texbox.Dock = DockStyle.Fill;
                        texbox.MaxLength = ((DataGridViewTextBoxColumn)item).MaxInputLength;

                        tableLayoutPanel3.Controls.Add(label, 0, 0);
                        tableLayoutPanel3.Controls.Add(texbox, 1, 0);
                    }
                    else
                    {
                        if (item.CellType.Name.Equals("DataGridViewCheckBoxCell"))
                        {
                            CheckBox check = new CheckBox();
                            check.Name = item.Name;
                            check.Dock = DockStyle.Fill;
                            check.Text = item.HeaderText;

                            if (item.Name.Contains("NotNull"))
                            {
                                check.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                                check.ForeColor = System.Drawing.SystemColors.HotTrack;
                            }

                            tableLayoutPanel3.Controls.Add(check, 1, 0);
                        }
                        else
                        {
                            if (item.CellType.Name.Equals("DataGridViewComboBoxCell"))
                            {

                                ComboBox combo = new ComboBox();
                                combo.Name = item.Name;
                                combo.Dock = DockStyle.Fill;

                                if (item.Name.Contains("NotNull"))
                                {
                                    label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                                    label.ForeColor = System.Drawing.SystemColors.HotTrack;
                                }

                                tableLayoutPanel3.Controls.Add(label, 0, 0);
                                tableLayoutPanel3.Controls.Add(combo, 1, 0);
                            }
                        }
                    }

                    tableLayoutPanel2.Controls.Add(tableLayoutPanel3, 0, contador);
                    contador += 1;                    
                }
                tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));                
            }
            catch (Exception ex)
            {
                MensajeError_Exception(ex);
            }
            return tableLayoutPanel2;
        }


        public static List<T> ObtenerControles<T>(this Control container) where T : Control
        {

            List<T> controls = new List<T>();

            foreach (Control c in container.Controls)
            {
                if (c is T)
                    controls.Add((T)c);

                controls.AddRange(ObtenerControles<T>(c));
            }
            return controls;
        }

    }
}
