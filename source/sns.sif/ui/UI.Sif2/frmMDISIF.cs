﻿using System;
using System.Windows.Forms;


namespace UI.Sif2
{
    public partial class frmMDISIF : RibbonForm
    {
        //private CatalogsClient _controller;

        public frmMDISIF()
        {
            InitializeComponent();
            //_controller = new CatalogsClient();
            HabilitarPermisosFormularios();
            //UsuarioConectado.Text = MetodosGenericos.UsuarioConectado.NombreCompleto;
        }

        private void HabilitarPermisosFormularios()
        {
            /*
            try
            {
                //var permisos = new SecurityClient().GetPrivilegioUsuario(MetodosGenericos.UsuarioConectado.Id).Data.ToList();

                foreach (var Modulos_Nivel1 in rbnMenu.Tabs)
                {

                    bool bandera_Activacion_modulo = false;

                    foreach (var Paneles_Nivel2 in Modulos_Nivel1.Panels)
                    {
                        bool bandera_Activacion_panel = false;

                        foreach (var formulario in Paneles_Nivel2.Items)
                        {
                            if (permisos.Select(x => x.CodigoTag).Contains(formulario.Tag))
                            {

                                formulario.Visible = true;

                                bandera_Activacion_panel = true;


                            }
                            else
                            {
                                formulario.Visible = false;
                            }

                        }

                        Paneles_Nivel2.Visible = bandera_Activacion_panel;

                        if (bandera_Activacion_panel)
                            bandera_Activacion_modulo = true;

                    }
                    Modulos_Nivel1.Visible = bandera_Activacion_modulo;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
            */
        }


        private void TSMItemExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void frmMDISIF_FormClosing(object sender, FormClosingEventArgs e)
        {
            var window = MessageBox.Show(
                        "¿Está seguro que desea salir de SIF?",
                        "Cerrar",
                        MessageBoxButtons.YesNo);

            e.Cancel = (window == DialogResult.No);
        }

        
        private void proveedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void rbbWarehouse_Click(object sender, EventArgs e)
        {
            frmWarehouse warehouse = new frmWarehouse();
            //FormWareHouse warehouse = new FormWareHouse();
            warehouse.MdiParent = this;
            warehouse.Show();
        }

        private void rbbCategory_Click(object sender, EventArgs e)
        {
            //var category = new FormCategory();
            //FormSalesman category = new FormSalesman();
            var category = new frmCategory();
            category.MdiParent = this;
            category.Show();
        }

        private void rbbCashbox_Click(object sender, EventArgs e)
        {
            var cashbox = new frmCashbox();
            cashbox.MdiParent = this;
            cashbox.Show();
        }

        private void frmMDISIF_Load(object sender, EventArgs e)
        {

        }

        private void rbbDenomination_Click(object sender, EventArgs e)
        {
            //FormDenominacion denomination = new FormDenominacion();
            //denomination.MdiParent = this;
            //denomination.Show();
        }

        private void rbbMeasurement_Click(object sender, EventArgs e)
        {
            frmMeasurement measurement = new frmMeasurement();
            //FormMeasurement measurement = new FormMeasurement();
            measurement.MdiParent = this;
            measurement.Show();
        }

        private void rbbSalesman_Click(object sender, EventArgs e)
        {
            var salesman = new FormSalesMan();
            salesman.MdiParent = this;
            salesman.Show();
        }

        private void rbbProvider_Click(object sender, EventArgs e)
        {
            var provider = new frmProvider();
            provider.MdiParent = this;
            provider.Show();
        }

        private void rbbProducts_Click(object sender, EventArgs e)
        {
            //frmProduct product = new frmProduct();
            //product.MdiParent = this;
            //product.Show();
        }

        private void rbbParameters_Click(object sender, EventArgs e)
        {
            frmConfiguration configuration = new frmConfiguration();
            configuration.MdiParent = this;
            configuration.Show();
        }

        private void romExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void rbbOutput_Click(object sender, EventArgs e)
        {
            //frmOutput Output = new frmOutput();
            //Output.MdiParent = this;
            //Output.Show();
        }

        private void rbbBill_Click(object sender, EventArgs e)
        {
            //frmBill Bill = new frmBill();
            //Bill.MdiParent = this;
            //Bill.Show();
        }
        private void rbbInput_Click(object sender, EventArgs e)
        {
            //frmInput Input = new frmInput();
            //Input.MdiParent = this;
            //Input.Show();
        }

        private void rbbCliente_Click(object sender, EventArgs e)
        {
            //frmCliente adminCliente = new frmCliente();
            //adminCliente.MdiParent = this;
            //adminCliente.Show();
        }

        private void rbbImpuesto_Click(object sender, EventArgs e)
        {
            frmImpuestos adminImpuesto = new frmImpuestos();
            adminImpuesto.MdiParent = this;
            adminImpuesto.Show();
        }

        private void rbbRoles_Click(object sender, EventArgs e)
        {
            //frmRole adminRole = new frmRole();
            //adminRole.MdiParent = this;
            //adminRole.Show();
        }

        private void rbbUsers_Click(object sender, EventArgs e)
        {
            //frmUsuario adminUsuario = new frmUsuario();
            //adminUsuario.MdiParent = this;
            //adminUsuario.Show();
        }

        private void rbbPrivilegios_Click(object sender, EventArgs e)
        {
            //frmPrivilegios adminPrivilegios = new frmPrivilegios();
            //adminPrivilegios.MdiParent = this;
            //adminPrivilegios.Show();
        }

        private void rbbMoneda_Click(object sender, EventArgs e)
        {
            FormMoneda moneda = new FormMoneda();
            moneda.MdiParent = this;
            moneda.Show();
        }
    }
}
