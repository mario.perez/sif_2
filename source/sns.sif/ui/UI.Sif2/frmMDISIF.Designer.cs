﻿namespace UI.Sif2
{
    partial class frmMDISIF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMDISIF));
            this.rbnMenu = new System.Windows.Forms.Ribbon();
            this.romExit = new System.Windows.Forms.RibbonOrbMenuItem();
            this.rbtCatalogs = new System.Windows.Forms.RibbonTab();
            this.rbpBasicCatalogs = new System.Windows.Forms.RibbonPanel();
            this.rbbWarehouse = new System.Windows.Forms.RibbonButton();
            this.rbbCategory = new System.Windows.Forms.RibbonButton();
            this.rbbCashbox = new System.Windows.Forms.RibbonButton();
            this.rbpControl = new System.Windows.Forms.RibbonPanel();
            this.rbbDenomination = new System.Windows.Forms.RibbonButton();
            this.rbbMeasurement = new System.Windows.Forms.RibbonButton();
            this.rbbImpuesto = new System.Windows.Forms.RibbonButton();
            this.rbbMoneda = new System.Windows.Forms.RibbonButton();
            this.rbbContacts = new System.Windows.Forms.RibbonPanel();
            this.rbbSalesman = new System.Windows.Forms.RibbonButton();
            this.rbbProvider = new System.Windows.Forms.RibbonButton();
            this.rbbCliente = new System.Windows.Forms.RibbonButton();
            this.rbtInventory = new System.Windows.Forms.RibbonTab();
            this.rbpBasicInventory = new System.Windows.Forms.RibbonPanel();
            this.rbbProducts = new System.Windows.Forms.RibbonButton();
            this.rbtTransaction = new System.Windows.Forms.RibbonTab();
            this.rbpMonetary = new System.Windows.Forms.RibbonPanel();
            this.rbbBill = new System.Windows.Forms.RibbonButton();
            this.rbbCompra = new System.Windows.Forms.RibbonButton();
            this.rbpPhisical = new System.Windows.Forms.RibbonPanel();
            this.rbbOutput = new System.Windows.Forms.RibbonButton();
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.rbbInput = new System.Windows.Forms.RibbonButton();
            this.rbpReporteria = new System.Windows.Forms.RibbonPanel();
            this.rbbFactura = new System.Windows.Forms.RibbonButton();
            this.rbbConfiguration = new System.Windows.Forms.RibbonTab();
            this.rbpSecurity = new System.Windows.Forms.RibbonPanel();
            this.rbbUsers = new System.Windows.Forms.RibbonButton();
            this.rbbRoles = new System.Windows.Forms.RibbonButton();
            this.rbbPrivilegios = new System.Windows.Forms.RibbonButton();
            this.rbpGeneral = new System.Windows.Forms.RibbonPanel();
            this.rbbParameters = new System.Windows.Forms.RibbonButton();
            this.rbpLocal = new System.Windows.Forms.RibbonPanel();
            this.z = new System.Windows.Forms.RibbonButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.UsuarioConectado = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbnMenu
            // 
            this.rbnMenu.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.rbnMenu.Location = new System.Drawing.Point(0, 0);
            this.rbnMenu.Minimized = false;
            this.rbnMenu.Name = "rbnMenu";
            // 
            // 
            // 
            this.rbnMenu.OrbDropDown.BorderRoundness = 8;
            this.rbnMenu.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.rbnMenu.OrbDropDown.MenuItems.Add(this.romExit);
            this.rbnMenu.OrbDropDown.Name = "";
            this.rbnMenu.OrbDropDown.Size = new System.Drawing.Size(527, 116);
            this.rbnMenu.OrbDropDown.TabIndex = 0;
            this.rbnMenu.OrbImage = ((System.Drawing.Image)(resources.GetObject("rbnMenu.OrbImage")));
            this.rbnMenu.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
            this.rbnMenu.Size = new System.Drawing.Size(926, 140);
            this.rbnMenu.TabIndex = 7;
            this.rbnMenu.Tabs.Add(this.rbtCatalogs);
            this.rbnMenu.Tabs.Add(this.rbtInventory);
            this.rbnMenu.Tabs.Add(this.rbtTransaction);
            this.rbnMenu.Tabs.Add(this.rbbConfiguration);
            this.rbnMenu.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
            this.rbnMenu.Tag = "MODINV";
            this.rbnMenu.Text = "ribbon1";
            this.rbnMenu.UseAlwaysStandardTheme = true;
            // 
            // romExit
            // 
            this.romExit.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.romExit.Image = ((System.Drawing.Image)(resources.GetObject("romExit.Image")));
            this.romExit.LargeImage = ((System.Drawing.Image)(resources.GetObject("romExit.LargeImage")));
            this.romExit.Name = "romExit";
            this.romExit.SmallImage = ((System.Drawing.Image)(resources.GetObject("romExit.SmallImage")));
            this.romExit.Text = "Salir";
            this.romExit.Click += new System.EventHandler(this.romExit_Click);
            // 
            // rbtCatalogs
            // 
            this.rbtCatalogs.Name = "rbtCatalogs";
            this.rbtCatalogs.Panels.Add(this.rbpBasicCatalogs);
            this.rbtCatalogs.Panels.Add(this.rbpControl);
            this.rbtCatalogs.Panels.Add(this.rbbContacts);
            this.rbtCatalogs.Tag = "CATALOGOINV";
            this.rbtCatalogs.Text = "Catálogos";
            // 
            // rbpBasicCatalogs
            // 
            this.rbpBasicCatalogs.Items.Add(this.rbbWarehouse);
            this.rbpBasicCatalogs.Items.Add(this.rbbCategory);
            this.rbpBasicCatalogs.Items.Add(this.rbbCashbox);
            this.rbpBasicCatalogs.Name = "rbpBasicCatalogs";
            this.rbpBasicCatalogs.Tag = "BASICOSCAT";
            this.rbpBasicCatalogs.Text = "Básicos";
            // 
            // rbbWarehouse
            // 
            this.rbbWarehouse.Image = ((System.Drawing.Image)(resources.GetObject("rbbWarehouse.Image")));
            this.rbbWarehouse.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbWarehouse.LargeImage")));
            this.rbbWarehouse.Name = "rbbWarehouse";
            this.rbbWarehouse.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbWarehouse.SmallImage")));
            this.rbbWarehouse.Tag = "BODEGA_BAS";
            this.rbbWarehouse.Text = "Bodegas";
            this.rbbWarehouse.Click += new System.EventHandler(this.rbbWarehouse_Click);
            // 
            // rbbCategory
            // 
            this.rbbCategory.Image = ((System.Drawing.Image)(resources.GetObject("rbbCategory.Image")));
            this.rbbCategory.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbCategory.LargeImage")));
            this.rbbCategory.Name = "rbbCategory";
            this.rbbCategory.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbCategory.SmallImage")));
            this.rbbCategory.Tag = "CATEGORIA_BAS";
            this.rbbCategory.Text = "Categorías";
            this.rbbCategory.Click += new System.EventHandler(this.rbbCategory_Click);
            // 
            // rbbCashbox
            // 
            this.rbbCashbox.Image = ((System.Drawing.Image)(resources.GetObject("rbbCashbox.Image")));
            this.rbbCashbox.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbCashbox.LargeImage")));
            this.rbbCashbox.Name = "rbbCashbox";
            this.rbbCashbox.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbCashbox.SmallImage")));
            this.rbbCashbox.Tag = "CAJAS_BAS";
            this.rbbCashbox.Text = "Cajas";
            this.rbbCashbox.Click += new System.EventHandler(this.rbbCashbox_Click);
            // 
            // rbpControl
            // 
            this.rbpControl.Items.Add(this.rbbDenomination);
            this.rbpControl.Items.Add(this.rbbMeasurement);
            this.rbpControl.Items.Add(this.rbbImpuesto);
            this.rbpControl.Items.Add(this.rbbMoneda);
            this.rbpControl.Name = "rbpControl";
            this.rbpControl.Tag = "CONTROLCAT";
            this.rbpControl.Text = "Control";
            // 
            // rbbDenomination
            // 
            this.rbbDenomination.Image = ((System.Drawing.Image)(resources.GetObject("rbbDenomination.Image")));
            this.rbbDenomination.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbDenomination.LargeImage")));
            this.rbbDenomination.Name = "rbbDenomination";
            this.rbbDenomination.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbDenomination.SmallImage")));
            this.rbbDenomination.Tag = "DENOMI_CONTR";
            this.rbbDenomination.Text = "Denominaciones";
            this.rbbDenomination.Click += new System.EventHandler(this.rbbDenomination_Click);
            // 
            // rbbMeasurement
            // 
            this.rbbMeasurement.Image = ((System.Drawing.Image)(resources.GetObject("rbbMeasurement.Image")));
            this.rbbMeasurement.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbMeasurement.LargeImage")));
            this.rbbMeasurement.Name = "rbbMeasurement";
            this.rbbMeasurement.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbMeasurement.SmallImage")));
            this.rbbMeasurement.Tag = "MEDIDAS_CONTR";
            this.rbbMeasurement.Text = "Medidas";
            this.rbbMeasurement.Click += new System.EventHandler(this.rbbMeasurement_Click);
            // 
            // rbbImpuesto
            // 
            this.rbbImpuesto.Image = global::UI.Sif2.Properties.Resources.iconfinder_13_3319631_32;
            this.rbbImpuesto.LargeImage = global::UI.Sif2.Properties.Resources.iconfinder_13_3319631_32;
            this.rbbImpuesto.Name = "rbbImpuesto";
            this.rbbImpuesto.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbImpuesto.SmallImage")));
            this.rbbImpuesto.Tag = "IMPUESTOS_CONTR";
            this.rbbImpuesto.Text = "Impuestos";
            this.rbbImpuesto.Click += new System.EventHandler(this.rbbImpuesto_Click);
            // 
            // rbbMoneda
            // 
            this.rbbMoneda.Image = global::UI.Sif2.Properties.Resources.moneda_1_;
            this.rbbMoneda.LargeImage = global::UI.Sif2.Properties.Resources.moneda_1_;
            this.rbbMoneda.Name = "rbbMoneda";
            this.rbbMoneda.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbMoneda.SmallImage")));
            this.rbbMoneda.Tag = "MONEDA_CONTR";
            this.rbbMoneda.Text = "Moneda";
            this.rbbMoneda.Click += new System.EventHandler(this.rbbMoneda_Click);
            // 
            // rbbContacts
            // 
            this.rbbContacts.Items.Add(this.rbbSalesman);
            this.rbbContacts.Items.Add(this.rbbProvider);
            this.rbbContacts.Items.Add(this.rbbCliente);
            this.rbbContacts.Name = "rbbContacts";
            this.rbbContacts.Tag = "CONTACTOCAT";
            this.rbbContacts.Text = "Contactos";
            // 
            // rbbSalesman
            // 
            this.rbbSalesman.Image = ((System.Drawing.Image)(resources.GetObject("rbbSalesman.Image")));
            this.rbbSalesman.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbSalesman.LargeImage")));
            this.rbbSalesman.Name = "rbbSalesman";
            this.rbbSalesman.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbSalesman.SmallImage")));
            this.rbbSalesman.Tag = "VENDE_CONTACT";
            this.rbbSalesman.Text = "Vendedores";
            this.rbbSalesman.Click += new System.EventHandler(this.rbbSalesman_Click);
            // 
            // rbbProvider
            // 
            this.rbbProvider.Image = ((System.Drawing.Image)(resources.GetObject("rbbProvider.Image")));
            this.rbbProvider.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbProvider.LargeImage")));
            this.rbbProvider.Name = "rbbProvider";
            this.rbbProvider.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbProvider.SmallImage")));
            this.rbbProvider.Tag = "PROVE_CONTACT";
            this.rbbProvider.Text = "Proveedores";
            this.rbbProvider.Click += new System.EventHandler(this.rbbProvider_Click);
            // 
            // rbbCliente
            // 
            this.rbbCliente.Image = global::UI.Sif2.Properties.Resources.clientesFinal_1_;
            this.rbbCliente.LargeImage = global::UI.Sif2.Properties.Resources.clientesFinal_1_;
            this.rbbCliente.Name = "rbbCliente";
            this.rbbCliente.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbCliente.SmallImage")));
            this.rbbCliente.Tag = "CLIENT_CONTACT";
            this.rbbCliente.Text = "Clientes";
            this.rbbCliente.Click += new System.EventHandler(this.rbbCliente_Click);
            // 
            // rbtInventory
            // 
            this.rbtInventory.Name = "rbtInventory";
            this.rbtInventory.Panels.Add(this.rbpBasicInventory);
            this.rbtInventory.Tag = "INVENTARIOINV";
            this.rbtInventory.Text = "Inventario";
            // 
            // rbpBasicInventory
            // 
            this.rbpBasicInventory.Items.Add(this.rbbProducts);
            this.rbpBasicInventory.Name = "rbpBasicInventory";
            this.rbpBasicInventory.Tag = "BASICO_INV";
            this.rbpBasicInventory.Text = "Básicos";
            // 
            // rbbProducts
            // 
            this.rbbProducts.Image = ((System.Drawing.Image)(resources.GetObject("rbbProducts.Image")));
            this.rbbProducts.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbProducts.LargeImage")));
            this.rbbProducts.Name = "rbbProducts";
            this.rbbProducts.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbProducts.SmallImage")));
            this.rbbProducts.Tag = "PRODUCTOS_BA";
            this.rbbProducts.Text = "Productos";
            this.rbbProducts.Click += new System.EventHandler(this.rbbProducts_Click);
            // 
            // rbtTransaction
            // 
            this.rbtTransaction.Name = "rbtTransaction";
            this.rbtTransaction.Panels.Add(this.rbpMonetary);
            this.rbtTransaction.Panels.Add(this.rbpPhisical);
            this.rbtTransaction.Panels.Add(this.rbpReporteria);
            this.rbtTransaction.Tag = "TRXINV";
            this.rbtTransaction.Text = "Transacciones";
            // 
            // rbpMonetary
            // 
            this.rbpMonetary.Items.Add(this.rbbBill);
            this.rbpMonetary.Items.Add(this.rbbCompra);
            this.rbpMonetary.Name = "rbpMonetary";
            this.rbpMonetary.Tag = "MONETARIAS_TRX";
            this.rbpMonetary.Text = "Monetarias";
            // 
            // rbbBill
            // 
            this.rbbBill.Image = ((System.Drawing.Image)(resources.GetObject("rbbBill.Image")));
            this.rbbBill.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbBill.LargeImage")));
            this.rbbBill.Name = "rbbBill";
            this.rbbBill.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbBill.SmallImage")));
            this.rbbBill.Tag = "FACTURAS_MONET";
            this.rbbBill.Text = "Factura";
            this.rbbBill.Click += new System.EventHandler(this.rbbBill_Click);
            // 
            // rbbCompra
            // 
            this.rbbCompra.Image = ((System.Drawing.Image)(resources.GetObject("rbbCompra.Image")));
            this.rbbCompra.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbCompra.LargeImage")));
            this.rbbCompra.Name = "rbbCompra";
            this.rbbCompra.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbCompra.SmallImage")));
            this.rbbCompra.Tag = "COMPRAS_MONET";
            this.rbbCompra.Text = "Compra";
            // 
            // rbpPhisical
            // 
            this.rbpPhisical.Items.Add(this.rbbOutput);
            this.rbpPhisical.Items.Add(this.rbbInput);
            this.rbpPhisical.Name = "rbpPhisical";
            this.rbpPhisical.Tag = "FISICAS_TRX";
            this.rbpPhisical.Text = "Físicas";
            // 
            // rbbOutput
            // 
            this.rbbOutput.DropDownItems.Add(this.ribbonButton1);
            this.rbbOutput.Image = ((System.Drawing.Image)(resources.GetObject("rbbOutput.Image")));
            this.rbbOutput.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbOutput.LargeImage")));
            this.rbbOutput.Name = "rbbOutput";
            this.rbbOutput.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbOutput.SmallImage")));
            this.rbbOutput.Tag = "SALIDA_FISICAS";
            this.rbbOutput.Text = "Salida";
            this.rbbOutput.Click += new System.EventHandler(this.rbbOutput_Click);
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.Image")));
            this.ribbonButton1.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.LargeImage")));
            this.ribbonButton1.Name = "ribbonButton1";
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "ribbonButton1";
            // 
            // rbbInput
            // 
            this.rbbInput.Image = ((System.Drawing.Image)(resources.GetObject("rbbInput.Image")));
            this.rbbInput.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbInput.LargeImage")));
            this.rbbInput.Name = "rbbInput";
            this.rbbInput.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbInput.SmallImage")));
            this.rbbInput.Tag = "ENTRADA_FISICAS";
            this.rbbInput.Text = "Entrada";
            this.rbbInput.Click += new System.EventHandler(this.rbbInput_Click);
            // 
            // rbpReporteria
            // 
            this.rbpReporteria.Items.Add(this.rbbFactura);
            this.rbpReporteria.Name = "rbpReporteria";
            this.rbpReporteria.Tag = "REPORT_PANELTRX";
            this.rbpReporteria.Text = "Reporteria";
            // 
            // rbbFactura
            // 
            this.rbbFactura.Image = global::UI.Sif2.Properties.Resources.reportes_png_2_2_;
            this.rbbFactura.LargeImage = global::UI.Sif2.Properties.Resources.reportes_png_2_2_;
            this.rbbFactura.Name = "rbbFactura";
            this.rbbFactura.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbFactura.SmallImage")));
            this.rbbFactura.Tag = "REPORT_TRX";
            this.rbbFactura.Text = "Reportes";
            // 
            // rbbConfiguration
            // 
            this.rbbConfiguration.Name = "rbbConfiguration";
            this.rbbConfiguration.Panels.Add(this.rbpSecurity);
            this.rbbConfiguration.Panels.Add(this.rbpGeneral);
            this.rbbConfiguration.Panels.Add(this.rbpLocal);
            this.rbbConfiguration.Tag = "CONFIGINV";
            this.rbbConfiguration.Text = "Configuraciones";
            // 
            // rbpSecurity
            // 
            this.rbpSecurity.Items.Add(this.rbbUsers);
            this.rbpSecurity.Items.Add(this.rbbRoles);
            this.rbpSecurity.Items.Add(this.rbbPrivilegios);
            this.rbpSecurity.Name = "rbpSecurity";
            this.rbpSecurity.Tag = "SEGURIDAD_CONFIG";
            this.rbpSecurity.Text = "Seguridad";
            // 
            // rbbUsers
            // 
            this.rbbUsers.Image = global::UI.Sif2.Properties.Resources.avatar_usuario_4040_1_;
            this.rbbUsers.LargeImage = global::UI.Sif2.Properties.Resources.avatar_usuario_4040_1_;
            this.rbbUsers.Name = "rbbUsers";
            this.rbbUsers.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbUsers.SmallImage")));
            this.rbbUsers.Tag = "USUARIO_SEGURIDAD";
            this.rbbUsers.Text = "Usuarios";
            this.rbbUsers.Click += new System.EventHandler(this.rbbUsers_Click);
            // 
            // rbbRoles
            // 
            this.rbbRoles.Image = global::UI.Sif2.Properties.Resources.rol_1_;
            this.rbbRoles.LargeImage = global::UI.Sif2.Properties.Resources.rol_1_;
            this.rbbRoles.Name = "rbbRoles";
            this.rbbRoles.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbRoles.SmallImage")));
            this.rbbRoles.Tag = "ROL_SEGURIDAD";
            this.rbbRoles.Text = "Roles";
            this.rbbRoles.Click += new System.EventHandler(this.rbbRoles_Click);
            // 
            // rbbPrivilegios
            // 
            this.rbbPrivilegios.Image = global::UI.Sif2.Properties.Resources.paneles2;
            this.rbbPrivilegios.LargeImage = global::UI.Sif2.Properties.Resources.paneles2;
            this.rbbPrivilegios.Name = "rbbPrivilegios";
            this.rbbPrivilegios.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbPrivilegios.SmallImage")));
            this.rbbPrivilegios.Tag = "PRIVILEGIO_SEGURIDAD";
            this.rbbPrivilegios.Text = "Privilegios";
            this.rbbPrivilegios.Click += new System.EventHandler(this.rbbPrivilegios_Click);
            // 
            // rbpGeneral
            // 
            this.rbpGeneral.Items.Add(this.rbbParameters);
            this.rbpGeneral.Name = "rbpGeneral";
            this.rbpGeneral.Tag = "GENERALES_CONFIG";
            this.rbpGeneral.Text = "Generales";
            // 
            // rbbParameters
            // 
            this.rbbParameters.Image = ((System.Drawing.Image)(resources.GetObject("rbbParameters.Image")));
            this.rbbParameters.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbbParameters.LargeImage")));
            this.rbbParameters.Name = "rbbParameters";
            this.rbbParameters.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbParameters.SmallImage")));
            this.rbbParameters.Tag = "PARAMETR_GENERAL";
            this.rbbParameters.Text = "Parametros";
            this.rbbParameters.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Right;
            this.rbbParameters.Click += new System.EventHandler(this.rbbParameters_Click);
            // 
            // rbpLocal
            // 
            this.rbpLocal.Name = "rbpLocal";
            this.rbpLocal.Text = "Locales";
            // 
            // z
            // 
            this.z.Image = ((System.Drawing.Image)(resources.GetObject("z.Image")));
            this.z.LargeImage = ((System.Drawing.Image)(resources.GetObject("z.LargeImage")));
            this.z.Name = "z";
            this.z.SmallImage = ((System.Drawing.Image)(resources.GetObject("z.SmallImage")));
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UsuarioConectado});
            this.statusStrip1.Location = new System.Drawing.Point(0, 439);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(926, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // UsuarioConectado
            // 
            this.UsuarioConectado.Name = "UsuarioConectado";
            this.UsuarioConectado.Size = new System.Drawing.Size(30, 17);
            this.UsuarioConectado.Text = "User";
            // 
            // frmMDISIF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(926, 461);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.rbnMenu);
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.Name = "frmMDISIF";
            this.Text = "SIF v2.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMDISIF_FormClosing);
            this.Load += new System.EventHandler(this.frmMDISIF_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Ribbon rbnMenu;
        private System.Windows.Forms.RibbonTab rbtCatalogs;
        private System.Windows.Forms.RibbonPanel rbpBasicCatalogs;
        private System.Windows.Forms.RibbonButton rbbWarehouse;
        private System.Windows.Forms.RibbonButton rbbCategory;
        private System.Windows.Forms.RibbonButton rbbCashbox;
        private System.Windows.Forms.RibbonPanel rbpControl;
        private System.Windows.Forms.RibbonButton rbbDenomination;
        private System.Windows.Forms.RibbonButton rbbMeasurement;
        private System.Windows.Forms.RibbonPanel rbbContacts;
        private System.Windows.Forms.RibbonButton rbbSalesman;
        private System.Windows.Forms.RibbonButton rbbProvider;
        private System.Windows.Forms.RibbonTab rbtInventory;
        private System.Windows.Forms.RibbonTab rbtTransaction;
        private System.Windows.Forms.RibbonPanel rbpBasicInventory;
        private System.Windows.Forms.RibbonButton rbbProducts;
        private System.Windows.Forms.RibbonTab rbbConfiguration;
        private System.Windows.Forms.RibbonPanel rbpGeneral;
        private System.Windows.Forms.RibbonPanel rbpLocal;
        private System.Windows.Forms.RibbonPanel rbpSecurity;
        private System.Windows.Forms.RibbonButton rbbParameters;
        private System.Windows.Forms.RibbonButton rbbUsers;
        private System.Windows.Forms.RibbonOrbMenuItem romExit;
        private System.Windows.Forms.RibbonPanel rbpMonetary;
        private System.Windows.Forms.RibbonPanel rbpPhisical;
        private System.Windows.Forms.RibbonButton rbbOutput;
        private System.Windows.Forms.RibbonButton ribbonButton1;
        private System.Windows.Forms.RibbonButton rbbInput;
        private System.Windows.Forms.RibbonButton rbbBill;
        private System.Windows.Forms.RibbonButton rbbCompra;
        private System.Windows.Forms.RibbonButton rbbCliente;
        private System.Windows.Forms.RibbonButton z;
        private System.Windows.Forms.RibbonButton rbbImpuesto;
        private System.Windows.Forms.RibbonButton rbbMoneda;
        private System.Windows.Forms.RibbonButton rbbRoles;
        private System.Windows.Forms.RibbonPanel rbpReporteria;
        private System.Windows.Forms.RibbonButton rbbFactura;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel UsuarioConectado;
        private System.Windows.Forms.RibbonButton rbbPrivilegios;
    }
}