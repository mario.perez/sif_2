﻿using serviceLayer.serviceClient.CatalogServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Sif2
{
    public partial class FormAdminCaja : FormAdminGenerico
    {
        CashBoxData Caja;
        TextBox txtCodigo, txtNombre;
        ComboBox comboBodega;
        CheckBox chkActivo;

        public FormAdminCaja(string op, DataGridViewColumnCollection col, string titulo, CashBoxData caja)
        {
            InitializeComponent();
            operacion = op;
            columnas = col;
            Caja = caja;
            Text = titulo;
        }

        private void FormAdminCaja_Load(object sender, EventArgs e)
        {
            panelControles.Controls.Add(MetodosGenericos.CreacionLayoutEntidad(columnas, "Admin"));
            LlenarComboBoxBodega();
            VincularControles();
            FillData();
            VerificarTipoOperacion();
        }

        public override void VerificarTipoOperacion()
        {
            base.VerificarTipoOperacion();

            if (operacion.Equals("U"))
            {
                txtCodigo = (TextBox)this.Controls.Find("Codigo_Filtro_NotNull", true)[0];
                txtCodigo.ReadOnly = true;
            }
            if (operacion.Equals("I"))
            {
                Caja = new CashBoxData();
            }
        }

        public override void VincularControles()
        {
            try
            {

                txtCodigo = (TextBox)this.Controls.Find("Codigo_Filtro_NotNull", true)[0];
                txtNombre = (TextBox)this.Controls.Find("Nombre_NotNull",true)[0];
                comboBodega = (ComboBox)this.Controls.Find("Bodega_NotNull",true)[0];
                chkActivo = (CheckBox)this.Controls.Find("Activo_NotNull", true)[0];
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void FillData()
        {
            try
            {

                if (Caja != null) {
                    txtCodigo.Text = Caja.Id;
                    txtNombre.Text = Caja.Name;
                    comboBodega.SelectedValue = Caja.Warehouse.Id;
                    chkActivo.Checked = Caja.IsActive;
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void ActualizarInformacion() {
            try
            {
                Caja.Id = txtCodigo.Text;
                Caja.Name = txtNombre.Text;
                Caja.Warehouse = new CatalogsClient().GetWarehouse(comboBodega.SelectedValue.ToString()).Data;
                Caja.IsActive = chkActivo.Checked;
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void LlenarComboBoxBodega()
        {
            try
            {                

                comboBodega = (ComboBox)this.Controls.Find("Bodega_NotNull", true)[0];

                comboBodega.DataSource = new CatalogsClient().GetAllWarehouses().Data.Select(x => new { Codigo = x.Id, Nombre = x.Value }).ToList();
                comboBodega.DisplayMember = "Nombre";
                comboBodega.ValueMember = "Codigo";

            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public override void Guardar()
        {
            try
            {
                ActualizarInformacion();
                GeneralMessageOfboolean respuesta = new GeneralMessageOfboolean();

                switch (operacion)
                {
                    case "I":
                        respuesta = new CatalogsClient().AddCashbox(Caja);
                        break;
                    case "U":
                        respuesta = new CatalogsClient().UpdateCashbox(Caja);
                        break;
                }
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
    }
}
