﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.Security;
using serviceLayer.Security.SecurityServiceReference;

namespace UI.Sif2
{
    public partial class frmPermiso : Form
    {
        private RolData rol = new RolData();
        private SecurityClient _controller;

        public frmPermiso(RolData r)
        {
            InitializeComponent();
            _controller = new SecurityClient();
            rol = r;
            CargarPrivilegiosNoAsignados(trvPermisos, null, 0);
        }        

        public void CargarPrivilegiosNoAsignados(TreeView view, TreeNode nodos, int idPadre)
        {

            PrivilegioData[] list = new List<PrivilegioData>().ToArray();
            var noAsignados = _controller.GetPrivilegioNoAsignado(idPadre);
            PrivilegioData[] PermisosAsignados = new List<PrivilegioData>().ToArray();
            PermisosAsignados = _controller.GetPrivilegioAsignado(idPadre, rol.Id).Data;

            int id;
            string nombreNodo;

            foreach (var item in noAsignados.Data)
            {
                id = item.Id;

                nombreNodo = item.Nombre;
                TreeNode node = new TreeNode(nombreNodo);
                node.Name = item.Id.ToString();
                node.Tag = item.CodigoInterno;                

                MetodosGenericos.AgregarImagenNodoPrivilegio(node, item.CodigoInterno);

                if (nodos == null)
                {
                    view.Nodes.Add(node);
                }                
                else {
                    //Si el nodo no es padre, quiere decir que es una hoja(ACCION)
                    if (!item.EsPadre//item.CodigoInterno.Equals("ACCION")
                        && !PermisosAsignados.Select(x => x.CodigoTag).Contains(item.CodigoTag)
                        )
                        {
                            nodos.Nodes.Add(node);
                    }
                    else
                    {
                        if (item.EsPadre//!item.CodigoInterno.Equals("ACCION")
                            )
                        {
                            nodos.Nodes.Add(node);
                            nodos.Expand();//Que me expanda aquellos nodos que tienen hijos
                        }
                    }
                }                
                CargarPrivilegiosNoAsignados(view, node, id);                
            }

            if (nodos != null) {
                if (!nodos.Tag.ToString().Equals("ACCION") && nodos.Nodes.Count == 0)
                {
                    nodos.Remove();
                }
            }
            
        }

        private void Checar_nodos_hijos(TreeNode e)
        {

            foreach (TreeNode item in e.Nodes)
            {
                item.Checked = e.Checked;
                if (item.Nodes.Count > 0)
                {
                    Checar_nodos_hijos(item);
                }
            }

        }

        private void Checar_Nodos_padres(TreeNode nodo)
        {

            if (nodo.Parent != null)
            {

                bool result = nodo.Checked;

                foreach (TreeNode item in nodo.Parent.Nodes)
                {
                    if ((item.Checked != nodo.Checked) && (item.Tag != nodo.Tag))
                    {
                        result = true;
                        break;
                    }
                    else
                        if ((item.Checked == nodo.Checked) && (item.Tag != nodo.Tag))
                    {
                        result = item.Checked;
                    }
                }

                nodo.Parent.Checked = result;
                Checar_Nodos_padres(nodo.Parent);
            }
        }

        private void trvPermisos_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            if (e.Node.Parent == null)
            {
                int d = (int)(0.2 * e.Bounds.Height);
                Rectangle rect = new Rectangle(d + trvPermisos.Margin.Left, d + e.Bounds.Top, e.Bounds.Height - d * 2, e.Bounds.Height - d * 2);
                e.Graphics.FillRectangle(new SolidBrush(Color.FromKnownColor(KnownColor.Control)), rect);
                e.Graphics.DrawRectangle(Pens.Silver, rect);
                StringFormat sf = new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center };
                e.Graphics.DrawString(e.Node.IsExpanded ? "-" : "+", trvPermisos.Font, new SolidBrush(Color.Blue), rect, sf);
                //Draw the dotted line connecting the expanding/collapsing button and the node Text
                using (Pen dotted = new Pen(Color.Black) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dot })
                {
                    e.Graphics.DrawLine(dotted, new Point(rect.Right + 1, rect.Top + rect.Height / 2), new Point(rect.Right + 4, rect.Top + rect.Height / 2));
                }
                //Draw text
                sf.Alignment = StringAlignment.Near;
                Rectangle textRect = new Rectangle(e.Bounds.Left + rect.Right + 4, e.Bounds.Top, e.Bounds.Width - rect.Right - 4, e.Bounds.Height);
                if (e.Node.IsSelected)
                {
                    SizeF textSize = e.Graphics.MeasureString(e.Node.Text, trvPermisos.Font);
                    e.Graphics.FillRectangle(new SolidBrush(SystemColors.Highlight), new RectangleF(textRect.Left, textRect.Top, textSize.Width, textRect.Height));
                }
                e.Graphics.DrawString(e.Node.Text, trvPermisos.Font, new SolidBrush(trvPermisos.ForeColor), textRect, sf);
            }
            else e.DrawDefault = true;
        }

        private void trvPermisos_AfterCheck(object sender, TreeViewEventArgs e)
        {
            trvPermisos.AfterCheck -= trvPermisos_AfterCheck;

            Checar_nodos_hijos(e.Node);

            if (e.Node.Parent != null)
            {
                Checar_Nodos_padres(e.Node);
            }

            trvPermisos.AfterCheck += trvPermisos_AfterCheck;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardarPrivilegiosAsignados();
        }

        private void GuardarPrivilegiosAsignados()
        {
            bool respuesta = false;
            try
            {
                TreeNodeCollection nodos = trvPermisos.Nodes;
                GuardarNodoRaiz();
                
                foreach (TreeNode nodo in nodos)
                {
                    GuardarPrivilegios_recursivo(nodo);
                }
                respuesta = true;
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                respuesta = false;
            }

            Interface1 miIntefaz = this.Owner as Interface1;
            if (miIntefaz != null)
            {
                miIntefaz.ObtenerObjeto<bool>(respuesta);
                Close();
            }

        }

        // Se guardara nodo raiz para los roles que por primera vez se le asignaran privilegios
        private void GuardarNodoRaiz() {           
            var p =
                    new PrivilegioData()
                    {
                        Id = Int32.Parse(trvPermisos.Nodes[0].Name.ToString())
                        ,
                        Nombre = trvPermisos.Nodes[0].Text
                        ,
                        CodigoInterno = trvPermisos.Nodes[0].Tag.ToString()
                    };
            var r = _controller.GetRolPrivilegioValidacion(p.Id, rol.Id).Data;

            if (r == null)
            {
                var insert = _controller.AddPrivilegioRol(p.Id, rol.Id);
                if (insert.CodeResponse > 0)
                {
                    MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(insert);
                }
            }           
        }

        private void GuardarPrivilegios_recursivo(TreeNode nodos)
        {            
            try
            {
                foreach (TreeNode item in nodos.Nodes)
                {
                    if (item.Checked)
                    {
                        //Validamos si ya existe el Privilegio para este usuario
                        var p =
                        new PrivilegioData()
                        {
                            Id = Int32.Parse(item.Name.ToString())
                            ,
                            Nombre = item.Text
                            ,
                            CodigoInterno = item.Tag.ToString()
                        };

                        var r = _controller.GetRolPrivilegioValidacion(p.Id,rol.Id).Data;

                        if (r == null)
                        {
                            var insert = _controller.AddPrivilegioRol(p.Id, rol.Id);
                            if (insert.CodeResponse > 0) {
                                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(insert);
                                break;
                            }
                        }
                    }
                    GuardarPrivilegios_recursivo(item);
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);                
            }            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
