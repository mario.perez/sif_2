﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.Security;
using serviceLayer.Security.SecurityServiceReference;

namespace UI.Sif2
{
    public partial class frmAdminUsuario : Form
    {
        private string operacion = string.Empty;
        private UsuarioData usuario;
        private UsuarioRolData[] ListaRoles = new List<UsuarioRolData>().ToArray();

        public frmAdminUsuario(string op, UsuarioData usu)
        {
            InitializeComponent();
            operacion = op;
            usuario = usu;

            NombrarFormulario();
            HabilitarControles_por_TipoOperacion();
            LlenarFormulario_Usuario();
            CargarRolesAsignados();
        }

        private void NombrarFormulario()
        {
            switch (operacion)
            {
                case "I":
                    Text = "Agregar Usuario";
                    break;
                case "U":
                    Text = "Modificar Usuario";
                    break;
                case "S":
                    Text = "Consultar Usuario";
                    break;
            }
        }

        private void LlenarFormulario_Usuario()
        {
            try
            {
                if (!operacion.Equals("I"))
                {
                    txtUsuarioLogin.Text = usuario.UsuarioLogin;
                    txtNombreCompleto.Text = usuario.NombreCompleto;
                    txtCorreo.Text = usuario.Correo;
                    txtContraseña.Text = usuario.Contraseña;
                    chkActivo.Checked = usuario.Activo;
                    chkBloqueado.Checked = usuario.Bloqueado;                    
                }                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void HabilitarControles_por_TipoOperacion()
        {
            if (operacion.Equals("S"))
            {
                txtUsuarioLogin.ReadOnly = txtNombreCompleto.ReadOnly = txtCorreo.ReadOnly = txtContraseña.ReadOnly = true;
                chkActivo.Checked = chkBloqueado.Checked = true;                
                groupBox1.Enabled = false;
                btnGuardarUsuario.Enabled = false;
                btnNuevoRol.Enabled = false;
                btnEliminarUsuarioRol.Enabled = false;
            }

            //Si el usuario va a crear un nuevo usuario, no debe hacer nada con la asignacion de rol
            if (operacion.Equals("I")) {
                tabControl1.Enabled = false;
            }
        }

        private void btnGuardarUsuario_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void ActualizarInformacion()
        {
            try
            {
                usuario.UsuarioLogin = txtUsuarioLogin.Text;
                usuario.NombreCompleto = txtNombreCompleto.Text;
                usuario.Correo = txtCorreo.Text;
                usuario.Contraseña = txtContraseña.Text;
                usuario.Activo = chkActivo.Checked;
                usuario.Bloqueado = chkBloqueado.Checked;
            }
            catch (Exception e)
            {
                MetodosGenericos.MensajeError_Exception(e);
            }
        }

        private void Guardar() {
            try
            {
                ActualizarInformacion();
                GeneralMessageOfboolean resultado = new GeneralMessageOfboolean();

                switch (operacion)
                {
                    case "I":
                        usuario.MaqCreacion = Environment.MachineName;
                        usuario.FechaCreacion = DateTime.Now;
                        usuario.AppCreacion = "Desktop";
                        resultado = new SecurityClient().AddUsuario(usuario);
                        break;
                    case "U":
                        usuario.MaqModificacion = Environment.MachineName;
                        usuario.FechaModificacion = DateTime.Now;
                        usuario.AppModificacion = "Desktop";
                        resultado = new SecurityClient().UpdateUsuario(usuario);
                        break;
                }
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void txtUsuarioLogin_Validating(object sender, CancelEventArgs e)
        {
            ValidarLoginUsuario();
        }

        private bool ValidarLoginUsuario() {
            try
            {
                errorProvider1.Clear();
                if (string.IsNullOrEmpty(txtUsuarioLogin.Text) || string.IsNullOrWhiteSpace(txtUsuarioLogin.Text)) {
                    errorProvider1.SetIconAlignment(txtUsuarioLogin, ErrorIconAlignment.MiddleLeft);
                    errorProvider1.SetError(txtUsuarioLogin,"Usuario no puede ser vacio");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return false;
            }
        }

        private bool ValidarContraseña()
        {
            try
            {
                errorProvider1.Clear();
                if (string.IsNullOrEmpty(txtContraseña.Text) || string.IsNullOrWhiteSpace(txtContraseña.Text))
                {
                    errorProvider1.SetIconAlignment(txtContraseña, ErrorIconAlignment.MiddleLeft);
                    errorProvider1.SetError(txtContraseña, "Contraseña no puede ser vacio");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return false;
            }
        }

        private void CargarRolesAsignados() {
            try
            {
                var lista = new SecurityClient().GetAllUsuarioRol(usuario.Id, 0, 0, "S").Data.ToArray();

                ListaRoles = lista;

                LlenarDataGrid("","");

            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void LlenarDataGrid(string nombreRol, string descripcionRol)
        {

            //dgvRoles.CellValueChanged -= dgvRoles_CellValueChanged;
            //MetodosGenericos.FormatearDataGridView(dgvRoles);
            //dgvRoles.CellValueChanged += dgvRoles_CellValueChanged;
            dgvRoles.Rows.Clear();
            foreach (var item in ListaRoles)
            {

                if (
                       (string.IsNullOrEmpty(nombreRol) || string.IsNullOrWhiteSpace(nombreRol) || item.NombreRol.Contains(nombreRol))
                       && (string.IsNullOrEmpty(descripcionRol) || string.IsNullOrWhiteSpace(descripcionRol) || item.DescripcionRol.Contains(descripcionRol))                       
                    )
                {
                    dgvRoles.Rows.Add(
                        item.Id
                        , item.NombreRol
                        , item.DescripcionRol
                        ,item.RolId
                    );

                    // Indicamos que los registros mayores al primero son de lectura
                    dgvRoles.Rows[dgvRoles.Rows.Count - 1].ReadOnly = true;

                }

            }
            // Al count le restamos -1 para que no tome el cuenta el registro usado para filtrar el grid
            lbl_totalRegistro.Text = "Total Registros: " + (dgvRoles.Rows.Count ).ToString("n2");
        }

        private void txtContraseña_Validating(object sender, CancelEventArgs e)
        {
            ValidarContraseña();
        }

        private void btnNuevoRol_Click(object sender, EventArgs e)
        {
            new frmRoleLista(usuario.Id,0,"frmAdminUsuario").ShowDialog();
        }

        private void dgvRoles_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == 0)
                {
                    if (dgvRoles.CurrentRow != null)
                    {

                        string Nombre = dgvRoles.CurrentRow.Cells["Nombre"].Value != null ? dgvRoles.CurrentRow.Cells["Nombre"].Value.ToString() : string.Empty;
                        string Descripcion = dgvRoles.CurrentRow.Cells["Descripcion"].Value != null ? dgvRoles.CurrentRow.Cells["Descripcion"].Value.ToString() : string.Empty;

                        LlenarDataGrid(Nombre, Descripcion);
                    }
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnConsultarRol_Click(object sender, EventArgs e)
        {
            ConsultarRol();
        }

        private void ConsultarRol()
        {
            try
            {
                var rol = ObtenerRolSeleccionado();
                if (rol != null) {
                    new frmAdminRol("S",rol).ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private RolData ObtenerRolSeleccionado() {
            RolData rolSeleccionado = null;

            try
            {
                if (dgvRoles.CurrentRow != null && dgvRoles.CurrentRow.Index > 0)
                {
                    rolSeleccionado = new RolData();
                    var id = Convert.ToInt32(dgvRoles.CurrentRow.Cells["RolId"].Value);
                    rolSeleccionado.Id = id;
                    rolSeleccionado.FechaCreacion = DateTime.Now;
                    rolSeleccionado.FechaModificacion = DateTime.Now;

                    rolSeleccionado = new SecurityClient().GetAllRol(rolSeleccionado).Data[0];
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
            return rolSeleccionado;
        }

        private void btnRefrescarRol_Click(object sender, EventArgs e)
        {
            CargarRolesAsignados();
        }

        private void btnEliminarUsuarioRol_Click(object sender, EventArgs e)
        {
            EliminarUsuarioRol();
        }

        private void EliminarUsuarioRol()
        {
            try
            {
                if (dgvRoles.CurrentRow != null && dgvRoles.CurrentRow.Index > 0)
                {
                    if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar el rol del usuario?") == DialogResult.Yes) 
                    {
                        int id = Int32.Parse(dgvRoles.CurrentRow.Cells["Id"].Value.ToString());
                        var remove = new SecurityClient().RemoveUsuarioRol(0, 0, id, "D");
                        MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(remove);
                        if (remove.CodeResponse == 0) {
                            CargarRolesAsignados();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
    }
}
