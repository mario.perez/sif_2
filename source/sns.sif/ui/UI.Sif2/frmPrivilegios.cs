﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.Security;
using serviceLayer.Security.SecurityServiceReference;

namespace UI.Sif2
{
    public partial class frmPrivilegios : Form
    {
        private SecurityClient _controller;
        private string operacion = string.Empty;
        private PrivilegioData privilegioSeleccionado = new PrivilegioData();

        public frmPrivilegios()
        {
            _controller = new SecurityClient();
            InitializeComponent();
            HabilitarControles_SoloLectura(true);
            RefrescarPrivilegiosTreeview();             
        }
        

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void RefrescarPrivilegiosTreeview() {
            trvPrivilegios.Nodes.Clear();
            CargarPrivilegiosNoAsignados(trvPrivilegios, null, 0);
        }

        public void CargarPrivilegiosNoAsignados(TreeView view, TreeNode nodos, int idPadre)
        {

            PrivilegioData[] list = new List<PrivilegioData>().ToArray();
            var privilegios = _controller.GetPrivilegioHijos(idPadre);

            int id;
            string nombreNodo;

            foreach (var item in privilegios.Data)
            {
                id = item.Id;

                nombreNodo = item.Nombre;
                TreeNode node = new TreeNode(nombreNodo);
                node.Name = item.Id.ToString();
                node.Tag = item.CodigoTag;
                node.ToolTipText = item.CodigoInterno;

                MetodosGenericos.AgregarImagenNodoPrivilegio(node, item.CodigoInterno);

                if (nodos == null)
                {
                    view.Nodes.Add(node);
                }
                else
                {
                    if (item.EsPadre)                            
                    {                        
                        nodos.Expand();//Que me expanda aquellos nodos que tienen hijos
                    }
                    nodos.Nodes.Add(node);
                }
                CargarPrivilegiosNoAsignados(view, node, id);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void trvPrivilegios_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            ObtenerNodoClickeado(e);
            
        }

        private void ObtenerNodoClickeado(TreeNodeMouseClickEventArgs e) {
            try
            {
                if (operacion.Equals("I")) {
                    txtNodoPadre.Tag = e.Node.Name;
                    txtNodoPadre.Text = e.Node.Text;
                    privilegioSeleccionado.PadreId = Int32.Parse(e.Node.Name.ToString());
                }
                else
                {                    
                   
                    txtCodigoTag.Text = e.Node.Tag.ToString();
                    txtNombre.Text = e.Node.Text;
                    cmbCodigoInterno.SelectedItem = e.Node.ToolTipText;
                    chkPadre.Checked = cmbCodigoInterno.Text.Equals("ACCION") ? false : true;

                    privilegioSeleccionado.Id = Int32.Parse(e.Node.Name.ToString());
                    privilegioSeleccionado.CodigoInterno = e.Node.ToolTipText;
                    privilegioSeleccionado.CodigoTag = e.Node.Tag.ToString();
                    privilegioSeleccionado.Nombre = e.Node.Text;

                    if (e.Node.Parent != null) {
                        txtNodoPadre.Tag = e.Node.Parent.Name;
                        txtNodoPadre.Text = e.Node.Parent.Text;
                        privilegioSeleccionado.PadreId = Int32.Parse(e.Node.Parent.Name.ToString());
                    }
                    else
                    {
                        txtNodoPadre.Tag = "0";
                        txtNodoPadre.Text = string.Empty;
                    }
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);                
            }
        }

        private void ActualizarInformacion() {
            //privilegioSeleccionado.Id = Int32.Parse(e.Node.Name.ToString());
            privilegioSeleccionado.CodigoInterno = cmbCodigoInterno.Text;
            privilegioSeleccionado.CodigoTag = txtCodigoTag.Text;
            privilegioSeleccionado.Nombre = txtNombre.Text;
            privilegioSeleccionado.PadreId = Int32.Parse(txtNodoPadre.Tag.ToString());
            privilegioSeleccionado.EsPadre = chkPadre.Checked;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            operacion = "U";
            HabilitarControles_SoloLectura(false);
            btnNuevo.Enabled = btnModificar.Enabled = btnEliminar.Enabled = false;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            operacion = "I";
            HabilitarControles_SoloLectura(false);
            btnNuevo.Enabled = btnModificar.Enabled = btnEliminar.Enabled = false;
        }

        private void HabilitarControles_SoloLectura(bool active) {
             txtCodigoTag.ReadOnly = txtNodoPadre.ReadOnly = txtNombre.ReadOnly = active;
            cmbCodigoInterno.Enabled = !active;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            operacion = "D";
            btnNuevo.Enabled = btnModificar.Enabled = btnEliminar.Enabled = false;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void Guardar() {
            try
            {
                ActualizarInformacion();
                GeneralMessageOfboolean resultado = new GeneralMessageOfboolean() ;
                switch (operacion)
                {
                    case "I":
                        resultado = _controller.AddPrivilegio(privilegioSeleccionado);
                        break;
                    case "U":
                        resultado = _controller.UpdatePrivilegio(privilegioSeleccionado);
                        break;
                    case "D":
                        resultado = _controller.RemovePrivilegio(privilegioSeleccionado);
                        break;
                }
                if (!string.IsNullOrEmpty(operacion)) {
                    MetodosGenericos.MensajeInformativo(resultado.MessageResponse);
                    HabilitarControles_SoloLectura(true);
                    operacion = string.Empty;
                    btnNuevo.Enabled = btnModificar.Enabled = btnEliminar.Enabled = true;
                    RefrescarPrivilegiosTreeview();
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            CancelarOperacion();
        }

        private void CancelarOperacion() {
            btnNuevo.Enabled = btnModificar.Enabled = btnEliminar.Enabled = true;
            operacion = string.Empty;
            privilegioSeleccionado = new PrivilegioData();
        }

        private void cmbCodigoInterno_SelectedIndexChanged(object sender, EventArgs e)
        {
            Marcar_EsPadre();
        }

        private void Marcar_EsPadre() {
            chkPadre.Checked = cmbCodigoInterno.Text.Equals("ACCION") ? false : true;
        }
    }
}
