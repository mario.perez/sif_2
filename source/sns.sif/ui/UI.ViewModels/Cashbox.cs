﻿using serviceLayer.SIF_DataContracts;

namespace UI.ViewModels
{
    public class Cashbox : CashBoxData
    {
        public Cashbox()
        {

        }
        public Cashbox(CashBoxData data)
        {
            this.Id = data.Id;
            this.Name = data.Name;
            this.IsActive = data.IsActive;
            this.Warehouse = data.Warehouse;
        }

        public override string ToString()
        {
            return string.Concat(Id, " - ", Name, "(", Warehouse.Value, ")");
        }
    }
}
