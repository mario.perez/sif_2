﻿using serviceLayer.SIF_DataContracts;

namespace UI.ViewModels
{
    public class Catalog : Catalogue
    {
        public Catalog()
        {

        }
        public Catalog(Catalogue data)
        {
            this.Id = data.Id;
            this.Value = data.Value;
            this.IsActive = data.IsActive;
        }

        public override string ToString()
        {
            return string.Concat(Id, " - ", Value);
        }

        public Catalogue ToCatalogue()
        {
            var result = new Catalogue();
            result.Id = this.Id;
            result.Value = this.Value;
            result.IsActive = this.IsActive;
            return result;
        }

    }
}
