﻿using serviceLayer.SIF_DataContracts;

namespace UI.ViewModels
{
    public class Provider : ProviderData
    {
        public Provider()
        { }

        public Provider(ProviderData data)
        {
            this.ProviderId = data.ProviderId;
            this.ProviderName = data.ProviderName;
            this.IsActive = data.IsActive;
            this.Phone = data.Phone;
            this.Address = data.Address;
            this.ProviderCodeId = data.ProviderCodeId;
            this.Email = data.Email;
            this.Contact = data.Contact;

        }

        public override string ToString()
        {
            return string.Concat(ProviderName);
        }
    }

}
