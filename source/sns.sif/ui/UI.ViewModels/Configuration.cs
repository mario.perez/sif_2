﻿using serviceLayer.SIF_DataContracts;

namespace UI.ViewModels
{
    public class Configuration : Configs
    {
        public Configuration()
        {

        }

        public Configuration(Configs data)
        {
            this.Id = data.Id;
            this.Description = data.Description;
            this.Value = data.Value;
        }

        public Configs ToConfig()
        {
            var result = new Configs();
            result.Description = this.Description;
            result.Id = this.Id;
            result.Value = this.Value;
            return result;
        }

        public override string ToString()
        {
            return Id;
        }
    }

}
