﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using dataAccess.CatalogManagement;
using businessLogic.Models;


namespace dataAccess.DatabaseAccessTest
{
    [TestClass]
    public class CashboxManagerTest
    {
        CashboxManager client;

        [TestMethod]
        public void CashBoxIntegralTest()
        {
            AddCashBoxTest();
            GetCashBoxTest();
            UpdateCashBoxTest();
            DeleteCashBoxTest();
        }
        //[TestMethod]
        public void GetCashBoxTest()
        {
            client = new CatalogManagement.CashboxManager();
            var resp = client.GetAll();
            Assert.IsNotNull(resp);
            Assert.AreNotEqual(0, resp.Count);
        }

        //[TestMethod]
        public void AddCashBoxTest()
        {
            client = new CatalogManagement.CashboxManager();
            var resp = client.Add(new Cashbox() { Id = "0", Name =  "Caja 1", IsActive =  true, WarehouseId = "0" });
            Assert.AreEqual(true, resp);
            var result = client.GetOne("0");
            Assert.AreEqual("Caja 1", result.Name);
        }
        //[TestMethod]
        public void UpdateCashBoxTest()
        {
            client = new CatalogManagement.CashboxManager();
            var resp = client.Update(new Cashbox() { Id = "0", Name = "Caja Uno", IsActive = true, WarehouseId = "0" });
            Assert.AreEqual(true, resp);
            var result = client.GetOne("0");
            Assert.AreEqual("Caja Uno", result.Name);
        }
        //[TestMethod]
        public void DeleteCashBoxTest()
        {
            client = new CatalogManagement.CashboxManager();
            var resp = client.Remove(new Cashbox() { Id = "0", Name = "General", IsActive = true, WarehouseId = "0" });
            Assert.AreEqual(true, resp);
        }
    }
}
