﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace dataAccess.DatabaseAccessTest
{
    [TestClass]
    public class WarehouseManagerTest
    {
        dataAccess.CatalogManagement.WarehouseManager client;

        [TestMethod]
        public void WarehouseIntegralTest()
        {
            AddWarehouse();
            GetWarehousesTest();
            UpdateWarehouse();
            DeleteWarehouse();
        }

        //[TestMethod]
        public void GetWarehousesTest()
        {
            client = new dataAccess.CatalogManagement.WarehouseManager();
            var resp = client.GetAll();
            Assert.IsNotNull(resp);
            Assert.AreNotEqual(0, resp.Count);

        }

        //[TestMethod]
        public void AddWarehouse()
        {
            client = new CatalogManagement.WarehouseManager();
            var resp = client.Add(new businessLogic.Models.Catalog("0", "General", true));
            Assert.AreEqual(true, resp);
            var result = client.GetOne("0");
            Assert.AreEqual( "General", result.Name);
        }

        //[TestMethod]
        public void UpdateWarehouse()
        {
            client = new CatalogManagement.WarehouseManager();
            var resp = client.Update(new businessLogic.Models.Catalog("0", "Tienda", false));
            Assert.AreEqual(true, resp);
            var result = client.GetOne("0");
            Assert.AreEqual("Tienda", result.Name);
        }


       // [TestMethod]
        public void DeleteWarehouse()
        {
            client = new CatalogManagement.WarehouseManager();
            var resp = client.Remove(new businessLogic.Models.Catalog("0", "General", true));
            Assert.AreEqual(true, resp);
        }

     }
}
