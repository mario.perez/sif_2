﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace dataAccess.DatabaseAccessTest
{
    [TestClass]
    public class CategoryManagerTest
    {
        dataAccess.CatalogManagement.CategoryManager client;

        [TestMethod]
        public void CategoryIntegralTest()
        {
            AddCategoriesTest();
            GetCategoriesTest();
            UpdateCategoryTest();
            DeleteCategoryTest();
        }

        //[TestMethod]
        public void AddCategoriesTest()
        {
            client = new CatalogManagement.CategoryManager();
            var resp = client.Add(new businessLogic.Models.Catalog("1", "Electronica", true));
            Assert.AreEqual(true, resp);
            var result = client.GetOne("1");
            Assert.AreEqual("Electronica", result.Name.Trim());
        }

        //[TestMethod]
        public void GetCategoriesTest()
        {
            client = new dataAccess.CatalogManagement.CategoryManager();
            var resp = client.GetAll();
            Assert.IsNotNull(resp);
            Assert.AreNotEqual(0, resp.Count);
        }

        //[TestMethod]
        public void UpdateCategoryTest()
        {
            client = new CatalogManagement.CategoryManager();
            var resp = client.Update(new businessLogic.Models.Catalog("1", "Informatica", true));
            Assert.AreEqual(true, resp);
            var result = client.GetOne("1");
            Assert.AreEqual("Informatica", result.Name);
        }

        //[TestMethod]
        public void DeleteCategoryTest()
        {
            client = new CatalogManagement.CategoryManager();
            var resp = client.Remove(new businessLogic.Models.Catalog("1", "Informatica", true));
            Assert.AreEqual(true, resp);
        }

    }
}
