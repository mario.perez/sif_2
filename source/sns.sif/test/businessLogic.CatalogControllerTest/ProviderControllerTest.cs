﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using businessLogic.CatalogController;

namespace businessLogic.CatalogControllerTest
{
    [TestClass]
    public class ProviderControllerTest
    {
        ProviderController test;

        [TestMethod]
        public void GetProviderTest()
        {
            test = new ProviderController();
            var result = test.ListCatalogs();
            Assert.IsNotNull(result);
        }
    }
}
