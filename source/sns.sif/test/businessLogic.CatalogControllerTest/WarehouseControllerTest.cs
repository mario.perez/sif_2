﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using crossCutting.Resources;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogControllerTest
{
    [TestClass]
    public class WarehouseControllerTest
    {
        businessLogic.CatalogController.WarehouseController client;

        [TestMethod]
        public void AddCatalogTest()
        {
            client = new CatalogController.WarehouseController();
            try
            { 
            client.AddCatalog(new Catalogue() {Id = "0", Value = "" , IsActive = true});
            }catch(ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            try
            { 
                client.AddCatalog(new Catalogue() { Id = "", Value = "Bodega", IsActive = false });
            }
            catch(ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            client.AddCatalog(new Catalogue() {Id = "0", Value = "Bodega" , IsActive = true});
            var Warehouses = client.ListCatalogs();
            Assert.AreNotEqual(0, Warehouses.Count);
        }

        [TestMethod]
        public void UpdateCatalogTest()
        {
            client = new CatalogController.WarehouseController();
            try
            {
                client.UpdateCatalog(new Catalogue() { Id = "0", Value = "Bodega", IsActive = true });
            }
            catch (ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }


            try
            {
                client.UpdateCatalog(new Catalogue() {Id = "0", Value = "" , IsActive = true});
            }
            catch(ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            try
            {
                client.UpdateCatalog(new Catalogue() { Id = "0", Value = "Tienda", IsActive = false });
            }
            catch (ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            client.UpdateCatalog(new Catalogue() { Id = "0", Value = "Tienda", IsActive = true });
            var resp = client.getCatalog("0");
            Assert.AreEqual("Tienda", resp.Value);



        }

        [TestMethod]
        public void RemoveCatalogTest()
        {
            client = new CatalogController.WarehouseController();

            try
            { 
                client.RemoveCatalog(new Catalogue() { Id = "", Value = "Tienda", IsActive = true });
            }
            catch(ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            client.RemoveCatalog(new Catalogue() { Id = "0", Value = "", IsActive = true });
            var resp = client.getCatalog("0");
            Assert.AreNotEqual("", resp.Id);
        }

    }
}
