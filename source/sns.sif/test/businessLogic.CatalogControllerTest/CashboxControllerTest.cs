﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using serviceLayer.SIF_DataContracts;

namespace businessLogic.CatalogControllerTest
{
    [TestClass]
    public class CashboxControllerTest
    {
        businessLogic.CatalogController.CashboxController client;

        [TestMethod]
        public void AddCatalogTest()
        {
            client = new CatalogController.CashboxController();
            try
            {
                client.AddCatalog(new CashBoxData() { Id = "0", Name= "", Warehouse = new Catalogue() { Id = "0", Value = "", IsActive= true }, IsActive = true });
            }
            catch (ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            try
            {
                client.AddCatalog(new CashBoxData() { Id = "", Name = "Bodega", Warehouse = new Catalogue() { Id = "0", Value = "", IsActive = true }, IsActive = true });
            }
            catch (ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            client.AddCatalog(new CashBoxData() { Id = "0", Name = "Bodega", Warehouse = new Catalogue() { Id = "0", Value = "", IsActive = true }, IsActive = true });
            var Warehouses = client.ListCatalogs();
            Assert.AreNotEqual(0, Warehouses.Count);
        }

        [TestMethod]
        public void UpdateCatalogTest()
        {
            client = new CatalogController.CashboxController();
            try
            {
                client.UpdateCatalog(new CashBoxData() { Id = "", Name = "Bodega", Warehouse = new Catalogue() { Id = "0", Value = "", IsActive = true }, IsActive = true });
            }
            catch (ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }


            try
            {
                client.UpdateCatalog(new CashBoxData() { Id = "0", Name = "", Warehouse = new Catalogue() { Id = "0", Value = "", IsActive = true }, IsActive = true });
            }
            catch (ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            try
            {
                client.UpdateCatalog(new CashBoxData() { Id = "0", Name = "Tienda", Warehouse = new Catalogue() { Id = "0", Value = "", IsActive = true }, IsActive = false });
            }
            catch (ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            client.UpdateCatalog(new CashBoxData() { Id = "0", Name = "Tienda", Warehouse = new Catalogue() { Id = "0", Value = "", IsActive = true }, IsActive = true });
            var resp = client.getCatalog("0");
            Assert.AreEqual("Tienda", resp.Name);



        }

        [TestMethod]
        public void RemoveCatalogTest()
        {
            client = new CatalogController.CashboxController();

            try
            {
                client.RemoveCatalog(new CashBoxData() { Id = "", Name = "Tienda", Warehouse = new Catalogue() { Id = "0", Value = "", IsActive = true }, IsActive = true });
            }
            catch (ArgumentException)
            {
                Assert.AreEqual(1, 1);
            }

            client.RemoveCatalog(new CashBoxData() { Id = "0", Name = "", Warehouse = new Catalogue() { Id = "0", Value = "", IsActive = true }, IsActive = true });
            var resp = client.getCatalog("0");
            Assert.AreNotEqual("", resp.Id);
        }

    }
}
