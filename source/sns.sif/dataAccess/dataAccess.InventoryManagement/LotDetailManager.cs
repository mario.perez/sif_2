﻿using System.Data;
using businessLogic.Models;
using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.InventoryManagement
{
    public class LotDetailManager:MasterDetailManager<LotDetail>
    {
        public LotDetailManager()
        {
            _procedureName = "sp_detalle_lote";
        }

        protected override LotDetail getCatalog(DataRow row, bool hasDetails)
        {
            var result = new LotDetail(row[0].ToString(),
                                        row[1].ToString(),
                                        double.Parse(row[2].ToString()),
                                        decimal.Parse(row[3].ToString()),
                                        row[4].ToString() == "S");
            return result;
        }

        protected override LotDetail getNewCatalog()
        {
            return new LotDetail();
        }

        protected override void setCatalog(DataAccess dataAccess, LotDetail data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.LotId);
            dataAccess.AdParameter("@i_codigoProducto", DBTypeEnum.VarChar, data.ProductId);
            dataAccess.AdParameter("@i_cantidad", DBTypeEnum.Numeric, data.Quantity);
            dataAccess.AdParameter("@i_costo", DBTypeEnum.Money, data.Cost);
            dataAccess.AdParameter("@i_esActivo", DBTypeEnum.VarChar, data.IsActive?"S":"N");

        }
    }
}
