﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dataAccess.BaseManagement;
using businessLogic.Models;
using System.Data;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.InventoryManagement
{
    public class LotManager:MasterDetailManager<Lot>
    {
        public LotManager()
        {
            _procedureName = "sp_lote";
        }

        protected override Lot getCatalog(DataRow row, bool hasDetails)
        {
            var result = new Lot(row[0].ToString(),
                                (DateTime)row[1],
                                row[2].ToString() == "S");
            if(hasDetails)
            {
                var detailCaller = new LotDetailManager();
                var details = detailCaller.GetAll(new LotDetail(result.Id, "", 0, 0, false));
            }
            return result;
        }

        protected override Lot getNewCatalog()
        {
            return new Lot();
        }

        protected override void setCatalog(DataAccess dataAccess, Lot data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.Id);
            dataAccess.AdParameter("@i_fechaIngreso", DBTypeEnum.DateTime, data.AdmissionDate);
            dataAccess.AdParameter("@i_esActivo", DBTypeEnum.VarChar, data.IsActive?"S":"N");
        }

    }
}
