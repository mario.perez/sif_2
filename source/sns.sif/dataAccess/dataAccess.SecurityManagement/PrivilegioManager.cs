﻿using System;
using System.Collections.Generic;
using System.Data;
using businessLogic.Models;
using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.SecurityManagement
{
    public class PrivilegioManager
    {
        private string _procedureName;

        public PrivilegioManager()
        {
            _procedureName = "sp_privilegio";
        }

        public bool GestionarPrivilegio(Privilegio data, string operacion)
        {
            DataAccess dataAccess = new DataAccess();            
            DataSet dataSet;            

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@Id", DBTypeEnum.Integer, data.Id);
            dataAccess.AdParameter("@CodigoTag", DBTypeEnum.NVarChar, data.CodigoTag);
            dataAccess.AdParameter("@Nombre", DBTypeEnum.NVarChar, data.Nombre);
            dataAccess.AdParameter("@EsPadre", DBTypeEnum.Bit, data.EsPadre);
            dataAccess.AdParameter("@PadreId", DBTypeEnum.Integer, data.PadreId);
            dataAccess.AdParameter("@CodigoInterno", DBTypeEnum.NVarChar, data.CodigoInterno);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        public DataTable GetTable(Privilegio data, string operacion)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@Id", DBTypeEnum.Integer, data.Id);
            dataAccess.AdParameter("@CodigoTag", DBTypeEnum.NVarChar, data.CodigoTag);
            dataAccess.AdParameter("@Nombre", DBTypeEnum.NVarChar, data.Nombre);
            dataAccess.AdParameter("@EsPadre", DBTypeEnum.Bit, data.EsPadre);
            dataAccess.AdParameter("@PadreId", DBTypeEnum.Integer, data.PadreId);
            dataAccess.AdParameter("@CodigoInterno", DBTypeEnum.NVarChar, data.CodigoInterno);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                    
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response;
        }

    }
}
