﻿using System;
using System.Collections.Generic;
using System.Data;
using businessLogic.Models;
using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.SecurityManagement
{
    public class RolManager
    {
        private string _procedureName;

        public RolManager()
        {
            _procedureName = "sp_rol";
        }

        public bool GestionarRol(Rol data, string operacion)
        {
            DataAccess dataAccess = new DataAccess();            
            DataSet dataSet;            

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@Id",DBTypeEnum.Integer, data.Id);
            dataAccess.AdParameter("@Nombre", DBTypeEnum.NVarChar, data.Nombre);
            dataAccess.AdParameter("@Descripcion", DBTypeEnum.NVarChar, data.Descripcion);
            dataAccess.AdParameter("@Activo", DBTypeEnum.Bit, data.Activo);
            dataAccess.AdParameter("@UserCreacion", DBTypeEnum.NVarChar, data.UserCreacion);
            dataAccess.AdParameter("@UserModificacion", DBTypeEnum.NVarChar, data.UserModificacion);
            dataAccess.AdParameter("@FechaCreacion", DBTypeEnum.DateTime, data.FechaCreacion);
            dataAccess.AdParameter("@FechaModificacion", DBTypeEnum.DateTime, data.FechaModificacion);
            dataAccess.AdParameter("@AppCreacion", DBTypeEnum.NVarChar, data.AppCreacion);
            dataAccess.AdParameter("@AppModificacion", DBTypeEnum.NVarChar, data.AppModificacion);
            dataAccess.AdParameter("@MaqCreacion", DBTypeEnum.NVarChar, data.MaqCreacion);
            dataAccess.AdParameter("@MaqModificacion", DBTypeEnum.NVarChar, data.MaqModificacion);
            
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        public DataTable GetTable(Rol data,string operacion)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@Id", DBTypeEnum.Integer, data.Id);
            dataAccess.AdParameter("@Nombre", DBTypeEnum.NVarChar, data.Nombre);
            dataAccess.AdParameter("@Descripcion", DBTypeEnum.NVarChar, data.Descripcion);
            dataAccess.AdParameter("@Activo", DBTypeEnum.Bit, data.Activo);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                    
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response;
        }

    }
}
