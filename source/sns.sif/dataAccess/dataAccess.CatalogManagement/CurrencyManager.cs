﻿using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;

namespace dataAccess.CatalogManagement
{
    public class CurrencyManager : CustomCatalogManager<Currency>, ICRUD<Currency>
    {
        public CurrencyManager()
        {
            _procedureName = "sp_moneda";
        }

        protected override Currency getNewCatalog()
        {
            return new Currency();
        }

        protected override Currency getCatalog(DataRow row)
        {
            var result = new Currency(row[0].ToString(),
                                row[1].ToString(),
                                int.Parse(row[2].ToString()),
                                row[3].ToString(),
                                row[4].ToString(),
                                row[5].ToString() == "S");
            return result;
        }

        protected override void setCatalog(DataAccess dataAccess, Currency data)
        {            
            dataAccess.AdParameter("@i_codigo",DBTypeEnum.VarChar,data.Id);
            dataAccess.AdParameter("@i_Decimales", DBTypeEnum.Decimal, data.Decimals);
            dataAccess.AdParameter("@i_Nombre", DBTypeEnum.VarChar, data.Name);
            dataAccess.AdParameter("@i_Simbolo", DBTypeEnum.VarChar, data.Symbol);
            dataAccess.AdParameter("@i_EsActivo", DBTypeEnum.Bit, data.IsActive);
            dataAccess.AdParameter("@i_NumeroMoneda", DBTypeEnum.VarChar, data.CurrencyNumber);
        }
    }
}
