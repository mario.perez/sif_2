﻿using System;
using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;


namespace dataAccess.CatalogManagement
{
    public class ConfigurationManager : CustomCatalogManager<Configuration>, ICRUD<Configuration>
    {

        public ConfigurationManager()
        {
            _procedureName = "sp_configuracion";
        }


        protected override void setCatalog(DataAccess dataAccess, Configuration data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.Id);
            dataAccess.AdParameter("@i_valor", DBTypeEnum.VarChar, data.Value);
            dataAccess.AdParameter("@i_descripcion", DBTypeEnum.VarChar, data.Description);
        }

        protected override Configuration getNewCatalog()
        {
            return new Configuration();
        }

        protected override Configuration getCatalog(DataRow row)
        {
            var result = new Configuration(row[0].ToString(), row[1].ToString(), row[2].ToString());
            return result;
        }

    }
}
