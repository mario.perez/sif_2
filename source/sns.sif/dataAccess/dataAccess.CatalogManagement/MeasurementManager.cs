﻿using System.Collections.Generic;
using businessLogic.Models;
using dataAccess.BaseManagement;

namespace dataAccess.CatalogManagement
{
    public class MeasurementManager : BaseCatalogManager, ICRUD
    {
        private string _procedureName;

        public MeasurementManager()
        {
            _procedureName = "sp_unidad_medida";
        }

        public bool Add(Catalog data)
        {
            var result = InsertData(_procedureName, data);
            return result;
        }

        public List<Catalog> GetAll()
        {
            var result = GetList(_procedureName);
            return result;
        }

        public Catalog GetOne(string id)
        {
            var result = GetData(_procedureName, id);
            return result;
        }

        public bool Remove(Catalog data)
        {
            var result = DeleteData(_procedureName, data.Id);
            return result;
        }

        public bool Update(Catalog data)
        {
            var result = UpdateData(_procedureName, data);
            return result;
        }
    }
}
