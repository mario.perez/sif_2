﻿using System;
using System.Collections.Generic;
using System.Data;
using businessLogic.Models;
using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.CatalogManagement
{
    public class RolPrivilegioManager
    {
        private string _procedureName;

        public RolPrivilegioManager()
        {
            _procedureName = "sp_rolprivilegio";
        }

        public bool GestionarRolPrivilegio(int Id, int RolId, int PrivilegioId, string operacion)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@Id", DBTypeEnum.Integer, Id);
            dataAccess.AdParameter("@RolId", DBTypeEnum.Integer, RolId);
            dataAccess.AdParameter("@PrivilegioId", DBTypeEnum.Integer, PrivilegioId);

            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        public DataTable GetTable(int Id, int RolId, int PrivilegioId, string operacion)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@Id", DBTypeEnum.Integer, Id);
            dataAccess.AdParameter("@RolId", DBTypeEnum.Integer, RolId);
            dataAccess.AdParameter("@PrivilegioId", DBTypeEnum.Integer, PrivilegioId);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];

                }
                else
                {
                    response = new DataTable();
                }
            }
            return response;
        }
    }
}
