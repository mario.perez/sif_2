﻿using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;

namespace dataAccess.CatalogManagement
{
    public class ProductManager: CustomCatalogManager<Product>, ICRUD<Product>
    {
        public ProductManager()
        {
            _procedureName = "sp_producto";
        }

        protected override Product getCatalog(DataRow row)
        {
            var result = new Product();
            result.ProductId = row[0].ToString();
            result.Category = new CategoryManager().GetOne(row[1].ToString());
            result.Description = row[2].ToString();            
            result.Measurement = new MeasurementManager().GetOne(row[3].ToString());
            
            result.IsActive = row[4].ToString() == "S";
            result.HasStock = row[5].ToString() == "S";
            result.UnitsPerPackage = int.Parse(row[6].ToString());
            return result;
        }

        protected override Product getNewCatalog()
        {
            return new Product();
        }

        protected override void setCatalog(DataAccess dataAccess, Product data)
        {
            dataAccess.AdParameter("@i_Codigo", DBTypeEnum.VarChar, data.ProductId);
            dataAccess.AdParameter("@i_CodigoCategoria", DBTypeEnum.VarChar, data.Category.Id);
            dataAccess.AdParameter("@i_Descripcion", DBTypeEnum.VarChar, data.Description);
            dataAccess.AdParameter("@i_UnidadMedida", DBTypeEnum.VarChar, data.Measurement.Id);
            dataAccess.AdParameter("@i_EsActivo", DBTypeEnum.VarChar, data.IsActive);
            dataAccess.AdParameter("@i_LlevaExistencia", DBTypeEnum.VarChar, data.HasStock ? "S" : "N");
            dataAccess.AdParameter("@i_UnidadesPorMedida", DBTypeEnum.Integer, data.UnitsPerPackage);
        }
    }
}
