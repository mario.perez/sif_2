﻿using System;
using System.Collections.Generic;
using System.Data;
using businessLogic.Models;
using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.CatalogManagement
{
    public class ImpuestoManager
    {
        private string _procedureName;

        public ImpuestoManager()
        {
            _procedureName = "sp_impuesto";
        }

        public bool GestionarImpuesto(Impuesto data, string operacion)
        {
            DataAccess dataAccess = new DataAccess();            
            DataSet dataSet;            

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@impuesto_id", DBTypeEnum.Integer, data.ImpuestoId);
            dataAccess.AdParameter("@Codigo", DBTypeEnum.NVarChar, data.Codigo);
            dataAccess.AdParameter("@Descripcion_impto", DBTypeEnum.NVarChar, data.DescripcionImpuesto);
            dataAccess.AdParameter("@valor_impto", DBTypeEnum.Decimal, data.ValorImpuesto);
            dataAccess.AdParameter("@predeterminado", DBTypeEnum.Bit, data.Predeterminado);
            dataAccess.AdParameter("@activo", DBTypeEnum.Bit, data.Activo);
            dataAccess.AdParameter("@UserCreacion", DBTypeEnum.NVarChar, data.UserCreacion);
            dataAccess.AdParameter("@UserModificacion", DBTypeEnum.NVarChar, data.UserModificacion);
            dataAccess.AdParameter("@FechaCreacion", DBTypeEnum.NVarChar, data.FechaCreacion);
            dataAccess.AdParameter("@FechaModificacion", DBTypeEnum.NVarChar, data.FechaModificacion);
            dataAccess.AdParameter("@AppCreacion", DBTypeEnum.NVarChar, data.AppCreacion);
            dataAccess.AdParameter("@AppModificacion", DBTypeEnum.NVarChar, data.AppModificacion);
            dataAccess.AdParameter("@MaqCreacion", DBTypeEnum.NVarChar, data.MaqCreacion);
            dataAccess.AdParameter("@MaqModoficacion", DBTypeEnum.NVarChar, data.MaqModificacion);
            dataAccess.AdParameter("@Nombre", DBTypeEnum.NVarChar, data.Nombre);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        public DataTable GetTable(Impuesto data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, "S");
            dataAccess.AdParameter("@impuesto_id", DBTypeEnum.Integer, data.ImpuestoId);
            dataAccess.AdParameter("@Codigo", DBTypeEnum.NVarChar, data.Codigo);
            dataAccess.AdParameter("@Nombre", DBTypeEnum.NVarChar, data.Nombre);
            dataAccess.AdParameter("@Descripcion_impto", DBTypeEnum.NVarChar, data.DescripcionImpuesto);
            dataAccess.AdParameter("@valor_impto", DBTypeEnum.Decimal, data.ValorImpuesto);
            dataAccess.AdParameter("@predeterminado", DBTypeEnum.Bit, data.Predeterminado);
            dataAccess.AdParameter("@activo", DBTypeEnum.Bit, data.Activo);
            dataAccess.AdParameter("@UserCreacion", DBTypeEnum.NVarChar, data.UserCreacion);
            dataAccess.AdParameter("@UserModificacion", DBTypeEnum.NVarChar, data.UserModificacion);
            dataAccess.AdParameter("@FechaCreacion", DBTypeEnum.DateTime, data.FechaCreacion);
            dataAccess.AdParameter("@FechaModificacion", DBTypeEnum.DateTime, data.FechaModificacion);
            dataAccess.AdParameter("@AppCreacion", DBTypeEnum.NVarChar, data.AppCreacion);
            dataAccess.AdParameter("@AppModificacion", DBTypeEnum.NVarChar, data.AppModificacion);
            dataAccess.AdParameter("@MaqCreacion", DBTypeEnum.NVarChar, data.MaqCreacion);
            dataAccess.AdParameter("@MaqModoficacion", DBTypeEnum.NVarChar, data.MaqModificacion);

            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                    
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response;
        }

    }
}
