﻿using System.Collections.Generic;
using businessLogic.Models;

namespace dataAccess.CatalogManagement
{
    interface ICRUD
    {
        bool Add(Catalog data);

        bool Remove(Catalog data);

        List<Catalog> GetAll();

        Catalog GetOne(string id);

        bool Update(Catalog data);
        
    }

    interface ICRUD<T>
    {
        bool Add(T data);

        bool Remove(T data);

        List<T> GetAll();

        T GetOne(string id);

        bool Update(T data);

    }
}
