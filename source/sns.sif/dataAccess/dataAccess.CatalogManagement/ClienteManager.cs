﻿using System;
using System.Collections.Generic;
using System.Data;
using businessLogic.Models;
using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.CatalogManagement
{
    public class ClienteManager
    {
        private string _procedureName;

        public ClienteManager()
        {
            _procedureName = "sp_cliente";
        }

        public bool GestionarCliente(Cliente data, string operacion)
        {
            DataAccess dataAccess = new DataAccess();            
            DataSet dataSet;            

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@cliente_id",DBTypeEnum.Integer, data.cliente_id);
            dataAccess.AdParameter("@codigo_clte",DBTypeEnum.NVarChar,data.codigo_clte);
            dataAccess.AdParameter("@primer_apellido", DBTypeEnum.NVarChar, data.primer_apellido);
            dataAccess.AdParameter("@segundo_apellido", DBTypeEnum.NVarChar, data.segundo_apellido);
            dataAccess.AdParameter("@primer_nombre",DBTypeEnum.NVarChar, data.primer_nombre);
            dataAccess.AdParameter("@segundo_nombre", DBTypeEnum.NVarChar, data.segundo_nombre);
            dataAccess.AdParameter("@nombre_factura",DBTypeEnum.NVarChar, data.nombre_factura);
            dataAccess.AdParameter("@direccion1",DBTypeEnum.NVarChar,data.direccion1);
            dataAccess.AdParameter("@direccion2", DBTypeEnum.NVarChar, data.direccion2);
            dataAccess.AdParameter("@direccion3", DBTypeEnum.NVarChar, data.direccion3);
            dataAccess.AdParameter("@ciudad",DBTypeEnum.NVarChar,data.ciudad);
            dataAccess.AdParameter("@departamento",DBTypeEnum.NVarChar, data.departamento);
            dataAccess.AdParameter("@pais",DBTypeEnum.NVarChar,data.pais);
            dataAccess.AdParameter("@telefono1", DBTypeEnum.NVarChar,data.telefono1);
            dataAccess.AdParameter("@telefono2", DBTypeEnum.NVarChar, data.telefono2);
            dataAccess.AdParameter("@fax", DBTypeEnum.NVarChar, data.fax);
            dataAccess.AdParameter("@Contacto", DBTypeEnum.NVarChar, data.Contacto);
            dataAccess.AdParameter("@e_mail1", DBTypeEnum.NVarChar, data.e_mail1);
            dataAccess.AdParameter("@e_mail2", DBTypeEnum.NVarChar, data.e_mail2);
            dataAccess.AdParameter("@activo", DBTypeEnum.Bit, data.activo);
            dataAccess.AdParameter("@ruc", DBTypeEnum.NVarChar, data.ruc);
            dataAccess.AdParameter("@maximo_Credito", DBTypeEnum.Decimal, data.maximo_Credito);
            dataAccess.AdParameter("@comentario", DBTypeEnum.NVarChar, data.comentario);
            dataAccess.AdParameter("@UserCreacion", DBTypeEnum.NVarChar, data.UserCreacion);
            dataAccess.AdParameter("@UserModificacion", DBTypeEnum.NVarChar, data.UserModificacion);
            dataAccess.AdParameter("@FechaCreacion", DBTypeEnum.DateTime, data.FechaCreacion);
            dataAccess.AdParameter("@FechaModificacion", DBTypeEnum.DateTime, data.FechaModificacion);
            dataAccess.AdParameter("@AppCreacion", DBTypeEnum.NVarChar, data.AppCreacion);
            dataAccess.AdParameter("@AppModificacion", DBTypeEnum.NVarChar, data.AppModificacion);
            dataAccess.AdParameter("@MaqCreacion", DBTypeEnum.NVarChar, data.MaqCreacion);
            dataAccess.AdParameter("@MaqModificacion", DBTypeEnum.NVarChar, data.MaqModificacion);
            dataAccess.AdParameter("@cedula", DBTypeEnum.NVarChar, data.Cedula);
            dataAccess.AdParameter("@foto",DBTypeEnum.VarBinary, data.Foto);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        public DataTable GetTable(Cliente data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, 'S');
            dataAccess.AdParameter("@cliente_id", DBTypeEnum.Integer, data.cliente_id);
            //dataAccess.AdParameter("@codigo_clte", DBTypeEnum.NVarChar, data.codigo_clte);
            //dataAccess.AdParameter("@primer_apellido", DBTypeEnum.NVarChar, data.primer_apellido);
            //dataAccess.AdParameter("@segundo_apellido", DBTypeEnum.NVarChar, data.segundo_apellido);
            //dataAccess.AdParameter("@primer_nombre", DBTypeEnum.NVarChar, data.primer_nombre);
            //dataAccess.AdParameter("@segundo_nombre", DBTypeEnum.NVarChar, data.segundo_nombre);
            //dataAccess.AdParameter("@nombre_factura", DBTypeEnum.NVarChar, data.nombre_factura);
            //dataAccess.AdParameter("@direccion1", DBTypeEnum.NVarChar, data.direccion1);
            //dataAccess.AdParameter("@direccion2", DBTypeEnum.NVarChar, data.direccion2);
            //dataAccess.AdParameter("@direccion3", DBTypeEnum.NVarChar, data.direccion3);
            //dataAccess.AdParameter("@ciudad", DBTypeEnum.NVarChar, data.ciudad);
            //dataAccess.AdParameter("@departamento", DBTypeEnum.NVarChar, data.departamento);
            //dataAccess.AdParameter("@pais", DBTypeEnum.NVarChar, data.pais);
            //dataAccess.AdParameter("@telefono1", DBTypeEnum.NVarChar, data.telefono1);
            //dataAccess.AdParameter("@telefono2", DBTypeEnum.NVarChar, data.telefono2);
            //dataAccess.AdParameter("@fax", DBTypeEnum.NVarChar, data.fax);
            //dataAccess.AdParameter("@Contacto", DBTypeEnum.NVarChar, data.Contacto);
            //dataAccess.AdParameter("@e_mail1", DBTypeEnum.NVarChar, data.e_mail1);
            //dataAccess.AdParameter("@e_mail2", DBTypeEnum.NVarChar, data.e_mail2);
            //dataAccess.AdParameter("@activo", DBTypeEnum.Bit, data.activo);
            //dataAccess.AdParameter("@ruc", DBTypeEnum.NVarChar, data.ruc);
            //dataAccess.AdParameter("@maximo_Credito", DBTypeEnum.Decimal, data.maximo_Credito);
            //dataAccess.AdParameter("@comentario", DBTypeEnum.NVarChar, data.comentario);
            //dataAccess.AdParameter("@UserCreacion", DBTypeEnum.NVarChar, data.UserCreacion);
            //dataAccess.AdParameter("@UserModificacion", DBTypeEnum.NVarChar, data.UserModificacion);
            //dataAccess.AdParameter("@FechaCreacion", DBTypeEnum.DateTime, data.FechaCreacion);
            //dataAccess.AdParameter("@FechaModificacion", DBTypeEnum.DateTime, data.FechaModificacion);
            //dataAccess.AdParameter("@AppCreacion", DBTypeEnum.NVarChar, data.AppCreacion);
            //dataAccess.AdParameter("@AppModificacion", DBTypeEnum.NVarChar, data.AppModificacion);
            //dataAccess.AdParameter("@MaqCreacion", DBTypeEnum.NVarChar, data.MaqCreacion);
            //dataAccess.AdParameter("@MaqModificacion", DBTypeEnum.NVarChar, data.MaqModificacion);
            //dataAccess.AdParameter("@cedula", DBTypeEnum.NVarChar, data.Cedula);
            //dataAccess.AdParameter("@foto", DBTypeEnum.VarBinary, data.Foto);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                    
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response;
        }

    }
}
