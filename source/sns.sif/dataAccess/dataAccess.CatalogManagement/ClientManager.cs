using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;


namespace dataAccess.CatalogManagement
{
    public class ClientManager : CustomCatalogManager<Client>, ICRUD<Client>
    {
        

        public ClientManager()
        {
            _procedureName = "sp_Cliente";
        }
        protected override Client getNewCatalog()
        {
            return new Client();
        }

        protected override Client getCatalog(DataRow row)
        {
            var result = new Client( 
				row[0].ToString(),
				row[1].ToString(),
				row[2].ToString(),
				row[3].ToString(),
				row[4].ToString(),
				row[5].ToString(),
				row[6].ToString() );
            return result;
        }

        protected override void setCatalog(DataAccess dataAccess, Client data)
        {
            
			dataAccess.AdParameter("@i_CodigoCliente", DBTypeEnum.VarChar, data.ClientId); 
			dataAccess.AdParameter("@i_NombreCliente", DBTypeEnum.VarChar, data.ClientName); 
			dataAccess.AdParameter("@i_Telefono", DBTypeEnum.VarChar, data.Phone); 
			dataAccess.AdParameter("@i_Tipo", DBTypeEnum.VarChar, data.Type); 
			dataAccess.AdParameter("@i_Direccion", DBTypeEnum.VarChar, data.Address); 
			dataAccess.AdParameter("@i_CorreoElectronico", DBTypeEnum.VarChar, data.Email); 
			dataAccess.AdParameter("@i_Contacto", DBTypeEnum.VarChar, data.Contact);
        }

    }
}

