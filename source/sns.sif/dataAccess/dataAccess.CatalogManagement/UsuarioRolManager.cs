﻿using System;
using System.Collections.Generic;
using System.Data;
using businessLogic.Models;
using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.CatalogManagement
{
    public class UsuarioRolManager
    {
        private string _procedureName;

        public UsuarioRolManager()
        {
            _procedureName = "sp_usuariorol";
        }

        public bool GestionarUsuarioRol(int usuarioId, int rolId, int id, string operacion)
        {
            DataAccess dataAccess = new DataAccess();            
            DataSet dataSet;            

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@Id",DBTypeEnum.Integer, id);
            dataAccess.AdParameter("@UsuarioId", DBTypeEnum.Integer, usuarioId);
            dataAccess.AdParameter("@RolId", DBTypeEnum.Integer, rolId);

            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        public DataTable GetTable(int usuarioId, int rolId, int id, string operacion,string codigoTag)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;

            bool result = false;
            dataAccess.AdParameter("@Operacion", DBTypeEnum.Char, operacion);
            dataAccess.AdParameter("@Id", DBTypeEnum.Integer, id);
            dataAccess.AdParameter("@UsuarioId", DBTypeEnum.Integer, usuarioId);
            dataAccess.AdParameter("@RolId", DBTypeEnum.Integer, rolId);
            dataAccess.AdParameter("@CodigoTag",DBTypeEnum.NVarChar,codigoTag);

            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                    
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response;
        }

    }
}
