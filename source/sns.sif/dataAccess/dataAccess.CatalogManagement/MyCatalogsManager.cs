using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;


namespace dataAccess.CatalogManagement
{
    public class MyCatalogsManager : CustomCatalogManager<MyCatalogs>, ICRUD<MyCatalogs>
    {
        

        public MyCatalogsManager()
        {
            _procedureName = "sp_MisCatalogos";
        }
        protected override MyCatalogs getNewCatalog()
        {
            return new MyCatalogs();
        }

        protected override MyCatalogs getCatalog(DataRow row)
        {
            var result = new MyCatalogs( 
				row[0].ToString(),
				row[1].ToString(),
				row[2].ToString() );
            return result;
        }

        protected override void setCatalog(DataAccess dataAccess, MyCatalogs data)
        {
            
			dataAccess.AdParameter("@i_Tabla", DBTypeEnum.VarChar, data.TableName); 
			dataAccess.AdParameter("@i_Codigo", DBTypeEnum.VarChar, data.ItemId); 
			dataAccess.AdParameter("@i_Descripcion", DBTypeEnum.VarChar, data.ItemDescription);
        }

    }
}

