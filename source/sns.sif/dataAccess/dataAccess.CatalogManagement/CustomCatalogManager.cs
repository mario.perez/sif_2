﻿using System;
using System.Collections.Generic;
using System.Data;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.CatalogManagement
{
    public class CustomCatalogManager<T>
    {
        protected string _procedureName;

        private static DataRowCollection GetTable(string procedureName)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "S");
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response.Rows;
        }

        private static DataRowCollection GetRow(string procedureName, string code)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "C");
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.Integer, code);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response.Rows;
        }

        protected List<T> GetList(string procedureName)
        {
            var response = GetTable(procedureName);
            List<T> catalogList = new List<T>();
            if(response == null)
            {
                return catalogList;
            }
            foreach(DataRow row in response)
            {
                T catalog = getCatalog(row);
                catalogList.Add(catalog);
            }
            return catalogList;
        }

        protected virtual T getCatalog(DataRow row)
        {
            throw new NotImplementedException();
        }

        protected  T GetData(string procedureName, string code)
        {
            var response = GetRow(procedureName, code);
            T result;
            result = getNewCatalog();
            foreach(DataRow row in response)
            {
                result = getCatalog(row);
            }
            return result;
        }

        protected virtual  T getNewCatalog()
        {
            throw new NotImplementedException();
        }

        protected bool InsertData(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "I");
            setCatalog(dataAccess,data);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        protected virtual void setCatalog(DataAccess dataAccess, T data)
        {
            throw new NotImplementedException();
        }

        protected bool DeleteData(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "D");
            setCatalog(dataAccess, data);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }


        protected bool UpdateData(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "U");
            setCatalog(dataAccess, data);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        public bool Add(T data)
        {
            var result = InsertData(_procedureName, data);
            return result;
        }

        public List<T> GetAll()
        {
            var result = GetList(_procedureName);
            return result;
        }

        public T GetOne(string id)
        {
            var result = GetData(_procedureName, id);
            return result;
        }

        public bool Remove(T data)
        {
            var result = DeleteData(_procedureName, data);
            return result;
        }

        public bool Update(T data)
        {
            var result = UpdateData(_procedureName, data);
            return result;
        }

    }
}
