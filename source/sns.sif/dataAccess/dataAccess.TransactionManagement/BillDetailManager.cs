﻿using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;


namespace dataAccess.TransactionManagement
{
    public class BillDetailManager : MasterDetailManager<BillDetail>
    {
        public BillDetailManager()
        {
            _procedureName = "sp_detalle_factura";
        }

        protected override BillDetail getCatalog(DataRow row, bool hasDetails)
        {
            var result = new BillDetail(row[0].ToString(),
                                    row[1].ToString(),
                                    row[2].ToString(),
                                    row[3].ToString(),
                                    float.Parse(row[4].ToString()),
                                    decimal.Parse(row[5].ToString()));
            return result;
        }

        protected override BillDetail getNewCatalog()
        {
            return new BillDetail();
        }

        protected override void setCatalog(DataAccess dataAccess, BillDetail data)
        {
            dataAccess.AdParameter("@i_CodigoDetalleFactura", DBTypeEnum.VarChar, data.BillDetailId);
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.BillId);
            dataAccess.AdParameter("@i_CodigoLote", DBTypeEnum.VarChar, data.LotId);
            dataAccess.AdParameter("@i_CodigoProducto", DBTypeEnum.VarChar, data.ProductId);
            dataAccess.AdParameter("@i_Cantidad", DBTypeEnum.Decimal, data.Amount);
            dataAccess.AdParameter("@i_Precio", DBTypeEnum.Decimal, data.Price);
        }

    }
}
