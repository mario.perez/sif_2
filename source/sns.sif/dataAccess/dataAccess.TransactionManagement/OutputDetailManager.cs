﻿using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;


namespace dataAccess.TransactionManagement
{
    public class OutputDetailManager: MasterDetailManager<OutputDetail>
    {
        public OutputDetailManager()
        {
            _procedureName = "sp_detalle_salida";
        }

        protected override OutputDetail getCatalog(DataRow row, bool hasDetails)
        {
            var result = new OutputDetail(row[0].ToString(), 
                                    row[1].ToString(), 
                                    row[2].ToString(), 
                                    row[3].ToString(),
                                    float.Parse(row[4].ToString()), 
                                    decimal.Parse(row[5].ToString()));
            return result;
        }

        protected override OutputDetail getNewCatalog()
        {
            return new OutputDetail();
        }

        protected override void setCatalog(DataAccess dataAccess, OutputDetail data)
        {
            dataAccess.AdParameter("@i_CodigoDetalleSalida", DBTypeEnum.VarChar, data.OutputDetailId);
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.OutputId);
            dataAccess.AdParameter("@i_CodigoLote", DBTypeEnum.VarChar, data.LotId);
            dataAccess.AdParameter("@i_CodigoProducto", DBTypeEnum.VarChar, data.ProductId);
            dataAccess.AdParameter("@i_Cantidad", DBTypeEnum.Decimal, data.Amount);
            dataAccess.AdParameter("@i_Precio", DBTypeEnum.Decimal, data.Price);
        }

    }
}
