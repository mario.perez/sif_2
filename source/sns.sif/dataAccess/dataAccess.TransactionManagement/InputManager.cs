﻿using System;
using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;
using System.Collections.Generic;

namespace dataAccess.TransactionManagement
{
    public class InputManager: TransactionManager<Input>
    {
        
        public InputManager()
        {
            _procedureName = "sp_entrada";
        }

        protected override Input getNewCatalog()
        {
            return new Input();

        }

        protected override Input getCatalog(DataRow row, bool hasDetails)
        {
            var result = new Input();
            result.InputId = row[0].ToString();
            result.Date = (DateTime)row[1];
            result.WarehouseId = int.Parse(row[2].ToString());
            result.Comment = row[3].ToString();
            result.IsCancelled = row[4].ToString() == "S";
            result.Type = int.Parse(row[5].ToString());
            result.IsRegistered = row[6].ToString() == "S";
            result.CancelUser = row[7].ToString();
            result.CancelAproveUser = row[8].ToString();
            result.CancelDate = (DateTime)row[9];
            if(hasDetails)
            {
                var detailCaller = new InputDetailManager();
                var details = detailCaller.GetAll(new InputDetail("", result.InputId, "", "", 0, 0));
                result.Details.AddRange(details);
            }
            return result;

        }
        protected override void setCatalog(DataAccess dataAccess, Input data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.InputId);
            dataAccess.AdParameter("@i_Fecha", DBTypeEnum.DateTime, data.Date);
            dataAccess.AdParameter("@i_Bodega", DBTypeEnum.Integer, data.WarehouseId);
            dataAccess.AdParameter("@i_Observaciones", DBTypeEnum.VarChar, data.Comment);
            dataAccess.AdParameter("@i_EsAnulada", DBTypeEnum.VarChar, data.IsCancelled ? "S" : "N");
            dataAccess.AdParameter("@i_Tipo", DBTypeEnum.TinyInt, data.Type);
            dataAccess.AdParameter("@i_EsRegistrada", DBTypeEnum.TinyInt, data.IsRegistered ? "S" : "N");
            dataAccess.AdParameter("@i_UsuarioAnula", DBTypeEnum.VarChar, data.CancelUser);
            dataAccess.AdParameter("@i_UsuarioApruebaAnula", DBTypeEnum.VarChar, data.CancelAproveUser);
            dataAccess.AdParameter("@i_FechaHoraAnula", DBTypeEnum.DateTime, data.CancelDate);
        }
    }
}
