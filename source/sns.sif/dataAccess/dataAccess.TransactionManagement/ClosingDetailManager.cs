﻿using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;

namespace dataAccess.TransactionManagement
{
    public class ClosingDetailManager: MasterDetailManager<ClosingDetail>
    {
        public ClosingDetailManager()
        {
            _procedureName = "sp_detalle_cierre";
        }

        protected override ClosingDetail getCatalog(DataRow row, bool hasDetails)
        {
            var result = new ClosingDetail(long.Parse(row[0].ToString()),
                                            int.Parse(row[1].ToString()),
                                            long.Parse(row[2].ToString()),
                                            decimal.Parse(row[4].ToString()));
            return result;
        }

        protected override ClosingDetail getNewCatalog()
        {
            return new ClosingDetail();
        }
        
        protected override void setCatalog(DataAccess dataAccess, ClosingDetail data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.BigInt, data.ClosingId);
            dataAccess.AdParameter("@i_CodigoDenominacion", DBTypeEnum.Integer, data.DenominationId);
            dataAccess.AdParameter("@i_Cantidad", DBTypeEnum.BigInt, data.Quantity);
            dataAccess.AdParameter("@i_MontoEquivalente", DBTypeEnum.Numeric, data.CashAmount);
        }

    }
}
