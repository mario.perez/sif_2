﻿using System;
using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;
using System.Collections.Generic;

namespace dataAccess.TransactionManagement
{
    public class ClosingManager: MasterDetailManager<Closing>
    {
        public ClosingManager()
        {
            _procedureName = "sp_cierre";
        }

        protected override Closing getCatalog(DataRow row, bool hasDetails)
        {
            var result = new Closing(long.Parse(row[0].ToString()),
                                    int.Parse(row[1].ToString()),
                                    (DateTime)row[2],
                                    (DateTime)row[3],
                                    row[4].ToString());
            if(hasDetails)
            {
                var detailCaller = new ClosingDetailManager();
                var details = detailCaller.GetAll(new ClosingDetail(result.Id,-1,0,0));
                result.Details.AddRange(details);
            }


            return result;
        }

        protected override Closing getNewCatalog()
        {
            return new Closing();
        }

        protected override void setCatalog(DataAccess dataAccess, Closing data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.BigInt, data.Id);
            dataAccess.AdParameter("@i_CodigoCaja", DBTypeEnum.Integer, data.CashboxId);
            dataAccess.AdParameter("@i_FechaHoraInicio", DBTypeEnum.DateTime, data.InitialDateTime);
            dataAccess.AdParameter("@i_FechaHoraFin", DBTypeEnum.DateTime, data.FinalDateTime);
            dataAccess.AdParameter("@i_NombreUsuario", DBTypeEnum.VarChar, data.UserName);
        }

    }
}
