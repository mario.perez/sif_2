﻿using System;
using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;
using System.Collections.Generic;

namespace dataAccess.TransactionManagement
{
    public class BillManager: TransactionManager<Bill>
    {
        public BillManager()
        {
            _procedureName = "sp_factura";
        }
        protected override Bill getNewCatalog()
        {
            return new Bill();
        }

        protected override Bill getCatalog(DataRow row, bool hasDetails)
        {
            var result = new Bill();
            result.BillId = row[0].ToString();
            result.Date = (DateTime)row[1];
            result.WarehouseId = int.Parse(row[2].ToString());
            result.Subtotal = decimal.Parse(row[3].ToString());
            result.Discount = decimal.Parse(row[4].ToString());
            result.Tax = decimal.Parse(row[5].ToString());
            result.TotalAmount = decimal.Parse(row[6].ToString());
            result.Cash = decimal.Parse(row[7].ToString());
            result.CreditCard = decimal.Parse(row[8].ToString());
            result.SalesmanId = row[9].ToString();
            result.CashboxId = row[10].ToString();
            result.TypePayment = row[11].ToString();
            result.IsCancelled = row[12].ToString() == "S";
            result.IsRegistered = row[13].ToString() == "S";
            result.DiscountRate = decimal.Parse(row[14].ToString());
            result.DiscountUser = row[15].ToString();
            result.DiscountAproveUser = row[16].ToString();
            result.Comments = row[17].ToString();
            result.CancelUser = row[18].ToString();
            result.CancelAproveUser = row[19].ToString();
            result.CancelDate = (DateTime)row[20];

            if (hasDetails)
            {
                var detailCaller = new BillDetailManager();
                var details = detailCaller.GetAll(new BillDetail("", result.BillId, "", "", 0, 0));
                result.Details.AddRange(details);
            }

            return result;
        }

        protected override void setCatalog(DataAccess dataAccess, Bill data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.BillId);
            dataAccess.AdParameter("@i_Fecha", DBTypeEnum.DateTime, data.Date);
            dataAccess.AdParameter("@i_Bodega", DBTypeEnum.Integer, data.WarehouseId);
            dataAccess.AdParameter("@i_TotalFactura", DBTypeEnum.Decimal, data.Subtotal);
            dataAccess.AdParameter("@i_Descuento", DBTypeEnum.Decimal, data.Discount);
            dataAccess.AdParameter("@i_Impuesto", DBTypeEnum.Decimal, data.Tax);
            dataAccess.AdParameter("@i_TotalNeto", DBTypeEnum.Decimal, data.TotalAmount);
            dataAccess.AdParameter("@i_Efectivo", DBTypeEnum.Decimal, data.Cash);
            dataAccess.AdParameter("@i_Tarjeta", DBTypeEnum.Decimal, data.CreditCard);
            dataAccess.AdParameter("@i_CodigoVendedor", DBTypeEnum.Decimal, data.SalesmanId);
            dataAccess.AdParameter("@i_CodigoCaja", DBTypeEnum.Integer, data.CashboxId);
            dataAccess.AdParameter("@i_TipoPago", DBTypeEnum.TinyInt, data.TypePayment);
            dataAccess.AdParameter("@i_EsAnulada", DBTypeEnum.VarChar, data.IsCancelled ? "S" : "N");
            dataAccess.AdParameter("@i_EsRegistrada", DBTypeEnum.TinyInt, data.IsRegistered ? "S" : "N");
            dataAccess.AdParameter("@i_PorcentajeDescuento", DBTypeEnum.Decimal, data.DiscountRate);
            dataAccess.AdParameter("@i_UsuarioDescuento", DBTypeEnum.VarChar, data.DiscountUser);
            dataAccess.AdParameter("@i_UsuarioApruebaDescuento", DBTypeEnum.VarChar, data.DiscountAproveUser);
            dataAccess.AdParameter("@i_Observaciones", DBTypeEnum.VarChar, data.Comments);
            dataAccess.AdParameter("@i_UsuarioAnula", DBTypeEnum.VarChar, data.CancelUser);
            dataAccess.AdParameter("@i_UsuarioApruebaAnula", DBTypeEnum.VarChar, data.CancelAproveUser);
            dataAccess.AdParameter("@i_FechaHoraAnula", DBTypeEnum.DateTime, data.CancelDate);

        }

    }
}
