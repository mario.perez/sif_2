﻿using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;

namespace dataAccess.TransactionManagement
{
    public class InputDetailManager: MasterDetailManager<InputDetail>
    {
        public InputDetailManager()
        {

        }

        protected override InputDetail getCatalog(DataRow row, bool hasDetails)
        {
            var result = new InputDetail(row[0].ToString(),
                                        row[1].ToString(),
                                        row[2].ToString(),
                                        row[3].ToString(),
                                    float.Parse(row[4].ToString()),
                                    decimal.Parse(row[5].ToString()));
            return result;
        }
        protected override InputDetail getNewCatalog()
        {
            return new InputDetail();
        }

        protected override void setCatalog(DataAccess dataAccess, InputDetail data)
        {
            dataAccess.AdParameter("@i_CodigoDetalleEntrada", DBTypeEnum.VarChar, data.InputDetailId);
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.InputId);
            dataAccess.AdParameter("@i_CodigoLote", DBTypeEnum.VarChar, data.LotId);
            dataAccess.AdParameter("@i_CodigoProducto", DBTypeEnum.VarChar, data.ProductId);
            dataAccess.AdParameter("@i_Cantidad", DBTypeEnum.Decimal, data.Amount);
            dataAccess.AdParameter("@i_Precio", DBTypeEnum.Decimal, data.Price);
        }
    }
}
