﻿using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;


namespace dataAccess.TransactionManagement
{
    public class PurchaseDetailManager : MasterDetailManager<PurchaseDetail>
    {
        public PurchaseDetailManager()
        {
            _procedureName = "sp_detalle_compra";
        }

        protected override PurchaseDetail getCatalog(DataRow row, bool hasDetails)
        {
            var result = new PurchaseDetail(row[0].ToString(),
                                    row[1].ToString(),
                                    row[2].ToString(),
                                    row[3].ToString(),
                                    float.Parse(row[4].ToString()),
                                    decimal.Parse(row[5].ToString()));
            return result;
        }

        protected override PurchaseDetail getNewCatalog()
        {
            return new PurchaseDetail();
        }

        protected override void setCatalog(DataAccess dataAccess, PurchaseDetail data)
        {
            dataAccess.AdParameter("@i_CodigoDetalleCompra", DBTypeEnum.VarChar, data.PurchaseDetailId);
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.PurchaseId);
            dataAccess.AdParameter("@i_CodigoLote", DBTypeEnum.VarChar, data.LotId);
            dataAccess.AdParameter("@i_CodigoProducto", DBTypeEnum.VarChar, data.ProductId);
            dataAccess.AdParameter("@i_Cantidad", DBTypeEnum.Decimal, data.Amount);
            dataAccess.AdParameter("@i_Precio", DBTypeEnum.Decimal, data.Price);
        }

    }
}
