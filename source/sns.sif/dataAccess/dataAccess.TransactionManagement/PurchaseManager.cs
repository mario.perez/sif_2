﻿using System;
using System.Data;
using businessLogic.Models;
using dataAccess.SqlDatabaseAccess;
using dataAccess.BaseManagement;
using System.Collections.Generic;

namespace dataAccess.TransactionManagement
{
    public class PurchaseManager : TransactionManager<Purchase>
    {
        public PurchaseManager()
        {
            _procedureName = "sp_compra";
        }

        protected override Purchase getNewCatalog()
        {
            return new Purchase();
        }

        protected override Purchase getCatalog(DataRow row, bool hasDetails)
        {
            var result = new Purchase();
            result.PurchaseId = row[0].ToString();
            result.ProviderId = row[1].ToString();
            result.Date = (DateTime)row[2];
            result.WarehouseId = int.Parse(row[3].ToString());
            result.Subtotal = decimal.Parse(row[4].ToString());
            result.Discount = decimal.Parse(row[5].ToString());
            result.Tax = decimal.Parse(row[6].ToString());
            result.TotalAmount = decimal.Parse(row[7].ToString());
            result.Comments = row[8].ToString();
            result.IsCancelled = row[9].ToString() == "S";
            result.IsRegistered= row[10].ToString() == "S";
            result.CancelUser = row[11].ToString();
            result.CancelAproveUser = row[12].ToString();
            result.CancelDate = (DateTime)row[13];

            if (hasDetails)
            {
                var detailCaller = new PurchaseDetailManager();
                var details = detailCaller.GetAll(new PurchaseDetail("", result.PurchaseId, "", "", 0, 0));
                result.Details.AddRange(details);
            }

            return result;
        }

        protected override void setCatalog(DataAccess dataAccess, Purchase data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.PurchaseId);
            dataAccess.AdParameter("@i_CodigoProveedor", DBTypeEnum.BigInt, data.PurchaseId);
            dataAccess.AdParameter("@i_Fecha", DBTypeEnum.DateTime, data.Date);
            dataAccess.AdParameter("@i_Bodega", DBTypeEnum.Integer, data.WarehouseId);
            dataAccess.AdParameter("@i_TotalCompra", DBTypeEnum.Decimal, data.Subtotal);
            dataAccess.AdParameter("@i_Descuento", DBTypeEnum.Decimal, data.Discount);
            dataAccess.AdParameter("@i_Impuesto", DBTypeEnum.Decimal, data.Tax);
            dataAccess.AdParameter("@i_TotalNeto", DBTypeEnum.Decimal, data.TotalAmount);
            dataAccess.AdParameter("@i_Observaciones", DBTypeEnum.VarChar, data.Comments);
            dataAccess.AdParameter("@i_EsAnulada", DBTypeEnum.VarChar, data.IsCancelled ? "S" : "N");
            dataAccess.AdParameter("@i_EsRegistrada", DBTypeEnum.TinyInt, data.IsRegistered ? "S" : "N");
            dataAccess.AdParameter("@i_UsuarioAnula", DBTypeEnum.VarChar, data.CancelUser);
            dataAccess.AdParameter("@i_UsuarioApruebaAnula", DBTypeEnum.VarChar, data.CancelAproveUser);
            dataAccess.AdParameter("@i_FechaHoraAnula", DBTypeEnum.DateTime, data.CancelDate);

        }
    }
}
