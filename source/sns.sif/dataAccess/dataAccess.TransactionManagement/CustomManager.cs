﻿using System;
using System.Collections.Generic;
using System.Data;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.TransactionManagement
{
    public class CustomManager<T>
    {
        protected string _procedureName;

        private DataRowCollection GetTable(string procedureName)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "S");
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response.Rows;
        }

        private DataRowCollection GetRow(string procedureName, T toSearch)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "C");
            setCatalog(dataAccess, toSearch);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response.Rows;
        }

        protected List<T> GetList(string procedureName)
        {
            var response = GetTable(procedureName);
            List<T> catalogList = new List<T>();
            if (response == null)
            {
                return catalogList;
            }
            foreach (DataRow row in response)
            {
                T catalog = getCatalog(row);
                catalogList.Add(catalog);
            }
            return catalogList;
        }

        protected virtual T getCatalog(DataRow row)
        {
            throw new NotImplementedException();
        }

        protected T GetData(string procedureName, T toSearch)
        {
            var response = GetRow(procedureName, toSearch);
            T result;
            result = getNewCatalog();
            foreach (DataRow row in response)
            {
                result = getCatalog(row);
            }
            return result;
        }

        protected virtual T getNewCatalog()
        {
            throw new NotImplementedException();
        }

        protected DataRowCollection InsertData(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "I");
            setCatalog(dataAccess, data);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response.Rows;
            
        }

        protected virtual void setCatalog(DataAccess dataAccess, T data)
        {
            throw new NotImplementedException();
        }

        protected bool DeleteData(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "D");
            setCatalog(dataAccess, data);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }


        protected bool UpdateData(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "U");
            setCatalog(dataAccess, data);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        public T Add(T data)
        {
            var response = InsertData(_procedureName, data);
            T result;
            result = getNewCatalog();
            foreach (DataRow row in response)
            {
                result = getCatalog(row);
            }
            return result;
        }

        public List<T> GetAll()
        {
            var result = GetList(_procedureName);
            return result;
        }

        public T GetOne(T toSearch)
        {
            var result = GetData(_procedureName, toSearch);
            return result;
        }

        public bool Remove(T data)
        {
            var result = DeleteData(_procedureName, data);
            return result;
        }

        public bool Update(T data)
        {
            var result = UpdateData(_procedureName, data);
            return result;
        }
    }
}
