﻿using System;
using System.Collections.Generic;
using System.Data;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.BaseManagement
{
    public class TransactionManager<T>:MasterDetailManager<T>
    {

        protected bool CancelTransaction(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;

            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "A");
            setCatalog(dataAccess, data);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }
        public bool Cancel(T data)
        {
            return CancelTransaction(_procedureName, data);
        }
    }
}
