﻿using System.Collections.Generic;
using businessLogic.Models;

namespace dataAccess.BaseManagement
{
    public interface ICRUD
    {
        bool Add(Catalog data);

        bool Remove(Catalog data);

        List<Catalog> GetAll();

        Catalog GetOne(string id);

        bool Update(Catalog data);
        
    }

    public interface ICRUD<T>
    {
        bool Add(T data);

        bool Remove(T data);

        List<T> GetAll(string id);

        T GetOne(string id);

        bool Update(T data);

    }
}
