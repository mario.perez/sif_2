﻿using System.Collections.Generic;
using System.Data;
using dataAccess.SqlDatabaseAccess;
using businessLogic.Models;

namespace dataAccess.BaseManagement
{
    public abstract class BaseCatalogManager
    {
        private static DataRowCollection GetTable(string procedureName)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "S");
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response.Rows;
        }

        private static DataRowCollection GetRow(string procedureName,string code)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "C");
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.Integer, code);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response.Rows;
        }

        protected static List<businessLogic.Models.Catalog> GetList(string procedureName)
        {
            var response = GetTable(procedureName);
            List<businessLogic.Models.Catalog> catalogList = new List<businessLogic.Models.Catalog>();
            if(response == null)
            {
                return catalogList;
            }
            foreach(DataRow row in response)
            {
                var catalogo = new Catalog(row[0].ToString(), row[1].ToString(),  row[2].ToString()== "S");
                catalogList.Add(catalogo);
            }
            return catalogList;
        }

        protected static Catalog GetData(string procedureName, string code)
        {
            var response = GetRow(procedureName,code);
            Catalog result = new Catalog();
            if(response == null)
            {
                return new Catalog();
            }
            foreach(DataRow row in response)
            {
                result = new Catalog(row[0].ToString(), row[1].ToString(), row[2].ToString() == "S");
            }
            return result;
        }

        protected static bool InsertData(string procedureName, Catalog catalog)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "I");
            dataAccess.AdParameter("@i_nombre", DBTypeEnum.VarChar, catalog.Name);
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, catalog.Id);
            dataAccess.AdParameter("@i_es_activo", DBTypeEnum.Bit, catalog.isActive);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        protected static bool DeleteData(string procedureName, string code)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "D");
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, code);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }
        protected static bool UpdateData(string procedureName, Catalog catalog)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "U");
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, catalog.Id);
            dataAccess.AdParameter("@i_nombre", DBTypeEnum.VarChar, catalog.Name);
            dataAccess.AdParameter("@i_es_activo", DBTypeEnum.Bit, catalog.isActive);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

    }
}
