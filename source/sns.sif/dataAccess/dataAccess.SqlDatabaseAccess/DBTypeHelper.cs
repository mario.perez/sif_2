﻿using System.Data;

namespace dataAccess.SqlDatabaseAccess
{
    public class DBTypeHelper
    {
        public static int ConvertDBType(DBTypeEnum sourceDBType)
        {
            int targetDBType = -1;

            switch (sourceDBType)
            {
                case DBTypeEnum.BigDateTime:
                    targetDBType = (int)SqlDbType.DateTime;
                    break;
                case DBTypeEnum.BigInt:
                    targetDBType = (int)SqlDbType.BigInt;
                    break;
                case DBTypeEnum.Binary:
                    targetDBType = (int)SqlDbType.Binary;
                    break;
                case DBTypeEnum.Bit:
                    targetDBType = (int)SqlDbType.Bit;
                    break;
                case DBTypeEnum.Char:
                    targetDBType = (int)SqlDbType.Char;
                    break;
                case DBTypeEnum.Date:
                    targetDBType = (int)SqlDbType.Date;
                    break;
                case DBTypeEnum.DateTime:
                    targetDBType = (int)SqlDbType.DateTime;
                    break;
                case DBTypeEnum.Decimal:
                    targetDBType = (int)SqlDbType.Decimal;
                    break;
                case DBTypeEnum.Double:
                    targetDBType = (int)SqlDbType.Decimal;
                    break;
                case DBTypeEnum.Image:
                    targetDBType = (int)SqlDbType.Image;
                    break;
                case DBTypeEnum.Integer:
                    targetDBType = (int)SqlDbType.Int;
                    break;
                case DBTypeEnum.Money:
                    targetDBType = (int)SqlDbType.Money;
                    break;
                case DBTypeEnum.NChar:
                    targetDBType = (int)SqlDbType.NChar;
                    break;
                case DBTypeEnum.Numeric:
                    targetDBType = (int)SqlDbType.Decimal;
                    break;
                case DBTypeEnum.NVarChar:
                    targetDBType = (int)SqlDbType.NVarChar;
                    break;
                case DBTypeEnum.Real:
                    targetDBType = (int)SqlDbType.Real;
                    break;
                case DBTypeEnum.SmallDateTime:
                    targetDBType = (int)SqlDbType.SmallDateTime;
                    break;
                case DBTypeEnum.SmallInt:
                    targetDBType = (int)SqlDbType.SmallInt;
                    break;
                case DBTypeEnum.SmallMoney:
                    targetDBType = (int)SqlDbType.SmallMoney;
                    break;
                case DBTypeEnum.Text:
                    targetDBType = (int)SqlDbType.Text;
                    break;
                case DBTypeEnum.Time:
                    targetDBType = (int)SqlDbType.Time;
                    break;
                case DBTypeEnum.TimeStamp:
                    targetDBType = (int)SqlDbType.Timestamp;
                    break;
                case DBTypeEnum.TinyInt:
                    targetDBType = (int)SqlDbType.TinyInt;
                    break;
                case DBTypeEnum.UniChar:
                    targetDBType = (int)SqlDbType.NChar;
                    break;
                case DBTypeEnum.Unitext:
                    targetDBType = (int)SqlDbType.NText;
                    break;
                case DBTypeEnum.UniVarChar:
                    targetDBType = (int)SqlDbType.NVarChar;
                    break;
                case DBTypeEnum.UnsignedBigInt:
                    targetDBType = (int)SqlDbType.BigInt;
                    break;
                case DBTypeEnum.UnsignedInt:
                    targetDBType = (int)SqlDbType.Int;
                    break;
                case DBTypeEnum.UnsignedSmallInt:
                    targetDBType = (int)SqlDbType.SmallInt;
                    break;
                case DBTypeEnum.Unsupported:
                    targetDBType = -1;
                    break;
                case DBTypeEnum.VarBinary:
                    targetDBType = (int)SqlDbType.VarBinary;
                    break;
                case DBTypeEnum.VarChar:
                    targetDBType = (int)SqlDbType.VarChar;
                    break;
                default:
                    targetDBType = (int)SqlDbType.VarChar;
                    break;
            }
            return targetDBType;
        }

    }

}
