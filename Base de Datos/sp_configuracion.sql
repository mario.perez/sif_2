USE SIF
go

IF OBJECT_ID('dbo.sp_configuracion') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_configuracion
    IF OBJECT_ID('dbo.sp_configuracion') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_configuracion >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_configuracion >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE sp_configuracion
	@i_user        varchar(32),
	@i_terminal    varchar(32),
	@i_operacion   char(1),
	@i_codigo      varchar(32) = '',
	@i_valor       varchar(max) = '',
	@i_descripcion varchar(max) = ''
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		SELECT   CodigoConfiguracion
				,Valor
				,Descripcion
		FROM Configuracion
		WHERE CodigoConfiguracion>@i_codigo
		ORDER BY CodigoConfiguracion asc

		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		SELECT   CodigoConfiguracion
				,Valor
				,Descripcion
		FROM Configuracion
		WHERE CodigoConfiguracion = @i_codigo
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Configuracion
		SET  Valor = @i_valor
			,Descripcion = @i_descripcion
		WHERE CodigoConfiguracion = @i_codigo

		return 0
	END
	
END
GO

IF OBJECT_ID('dbo.sp_configuracion') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_configuracion') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_configuracion >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_configuracion >>>'
END
go
