USE [SIF]
GO

/****** Object:  StoredProcedure [dbo].[sp_unidad_medida]    Script Date: 6/9/2018 11:42:34 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.sp_unidad_medida') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_unidad_medida
    IF OBJECT_ID('dbo.sp_unidad_medida') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_unidad_medida >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_unidad_medida >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <24/04/2018>
-- Description:	<SCRUD para Unidad de medida.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_unidad_medida]
	@i_user        varchar(32),
	@i_terminal    varchar(32),
	@i_operacion   char(1),
	@i_codigo      int = -1,
	@i_nombre      varchar(32) = '',
	@i_es_activo   char(1) = 'N'
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		select  CodigoUnidadMedida,
				Nombre,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from UnidadMedida
		where EsActivo = 1
		AND CodigoUnidadMedida>@i_codigo
		order by CodigoUnidadMedida asc
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		select  CodigoUnidadMedida,
				Nombre,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from UnidadMedida
		WHERE CodigoUnidadMedida = @i_codigo
		AND EsActivo = 1

		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = 0
		
		Select @i_codigo = max(CodigoUnidadMedida) from UnidadMedida
		
		Set @i_codigo = isnull(@i_codigo,0)
		
		INSERT INTO  UnidadMedida(CodigoUnidadMedida, Nombre, EsActivo)
		VALUES (@i_codigo+1, @i_nombre, 1)
		 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE UnidadMedida
		set Nombre = @i_nombre			
		WHERE CodigoUnidadMedida = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE UnidadMedida
		set EsActivo = 0		
		WHERE CodigoUnidadMedida = @i_codigo
		
		return 0
	END

END

GO


IF OBJECT_ID('dbo.sp_unidad_medida') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_unidad_medida') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_unidad_medida >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_unidad_medida >>>'
END
go


