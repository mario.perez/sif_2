USE [SIF]
GO

/****** Object:  StoredProcedure [dbo].[sp_bodega]    Script Date: 6/9/2018 11:02:00 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.sp_bodega') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_bodega
    IF OBJECT_ID('dbo.sp_bodega') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_bodega >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_bodega >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <24/04/2018>
-- Description:	<SCRUD para Bodega.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_bodega]
	@i_user        varchar(32),
	@i_terminal    varchar(32),
	@i_operacion   char(1),
	@i_codigo      int = -1,
	@i_nombre      varchar(32) = '',
	@i_es_activo   char(1) = 'N'
AS
BEGIN
	
	IF @i_operacion = 'S'
	BEGIN
		select  CodigoBodega,
				Nombre,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Bodega
		where EsActivo = 1
		and CodigoBodega > @i_codigo
		order by CodigoBodega asc
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		select  CodigoBodega,
				Nombre,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Bodega
		WHERE CodigoBodega = @i_codigo
		AND EsActivo = 1

		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = 0
		
		Select @i_codigo = max(CodigoBodega) from Bodega
		
		Set @i_codigo = isnull(@i_codigo,0)
		
		INSERT INTO  Bodega(CodigoBodega, Nombre, EsActivo)
		VALUES (@i_codigo+1, @i_nombre, 1)
		 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Bodega
		set Nombre = @i_nombre
		WHERE CodigoBodega = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE Bodega
		set EsActivo = 0
		WHERE CodigoBodega = @i_codigo
		
		return 0
	END

END

GO

 
IF OBJECT_ID('dbo.sp_bodega') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_bodega') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_bodega >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_bodega >>>'
END
go

