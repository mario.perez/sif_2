USE SIF
go

IF OBJECT_ID('sp_detalle_factura') IS NOT NULL
BEGIN
    DROP PROCEDURE sp_detalle_factura
    IF OBJECT_ID('sp_detalle_factura') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE sp_detalle_factura >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE sp_detalle_factura >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_detalle_factura] 
    @i_operacion char(1), 
    @i_CodigoDetalleFactura varchar(16) = '', 
    @i_codigo varchar(32) = '', 
    @i_CodigoProducto varchar(32) = '', 
    @i_CodigoLote varchar(32) = '', 
    @i_Cantidad float =0, 
    @i_Precio money =0
 AS BEGIN 

IF @i_operacion = 'S' 
BEGIN 

	SELECT CodigoDetalleFactura, CodigoFactura, CodigoProducto, CodigoLote, Cantidad, Precio
	FROM DetalleFactura
	WHERE CodigoFactura = @i_codigo

END
/*
IF @i_operacion = 'C' 
BEGIN 

	SELECT CodigoDetalleFactura, CodigoFactura, CodigoProducto, CodigoLote, Cantidad, Precio
	FROM DetalleFactura
	WHERE CodigoDetalleFactura = @i_CodigoDetalleFactura

END
*/

IF @i_operacion = 'I' 
BEGIN 

	INSERT DetalleFactura (CodigoDetalleFactura, CodigoFactura, CodigoProducto, CodigoLote, Cantidad, Precio)
	VALUES (@i_CodigoDetalleFactura, @i_codigo, @i_CodigoProducto, @i_CodigoLote, @i_Cantidad, @i_Precio ) 

END

IF @i_operacion = 'U' 
BEGIN 

	update t set CodigoFactura = @i_codigo, CodigoProducto = @i_CodigoProducto, CodigoLote = @i_CodigoLote, Cantidad = @i_Cantidad, Precio = @i_Precio
	from DetalleFactura t 
	WHERE CodigoDetalleFactura = @i_CodigoDetalleFactura
	and CodigoFactura = @i_codigo

END

IF @i_operacion = 'D' 
BEGIN 

	DELETE FROM DetalleFactura
	WHERE CodigoDetalleFactura = @i_CodigoDetalleFactura
	and CodigoFactura = @i_codigo
	
END
END
GO

IF OBJECT_ID('sp_detalle_factura') IS NOT NULL
BEGIN
    IF OBJECT_ID('sp_detalle_factura') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE sp_detalle_factura >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE sp_detalle_factura >>>'
END
go
