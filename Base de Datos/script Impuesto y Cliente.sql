USE [SIF]
GO
/****** Object:  StoredProcedure [dbo].[sp_cliente]    Script Date: 16/8/2019 8:12:26 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_cliente] 
 @Operacion char(1), 
 @cliente_id int = 0, 
 @codigo_clte nvarchar(50) = NULL , 
 @primer_apellido nvarchar(60)  = NULL, 
 @segundo_apellido nvarchar(60) = NULL, 
 @primer_nombre nvarchar(60) = NULL, 
 @segundo_nombre nvarchar(60) = NULL, 
 @nombre_factura nvarchar(80) = NULL, 
 @direccion1 nvarchar(60) = NULL, 
 @direccion2 nvarchar(60) = NULL, 
 @direccion3 nvarchar(60) = NULL, 
 @ciudad nvarchar(50) = NULL, 
 @departamento nvarchar(50) = NULL, 
 @pais nvarchar(60) = NULL, 
 @telefono1 nvarchar(50) = NULL, 
 @telefono2 nvarchar(50) = NULL, 
 @fax nvarchar(50) = NULL, 
 @Contacto nvarchar(50) = NULL, 
 @e_mail1 nvarchar(100) = NULL, 
 @e_mail2 nvarchar(100) = NULL, 
 @activo bit = NULL, 
 @ruc nvarchar(50) = NULL, 
 @maximo_Credito decimal(18,2) = NULL, 
 @comentario nvarchar(500) = NULL, 
 @UserCreacion nvarchar(50) = NULL, 
 @UserModificacion nvarchar(50) = NULL, 
 @FechaCreacion datetime = NULL, 
 @FechaModificacion datetime = NULL, 
 @AppCreacion nvarchar(50) = NULL, 
 @AppModificacion nvarchar(50) = NULL, 
 @MaqCreacion nvarchar(50) = NULL, 
 @MaqModificacion nvarchar(50) = NULL,
 @i_user nvarchar(50)= NULL,
 @i_terminal nvarchar(50) = NULL,
 @cedula nvarchar(20) =  null,
 @foto varbinary(max) = null
 AS BEGIN 

 IF @OPERACION = 'S'
 BEGIN
 SELECT top 80
cliente_id , codigo_clte , primer_apellido , segundo_apellido , primer_nombre , segundo_nombre , 
nombre_factura , direccion1 , direccion2 , direccion3 , ciudad , departamento , pais , telefono1 , 
telefono2 , fax , Contacto , e_mail1 , e_mail2 , activo , ruc , maximo_Credito , comentario , UserCreacion , 
UserModificacion , FechaCreacion , FechaModificacion , AppCreacion , AppModificacion , MaqCreacion , MaqModificacion 
, Cedula
, Foto
FROM Cliente
where 
(cliente_id = @cliente_id or @cliente_id = 0)
and (codigo_clte = @codigo_clte or @codigo_clte = '' or @codigo_clte is null)
and (primer_nombre = @primer_nombre or @primer_nombre = '' or @primer_nombre is null)
and (primer_apellido = @primer_apellido or @primer_apellido = '' or @primer_apellido is null)
and (segundo_nombre = @segundo_nombre or @segundo_nombre = '' or @segundo_nombre is null)
and (segundo_apellido = @segundo_apellido or @segundo_apellido = '' or @segundo_apellido is null)

 END

 --IF @OPERACION = 'C'
 --BEGIN
 --SELECT cliente_id , codigo_clte , primer_apellido , segundo_apellido , primer_nombre , segundo_nombre , nombre_factura , direccion1 , direccion2 , direccion3 , ciudad , departamento , pais , telefono1 , telefono2 , fax , Contacto , e_mail1 , e_mail2 , activo , ruc , maximo_Credito , comentario , UserCreacion , UserModificacion , FechaCreacion , FechaModificacion , AppCreacion , AppModificacion , MaqCreacion , MaqModificacion FROM Cliente WHERE cliente_id = @cliente_id
 --END

 IF @OPERACION = 'I'
 BEGIN
 INSERT Cliente 
 ( codigo_clte , primer_apellido , segundo_apellido , primer_nombre , segundo_nombre ,
  nombre_factura , direccion1 , direccion2 , direccion3 , ciudad , departamento , pais , telefono1 , 
  telefono2 , fax , Contacto , e_mail1 , e_mail2 , activo , ruc , maximo_Credito , comentario ,
   UserCreacion , UserModificacion , FechaCreacion , FechaModificacion , AppCreacion , AppModificacion , 
   MaqCreacion , MaqModificacion, Cedula, Foto) VALUES 
   ( @codigo_clte , @primer_apellido , @segundo_apellido , @primer_nombre , @segundo_nombre , 
   @nombre_factura , @direccion1 , @direccion2 , @direccion3 , @ciudad , @departamento , @pais , @telefono1 , 
   @telefono2 , @fax , @Contacto , @e_mail1 , @e_mail2 , @activo , @ruc , @maximo_Credito , @comentario , 
   @UserCreacion , @UserModificacion , @FechaCreacion , @FechaModificacion , @AppCreacion , @AppModificacion ,
    @MaqCreacion , @MaqModificacion , @cedula, @foto) END

 IF @OPERACION = 'U'
 BEGIN
 update t set codigo_clte = @codigo_clte , primer_apellido = @primer_apellido , segundo_apellido = @segundo_apellido , primer_nombre = @primer_nombre , segundo_nombre = @segundo_nombre , nombre_factura = @nombre_factura , direccion1 = @direccion1 , direccion2 = @direccion2 , direccion3 = @direccion3 , ciudad = @ciudad , departamento = @departamento , pais = @pais , telefono1 = @telefono1 , telefono2 = @telefono2 , fax = @fax , Contacto = @Contacto , e_mail1 = @e_mail1 , e_mail2 = @e_mail2 , activo = @activo , ruc = @ruc , maximo_Credito = @maximo_Credito , comentario = @comentario , UserCreacion = @UserCreacion , UserModificacion = @UserModificacion , FechaCreacion = @FechaCreacion , FechaModificacion = @FechaModificacion , AppCreacion = @AppCreacion , AppModificacion = @AppModificacion , MaqCreacion = @MaqCreacion , MaqModificacion = @MaqModificacion 
 , Cedula = @cedula, foto = @foto
 from Cliente t WHERE cliente_id = @cliente_id
 END

 IF @OPERACION = 'D'
 BEGIN
 DELETE FROM Cliente WHERE cliente_id = @cliente_id
 END

 END
GO
ALTER AUTHORIZATION ON [dbo].[sp_cliente] TO  SCHEMA OWNER 
GO
/****** Object:  StoredProcedure [dbo].[sp_impuesto]    Script Date: 16/8/2019 8:12:26 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_impuesto] 
 @Operacion char(1), 
 @impuesto_id int = 0, 
 @Codigo nvarchar(10) = null , 
 @Descripcion_impto nvarchar(100) = '', 
 @valor_impto decimal(18,2) = null, 
 @predeterminado bit = 0, 
 @UserCreacion nvarchar(50) = null, 
 @UserModificacion nvarchar(50) = null, 
 @FechaCreacion datetime = null, 
 @FechaModificacion datetime = null, 
 @AppCreacion nvarchar(50) = null, 
 @AppModificacion nvarchar(50) = null, 
 @MaqCreacion nvarchar(50) = null, 
 @MaqModoficacion nvarchar(50) = null,
 @activo bit = null,
  @i_user nvarchar(50)= NULL ,
 @i_terminal nvarchar(50) = NULL,
 @Nombre nvarchar(50) = ''
 AS BEGIN 

 IF @OPERACION = 'S'
 BEGIN
 SELECT 
impuesto_id , Codigo , Descripcion_impto , valor_impto , predeterminado , UserCreacion , UserModificacion 
, FechaCreacion , FechaModificacion , AppCreacion , AppModificacion , MaqCreacion , MaqModoficacion 
, Activo
, Nombre
FROM Impuesto
where (impuesto_Id = @impuesto_id or @impuesto_id = 0)
and (codigo = @codigo or @codigo = '' or @codigo is null)
and (Nombre like '%'+@Nombre+'%' or @Nombre = '' or @Nombre is null)
 END

 --IF @OPERACION = 'C'
 --BEGIN
 --SELECT impuesto_id , Codigo , Descripcion_impto , valor_impto , predeterminado , UserCreacion , UserModificacion , FechaCreacion , FechaModificacion , AppCreacion , AppModificacion , MaqCreacion , MaqModoficacion FROM Impuesto WHERE impuesto_id = @impuesto_id
 --END

 IF @OPERACION = 'I'
 BEGIN
	 INSERT Impuesto (Codigo , Descripcion_impto , valor_impto , predeterminado , UserCreacion , UserModificacion 
	 , FechaCreacion , FechaModificacion , AppCreacion , AppModificacion , MaqCreacion , MaqModoficacion,activo, Nombre)
	  VALUES (@Codigo , @Descripcion_impto , @valor_impto , @predeterminado , @UserCreacion , @UserModificacion ,
	   @FechaCreacion , @FechaModificacion , @AppCreacion ,  @AppModificacion , @MaqCreacion , @MaqModoficacion,@activo, @Nombre ) 
  END

 IF @OPERACION = 'U'
 BEGIN
 update t set Codigo = @Codigo , Descripcion_impto = @Descripcion_impto , valor_impto = @valor_impto , 
 predeterminado = @predeterminado , UserCreacion = @UserCreacion , UserModificacion = @UserModificacion , 
 FechaCreacion = @FechaCreacion , FechaModificacion = @FechaModificacion , AppCreacion = @AppCreacion , 
 AppModificacion = @AppModificacion , MaqCreacion = @MaqCreacion , MaqModoficacion = @MaqModoficacion
 , activo = @activo
 , Nombre = @Nombre
  from Impuesto t WHERE impuesto_id = @impuesto_id
 END

 IF @OPERACION = 'D'
 BEGIN
 DELETE FROM Impuesto WHERE impuesto_id = @impuesto_id
 END

 END
GO
ALTER AUTHORIZATION ON [dbo].[sp_impuesto] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 16/8/2019 8:12:26 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[cliente_id] [int] IDENTITY(1,1) NOT NULL,
	[codigo_clte] [nvarchar](50) NULL,
	[Cedula] [nvarchar](20) NULL,
	[primer_apellido] [nvarchar](60) NOT NULL,
	[segundo_apellido] [nvarchar](60) NULL,
	[primer_nombre] [nvarchar](60) NOT NULL,
	[segundo_nombre] [nvarchar](60) NULL,
	[nombre_factura] [nvarchar](80) NULL,
	[direccion1] [nvarchar](60) NULL,
	[direccion2] [nvarchar](60) NULL,
	[direccion3] [nvarchar](60) NULL,
	[ciudad] [nvarchar](50) NULL,
	[departamento] [nvarchar](50) NULL,
	[pais] [nvarchar](60) NULL,
	[telefono1] [nvarchar](50) NULL,
	[telefono2] [nvarchar](50) NULL,
	[fax] [nvarchar](50) NULL,
	[Contacto] [nvarchar](50) NULL,
	[e_mail1] [nvarchar](100) NULL,
	[e_mail2] [nvarchar](100) NULL,
	[activo] [bit] NULL,
	[maximo_Credito] [decimal](18, 2) NULL,
	[comentario] [nvarchar](500) NULL,
	[UserCreacion] [nvarchar](50) NULL,
	[UserModificacion] [nvarchar](50) NULL,
	[FechaCreacion] [datetime] NULL,
	[FechaModificacion] [datetime] NULL,
	[AppCreacion] [nvarchar](50) NULL,
	[AppModificacion] [nvarchar](50) NULL,
	[MaqCreacion] [nvarchar](50) NULL,
	[MaqModificacion] [nvarchar](50) NULL,
	[ruc] [nvarchar](50) NULL,
	[Foto] [varbinary](max) NULL,
 CONSTRAINT [PK_clientes] PRIMARY KEY CLUSTERED 
(
	[cliente_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER AUTHORIZATION ON [dbo].[Cliente] TO  SCHEMA OWNER 
GO
/****** Object:  Table [dbo].[Impuesto]    Script Date: 16/8/2019 8:12:26 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Impuesto](
	[impuesto_id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](10) NULL,
	[Descripcion_impto] [nvarchar](100) NULL,
	[valor_impto] [decimal](18, 4) NULL,
	[predeterminado] [bit] NULL,
	[UserCreacion] [nvarchar](50) NULL,
	[UserModificacion] [nvarchar](50) NULL,
	[FechaCreacion] [datetime] NULL,
	[FechaModificacion] [datetime] NULL,
	[AppCreacion] [nvarchar](50) NULL,
	[AppModificacion] [nvarchar](50) NULL,
	[MaqCreacion] [nvarchar](50) NULL,
	[MaqModoficacion] [nvarchar](50) NULL,
	[Activo] [bit] NULL,
	[Nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_impuestos] PRIMARY KEY CLUSTERED 
(
	[impuesto_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER AUTHORIZATION ON [dbo].[Impuesto] TO  SCHEMA OWNER 
GO
