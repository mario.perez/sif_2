USE SIF
go

IF OBJECT_ID('sp_detalle_compra') IS NOT NULL
BEGIN
    DROP PROCEDURE sp_detalle_compra
    IF OBJECT_ID('sp_detalle_compra') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE sp_detalle_compra >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE sp_detalle_compra >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_detalle_compra] 
    @i_operacion char(1), 
    @i_CodigoDetalleCompra varchar(16) = '', 
    @i_codigo              varchar(32) = '', 
    @i_CodigoProducto      varchar(32) = '', 
    @i_CodigoLote          varchar(32) = '', 
    @i_Cantidad            float = 0, 
    @i_Precio              money = 0
 AS BEGIN 

IF @i_operacion = 'S' 
BEGIN 

	SELECT CodigoDetalleCompra, CodigoCompra, CodigoProducto, CodigoLote, Cantidad, Precio
	FROM DetalleCompra
	where CodigoCompra = @i_codigo

END
/*
IF @i_operacion = 'C' 
BEGIN 

	SELECT CodigoDetalleCompra, CodigoCompra, CodigoProducto, CodigoLote, Cantidad, Precio
	FROM DetalleCompra
	WHERE CodigoDetalleCompra = @i_CodigoDetalleCompra

END
*/
IF @i_operacion = 'I' 
BEGIN 

	Select @i_CodigoDetalleCompra = max(convert(bigint,CodigoDetalleCompra)) from DetalleCompra where CodigoCompra = @i_codigo
	
	set @i_CodigoDetalleCompra = isnull(@i_CodigoDetalleCompra,0) + 1
	
	set @i_CodigoDetalleCompra = right('0000000000000000' + @i_CodigoDetalleCompra,16)

	
	INSERT DetalleCompra (CodigoDetalleCompra, CodigoCompra, CodigoProducto, CodigoLote, Cantidad, Precio)
	VALUES (@i_CodigoDetalleCompra, @i_codigo, @i_CodigoProducto, @i_CodigoLote, @i_Cantidad, @i_Precio ) 

END

IF @i_operacion = 'U' 
BEGIN 

	update t set CodigoCompra = @i_codigo, CodigoProducto = @i_CodigoProducto, CodigoLote = @i_CodigoLote, Cantidad = @i_Cantidad, Precio = @i_Precio
	from DetalleCompra t 
	WHERE CodigoDetalleCompra = @i_CodigoDetalleCompra
	and CodigoCompra = @i_codigo

END

IF @i_operacion = 'D' 
BEGIN 

	DELETE FROM DetalleCompra
	WHERE CodigoDetalleCompra = @i_CodigoDetalleCompra
	and CodigoCompra = @i_codigo
END
END
GO

IF OBJECT_ID('sp_detalle_compra') IS NOT NULL
BEGIN
    IF OBJECT_ID('sp_detalle_compra') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE sp_detalle_compra >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE sp_detalle_compra >>>'
END
go
