use SIF
go

if exists(select 1 from sysobjects where name = 'Caja')
    drop table Caja
go
CREATE TABLE [dbo].[Caja](          
    [CodigoCaja]   [int]         NOT NULL,
	[Nombre]       [varchar](32) NOT NULL,
	[CodigoBodega] [int]        NOT NULL,
	[EsActivo]     [bit]         NOT NULL
 CONSTRAINT [PK_Caja] PRIMARY KEY CLUSTERED
(
    [CodigoCaja] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'Bodega')
    drop table Bodega
go
CREATE TABLE [dbo].[Bodega](          
    [CodigoBodega] [int]         NOT NULL,
	[Nombre]       [varchar](32) NOT NULL,
	[EsActivo]     [bit]         NOT NULL
 CONSTRAINT [PK_Bodega] PRIMARY KEY CLUSTERED
(
    [CodigoBodega] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'Categoria')
    drop table Categoria
go
CREATE TABLE [dbo].[Categoria](          
	[CodigoCategoria] [varchar](16)  NOT NULL,
	[Nombre]          [varchar](64) NOT NULL,
	[EsActivo]        [bit]         NOT NULL
 CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED
(
    [CodigoCategoria] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'Vendedor')
    drop table Vendedor
go
CREATE TABLE [dbo].[Vendedor](          
	[CodigoVendedor]  [varchar](16)  NOT NULL,
	[Nombre]          [varchar](64) NOT NULL,
	[EsActivo]        [bit]         NOT NULL
 CONSTRAINT [PK_Vendedor] PRIMARY KEY CLUSTERED
(
    [CodigoVendedor] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'UnidadMedida')
    drop table UnidadMedida
go
CREATE TABLE [dbo].[UnidadMedida](          
	[CodigoUnidadMedida] [int]         NOT NULL,
	[Nombre]             [varchar](64) NOT NULL,
	[EsActivo]           [bit]         NOT NULL
 CONSTRAINT [PK_UnidadMedida] PRIMARY KEY CLUSTERED
(
    [CodigoUnidadMedida] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'Proveedor')
    drop table Proveedor
go
CREATE TABLE [dbo].[Proveedor](          
    [CodigoProveedor]   [bigint]       NOT NULL, 
	[Identificacion]    [varchar](32)  NOT NULL,
	[NombreProveedor]   [varchar](128) NOT NULL,
	[Telefono]          [varchar](32)  NULL,
	[Direccion]         [varchar](Max) NULL,
	[CorreoElectronico] [varchar](128) NULL,
	[Contacto]          [varchar](256) NULL,
	[EsActivo]          [bit]         NOT NULL
 CONSTRAINT [PK_Proveedor] PRIMARY KEY CLUSTERED
(
    [CodigoProveedor] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'Cliente')
    drop table Cliente
go
CREATE TABLE [dbo].[Cliente](          
	
	CodigoCliente		varchar(64) NOT NULL,
	NombreCliente		varchar(128) NOT NULL,
	Telefono			varchar(32) NOT NULL,
	Tipo				varchar(10) NOT NULL,
	Direccion			varchar(Max) NOT NULL,
	CorreoElectronico	varchar(128) NOT NULL,
	Contacto			varchar(256) NOT NULL,
	EsActivo			bit NOT NULL
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED
(
    
CodigoCliente	 asc
) ON [PRIMARY]
) ON [PRIMARY]
GO



if exists(select 1 from sysobjects where name = 'Configuracion')
    drop table Configuracion
go
CREATE TABLE [dbo].[Configuracion](          
	[CodigoConfiguracion] [varchar](32)  NOT NULL,
	[Valor]           [varchar](max) NOT NULL,
	[Descripcion]     [varchar](max) NULL,
 CONSTRAINT [PK_Configuracion] PRIMARY KEY CLUSTERED
(
    [CodigoConfiguracion] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'Producto')
    drop table Producto
go
CREATE TABLE [dbo].[Producto](          
	[CodigoProducto]    [varchar](32)  NOT NULL,
	[CodigoCategoria]   [bigint]       NOT NULL,
	[Descripcion]       [varchar](256) NOT NULL,
	[UnidadMedida]      [int]          NOT NULL,
	[EsActivo]          [bit]          NOT NULL,
	[LlevaExistencia]   [bit]          NOT NULL,
	[UnidadesPorMedida] [int]          NULL
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED
(
    [CodigoProducto] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO



if exists(select 1 from sysobjects where name = 'Denominacion')
    drop table Denominacion
go
CREATE TABLE [dbo].[Denominacion](          
	[CodigoDenominacion] [int]          NOT NULL,
	[Descripcion]        [varchar](256) NOT NULL,
	[Moneda]             [varchar](32)  NOT NULL,
	[Factor]             [Numeric](15,4) NOT NULL,
	[EsEfectivo]         [bit]          NOT NULL,
	[EsActivo]           [bit]          NOT NULL
 CONSTRAINT [PK_Denominacion] PRIMARY KEY CLUSTERED
(
    [CodigoDenominacion] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'Cierre')
    drop table Cierre
go
CREATE TABLE [dbo].[Cierre](          
	[CodigoCierre]      [bigint]          NOT NULL,
	[CodigoCaja]        [int]          NOT NULL,
	[FechaHoraInicio]   [datetime]     NOT NULL,
	[FechaHoraFin]      [datetime]     NULL,
	[NombreUsuario]     [varchar](32)  NOT NULL
 CONSTRAINT [PK_Cierre] PRIMARY KEY CLUSTERED
(
    [CodigoCierre] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'DetalleCierre')
    drop table DetalleCierre
go
CREATE TABLE [dbo].[DetalleCierre](          
	[CodigoCierre]         [bigint]       NOT NULL,
	[CodigoDenominacion]   [int]          NOT NULL,
	[Cantidad]             [bigint]       NOT NULL,
	[MontoEquivalente]     [Numeric](7,4) NOT NULL
 CONSTRAINT [PK_DetalleCierre] PRIMARY KEY CLUSTERED
(
    [CodigoCierre] ASC,
	[CodigoDenominacion] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'Usuario')
    drop table Usuario
go
CREATE TABLE [dbo].[Usuario](          
	[NombreUsuario] [varchar](32) NOT NULL,
	[Clave]         [varchar](32) NOT NULL,
	[CodigoRol]     [int]         NOT NULL,
	[EsActivo]      [bit]         NOT NULL
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED
(
    [NombreUsuario] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO



if exists(select 1 from sysobjects where name = 'Lote')
    drop table Lote
go
CREATE TABLE [dbo].[Lote](          
	[CodigoLote]    [varchar](32) NOT NULL,
	[FechaIngreso]  [datetime]    NOT NULL,
	[EsActivo]      [bit]         NOT NULL
 CONSTRAINT [PK_Lote] PRIMARY KEY CLUSTERED
(
    [CodigoLote] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'DetalleLote')
    drop table DetalleLote
go
CREATE TABLE [dbo].[DetalleLote](          
	[CodigoLote]     [varchar](32)  NOT NULL,
	[CodigoProducto] [varchar](32)  NOT NULL,
	[Cantidad]       [numeric](7,4) NOT NULL,
	[Costo]          [money]        NOT NULL,
	[EsActivo]       [bit]         NOT NULL
 CONSTRAINT [PK_DetalleLote] PRIMARY KEY CLUSTERED
(
    [CodigoLote] ASC,
	[CodigoProducto] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'Inventario')
    drop table Inventario
go
CREATE TABLE [dbo].[Inventario](     
    [CodigoBodega]   [int]          NOT NULL,     
	[CodigoLote]     [varchar](32)  NOT NULL,
	[CodigoProducto] [varchar](32)  NOT NULL,
	[Cantidad]       [numeric](7,4) NOT NULL,
	[Precio]         [money]        NOT NULL,
	[EsActivo]       [bit]          NOT NULL
 CONSTRAINT [PK_Inventario] PRIMARY KEY CLUSTERED
(
    [CodigoLote] ASC,
	[CodigoProducto] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'Moneda')
    drop table Moneda
go
CREATE TABLE [dbo].[Moneda](     
    [CodigoMoneda]   [varchar](8)   NOT NULL,     
	[NumeroMoneda]   [varchar](8)   NOT NULL,
	[Decimales]      [int]          NOT NULL,
	[Nombre]         [varchar](128) NOT NULL,
	[Simbolo]        [varchar](16)  NOT NULL,
	[EsActivo]       [bit]          NOT NULL
 CONSTRAINT [PK_Moneda] PRIMARY KEY CLUSTERED
(
    [CodigoMoneda] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'Salida')
    drop table Salida
go
CREATE TABLE [dbo].[Salida](     
    [CodigoSalida]        [varchar](32)   NOT NULL,     
	[Fecha]               [datetime]      NOT NULL,
	[Bodega]              [int]           NOT NULL,
	[Observaciones]       [varchar](max)  NULL,
	[EsAnulada]           [bit]           NOT NULL,
	[Tipo]                [tinyint]       NOT NULL,
	[EsRegistrada]        [bit]           NOT NULL,
	[UsuarioAnula]        [varchar](32)   NULL,
	[UsuarioApruebaAnula] [varchar](32)   NULL,
	[FechaHoraAnula]      [datetime]      NULL
 CONSTRAINT [PK_Salida] PRIMARY KEY CLUSTERED
(
    [CodigoSalida] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'DetalleSalida')
    drop table DetalleSalida
go
CREATE TABLE [dbo].[DetalleSalida](     
    [CodigoDetalleSalida] [varchar](16) NOT NULL,
	[CodigoSalida]        [varchar](32) NOT NULL,
	[CodigoProducto]      [varchar](32) NOT NULL,
	[CodigoLote]          [varchar](32) NOT NULL, 
	[Cantidad]            [float]       NOT NULL,
	[Precio]              [money]       NOT NULL
 CONSTRAINT [PK_DetalleSalida] PRIMARY KEY CLUSTERED
(
    [CodigoSalida] ASC,
	[CodigoDetalleSalida] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'Compra')
    drop table Compra
go
CREATE TABLE [dbo].[Compra](     
    [CodigoCompra]        [varchar](32)   NOT NULL,
    [CodigoProveedor]     [bigint]        NOT NULL, 	
	[Fecha]               [datetime]      NOT NULL,
	[Bodega]              [int]           NOT NULL,
	[TotalCompra]         [money]         NOT NULL,
	[Descuento]           [money]         NOT NULL,
	[Impuesto]            [money]         NOT NULL,
	[TotalNeto]           [money]         NOT NULL,
	[Observaciones]       [varchar](max)  NULL,
	[EsAnulada]           [bit]           NOT NULL,
	[EsRegistrada]        [bit]           NOT NULL,
	[UsuarioAnula]        [varchar](32)   NULL,
	[UsuarioApruebaAnula] [varchar](32)   NULL,
	[FechaHoraAnula]      [datetime]      NULL
 CONSTRAINT [PK_Compra] PRIMARY KEY CLUSTERED
(
    [CodigoCompra] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'DetalleCompra')
    drop table DetalleCompra
go
CREATE TABLE [dbo].[DetalleCompra](     
    [CodigoDetalleCompra] [varchar](16) NOT NULL,
	[CodigoCompra]        [varchar](32) NOT NULL,
	[CodigoProducto]      [varchar](32) NOT NULL,
	[CodigoLote]          [varchar](32) NOT NULL, 
	[Cantidad]            [float]       NOT NULL,
	[Precio]              [money]       NOT NULL
 CONSTRAINT [PK_DetalleCompra] PRIMARY KEY CLUSTERED
(
    [CodigoCompra] ASC,
	[CodigoDetalleCompra] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'Factura')
    drop table Factura
go
CREATE TABLE [dbo].[Factura](     
    [CodigoFactura]           [varchar](32)   NOT NULL,
    [Fecha]                   [datetime]      NOT NULL,
	[Bodega]                  [int]           NOT NULL,
	[TotalFactura]            [money]         NOT NULL,
	[Descuento]               [money]         NOT NULL,
	[Impuesto]                [money]         NOT NULL,
	[TotalNeto]               [money]         NOT NULL,
	[Efectivo]                [money]         NOT NULL,
	[Tarjeta]                 [money]         NOT NULL,
	[CodigoVendedor]          [varchar](16)   NOT NULL,	
	[CodigoCaja]              [int]           NOT NULL,
	[Cliente]                 [varchar](64)   NOT NULL,
	[TipoPago]                [tinyint]       NOT NULL,
	[EsAnulada]               [bit]           NOT NULL,
	[EsRegistrada]            [bit]           NOT NULL,
	[PorcentajeDescuento]     [money]         NOT NULL,
	[UsuarioDescuento]        [varchar](32)   NULL,
	[UsuarioApruebaDescuento] [varchar](32)   NULL,
	[Observaciones]           [varchar](max)  NULL,
	[UsuarioAnula]            [varchar](32)   NULL,
	[UsuarioApruebaAnula]     [varchar](32)   NULL,
	[FechaHoraAnula]          [datetime]      NULL
 CONSTRAINT [PK_Factura] PRIMARY KEY CLUSTERED
(
    [CodigoFactura] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'DetalleFactura')
    drop table DetalleFactura
go
CREATE TABLE [dbo].[DetalleFactura](     
    [CodigoDetalleFactura] [varchar](16) NOT NULL,
	[CodigoFactura]        [varchar](32) NOT NULL,
	[CodigoProducto]       [varchar](32) NOT NULL,
	[CodigoLote]           [varchar](32) NOT NULL, 
	[Cantidad]             [float]       NOT NULL,
	[Precio]               [money]       NOT NULL
 CONSTRAINT [PK_DetalleFactura] PRIMARY KEY CLUSTERED
(
    [CodigoFactura] ASC,
	[CodigoDetalleFactura] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO




if exists(select 1 from sysobjects where name = 'Entrada')
    drop table Entrada
go
CREATE TABLE [dbo].[Entrada](     
    [CodigoEntrada]        [varchar](32)   NOT NULL,     
	[Fecha]               [datetime]      NOT NULL,
	[Bodega]              [int]           NOT NULL,
	[CodigoProveedor]     [bigint]        NOT NULL, 	
	[Observaciones]       [varchar](max)  NULL,
	[EsAnulada]           [bit]           NOT NULL,
	[Tipo]                [tinyint]       NOT NULL,
	[EsRegistrada]        [bit]           NOT NULL,
	[UsuarioAnula]        [varchar](32)   NULL,
	[UsuarioApruebaAnula] [varchar](32)   NULL,
	[FechaHoraAnula]      [datetime]      NULL
 CONSTRAINT [PK_Entrada] PRIMARY KEY CLUSTERED
(
    [CodigoEntrada] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

if exists(select 1 from sysobjects where name = 'DetalleEntrada')
    drop table DetalleEntrada
go
CREATE TABLE [dbo].[DetalleEntrada](     
    [CodigoDetalleEntrada] [varchar](16) NOT NULL,
	[CodigoEntrada]        [varchar](32) NOT NULL,
	[CodigoProducto]      [varchar](32) NOT NULL,
	[CodigoLote]          [varchar](32) NOT NULL, 
	[Cantidad]            [float]       NOT NULL,
	[Precio]              [money]       NOT NULL
 CONSTRAINT [PK_DetalleEntrada] PRIMARY KEY CLUSTERED
(
    [CodigoEntrada] ASC,
	[CodigoDetalleEntrada] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO


if exists(select 1 from sysobjects where name = 'MisCatalogos')
    drop table MisCatalogos
go
CREATE TABLE [dbo].[MisCatalogos](          
	
	Tabla		varchar(32) NOT NULL,
	CodigoMisCatalogos		varchar(10) NOT NULL,
	Descripcion		varchar(64) NOT NULL,
	EsActivo		varchar(1) NOT NULL
 CONSTRAINT [PK_MisCatalogos] PRIMARY KEY CLUSTERED
(
    
Tabla	 asc,
CodigoMisCatalogos	 asc
) ON [PRIMARY]
) ON [PRIMARY]
GO

