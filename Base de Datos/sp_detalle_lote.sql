USE SIF
go

IF OBJECT_ID('sp_detalle_cierre') IS NOT NULL
BEGIN
    DROP PROCEDURE sp_detalle_cierre
    IF OBJECT_ID('sp_detalle_cierre') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE sp_detalle_cierre >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE sp_detalle_cierre >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE sp_detalle_cierre
	@i_user                 varchar(32),
	@i_terminal             varchar(32),
	@i_operacion            char(1),
	@i_codigo               varchar(32) = '',
	@i_codigoProducto       varchar(32) = ''
   ,@i_cantidad             numeric(7,4) = 0
   ,@i_costo                money       = 0
   ,@i_esActivo             varchar(1)  = 'N'
   
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		
		SELECT CodigoLote    
			  ,CodigoProducto
			  ,Cantidad      
			  ,Costo         
			  ,case EsActivo when 1 then 'S' else 'N' end as EsActivo      
		FROM DetalleLote
		WHERE  EsActivo = 1
		AND ((@i_codigo<>'' and CodigoLote = @i_codigo)
			OR (@i_codigoProducto <>'' and CodigoProducto = @i_codigoProducto)
			)
			
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		
		SELECT CodigoLote    
			  ,CodigoProducto
			  ,Cantidad      
			  ,Costo         
			  ,case EsActivo when 1 then 'S' else 'N' end as EsActivo      
		FROM DetalleLote
		WHERE 1=1
		and CodigoLote = @i_codigo
		and CodigoProducto = @i_codigoProducto
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		
INSERT INTO [dbo].[DetalleLote]
           ([CodigoLote],[CodigoProducto],[Cantidad],[Costo],[EsActivo])
     VALUES
           (@i_codigo,@i_codigoProducto,@i_cantidad ,@i_costo,case @i_esActivo when 'S' then 1 else 0 end)
			 
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE [dbo].[DetalleLote]
		   SET [CodigoLote] = @i_codigo
			  ,[CodigoProducto] = @i_codigoProducto
			  ,[Cantidad] = @i_cantidad
			  ,[Costo] = @i_costo
			  ,[EsActivo] = case @i_esActivo when 'S' then 1 else 0 end
		 WHERE CodigoLote = @i_codigo
		 and CodigoProducto = @i_codigoProducto
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		DELETE 
		FROM DetalleLote
		WHERE CodigoLote = @i_codigo
		 and CodigoProducto = @i_codigoProducto
		
		return 0
	END

END
GO

IF OBJECT_ID('sp_detalle_cierre') IS NOT NULL
BEGIN
    IF OBJECT_ID('sp_detalle_cierre') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE sp_detalle_cierre >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE sp_detalle_cierre >>>'
END
go
