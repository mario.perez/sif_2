USE [SIF]
GO
/****** Object:  StoredProcedure [dbo].[sp_proveedor]    Script Date: 7/9/2018 9:53:31 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_proveedor') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_proveedor
    IF OBJECT_ID('dbo.sp_proveedor') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_proveedor >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_proveedor >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <24/04/2018>
-- Description:	<SCRUD para Vendedor.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_proveedor]
	@i_user        varchar(32),
	@i_terminal    varchar(32),
	@i_operacion   char(1),
	@i_codigo      int = -1,
	@i_identificacion varchar(32) = '',
	@i_nombre      varchar(32) = '',
	@i_telefono    varchar(16) = '',
	@i_direccion   varchar(max) = '',
	@i_correo      varchar(128) = '',
	@i_contacto    varchar(256) = '',
	@i_es_activo   bit = 1
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		select   CodigoProveedor
				, Identificacion
		        ,NombreProveedor
		        ,Telefono
				,Direccion
				, CorreoElectronico
				, Contacto
				,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Proveedor
		where EsActivo = 1
		AND CodigoProveedor > @i_codigo
		ORDER BY CodigoProveedor asc
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		select CodigoProveedor
				, Identificacion
		        ,NombreProveedor
		        ,Telefono
				,Direccion
				, CorreoElectronico
				, Contacto
				,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Proveedor
		WHERE CodigoProveedor = @i_codigo
		AND EsActivo = 1


		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = 0
		
		Select @i_codigo = max(CodigoProveedor) from Proveedor
		
		Set @i_codigo = isnull(@i_codigo,0)
		
		INSERT INTO  Proveedor(CodigoProveedor, Identificacion, NombreProveedor,Telefono, Direccion, CorreoElectronico
								, Contacto,EsActivo)
		VALUES (@i_codigo+1, @i_identificacion, @i_nombre, @i_telefono, @i_direccion, @i_correo, @i_contacto, 1)
		 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Proveedor
		set 
			Identificacion = @i_identificacion,
			NombreProveedor = @i_nombre,
			Telefono = @i_telefono,
			Direccion = @i_direccion,
			CorreoElectronico = @i_correo,
			Contacto = @i_contacto			
		WHERE CodigoProveedor = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE Proveedor
		set EsActivo = 0
		WHERE CodigoProveedor = @i_codigo
		
		return 0
	END

END
GO

IF OBJECT_ID('dbo.sp_proveedor') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_proveedor') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_proveedor >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_proveedor >>>'
END
go
