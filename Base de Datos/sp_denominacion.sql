USE [SIF]
GO
/****** Object:  StoredProcedure [dbo].[sp_denominacion]    Script Date: 7/9/2018 7:38:21 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_denominacion') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_denominacion
    IF OBJECT_ID('dbo.sp_denominacion') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_denominacion >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_denominacion >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <24/04/2018>
-- Description:	<SCRUD para Denominacion.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_denominacion]
	@i_user        varchar(32),
	@i_terminal    varchar(32),
	@i_operacion   char(1),
	@i_codigo      int = -1,
	@i_descripcion varchar(256) = '',
	@i_Moneda      varchar(32)  = ''
   ,@i_Factor      Numeric(15,4) = 0
   ,@i_EsEfectivo  bit = 1
   ,@i_EsActivo    bit = 1
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		SELECT CodigoDenominacion
			  ,Descripcion
			  ,Moneda
			  ,Factor
			  ,case EsEfectivo when 1 then 'S' else 'N' end as EsEfectivo
			  ,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM Denominacion
		where EsActivo = 1
		and CodigoDenominacion > @i_codigo
		order by CodigoDenominacion asc
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		SELECT CodigoDenominacion
			  ,Descripcion
			  ,Moneda
			  ,Factor
			  ,case EsEfectivo when 1 then 'S' else 'N' end as EsEfectivo
			  ,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM Denominacion
		where CodigoDenominacion = @i_codigo
		and EsActivo = 1
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = 0
		
		Select @i_codigo = max(CodigoDenominacion) from Denominacion
		
		Set @i_codigo = isnull(@i_codigo,0)
		
		INSERT INTO Denominacion
			   (CodigoDenominacion ,Descripcion,Moneda,Factor,EsEfectivo,EsActivo)
		 VALUES
			   (@i_codigo+1,@i_descripcion ,@i_Moneda ,@i_Factor,@i_EsEfectivo,1)
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Denominacion
		   SET 
			   Descripcion = @i_descripcion
			  ,Moneda = @i_Moneda
			  ,Factor = @i_Factor
			  ,EsEfectivo = @i_EsEfectivo
		 WHERE CodigoDenominacion = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE Denominacion
		   SET EsActivo = 0
		WHERE CodigoDenominacion = @i_codigo
		
		return 0
	END

END

GO


IF OBJECT_ID('dbo.sp_denominacion') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_denominacion') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_denominacion >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_denominacion >>>'
END
go
