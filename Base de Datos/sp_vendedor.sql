USE [SIF]
GO

/****** Object:  StoredProcedure [dbo].[sp_vendedor]    Script Date: 6/9/2018 1:03:16 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.sp_vendedor') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_vendedor
    IF OBJECT_ID('dbo.sp_vendedor') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_vendedor >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_vendedor >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <24/04/2018>
-- Description:	<SCRUD para Vendedor.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_vendedor]
	@i_user        varchar(32),
	@i_terminal    varchar(32),
	@i_operacion   char(1),
	@i_codigo      int = -1,
	@i_nombre      varchar(32) = '',
	@i_es_activo   char(1) = 'N'
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		select  CodigoVendedor,
				Nombre,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Vendedor
		where EsActivo = 1
		and CodigoVendedor > @i_codigo
		ORDER BY CodigoVendedor asc
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		select  CodigoVendedor,
				Nombre,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Vendedor
		WHERE CodigoVendedor = @i_codigo
		AND EsActivo = 1

		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = 0
		
		Select @i_codigo = max(CodigoVendedor) from vendedor
		
		Set @i_codigo = isnull(@i_codigo,0)
		
		INSERT INTO  Vendedor(CodigoVendedor, Nombre, EsActivo)
		VALUES (@i_codigo+1, @i_nombre, 1)
		 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Vendedor
		set Nombre = @i_nombre			
		WHERE CodigoVendedor = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE Vendedor
		set EsActivo = 0	
		WHERE CodigoVendedor = @i_codigo
		
		return 0
	END

END

GO


IF OBJECT_ID('dbo.sp_vendedor') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_vendedor') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_vendedor >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_vendedor >>>'
END
go
