USE [SIF]
GO
/****** Object:  StoredProcedure [dbo].[sp_caja]    Script Date: 6/9/2018 9:30:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_caja') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_caja
    IF OBJECT_ID('dbo.sp_caja') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_caja >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_caja >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <24/04/2018>
-- Description:	<SCRUD para Caja.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_caja]
	@i_user          varchar(32),
	@i_terminal      varchar(32),
	@i_operacion     char(1),
	@i_codigo        int = -1,
	@i_nombre        varchar(32) = '',
	@i_codigo_bodega int = -1,
	@i_es_activo     bit = 1
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		select  CodigoCaja,
				Nombre,
				CodigoBodega,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Caja
		where EsActivo = 1
		and CodigoCaja > @i_codigo
		order by CodigoCaja asc
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		select  CodigoCaja,
				Nombre,
				CodigoBodega,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Caja
		WHERE CodigoCaja = @i_codigo
		AND EsActivo = 1

		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = 0
		
		Select @i_codigo = max(CodigoCaja) from Caja
		
		Set @i_codigo = isnull(@i_codigo,0)
		
		INSERT INTO  Caja(CodigoCaja, Nombre, CodigoBodega, EsActivo)
		VALUES (@i_codigo+1, @i_nombre, @i_codigo_bodega, 1)
		 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Caja
		set Nombre = @i_nombre,
			CodigoBodega = @i_codigo_bodega			
		WHERE CodigoCaja = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE Caja
		set EsActivo = 0			
		WHERE CodigoCaja = @i_codigo
		
		return 0
	END

END
GO


IF OBJECT_ID('dbo.sp_caja') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_caja') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_caja >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_caja >>>'
END
go

