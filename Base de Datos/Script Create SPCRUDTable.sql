USE SIF
go

IF OBJECT_ID('dbo.CreateProcedureCrud') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.CreateProcedureCrud
    IF OBJECT_ID('dbo.CreateProcedureCrud') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.CreateProcedureCrud >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.CreateProcedureCrud >>>'
END
go



/****** Object:  StoredProcedure [dbo].[CreateProcedureCrud]    Script Date: 14/7/2018 10:04:43 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[CreateProcedureCrud]
@NombreTabla nvarchar(50) 
,@NombreSPCRUD nvarchar(50) 
as
begin
	
	Declare @tabla nvarchar(50)
	Declare @column nvarchar(50)
	Declare @isNullable nvarchar(50)
	Declare @DataType nvarchar(50)
	Declare @maxLength nvarchar(50)
	DECLARE @ORDINAL_POSITION INT
	DECLARE @FILTRO NVARCHAR(50) = ''

	Declare @SCRIPT_ENCABEZADO nvarchar(100) = 'CREATE PROCEDURE '+@NombreSPCRUD 
	Declare @SCRIPT_VARIABLES nVARCHAR(MAX) = CHAR(13)+CHAR(10) + '    @i_operacion char(1), ' -- CONTENDRA LAS VARIABLES QUE PERTENECEN A LAS COLUMNAS DE LA TABLA

	DECLARE @SCRIPT_S NVARCHAR(MAX) = '' -- SELECT FROM
	DECLARE @SCRIPT_C NVARCHAR(MAX) = '' -- SELECT FROM WHERE
	DECLARE @SCRIPT_U NVARCHAR(MAX) = '' -- UPDATE
	DECLARE @SCRIPT_I NVARCHAR(MAX) = '' -- INSERT
	DECLARE @SCRIPT_D NVARCHAR(MAX) = '' -- DELETE

	DECLARE @SCRIPT_VARIABLES_I NVARCHAR(MAX) = ''

	Declare cursorT Cursor FOR 
	select
	t.TABLE_NAME
	, t.COLUMN_NAME
	, t.IS_NULLABLE
	, t.DATA_TYPE
	, t.CHARACTER_MAXIMUM_LENGTH
	, T.ORDINAL_POSITION
	from INFORMATION_SCHEMA.COLUMNS t WHERE TABLE_NAME = @NombreTabla

	print '<< INICIANDO CREACI�N DE SP '+@NombreSPCRUD+' >>'

	open cursorT
	FETCH NEXT FROM cursorT INTO @tabla, @column, @isNullable, @DataType, @maxLength, @ORDINAL_POSITION
	while @@FETCH_STATUS = 0
	begin			

		-- Capturamos las variables que posee para que sean declarables

		IF @DataType in ('int' ,'bit' ,'bigint', 'datetime','date', 'tinyint','float','money')
		BEGIN
			SET @SCRIPT_VARIABLES = @SCRIPT_VARIABLES + CHAR(13)+CHAR(10) +'    @i_'+@COLUMN+' '+@DataType+', '
			SET @SCRIPT_VARIABLES_I = @SCRIPT_VARIABLES_I + '@i_'+@column+', '
		END

		IF @ORDINAL_POSITION = '1'
		BEGIN
			SET @FILTRO = @FILTRO+@column+' = @i_'+@column
		END
		
		IF @DataType in ('char', 'varchar','nvarchar')
		begin
			set @SCRIPT_VARIABLES = @SCRIPT_VARIABLES+ CHAR(13)+CHAR(10) +'    @i_'+@column+' '+@DataType+'('+case @maxLength when '-1' then 'max' else @maxLength end +'), '
			SET @SCRIPT_VARIABLES_I = @SCRIPT_VARIABLES_I + '@i_'+@column+', '
		end

		-- CAPTURAMOS LAS COLUMNAS PARA FORMAR LA i_operacion 'S'

		SET @SCRIPT_S = @SCRIPT_S + @column+', '
		SET @SCRIPT_C = @SCRIPT_C + @column+', '
		SET @SCRIPT_I = @SCRIPT_I + @column+', '

		if @ORDINAL_POSITION > 1
		begin
			SET @SCRIPT_U = @SCRIPT_U + @column+' = @i_'+@column+', '
		end

		FETCH NEXT FROM cursorT INTO @tabla, @column, @isNullable, @DataType, @maxLength, @ORDINAL_POSITION

	end
	close cursorT
	deallocate cursorT

	SET @SCRIPT_S = CHAR(13)+CHAR(10) +'IF @i_operacion = ''S'' '+ CHAR(13)+CHAR(10) 
					+'BEGIN '+ CHAR(13)+CHAR(10) 
					+ CHAR(13)+CHAR(10) 
					+'	SELECT '+SUBSTRING(@SCRIPT_S,1,LEN(@SCRIPT_S)-1)+ CHAR(13)+CHAR(10) 
					+'	FROM '+@NombreTabla+ CHAR(13)+CHAR(10)
					+ CHAR(13)+CHAR(10) 
					+'END'

	SET @SCRIPT_C = CHAR(13)+CHAR(10) +'IF @i_operacion = ''C'' '+ CHAR(13)+CHAR(10) 
					+'BEGIN '+ CHAR(13)+CHAR(10) 
					+ CHAR(13)+CHAR(10) 
					+'	SELECT '+SUBSTRING(@SCRIPT_C,1,LEN(@SCRIPT_C)-1)+CHAR(13)+CHAR(10)  
					+'	FROM '+@NombreTabla + CHAR(13)+CHAR(10) 
					+'	WHERE '+@FILTRO+CHAR(13)+CHAR(10)
					+ CHAR(13)+CHAR(10) 
					+'END'

	SET @SCRIPT_I = CHAR(13)+CHAR(10) +'IF @i_operacion = ''I'' '+ CHAR(13)+CHAR(10) 
					+'BEGIN '+ CHAR(13)+CHAR(10) 
					+ CHAR(13)+CHAR(10)
					+'	INSERT '+@NombreTabla+' ('+SUBSTRING(@SCRIPT_I,1,LEN(@SCRIPT_I)-1)+')'+CHAR(13)+CHAR(10)
					+'	VALUES ('+SUBSTRING(@SCRIPT_VARIABLES_I,1,LEN(@SCRIPT_VARIABLES_I)-1)+' ) '+CHAR(13)+CHAR(10)
					+ CHAR(13)+CHAR(10)
					+'END'
	
	set @SCRIPT_U = CHAR(13)+CHAR(10) +'IF @i_operacion = ''U'' '+ CHAR(13)+CHAR(10) 
					+'BEGIN '+CHAR(13)+CHAR(10)
					+ CHAR(13)+CHAR(10)
					+'	update t set '+SUBSTRING(@SCRIPT_U,1,LEN(@SCRIPT_U)-1)+CHAR(13)+CHAR(10)
					+'	from '+@NombreTabla+' t '+CHAR(13)+CHAR(10)
					+'	WHERE '+@FILTRO+CHAR(13)+CHAR(10)
					+ CHAR(13)+CHAR(10)
					+'END'

	SET @SCRIPT_D = CHAR(13)+CHAR(10) +'IF @i_operacion = ''D'' '+ CHAR(13)+CHAR(10) 
					+'BEGIN '+CHAR(13)+CHAR(10)
					+ CHAR(13)+CHAR(10)
					+'	DELETE FROM '+@NombreTabla+CHAR(13)+CHAR(10)
					+'	WHERE '+@FILTRO+CHAR(13)+CHAR(10)
					+ CHAR(13)+CHAR(10)
					+'END'

	SET @SCRIPT_VARIABLES = SUBSTRING(@SCRIPT_VARIABLES,1,LEN(@SCRIPT_VARIABLES)-1)
	
	DECLARE @SCRIPT_CONTENEDOR NVARCHAR(MAX) = ''
	SET @SCRIPT_CONTENEDOR = @SCRIPT_ENCABEZADO+' '+@SCRIPT_VARIABLES+ CHAR(13)+CHAR(10) +' AS BEGIN '
							+CHAR(13)+CHAR(10) + @SCRIPT_S
							+CHAR(13)+CHAR(10) + @SCRIPT_C
							+CHAR(13)+CHAR(10) + @SCRIPT_I
							+CHAR(13)+CHAR(10) + @SCRIPT_U
							+CHAR(13)+CHAR(10) + @SCRIPT_D
							+CHAR(13)+CHAR(10)
							+'END'

	print '<< FINALIZAMOS CREACI�N DE SP '+@NombreSPCRUD+' >>'
	print @script_contenedor

	EXEC sp_executesql @SCRIPT_CONTENEDOR

end
GO


IF OBJECT_ID('dbo.CreateProcedureCrud') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.CreateProcedureCrud') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.CreateProcedureCrud >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.CreateProcedureCrud >>>'
END
go
