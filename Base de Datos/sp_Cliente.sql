USE SIF
go

IF OBJECT_ID('dbo.sp_Cliente') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_Cliente
    IF OBJECT_ID('dbo.sp_Cliente') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_Cliente >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_Cliente >>>'
END
go

-- =============================================
-- Author:		<sns-scrud-generator>
-- Create date: <12/11/2018>
-- Description:	<SCRUD para Cliente.>
-- =============================================
CREATE PROCEDURE sp_Cliente
   @i_user               varchar(32)
  ,@i_terminal           varchar(32)
  ,@i_operacion          char(1)
  
  ,@i_Codigo		varchar(64)	 = '' 
  ,@i_NombreCliente		varchar(128)	 = '' 
  ,@i_Telefono		varchar(32)	 = '' 
  ,@i_Tipo		varchar(10)	 = '' 
  ,@i_Direccion		varchar(Max)	 = '' 
  ,@i_CorreoElectronico		varchar(128)	 = '' 
  ,@i_Contacto		varchar(256)	 = '' 
  ,@i_EsActivo		varchar(1)	 = 'N'
	
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		SELECT  
		CodigoCliente,
		NombreCliente,
		Telefono,
		Tipo,
		Direccion,
		CorreoElectronico,
		Contacto,
		case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM Cliente
		WHERE EsActivo = 1
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		SELECT  
		CodigoCliente,
		NombreCliente,
		Telefono,
		Tipo,
		Direccion,
		CorreoElectronico,
		Contacto,
		case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM Cliente
		WHERE CodigoCliente = @i_Codigo
		AND EsActivo = 1
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		INSERT INTO  Cliente
			( 
			CodigoCliente,
			NombreCliente,
			Telefono,
			Tipo,
			Direccion,
			CorreoElectronico,
			Contacto,
			EsActivo
			)
		VALUES 
			( 
			@i_Codigo,
			@i_NombreCliente,
			@i_Telefono,
			@i_Tipo,
			@i_Direccion,
			@i_CorreoElectronico,
			@i_Contacto,
			case @i_EsActivo when 'S' then 1 else 0 end  
			)
		 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Cliente
		SET  
			NombreCliente = @i_NombreCliente,
			Telefono = @i_Telefono,
			Tipo = @i_Tipo,
			Direccion = @i_Direccion,
			CorreoElectronico = @i_CorreoElectronico,
			Contacto = @i_Contacto
		WHERE CodigoCliente = @i_Codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
				
		UPDATE Cliente
		SET  EsActivo = 0		    
		WHERE CodigoCliente = @i_Codigo
		
		return 0
	END

END
GO

IF OBJECT_ID('dbo.sp_Cliente') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_Cliente') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_Cliente >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_Cliente >>>'
END
go

