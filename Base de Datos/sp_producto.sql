USE SIF
go

IF OBJECT_ID('dbo.sp_producto') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_producto
    IF OBJECT_ID('dbo.sp_producto') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_producto >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_producto >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE sp_producto
   @i_user               varchar(32)
  ,@i_terminal           varchar(32)
  ,@i_operacion          char(1)
  ,@i_Codigo             varchar(32) = ''
  ,@i_CodigoCategoria    bigint      = 0 
  ,@i_Descripcion        varchar(256)= ''
  ,@i_UnidadMedida       int         = 0
  ,@i_EsActivo           varchar(1)  = 'N'
  ,@i_LlevaExistencia    varchar(1)  = 'N'
  ,@i_UnidadesPorMedida  int         = 0
	
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		select  CodigoProducto  --0 
		       ,CodigoCategoria  --1
		       ,Descripcion      --2
		       ,UnidadMedida     --3
			   ,case EsActivo when 1 then 'S' else 'N' end as EsActivo--4
			   ,case LlevaExistencia when 1 then 'S' else 'N' end as LlevaExistencia--5
		       ,UnidadesPorMedida--6
		from Producto
		where EsActivo = 1
		AND CodigoProducto > @i_Codigo
		ORDER BY CodigoProducto asc
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		select  CodigoProducto   
		       ,CodigoCategoria  
		       ,Descripcion      
		       ,UnidadMedida            
			   ,case EsActivo when 1 then 'S' else 'N' end as EsActivo
			   ,case LlevaExistencia when 1 then 'S' else 'N' end as LlevaExistencia
		       ,UnidadesPorMedida
		from Producto
		WHERE CodigoProducto = @i_Codigo
		and EsActivo = 1
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		INSERT INTO  Producto
			( CodigoProducto   
			 ,CodigoCategoria  
			 ,Descripcion      
			 ,UnidadMedida        
			 ,EsActivo         
			 ,LlevaExistencia  
			 ,UnidadesPorMedida
			)
		VALUES 
			(@i_Codigo   
			 ,@i_CodigoCategoria  
			 ,@i_Descripcion      
			 ,@i_UnidadMedida           
			 ,1         
			 ,case @i_LlevaExistencia when 'S' then 1 else 0 end   
			 ,@i_UnidadesPorMedida  
			)
		 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Producto
		set  CodigoCategoria   = @i_CodigoCategoria  
		    ,Descripcion       = @i_Descripcion      
		    ,UnidadMedida      = @i_UnidadMedida     
		    ,LlevaExistencia   = case @i_LlevaExistencia when 'S' then 1 else 0 end  
		    ,UnidadesPorMedida = @i_UnidadesPorMedida
		WHERE CodigoProducto = @i_Codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
				
		UPDATE Producto
		set  EsActivo          = 0		    
		WHERE CodigoProducto = @i_Codigo
		
		return 0
	END

END
GO

IF OBJECT_ID('dbo.sp_producto') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_producto') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_producto >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_producto >>>'
END
go
