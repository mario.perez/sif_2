USE SIF
go

IF OBJECT_ID('dbo.sp_salida') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_salida
    IF OBJECT_ID('dbo.sp_salida') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_salida >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_salida >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para salida.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_salida] 
    @i_operacion char(1), 
    @i_codigo varchar(32) = '', 
    @i_Fecha datetime = '19000101', 
    @i_Bodega int = -1, 
    @i_Observaciones varchar(max) = '', 
    @i_EsAnulada varchar(1) = 'N', 
    @i_Tipo tinyint = 0, 
    @i_EsRegistrada varchar(1) = 'N', 
    @i_UsuarioAnula varchar(32) = '', 
    @i_UsuarioApruebaAnula varchar(32) = '', 
    @i_FechaHoraAnula datetime = '19000101'
 AS BEGIN 

IF @i_operacion = 'S' 
BEGIN 

	SELECT CodigoSalida, Fecha, Bodega, Observaciones, case EsAnulada when 1 then 'S' else 'N' end as EsAnulada,
			Tipo, case EsRegistrada when 1 then 'S' else 'N' end as EsRegistrada  , UsuarioAnula, 
			UsuarioApruebaAnula, FechaHoraAnula
	FROM Salida
	WHERE ((@i_codigo<>'' and CodigoSalida = @i_codigo)
	OR (@i_Fecha<>'19000101' and Fecha = @i_Fecha))

END

IF @i_operacion = 'C' 
BEGIN 

	SELECT CodigoSalida, Fecha, Bodega, Observaciones, case EsAnulada when 1 then 'S' else 'N' end as EsAnulada,
			Tipo, case EsRegistrada when 1 then 'S' else 'N' end as EsRegistrada  , UsuarioAnula, 
			UsuarioApruebaAnula, FechaHoraAnula
	FROM Salida
	WHERE CodigoSalida = @i_codigo

END

IF @i_operacion = 'I' 
BEGIN 

	Select @i_codigo = max(convert(bigint,CodigoSalida)) from Salida
	
	set @i_codigo = isnull(@i_codigo,0) + 1
	
	set @i_codigo = right('00000000000000000000000000000000' + @i_codigo,32)

	INSERT Salida (CodigoSalida, Fecha, Bodega, Observaciones, 
					EsAnulada, Tipo, 
					EsRegistrada, 
					UsuarioAnula, UsuarioApruebaAnula, FechaHoraAnula)
	VALUES (@i_codigo, @i_Fecha, @i_Bodega, @i_Observaciones, 
			case @i_EsAnulada when 'S' then 1 else 0 end, @i_Tipo, 
			case @i_EsRegistrada when 'S' then 1 else 0 end, 
			@i_UsuarioAnula, @i_UsuarioApruebaAnula, @i_FechaHoraAnula ) 

END

IF @i_operacion = 'U' 
BEGIN 

	update t set Fecha = @i_Fecha, Bodega = @i_Bodega, Observaciones = @i_Observaciones, 
				 EsAnulada = case @i_EsAnulada when 'S' then 1 else 0 end, Tipo = @i_Tipo, 
				 EsRegistrada = case @i_EsRegistrada when 'S' then 1 else 0 end, 
				 UsuarioAnula = @i_UsuarioAnula, UsuarioApruebaAnula = @i_UsuarioApruebaAnula, 
				 FechaHoraAnula = @i_FechaHoraAnula
	from Salida t 
	WHERE CodigoSalida = @i_codigo

END

IF @i_operacion = 'D' 
BEGIN 

	DELETE FROM Salida
	WHERE CodigoSalida = @i_codigo

END
END
GO

IF OBJECT_ID('dbo.sp_salida') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_salida') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_salida >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_salida >>>'
END
go
