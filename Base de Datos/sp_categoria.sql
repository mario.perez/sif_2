USE [SIF]
GO
/****** Object:  StoredProcedure [dbo].[sp_categoria]    Script Date: 6/9/2018 8:27:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.sp_categoria') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_categoria
    IF OBJECT_ID('dbo.sp_categoria') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_categoria >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_categoria >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <24/04/2018>
-- Description:	<SCRUD para Categoria.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_categoria]
	@i_user        varchar(32),
	@i_terminal    varchar(32),
	@i_operacion   char(1),
	@i_codigo      varchar(16) = '',
	@i_nombre      varchar(32) = '',
	@i_es_activo   bit = 1
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		select  CodigoCategoria,
				Nombre,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Categoria
		where EsActivo = 1
		and CodigoCategoria > @i_codigo
		order by CodigoCategoria asc
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		select  CodigoCategoria,
				Nombre,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Categoria
		WHERE CodigoCategoria = @i_codigo
		AND EsActivo = 1

		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN	

		set @i_codigo = 0
		
		Select @i_codigo = max(CodigoCategoria) +1 from Categoria
		
		Set @i_codigo = isnull(@i_codigo,0)		
				
		INSERT INTO  Categoria(CodigoCategoria, Nombre, EsActivo)
		VALUES (@i_codigo, @i_nombre, 1)
		 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Categoria
		set Nombre = @i_nombre
		WHERE CodigoCategoria = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE Categoria
		set EsActivo = 0
		WHERE CodigoCategoria = @i_codigo
		
		return 0
	END

END
GO


IF OBJECT_ID('dbo.sp_categoria') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_categoria') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_categoria >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_categoria >>>'
END
go



