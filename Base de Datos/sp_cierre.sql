USE [SIF]
GO
/****** Object:  StoredProcedure [dbo].[sp_cierre]    Script Date: 6/9/2018 8:27:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF OBJECT_ID('dbo.sp_cierre') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_cierre
    IF OBJECT_ID('dbo.sp_cierre') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_cierre >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_cierre >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_cierre]
	@i_user              varchar(32),
	@i_terminal          varchar(32),

	@i_operacion         char(1) = '-1', 
	@i_codigo            bigint = -1   
   ,@i_CodigoCaja        int = -1
   ,@i_FechaHoraInicio   datetime = '19670414'
   ,@i_FechaHoraFin      datetime = '19670414'
   ,@i_NombreUsuario     varchar(32)
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		
		SELECT CodigoCierre
			,CodigoCaja
			,FechaHoraInicio
			,FechaHoraFin
			,NombreUsuario
		FROM Cierre
		WHERE CodigoCierre > @i_codigo
		order by CodigoCierre asc
		
		print '<< Cierres Consultados >>'

		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		
		SELECT CodigoCierre
			,CodigoCaja
			,FechaHoraInicio
			,FechaHoraFin
			,NombreUsuario
		FROM Cierre
		WHERE @i_codigo = cierre.CodigoCierre
		
		print '<< Cierre Consultado >>'

		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		Select @i_codigo = max(CodigoCierre) + 1 from Cierre
		
		Set @i_codigo = isnull(@i_codigo,0)
				
				
		INSERT INTO Cierre
				   (CodigoCierre
				   ,CodigoCaja
				   ,FechaHoraInicio
				   ,FechaHoraFin
				   ,NombreUsuario)
			 VALUES
				   (@i_codigo
				   ,@i_CodigoCaja
				   ,@i_FechaHoraInicio
				   ,@i_FechaHoraFin
				   ,@i_NombreUsuario)

		print '<< Cierre Creado >>'
		 
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
				
		UPDATE Cierre
		   SET CodigoCaja = @i_CodigoCaja
			  ,FechaHoraInicio = @i_FechaHoraInicio
			  ,FechaHoraFin = @i_FechaHoraFin
			  ,NombreUsuario = @i_NombreUsuario
		 WHERE CodigoCierre = @i_codigo

		 print '<< Cierre Actualizado >>'
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
	
		DELETE 
		FROM DetalleCierre
		WHERE CodigoCierre = @i_codigo
		
		
		DELETE
		FROM Cierre
		WHERE CodigoCierre = @i_codigo

		print '<< Cierre Eliminado>> '

		return 0
	END

END
go

IF OBJECT_ID('dbo.sp_cierre') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_cierre') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_cierre >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_cierre >>>'
END
go



