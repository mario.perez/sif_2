USE SIF
go

IF OBJECT_ID('sp_detalle_salida') IS NOT NULL
BEGIN
    DROP PROCEDURE sp_detalle_salida
    IF OBJECT_ID('sp_detalle_salida') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE sp_detalle_salida >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE sp_detalle_salida >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_detalle_salida] 
    @i_operacion char(1), 
    @i_CodigoDetalleSalida varchar(16) = '', 
    @i_codigo varchar(32)= '', 
    @i_CodigoLote varchar(32)= '', 
    @i_CodigoProducto varchar(32) = '', 
    @i_Cantidad money = 0, 
    @i_Precio money = 0
 AS BEGIN 

IF @i_operacion = 'S' 
BEGIN 

	SELECT CodigoDetalleSalida, CodigoSalida, CodigoLote,CodigoProducto, Cantidad, Precio
	FROM DetalleSalida
	WHERE CodigoSalida = @i_codigo

END
/*
IF @i_operacion = 'C' 
BEGIN 

	SELECT CodigoDetalleSalida, CodigoSalida, CodigoProducto, Cantidad, Precio
	FROM DetalleSalida
	WHERE CodigoDetalleSalida = @i_CodigoDetalleSalida

END*/

IF @i_operacion = 'I' 
BEGIN 

	INSERT DetalleSalida (CodigoDetalleSalida, CodigoSalida, CodigoLote, CodigoProducto, Cantidad, Precio)
	VALUES (@i_CodigoDetalleSalida, @i_codigo, @i_CodigoLote, @i_CodigoProducto, @i_Cantidad, @i_Precio ) 

END

IF @i_operacion = 'U' 
BEGIN 

	update t set CodigoSalida = @i_codigo, CodigoProducto = @i_CodigoProducto, 
				 Cantidad = @i_Cantidad, Precio = @i_Precio,
				 CodigoLote = @i_CodigoLote
	from DetalleSalida t 
	WHERE CodigoDetalleSalida = @i_CodigoDetalleSalida
	and CodigoSalida = @i_codigo

END

IF @i_operacion = 'D' 
BEGIN 

	DELETE FROM DetalleSalida
	WHERE CodigoDetalleSalida = @i_CodigoDetalleSalida
	and CodigoSalida = @i_codigo

END
END
GO

IF OBJECT_ID('sp_detalle_salida') IS NOT NULL
BEGIN
    IF OBJECT_ID('sp_detalle_salida') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE sp_detalle_salida >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE sp_detalle_salida >>>'
END
go
