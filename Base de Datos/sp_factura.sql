USE SIF
go

IF OBJECT_ID('dbo.sp_factura') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_factura
    IF OBJECT_ID('dbo.sp_factura') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_factura >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_factura >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para salida.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_factura] 
    @i_operacion char(1), 
    @i_codigo varchar(32) = '', 
    @i_Fecha datetime = '19000101', 
    @i_Bodega int = -1, 
    @i_TotalFactura money = 0, 
    @i_Descuento money = 0, 
    @i_Impuesto money = 0, 
    @i_TotalNeto money = 0, 
    @i_Efectivo money = 0, 
    @i_Tarjeta money = 0, 
    @i_CodigoVendedor varchar(16) = '', 
    @i_CodigoCaja int = -1, 
    @i_TipoPago tinyint = 0, 
    @i_EsAnulada varchar(1) = 'N', 
    @i_EsRegistrada varchar(1) = 'N', 
    @i_PorcentajeDescuento money = 0, 
    @i_UsuarioDescuento varchar(32) = '', 
    @i_UsuarioApruebaDescuento varchar(32) = '', 
    @i_Observaciones varchar(max) = '', 
    @i_UsuarioAnula varchar(32) = '', 
    @i_UsuarioApruebaAnula varchar(32) = '', 
    @i_FechaHoraAnula datetime = '19000101'
 AS BEGIN 

IF @i_operacion = 'S' 
BEGIN 

	SELECT CodigoFactura, Fecha, Bodega, TotalFactura, Descuento, Impuesto, TotalNeto, 
			Efectivo, Tarjeta, CodigoVendedor, CodigoCaja, TipoPago, 
			case EsAnulada when 1 then 'S' else 'N' end as EsAnulada, 
			case EsRegistrada when 1 then 'S' else 'N' end as EsRegistrada, 
			PorcentajeDescuento, UsuarioDescuento, UsuarioApruebaDescuento, Observaciones, 
			UsuarioAnula, UsuarioApruebaAnula, FechaHoraAnula
	FROM Factura
	WHERE ((@i_codigo<>'' and CodigoFactura = @i_codigo)
	OR (@i_Fecha<>'19000101' and Fecha = @i_Fecha))

END

IF @i_operacion = 'C' 
BEGIN 

	SELECT CodigoFactura, Fecha, Bodega, TotalFactura, Descuento, Impuesto, TotalNeto, 
			Efectivo, Tarjeta, CodigoVendedor, CodigoCaja, TipoPago, 
			case EsAnulada when 1 then 'S' else 'N' end as EsAnulada, 
			case EsRegistrada when 1 then 'S' else 'N' end as EsRegistrada, 
			PorcentajeDescuento, UsuarioDescuento, UsuarioApruebaDescuento, Observaciones, 
			UsuarioAnula, UsuarioApruebaAnula, FechaHoraAnula
	FROM Factura
	WHERE CodigoFactura = @i_codigo

END

IF @i_operacion = 'I' 
BEGIN 
	
	Select @i_codigo = max(convert(bigint,CodigoFactura)) from Factura
	
	set @i_codigo = isnull(@i_codigo,0) + 1
	
	set @i_codigo = right('00000000000000000000000000000000' + @i_codigo,32)
	
	INSERT Factura (CodigoFactura, Fecha, Bodega, TotalFactura, Descuento, Impuesto, TotalNeto, 
					Efectivo, Tarjeta, CodigoVendedor, CodigoCaja, TipoPago, 
					EsAnulada, 
					EsRegistrada, 
					PorcentajeDescuento, UsuarioDescuento, UsuarioApruebaDescuento, Observaciones,
					UsuarioAnula, UsuarioApruebaAnula, FechaHoraAnula)
	VALUES (@i_codigo, @i_Fecha, @i_Bodega, @i_TotalFactura, @i_Descuento, @i_Impuesto, @i_TotalNeto, 
			@i_Efectivo, @i_Tarjeta, @i_CodigoVendedor, @i_CodigoCaja, @i_TipoPago, 
			case @i_EsAnulada when 'S' then 1 else 0 end, 
			case @i_EsRegistrada when 'S' then 1 else 0 end, 
			@i_PorcentajeDescuento, @i_UsuarioDescuento, @i_UsuarioApruebaDescuento, @i_Observaciones, 
			@i_UsuarioAnula, @i_UsuarioApruebaAnula, @i_FechaHoraAnula ) 

END

IF @i_operacion = 'U' 
BEGIN 

	update t set Fecha = @i_Fecha, Bodega = @i_Bodega, TotalFactura = @i_TotalFactura, 
				 Descuento = @i_Descuento, Impuesto = @i_Impuesto, TotalNeto = @i_TotalNeto, 
				 Efectivo = @i_Efectivo, Tarjeta = @i_Tarjeta, CodigoVendedor = @i_CodigoVendedor, 
				 CodigoCaja = @i_CodigoCaja, TipoPago = @i_TipoPago, 
				 EsAnulada = case @i_EsAnulada when 'S' then 1 else 0 end, 
				 EsRegistrada = case @i_EsRegistrada when 'S' then 1 else 0 end, 
				 PorcentajeDescuento = @i_PorcentajeDescuento, UsuarioDescuento = @i_UsuarioDescuento, 
				 UsuarioApruebaDescuento = @i_UsuarioApruebaDescuento, Observaciones = @i_Observaciones, 
				 UsuarioAnula = @i_UsuarioAnula, UsuarioApruebaAnula = @i_UsuarioApruebaAnula, 
				 FechaHoraAnula = @i_FechaHoraAnula
	from Factura t 
	WHERE CodigoFactura = @i_codigo

END

IF @i_operacion = 'D' 
BEGIN 

	DELETE FROM Factura
	WHERE CodigoFactura = @i_codigo

END
END
GO

IF OBJECT_ID('dbo.sp_factura') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_factura') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_factura >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_factura >>>'
END
go
