USE SIF
go

IF OBJECT_ID('dbo.sp_lote') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_lote
    IF OBJECT_ID('dbo.sp_lote') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_lote >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_lote >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE sp_lote
	@i_user              varchar(32),
	@i_terminal          varchar(32),
	@i_operacion         char(1),
	@i_codigo            varchar(32) = ''
   ,@i_fechaIngreso      datetime = '19000101'  
   ,@i_esActivo          bit      = 0  
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		
		SELECT CodigoLote  
		      ,FechaIngreso
		      ,case EsActivo when 1 then 'S' else 'N' end as EsActivo    
		FROM Lote
		WHERE EsActivo = 1
		AND ((@i_codigo<>'' and CodigoLote = @i_codigo)
			OR (@i_fechaIngreso<>'19000101' and FechaIngreso = @i_fechaIngreso))
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		
		SELECT CodigoLote  
		      ,FechaIngreso
		      ,case EsActivo when 1 then 'S' else 'N' end as EsActivo    
		FROM Lote
		where CodigoLote = @i_codigo
		and EsActivo = 1
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
				
		INSERT INTO Lote
				   (CodigoLote
				   ,FechaIngreso
				   ,EsActivo
				   )
			 VALUES
				   (@i_codigo
				   ,@i_fechaIngreso
				   ,@i_esActivo
				   )
		 
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
				
		UPDATE Lote
		   SET FechaIngreso = @i_fechaIngreso
			  ,EsActivo = @i_esActivo
		 WHERE CodigoLote = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
	
		DELETE 
		FROM DetalleLote
		WHERE CodigoLote = @i_codigo
		
		
		DELETE
		FROM Lote
		WHERE CodigoLote = @i_codigo
		
		return 0
	END

END
GO

IF OBJECT_ID('dbo.sp_lote') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_lote') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_lote >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_lote >>>'
END
go
