USE SIF
go

IF OBJECT_ID('dbo.sp_compra') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_compra
    IF OBJECT_ID('dbo.sp_compra') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_compra >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_compra >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para salida.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_compra] 
    @i_operacion char(1), 
    @i_codigo varchar(32), 
    @i_CodigoProveedor     bigint = -1,
    @i_Fecha               datetime = '19000101', 
    @i_Bodega              int = -1, 
    @i_TotalCompra         money = 0, 
    @i_Descuento           money = 0, 
    @i_Impuesto            money = 0, 
    @i_TotalNeto           money = 0, 
    @i_Observaciones       varchar(max) = '', 
    @i_EsAnulada           varchar(1) = 'N', 
    @i_EsRegistrada        varchar(1) = 'N',  
    @i_UsuarioAnula        varchar(32) = '', 
    @i_UsuarioApruebaAnula varchar(32) = '', 
    @i_FechaHoraAnula      datetime = '19000101'
 AS BEGIN 

IF @i_operacion = 'S' 
BEGIN 

	SELECT CodigoCompra, CodigoProveedor, Fecha, Bodega, TotalCompra, Descuento, 
			Impuesto, TotalNeto, Observaciones, 
			case EsAnulada when 1 then 'S' else 'N' end as EsAnulada, 
			case EsRegistrada when 1 then 'S' else 'N' end as EsRegistrada, UsuarioAnula, 
			UsuarioApruebaAnula, FechaHoraAnula
	FROM Compra
	where ((@i_codigo<>'' and CodigoCompra = @i_codigo)
	OR (@i_Fecha<>'19000101' and Fecha = @i_Fecha))


END

IF @i_operacion = 'C' 
BEGIN 

	SELECT CodigoCompra, CodigoProveedor, Fecha, Bodega, TotalCompra, Descuento, 
			Impuesto, TotalNeto, Observaciones, 
			case EsAnulada when 1 then 'S' else 'N' end as EsAnulada, 
			case EsRegistrada when 1 then 'S' else 'N' end as EsRegistrada, UsuarioAnula, 
			UsuarioApruebaAnula, FechaHoraAnula
	FROM Compra
	WHERE CodigoCompra = @i_codigo

END

IF @i_operacion = 'I' 
BEGIN 

	Select @i_codigo = max(convert(bigint,CodigoCompra)) from Compra
	
	set @i_codigo = isnull(@i_codigo,0) + 1
	
	set @i_codigo = right('00000000000000000000000000000000' + @i_codigo,32)

	INSERT Compra (CodigoCompra, CodigoProveedor, Fecha, Bodega, TotalCompra, Descuento, Impuesto,
					TotalNeto, Observaciones, 
					EsAnulada, 
					EsRegistrada, 
					UsuarioAnula, UsuarioApruebaAnula, FechaHoraAnula)
	VALUES (@i_codigo, @i_CodigoProveedor, @i_Fecha, @i_Bodega, @i_TotalCompra, @i_Descuento, @i_Impuesto,
	        @i_TotalNeto, @i_Observaciones, 
			case @i_EsAnulada when 'S' then 1 else 0 end, 
			case @i_EsRegistrada when 'S' then 1 else 0 end, 
			@i_UsuarioAnula, @i_UsuarioApruebaAnula, @i_FechaHoraAnula ) 

END

IF @i_operacion = 'U' 
BEGIN 

	update t set CodigoProveedor = @i_CodigoProveedor, Fecha = @i_Fecha, Bodega = @i_Bodega, 
				 TotalCompra = @i_TotalCompra, Descuento = @i_Descuento, Impuesto = @i_Impuesto, 
				 TotalNeto = @i_TotalNeto, Observaciones = @i_Observaciones, 
				 EsAnulada = case @i_EsAnulada when 'S' then 1 else 0 end, 
				 EsRegistrada = case @i_EsRegistrada when 'S' then 1 else 0 end,  
				 UsuarioAnula = @i_UsuarioAnula, UsuarioApruebaAnula = @i_UsuarioApruebaAnula, 
				 FechaHoraAnula = @i_FechaHoraAnula
	from Compra t 
	WHERE CodigoCompra = @i_codigo

END

IF @i_operacion = 'D' 
BEGIN 

	DELETE FROM Compra
	WHERE CodigoCompra = @i_codigo

END
END
GO

IF OBJECT_ID('dbo.sp_compra') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_compra') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_compra >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_compra >>>'
END
go
