USE [SIF]
GO

/****** Object:  StoredProcedure [dbo].[sp_moneda]    Script Date: 5/9/2018 1:15:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




IF OBJECT_ID('dbo.sp_moneda') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_moneda
    IF OBJECT_ID('dbo.sp_moneda') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_moneda >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_moneda >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <24/04/2018>
-- Description:	<SCRUD para Moneda.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_moneda]
	@i_user            varchar(32),
	@i_terminal        varchar(32),
	@i_operacion       char(1),
	@i_codigo    varchar(8) = '',
	@i_Decimales int = 0,
	@i_Nombre varchar(128) = ''
	,@i_Simbolo varchar(16) = ''
	,@i_EsActivo bit = 0
	,@i_NumeroMoneda varchar(8) = ''
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		SELECT CodigoMoneda  
			   ,NumeroMoneda 
			   ,Decimales    
			   ,Nombre       
			   ,Simbolo      
			   ,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM Moneda
		where EsActivo = 1
		and CodigoMoneda > @i_codigo
		ORDER BY CodigoMoneda asc
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		SELECT CodigoMoneda  
			   ,NumeroMoneda 
			   ,Decimales    
			   ,Nombre       
			   ,Simbolo      
			   ,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM Moneda
		where EsActivo = 1
		and CodigoMoneda = @i_codigo
		
		return 0
	END

	IF @i_operacion = 'I'
	BEGIN
		INSERT Moneda (CodigoMoneda, NumeroMoneda, Decimales, Nombre, Simbolo, EsActivo)
		values (@i_codigo,@i_NumeroMoneda,@i_Decimales, @i_Nombre, @i_Simbolo, 1)

		return 0
	END

	IF @i_operacion = 'D'
	BEGIN
		update M SET
		M.EsActivo = 0
		from MONEDA M
		WHERE M.CodigoMoneda = @i_codigo
	END

	IF @i_operacion = 'U'
	BEGIN
		update M SET
		M.Decimales = @i_Decimales
		, M.Nombre = @i_Nombre
		, M.NumeroMoneda = @i_NumeroMoneda
		, M.Simbolo = @i_Simbolo
		from MONEDA M
		WHERE M.CodigoMoneda = @i_codigo
	END
	
END

GO


IF OBJECT_ID('dbo.sp_moneda') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_moneda') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_moneda >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_moneda >>>'
END
go



