USE SIF
go

IF OBJECT_ID('sp_detalle_cierre') IS NOT NULL
BEGIN
    DROP PROCEDURE sp_detalle_cierre
    IF OBJECT_ID('sp_detalle_cierre') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE sp_detalle_cierre >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE sp_detalle_cierre >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE sp_detalle_cierre
	@i_user                 varchar(32),
	@i_terminal             varchar(32),
	@i_operacion            char(1),
	@i_codigo               bigint = -1,
	@i_CodigoDenominacion   int    = -1
   ,@i_Cantidad             bigint = 0     
   ,@i_MontoEquivalente     Numeric(7,4) = 0
   
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		
		SELECT CodigoCierre
			  ,CodigoDenominacion
			  ,Cantidad
			  ,MontoEquivalente
		FROM DetalleCierre
		WHERE  CodigoCierre = @i_codigo
		
		return 0
	END
	
	/*IF @i_operacion = 'C'
	BEGIN
		
		SELECT CodigoCierre
			  ,CodigoDenominacion
			  ,Cantidad
			  ,MontoEquivalente
		FROM DetalleCierre
		WHERE  CodigoCierre = @i_codigo
		
		return 0
	END*/
	
	IF @i_operacion = 'I'
	BEGIN
		
		INSERT INTO dbo.DetalleCierre
			   (CodigoCierre,CodigoDenominacion,Cantidad,MontoEquivalente)
		VALUES
			   (@i_codigo,@i_CodigoDenominacion,@i_Cantidad,@i_MontoEquivalente)
			 
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE DetalleCierre
		   SET CodigoDenominacion = @i_CodigoDenominacion
			  ,Cantidad = @i_Cantidad
			  ,MontoEquivalente = @i_MontoEquivalente
		WHERE CodigoCierre = @i_codigo
		AND	  CodigoDenominacion = @i_CodigoDenominacion
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		DELETE 
		FROM DetalleCierre
		WHERE  CodigoCierre = @i_codigo
		and CodigoDenominacion = @i_CodigoDenominacion
		
		return 0
	END

END
GO

IF OBJECT_ID('sp_detalle_cierre') IS NOT NULL
BEGIN
    IF OBJECT_ID('sp_detalle_cierre') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE sp_detalle_cierre >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE sp_detalle_cierre >>>'
END
go
