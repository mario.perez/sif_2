USE SIF
go

IF OBJECT_ID('dbo.sp_inventario') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_inventario
    IF OBJECT_ID('dbo.sp_inventario') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_inventario >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_inventario >>>'
END
go

-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <03/06/2018>
-- Description:	<SCRUD para Proveedor.>
-- =============================================
CREATE PROCEDURE sp_inventario
	@i_user        varchar(32),
	@i_terminal    varchar(32),
	@i_operacion   char(1),
	@i_codigoBodega]   [int]         
   ,@i_codigoLote]     [varchar](32) 
   ,@i_codigoProducto] [varchar](32) 
   ,@i_cantidad]       [numeric](7,4)
   ,@i_precio]         [money]       
   ,@i_esActivo]       [bit]         
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		SELECT CodigoBodega
			  ,CodigoLote
			  ,CodigoProducto
			  ,Cantidad
			  ,Precio
			  ,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM Inventario
		where EsActivo = 1
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		SELECT CodigoBodega
			  ,CodigoLote
			  ,CodigoProducto
			  ,Cantidad
			  ,Precio
			  ,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM Inventario
		where EsActivo = 1
		and CodigoBodega = @i_codigoBodega
		and CodigoLote = @i_codigoLote
		and CodigoProducto = @i_codigoProducto
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = 0
		
		Select @i_codigo = max(CodigoDenominacion) from Denominacion
		
		Set @i_codigo = isnull(@i_codigo,0)
		
		INSERT INTO Denominacion
			   (CodigoDenominacion ,Descripcion,Moneda,Factor,EsEfectivo,EsActivo)
		 VALUES
			   (@i_codigo,@i_descripcion ,@i_Moneda ,@i_Factor,@i_EsEfectivo,1)
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Denominacion
		   SET 
			   Descripcion = @i_descripcion
			  ,Moneda = @i_Moneda
			  ,Factor = @i_Factor
			  ,EsEfectivo = @i_EsEfectivo
			  ,EsActivo = @i_EsActivo
		 WHERE CodigoDenominacion = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		DELETE
		FROM Denominacion
		where CodigoDenominacion = @i_codigo
		
		return 0
	END

END
GO

IF OBJECT_ID('dbo.sp_inventario') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_inventario') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_inventario >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_inventario >>>'
END
go
