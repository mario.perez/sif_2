USE SIF
go

IF OBJECT_ID('dbo.sp_MisCatalogos') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_MisCatalogos
    IF OBJECT_ID('dbo.sp_MisCatalogos') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_MisCatalogos >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_MisCatalogos >>>'
END
go

-- =============================================
-- Author:		<sns-scrud-generator>
-- Create date: <8/11/2018>
-- Description:	<SCRUD para MisCatalogos.>
-- =============================================
CREATE PROCEDURE sp_MisCatalogos
   @i_user               varchar(32)
  ,@i_terminal           varchar(32)
  ,@i_operacion          char(1)
  
  ,@i_Tabla		varchar(32)	 = '' 
  ,@i_Codigo		varchar(10)	 = '' 
  ,@i_Descripcion		varchar(64)	 = '' 
  ,@i_EsActivo		varchar(1)	 = 'N'
	
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		SELECT  
		Tabla,
		CodigoMisCatalogos,
		Descripcion,
		case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM MisCatalogos
		WHERE EsActivo = 1
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		SELECT  
		Tabla,
		CodigoMisCatalogos,
		Descripcion,
		case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM MisCatalogos
		WHERE CodigoMisCatalogos = @i_Codigo
		AND EsActivo = 1
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		INSERT INTO  MisCatalogos
			( 
			Tabla,
			CodigoMisCatalogos,
			Descripcion,
			EsActivo
			)
		VALUES 
			( 
			@i_Tabla,
			@i_Codigo,
			@i_Descripcion,
			case @i_EsActivo when 'S' then 1 else 0 end  
			)
		 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE MisCatalogos
		SET  
			Tabla = @i_Tabla,
			Descripcion = @i_Descripcion
		WHERE CodigoMisCatalogos = @i_Codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
				
		UPDATE MisCatalogos
		SET  EsActivo = 0		    
		WHERE CodigoMisCatalogos = @i_Codigo
		
		return 0
	END

END
GO

IF OBJECT_ID('dbo.sp_MisCatalogos') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_MisCatalogos') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_MisCatalogos >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_MisCatalogos >>>'
END
go

